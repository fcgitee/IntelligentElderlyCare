import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { SmartReportControl, SmartReportModuleControl, AjaxRemoteDataQueryer, ReportDataView, DataDisplayerModuleControl } from "src/business/report/smart";
import { } from "../echartCommonVar";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { ListDisplayControl } from "src/business/report/displayers/nh-welfare/list-display";
import { VideoControl } from "src/business/components/buss-components/video";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { SingleBubbleControl } from "src/business/report/displayers/nh-welfare/single-bubble";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { NewCommonLayoutLeftControl } from "src/projects/components/layout/new-common-layout/left";
import { NewCommonLayoutMiddleControl } from "src/projects/components/layout/new-common-layout/middle";
import { NewCommonLayoutRightControl } from "src/projects/components/layout/new-common-layout/right";
import { NewCommonLayoutDetailRightControl } from "src/projects/components/layout/new-common-layout/right-detail";
import { NewCommonLayoutDetailLeftControl } from "src/projects/components/layout/new-common-layout/left-detail";
import { RowTableDisplayControl } from "src/business/report/displayers/nh-welfare/row-table-display";
import { iconMap, getCallCenterEle, legendOption } from "src/projects/util-tool";
import { LegendMenuControl } from "src/projects/components/lengend-menu";
import { scrollAnimation } from "src/business/util_tool";

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubbleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_bubble`,
    `new_sum_statistical_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老院总数统计--星级饼图--饼图
 */
const new_sum_statistical_chartDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_chart`,
    `new_sum_statistical_chart`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构入住人数排名--床位--表格
 */
const new_institutions_occupancy_ranking_bedDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_institutions_occupancy_ranking_bed`,
    `new_institutions_occupancy_ranking_bed`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构入住人数排名--长者数量--表格
 */
const new_institutions_occupancy_ranking_eldDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_institutions_occupancy_ranking_eld`,
    `new_institutions_occupancy_ranking_eld`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sumDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_household_registration_elder_sum`,
    `new_household_registration_elder_sum`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 镇街养老机构数量--柱状图
 */
const new_org_statistics_street_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_statistics_street_quantity`,
    `new_org_statistics_street_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_statistics_time_subscribe_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_statistics_time_subscribe_quantity`,
    `new_org_statistics_time_subscribe_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老机构分类--柱状图 
 */
const new_institutions_classificationDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_institutions_classification`,
    `new_institutions_classification`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statisticalDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_statistical`,
    `new_activity_statistical`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--卡片
 */
const new_activity_list_welfare_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_list_welfare_single`,
    `new_activity_list_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 每月活动数量趋势--折线图
 */
const new_activity_trend_monthDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_trend_month`,
    `new_activity_trend_month`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 视频播放--视频
 */
const new_video_monitoringDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_video_monitoring`,
    `new_video_monitoring`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 地图--地图
 */
const new_aswhole_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_map`,
    `new_aswhole_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubbleDataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_bubble`,
    dataSourceID: new_sum_statistical_bubbleDataSource.id
});

/**
 * 养老院总数统计--星级饼图--饼图
 */
const new_sum_statistical_chartDataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_chart1`,
    dataSourceID: new_sum_statistical_chartDataSource.id
});

/**
 * 机构入住人数排名--床位--表格
 */
const new_institutions_occupancy_ranking_bedDataView = createObject(ReportDataView, {
    id: `data_view_new_institutions_occupancy_ranking_bed`,
    dataSourceID: new_institutions_occupancy_ranking_bedDataSource.id
});

/**
 * 机构入住人数排名--长者数量--表格
 */
const new_institutions_occupancy_ranking_eldDataView = createObject(ReportDataView, {
    id: `data_view_new_institutions_occupancy_ranking_eld`,
    dataSourceID: new_institutions_occupancy_ranking_eldDataSource.id
});

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sumDataView = createObject(ReportDataView, {
    id: `data_view_new_household_registration_elder_sum`,
    dataSourceID: new_household_registration_elder_sumDataSource.id
});

/**
 * 镇街养老机构数量--柱状图
 */
const new_org_statistics_street_quantityDataView = createObject(ReportDataView, {
    id: `data_view_new_org_statistics_street_quantity`,
    dataSourceID: new_org_statistics_street_quantityDataSource.id
});

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_statistics_time_subscribe_quantityDataView = createObject(ReportDataView, {
    id: `data_view_new_org_statistics_time_subscribe_quantity`,
    dataSourceID: new_org_statistics_time_subscribe_quantityDataSource.id
});

/**
 * 养老机构分类--柱状图
 */
const new_institutions_classificationDataView = createObject(ReportDataView, {
    id: `data_view_new_institutions_classification`,
    dataSourceID: new_institutions_classificationDataSource.id
});

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statisticalDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_statistical`,
    dataSourceID: new_activity_statisticalDataSource.id
});

/**
 * 活动列表--卡片
 */
const new_activity_list_welfare_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_list_welfare_single`,
    dataSourceID: new_activity_list_welfare_singleDataSource.id
});

/**
 * 每月活动数量趋势--折线图
 */
const new_activity_trend_monthDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_trend_month`,
    dataSourceID: new_activity_trend_monthDataSource.id
});

/**
 * 视频播放--视频
 */
const new_video_monitoringDataView = createObject(ReportDataView, {
    id: `data_view_new_video_monitoring`,
    dataSourceID: new_video_monitoringDataSource.id
});

/**
 * 地图--地图
 */
const new_aswhole_mapDataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_map`,
    dataSourceID: new_aswhole_mapDataSource.id
});

/**
 * 养老院总数统计--星级饼图--饼图
 */
let new_sum_statistical_chartOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老院总数统计--星级饼图--饼图
 */
let new_household_registration_elder_sumOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 镇街养老机构数量--柱状图
 */
let new_org_statistics_street_quantityOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        name: "家",
        // axisLabel: {
        //     showMinLabel: false,
        // }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 养老机构分类--柱状图
 */
let new_institutions_classificationOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    grid: {
        bottom: 100
    },
    yAxis: {
        type: 'value',
        name: "家",
        // axisLabel: {
        //     showMinLabel: false,
        // }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 养老机构预约数量走势--折线图
 */
let new_org_statistics_time_subscribe_quantityOption: EChartsOptions = {
    legend: {
        show: true,
    },
    grid: {
        right: 60
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: "人"
    },
    series: [
        { type: 'line' }
    ],
};

/**
 * 每月活动数量趋势--折线图
 */
let new_activity_trend_monthOption: EChartsOptions = {
    legend: {
        show: true,
    },
    grid: {
        right: 70
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: "次"
    },
    series: [
        { type: 'line' }
    ],
};

// 地图--地图
let option: EChartsOptions = {
    legend: {
        selected: {
            "养老机构": true,
            "社区幸福院": false,
            "居家服务商": false,
            "服务人员_服务中": false,
            "服务人员_空闲": false,
            "长者_住址": false,
            "长者_设备": false
        },
        data: ["养老机构", "社区幸福院", "居家服务商", "服务人员_服务中", "服务人员_空闲", "长者_住址", "长者_设备"]
    },
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubbleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_bubble_pie',
        titleable: true,
        title: '养老院总数统计',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel",
        groupIndex: 0,
        dataViews: [new_sum_statistical_bubbleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 养老院总数统计--星级饼图--饼图
 */
const new_sum_statistical_chartModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_chart_pie',
        titleable: true,
        title: '养老院星级统计',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel2",
        groupIndex: 1,
        dataViews: [new_sum_statistical_chartDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_sum_statistical_chartdisplayer',
            new_sum_statistical_chartOption
        )
    }
);

/**
 * 机构入住人数排名--床位--表格
 */
const new_institutions_occupancy_ranking_bedDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_institutions_occupancy_ranking_bed-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '机构入住人数排名',
        dataViews: [new_institutions_occupancy_ranking_bedDataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel6",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '机构名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '床位总数',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 机构入住长者排名--长者数量--表格
 */
const new_institutions_occupancy_ranking_eldDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_institutions_occupancy_ranking_eld-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [new_institutions_occupancy_ranking_eldDataView],
        groupIndex: 3,
        title: '机构入住长者排名',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel4",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '机构名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '长者数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sumModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_household_registration_elder_sum_pie',
        titleable: true,
        title: '入住机构户籍长者数量',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel4",
        groupIndex: 3,
        dataViews: [new_household_registration_elder_sumDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_household_registration_elder_sumdisplayer',
            new_household_registration_elder_sumOption
        )
    }
);

/**
 * 镇街养老机构数量--柱状图
 */
const new_org_statistics_street_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_statistics_street_quantity_pie',
        titleable: true,
        title: '镇街养老机构数量',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel3",
        groupIndex: 2,
        dataViews: [new_org_statistics_street_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_street_quantity_bar_displayer',
            new_org_statistics_street_quantityOption
        )
    }
);

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_statistics_time_subscribe_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_statistics_time_subscribe_quantity_line',
        title: '养老机构预约数量走势',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel",
        groupIndex: 0,
        dataViews: [new_org_statistics_time_subscribe_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_org_statistics_time_subscribe_quantity_displayer',
            new_org_statistics_time_subscribe_quantityOption
        )
    }
);

/**
 * 养老机构分类--柱状图
 */
const new_institutions_classificationModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_institutions_classification_pie',
        titleable: true,
        title: '养老机构分类',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel2",
        groupIndex: 1,
        dataViews: [new_institutions_classificationDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                yAxisFieldNames: ['y'],
                xAxisFieldName: 'x',
            },
            'new_institutions_classificationdisplayer',
            new_institutions_classificationOption
        )
    }
);

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statisticalModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_statistical-num',
        title: '活动列表--统计数据',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel3",
        groupIndex: 2,
        dataViews: [new_activity_statisticalDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                // layout: "horizontal",
                square_span: 12,
                name_fields: ['活动次数', '参与人数'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 每月活动数量趋势--折线图
 */
const new_activity_trend_monthModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_trend_month_line',
        title: '每月活动数量趋势',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel4",
        groupIndex: 3,
        dataViews: [new_activity_trend_monthDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_activity_trend_month_displayer',
            new_activity_trend_monthOption
        )
    }
);

/**
 * 地图-地图
 */
const new_aswhole_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_map_community',
        // title: '幸福院位置',
        titleable: false,
        dataViews: [new_aswhole_mapDataView],
        drillDialogModules: ["leftDetailControl", "rightDetailControl"],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'longitude',
                latitudeFieldName: 'latitude',
                callLatitudeFieldName: "y",
                callLongitudeFieldName: "x",
                nameFieldName: "name",
                ponitIconSizeFieldName: "num",
                getElementFn: getCallCenterEle,
                scatterObject: iconMap
            },
            "com_map_location_community_displayer",
            option
        )
    }
);

/**
 * 视频播放--视频
 */
const org_detail_video_surveillanceDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_video_monitoring_displayer',
        titleable: true,
        title: '视频播放',
        // groupType: GroupContainerType.carousel,
        // group: "welfare-sum-right-carousel6",
        // groupIndex: 5,
        isDialog: true,
        parentContainerID: new_aswhole_mapModuleControl.id,
        dataViews: [new_video_monitoringDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "/build/upload/video/test.mp4",
                    type: "auto",
                    autoplay: true,
                    hotkey: false
                },
                width: 3720,
                height: 2800
            }
        )
    }
);

/**
 * 活动列表--卡片
 */
const new_activity_list_welfare_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_list_welfare_single_module',
        titleable: true,
        title: '活动列表',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel",
        groupIndex: 0,
        drillDialogModules: [org_detail_video_surveillanceDialogModuleControl.id!],
        dataViews: [new_activity_list_welfare_singleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '活动列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);

scrollAnimation("list-div", 16);

/** 
 * 单家
 */

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statistical_welfare_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_statistical_welfare_single`,
    `new_activity_statistical_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--统计--方块
 */
const new_activity_statistical_welfare_single_DataView = createObject(ReportDataView, {
    id: `data-view-new_activity_statistical_welfare_single`,
    dataSourceID: new_activity_statistical_welfare_singleDataSource.id
});

/**
 * 活动列表--统计--方块
 */
const new_activity_statistical_welfare_single_square_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_statistical_welfare_single',
        titleable: true,
        title: "活动统计",
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel",
        groupIndex: 0,
        containerType: ContainerType.frameContain,
        dataViews: [new_activity_statistical_welfare_single_DataView],
        displayer: createObject(
            MoreSquaresControl,
            {
                name_fields: ["活动次数", "参与人数"],
                value_field: "y",
                square_span: 12
            }
        )
    }
);

/**
 * 服务人员列表--列表
 */
const new_org_detail_personal_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_personal_list`,
    `new_org_detail_personal_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员列表--列表
 */
const new_org_detail_personal_listDataView = createObject(ReportDataView, {
    id: `data_view_new_org_detail_personal_list`,
    dataSourceID: new_org_detail_personal_listDataSource.id
});

/**
 * 服务人员列表--列表
 */
const new_org_detail_personal_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_personal_list',
        titleable: true,
        title: '服务人员列表',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel3",
        groupIndex: 2,
        dataViews: [new_org_detail_personal_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务人员列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2', 'y3'],
                height_content: 900
            }
        )
    }
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_service_products_list`,
    `new_org_detail_service_products_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listDataView = createObject(ReportDataView, {
    id: `data_view_new_org_detail_service_products_list`,
    dataSourceID: new_org_detail_service_products_listDataSource.id
});

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_service_products_list',
        titleable: true,
        title: '服务清单',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel5",
        groupIndex: 4,
        dataViews: [new_org_detail_service_products_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务清单',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2'],
                height_content: 900

            }
        )
    }
);
new_org_detail_service_products_listModuleControl;

/**
 * 监控播放--监控
 */
const org_monitorDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_monitorDialogModuleControl',
        titleable: true,
        title: '监控播放',
        // groupType: GroupContainerType.carousel,
        // group: "welfare-sum-right-carousel6",
        // groupIndex: 5,
        isDialog: true,
        parentContainerID: new_aswhole_mapModuleControl.id,
        dataViews: [new_video_monitoringDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://183.235.223.56:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    hotkey: false,
                    muted: false
                },
                width: 3720,
                height: 2800
            }
        )
    }
);

/**
 * 监控列表--列表
 */
const new_monitor_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_monitor_list`,
    `new_monitor_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 监控列表--列表
 */
const new_monitor_listDataView = createObject(ReportDataView, {
    id: `data_view_new_monitor_list`,
    dataSourceID: new_monitor_listDataSource.id
});

/**
 * 监控列表--列表
 */
const new_monitor_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_monitor_list',
        titleable: true,
        title: '监控列表',
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel5",
        groupIndex: 4,
        dataViews: [new_monitor_listDataView],
        drillDialogModules: [org_monitorDialogModuleControl.id!],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '监控列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2'],
                height_content: 900

            }
        )
    }
);

/**
 * 机构图片轮播--图片轮播
 */
const new_org_detail_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_picture_list`,
    `new_org_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构图片轮播--图片轮播
 */
const new_org_detail_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-new_org_detail_picture_list-carousel`,
    dataSourceID: new_org_detail_picture_listCarouselDataSource.id
});

/**
 * 机构图片轮播--图片轮播
 */
const new_org_detail_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_picture_list-carousel',
        title: '福利院环境',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [new_org_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

/**
 * 机构介绍--介绍
 */
const new_org_detail_welfare_centreCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_welfare_centre`,
    `new_org_detail_welfare_centre`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构介绍--介绍
 */
const new_org_detail_welfare_centre_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-new_org_detail_welfare_centre`,
    dataSourceID: new_org_detail_welfare_centreCarouselDataSource.id
});

/**
 * 机构介绍--介绍
 */
const new_org_detail_welfare_centre_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_welfare_centre',
        title: '机构介绍',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [new_org_detail_welfare_centre_carousel_DataView],
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['星级', '机构性质', '类型', '建立日期', '电话', '地址', '院内设施', '服务类型'],
                max_height: 900
            }
        )
    }
);

// /**
//  * 机构养老--福利院简介
//  */
// const new_org_detail_welfare_centreRoomStatusDataSource = new AjaxRemoteDataQueryer(
//     `data_source_new_org_detail_welfare_centre_room_status`,
//     `new_org_detail_welfare_centre_room_status`,
//     new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
// );

// /**
//  * 机构养老--房态图
//  */
// const new_org_detail_welfare_centre_room_status_DataView = createObject(ReportDataView, {
//     id: `data-view-org_detail-welfare-centre-room-status`,
//     dataSourceID: new_org_detail_welfare_centreRoomStatusDataSource.id
// });

// /**
//  * 机构养老--房态图
//  */
// const new_org_detail_welfare_centre_room_status_DisplayerModule = createObject(
//     DataDisplayerModuleControl,
//     {
//         id: 'new_org_detail_welfare_centre_room_status',
//         title: '房态图',
//         titleable: true,
//         groupType: GroupContainerType.carousel,
//         group: "welfare-sum-right-carousel6",
//         groupIndex: 5,
//         containerType: ContainerType.frameContain,
//         dataViews: [new_org_detail_welfare_centre_room_status_DataView],
//         displayer: createObject(
//             RoomStatusDisplayControl
//         )
//     }
// );

/**
 * 养老机构详情--床位总览--表格
 */
const new_org_detail_welfare_centre_room_status_DataSource = new AjaxRemoteDataQueryer(
    `data-source-new_org_detail_welfare_centre_room_status`,
    `new_org_detail_welfare_centre_room_status`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老机构详情--床位总览--表格
 */
const new_org_detail_welfare_centre_room_status_DataView = createObject(ReportDataView, {
    id: `data-view-new_org_detail_welfare_centre_room_status`,
    dataSourceID: new_org_detail_welfare_centre_room_status_DataSource.id
});

/**
 * 养老机构详情--床位总览--表格
 */
const new_org_detail_welfare_centre_room_status_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'welfare-table',
        titleable: true,
        title: "床位总览",
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-right-carousel6",
        groupIndex: 5,
        dataViews: [new_org_detail_welfare_centre_room_status_DataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            RowTableDisplayControl,
            {
                data_map: [
                    {
                        type: 1,
                        desc: "在院",
                        color: "inherit"
                    },
                    {
                        type: 2,
                        desc: "新入住",
                        color: "#F5A623"
                    },
                    {
                        type: 3,
                        desc: "请假",
                        color: "#7ED321"
                    },
                    {
                        type: 4,
                        desc: "退住",
                        color: "#001DF9"
                    },
                    {
                        type: 5,
                        desc: "转院",
                        color: "#B100FF"
                    },
                    {
                        type: 6,
                        desc: "死亡",
                        color: "#9B9B9B"
                    }
                ]
            }
        )
    }
);

scrollAnimation("ant-table-tbody", 16);

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubble_welfare_single_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_bubble_welfare_single`,
    `new_sum_statistical_bubble_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubble_welfare_single_bubble_DataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_bubble_welfare_single`,
    dataSourceID: new_sum_statistical_bubble_welfare_single_DataSource.id
});

/**
 * 养老院总数统计--泡泡
 */
const new_sum_statistical_bubble_welfare_single_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_bubble_welfare_single-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "养老院总数统计",
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel",
        groupIndex: 0,
        dataViews: [new_sum_statistical_bubble_welfare_single_bubble_DataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 养老院总数统计--星级饼图--饼图
 */
const new_sum_statistical_chart_welfare_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_chart_welfare_single`,
    `new_sum_statistical_chart_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老院总数统计--饼图
 */
const new_sum_statistical_chart_welfare_single_DataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_chart_welfare_single`,
    dataSourceID: new_sum_statistical_chart_welfare_singleDataSource.id
});

/**
 * 养老院总数统计--饼图
 */
let new_sum_statistical_chart_welfare_single_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老院总数统计--左部详情--饼图
 */
const new_sum_statistical_chart_welfare_single_detail_leftdisplayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_chart_welfare_single_multi_line',
        title: '养老院星级统计',
        titleable: true,
        dataViews: [new_sum_statistical_chart_welfare_single_DataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel2",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_subsidy_elder_quantity_pie_displayer',
            new_sum_statistical_chart_welfare_single_multilineOption
        )
    }
);

// /**
//  * 养老院总数统计--饼图
//  */
// const new_sum_statistical_chart_displayModuleControl = createObject(
//     DataDisplayerModuleControl,
//     {
//         id: 'new_sum_statistical_chart_multi_line',
//         // title: '长者数量趋势图',
//         titleable: true,
//         dataViews: [new_sum_statistical_chart_DataView],
//         groupIndex: 1,
//         groupType: GroupContainerType.carousel,
//         group: "welfare-sum-left-carousel2",
//         containerType: ContainerType.frameContain,
//         displayer: createPieChartDataDisplayer(
//             {
//                 pieValueFieldName: 'y',
//                 pieNameFieldName: 'x',
//             },
//             'eld_statistics_subsidy_elder_quantity_pie_displayer',
//             new_sum_statistical_chart_multilineOption
//         )
//     }
// );

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_detail_time_subscribe_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_time_subscribe_quantity`,
    `new_org_detail_time_subscribe_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_detail_time_subscribe_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_line-new_org_detail_time_subscribe_quantity`,
    dataSourceID: new_org_detail_time_subscribe_quantity_DataSource.id
});

/**
 * 养老机构预约数量走势--折线图
 */
let new_org_detail_time_subscribe_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: false,
    },
    grid: {
        right: 60
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '人',
    },
    series: [
        { name: '长者预约数量', type: 'line' }
    ],
};

/**
 * 养老机构预约数量走势--折线图
 */
const new_org_detail_time_subscribe_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_time_subscribe_quantity_multi_line',
        title: '长者预约数量走势',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel6",
        groupIndex: 4,
        dataViews: [new_org_detail_time_subscribe_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_org_detail_time_subscribe_quantity_multi_line_displayer',
            new_org_detail_time_subscribe_quantity_multilineOption
        )
    }
);

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sum_welfare_single_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_household_registration_elder_sum_welfare_single`,
    `new_household_registration_elder_sum_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sum_welfare_single_DataView = createObject(ReportDataView, {
    id: `data_view_new_household_registration_elder_sum_welfare_single`,
    dataSourceID: new_household_registration_elder_sum_welfare_single_DataSource.id
});

/**
 * 入住机构户籍长者数量--饼图
 */
let new_household_registration_elder_sum_welfare_single_pieOption: EChartsOptions = {
    legend: {
        show: true
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 入住机构户籍长者数量--饼图
 */
const new_household_registration_elder_sum_welfare_single_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_household_registration_elder_sum_welfare_single_pie',
        title: '入住机构户籍长者数量',
        titleable: true,
        dataViews: [new_household_registration_elder_sum_welfare_single_DataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "welfare-sum-left-carousel4",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_household_registration_elder_sum_welfare_single_pie_displayer',
            new_household_registration_elder_sum_welfare_single_pieOption
        )
    }
);

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                layout: layoutSelect,
            }
        )
    }
);

/** 
 * 图例控制器
 */
const legendControl = createObject(
    LegendMenuControl,
    {
        display_control: new_aswhole_mapModuleControl.displayer,
        option: legendOption
    }
);

const layoutLeft = createObject(NewCommonLayoutLeftControl);
const layoutMiddle = createObject(
    NewCommonLayoutMiddleControl,
    {
        legend_control: legendControl
    }
);
const layoutRight = createObject(NewCommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        id: "welfare-sum-leftControl",
        containerType: ContainerType.withdialog,
        placement: 'left',
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_sum_statistical_bubbleDataSource,
                    new_sum_statistical_chartDataSource,
                    new_household_registration_elder_sumDataSource,
                    new_org_statistics_street_quantityDataSource,
                ],
                modules: [
                    // 1
                    new_sum_statistical_bubbleModuleControl,
                    // 2
                    new_sum_statistical_chartModuleControl,
                    // 3
                    new_org_statistics_street_quantityModuleControl,
                    // 4
                    new_household_registration_elder_sumModuleControl
                ],
                layout: layoutLeft,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "养老院" },
                autoQuery: 1
            }
        )
    }
);

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_aswhole_mapDataSource
                ],
                modules: [
                    new_aswhole_mapModuleControl
                ],
                layout: layoutMiddle,
                autoQuery: 5000,
            }
        )
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        id: "welfare-sum-rightControl",
        containerType: ContainerType.withdialog,
        titleable: false,
        report: createObject(
            SmartReportControl,
            {
                // title: "",
                dataSources: [
                    new_institutions_occupancy_ranking_bedDataSource,
                    new_institutions_occupancy_ranking_eldDataSource,
                    // new_org_statistics_street_quantityDataSource,
                    new_org_statistics_time_subscribe_quantityDataSource,
                    new_institutions_classificationDataSource,
                    new_activity_statisticalDataSource,
                    new_activity_list_welfare_singleDataSource,
                    new_activity_trend_monthDataSource,
                    new_video_monitoringDataSource
                ],
                modules: [
                    // 1
                    new_org_statistics_time_subscribe_quantityModuleControl,
                    // 2
                    new_institutions_classificationModuleControl,
                    // 3
                    new_activity_list_welfare_singleModuleControl,
                    new_activity_statisticalModuleControl,
                    // 4
                    new_activity_trend_monthModuleControl,
                    // 5
                    new_institutions_occupancy_ranking_eldDisplayerModule,
                    // 6
                    new_institutions_occupancy_ranking_bedDisplayerModule,
                    // 弹窗
                    org_detail_video_surveillanceDialogModuleControl,
                    // 弹窗
                    org_monitorDialogModuleControl
                ],
                layout: layoutRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "养老院" },
                autoQuery: 1
            }
        )
    }
);

const layoutDetailLeft = createObject(NewCommonLayoutDetailLeftControl);
const layoutDetailRight = createObject(NewCommonLayoutDetailRightControl);

const leftDetailControl = createObject(
    SmartReportModuleControl,
    {
        id: "leftDetailControl",
        containerType: ContainerType.blank,
        isDialog: true,
        title: "",
        titleable: true,
        parentContainerID: "welfare-sum-leftControl",
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_sum_statistical_bubble_welfare_single_DataSource,
                    new_sum_statistical_chart_welfare_singleDataSource,
                    new_org_detail_time_subscribe_quantity_DataSource,
                    new_household_registration_elder_sum_welfare_single_DataSource
                ],
                modules: [
                    // 1
                    new_sum_statistical_bubble_welfare_single_multilineDisplayerModule,
                    // 2
                    new_sum_statistical_chart_welfare_single_detail_leftdisplayModuleControl,
                    // 3
                    new_org_detail_time_subscribe_quantity_displayModuleControl,
                    // 4
                    new_household_registration_elder_sum_welfare_single_displayModuleControl
                ],
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "养老院" },
                layout: layoutDetailLeft,
                autoQuery: 1
            }
        )
    }
);

const rightDetailControl = createObject(
    SmartReportModuleControl,
    {
        id: "rightDetailControl",
        containerType: ContainerType.blank,
        isDialog: true,
        parentContainerID: "welfare-sum-rightControl",
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_activity_statistical_welfare_singleDataSource,
                    new_activity_list_welfare_singleDataSource,
                    new_org_detail_personal_listDataSource,
                    new_monitor_listDataSource,
                    new_org_detail_picture_listCarouselDataSource,
                    new_org_detail_welfare_centreCarouselDataSource,
                    new_org_detail_welfare_centre_room_status_DataSource,
                    new_sum_statistical_chartDataSource
                ],
                modules: [
                    // 1
                    new_activity_statistical_welfare_single_square_DisplayerModule,
                    new_activity_list_welfare_singleModuleControl,
                    // 2
                    new_org_detail_picture_list_DisplayerModule,
                    // 3
                    new_org_detail_personal_listModuleControl,
                    // 4
                    new_org_detail_welfare_centre_DisplayerModule,
                    // 5
                    new_monitor_listModuleControl,
                    // 6
                    new_org_detail_welfare_centre_room_status_DisplayerModule
                ],
                layout: layoutDetailRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "养老院" },
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(CommonLayoutControl);
const irWelfareSumControl = createObject(
    SmartReportControl,
    {
        id: "irWelfareSum",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightControl, leftDetailControl, rightDetailControl,],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        isMain: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irWelfareSumControl };