import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, AjaxRemoteDataQueryer, ReportDataView } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { VideoControl } from "src/business/components/buss-components/video";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { VideoDisplayerControl } from "src/business/report/displayers/video";

/**
 * 服务商--服务商总数--数据源--数值
 */
const sep_statistics_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_total_quantity`,
    `sep_statistics_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商简介
 */
const sep_detail_service_providerDataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_service_provider`,
    `sep_detail_service_provider`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务商--服务总次数--数据源--数值
 */
const sep_detail_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_total_quantity`,
    `sep_detail_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务商--幸福院位置--数据源--星空图
 */
const sep_map_location_service_providerDataSource = new AjaxRemoteDataQueryer(
    `data-source-sep_map_location_service_provider`,
    `sep_map_location_service_provider`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商介绍--数据源--图片轮播
 */
const sep_detail_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_picture_list`,
    `sep_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务商--活动介绍--数据源--图片轮播
 */
const sep_detail_activity_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_activity_picture_list`,
    `sep_detail_activity_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务商--服务商总数--数据视图--数值
 */
const sep_statistics_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_total_quantity`,
    dataSourceID: sep_statistics_total_quantityDataSource.id
});

/**
 * 服务商--服务商总数--数据视图--数值
 */
const sep_detail_service_providerDataView = createObject(ReportDataView, {
    id: `data_view_sep_detail_service_provider`,
    dataSourceID: sep_detail_service_providerDataSource.id
});

/**
 * 服务商--服务总次数--数据视图--数值
 */
const sep_detail_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_sep_detail_total_quantity`,
    dataSourceID: sep_detail_total_quantityDataSource.id
});
/**
 * 服务商--幸福院位置--数据视图--星空图
 */
const sep_map_location_service_providerDataView = createObject(ReportDataView, {
    id: `data-view-sep_map_location_service_provider`,
    dataSourceID: sep_map_location_service_providerDataSource.id
});
/**
 * 服务商--服务商介绍--数据视图--图片轮播
 */
const sep_detail_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-sep_detail_picture_list-carousel`,
    dataSourceID: sep_detail_picture_listCarouselDataSource.id
});
/**
 * 服务商--活动介绍--数据视图--图片轮播
 */
const sep_detail_activity_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-sep_detail_activity_picture_list-carousel`,
    dataSourceID: sep_detail_activity_picture_listCarouselDataSource.id
});

/**
 * 服务商--服务商服务满意度对比--数据源--饼图
 */
const sep_statistics_nature_satisfaction_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_nature_satisfaction_quantity`,
    `sep_statistics_nature_satisfaction_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商服务评分对比--数据源--饼图
 */
const sep_statistics_nature_score_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_nature_score_quantity`,
    `sep_statistics_nature_score_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商服务人数统计--数据源--饼图
 */
const sep_statistics_nature_service_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_nature_service_personal_quantity`,
    `sep_statistics_nature_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商服务次数统计--数据源--饼图
 */
const sep_statistics_nature_service_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_nature_service_quantity`,
    `sep_statistics_nature_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--宣传视频--服务商介绍
 */
const sep_detail_promotional_video_surveillance_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_promotional_video_surveillance`,
    `sep_detail_promotional_video_surveillance`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务商--视频监控--服务商现场
 */
const sep_detail_video_surveillance_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_detail_video_surveillance`,
    `sep_detail_video_surveillance`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务商--服务商服务收入统计--数据源--饼图
 */
const sep_statistics_nature_income_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_nature_income_quantity`,
    `sep_statistics_nature_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商数量走势--数据源--折线图
 */
const sep_statistics_time_service_provider_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_sep_statistics_time_service_provider_quantity`,
    `sep_statistics_time_service_provider_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务商--服务商服务满意度对比--数据视图--饼图
 */
const sep_statistics_nature_satisfaction_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_nature_satisfaction_quantity`,
    dataSourceID: sep_statistics_nature_satisfaction_quantity_DataSource.id
});
/**
 * 服务商--服务商服务评分对比--数据视图--饼图
 */
const sep_statistics_nature_score_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_nature_score_quantity`,
    dataSourceID: sep_statistics_nature_score_quantity_DataSource.id
});
/**
 * 服务商--服务商服务人数统计--数据视图--饼图
 */
const sep_statistics_nature_service_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_nature_service_personal_quantity`,
    dataSourceID: sep_statistics_nature_service_personal_quantity_DataSource.id
});
/**
 * 服务商--服务商服务次数统计--数据视图--饼图
 */
const sep_statistics_nature_service_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_nature_service_quantity`,
    dataSourceID: sep_statistics_nature_service_quantity_DataSource.id
});

/**
 * 服务商--宣传视频--服务商介绍
 */
const sep_detail_promotional_video_surveillance_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_detail_promotional_video_surveillance`,
    dataSourceID: sep_detail_promotional_video_surveillance_DataSource.id
});

/**
 * 服务商--视频监控--服务商现场
 */
const sep_detail_video_surveillance_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_detail_video_surveillance`,
    dataSourceID: sep_detail_video_surveillance_DataSource.id
});

/**
 * 服务商--服务商服务收入统计--数据视图--饼图
 */
const sep_statistics_nature_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_nature_income_quantity`,
    dataSourceID: sep_statistics_nature_income_quantity_DataSource.id
});
/**
 * 服务商--服务商数量走势--数据视图--折线图
 */
const sep_statistics_time_service_provider_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_sep_statistics_time_service_provider_quantity`,
    dataSourceID: sep_statistics_time_service_provider_quantity_DataSource.id
});
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};
/**
 * 服务商--服务商总数--数值
 */
const sep_statistics_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_total_quantity-num',
        title: '服务商总数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [sep_statistics_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['总服务次数', '总服务人次', '总服务收入'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 服务商--服务商简介--数值
 */
const sep_detail_service_providerModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_service_provider-num',
        title: '服务商简介',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        groupIndex: 1,
        dataViews: [sep_detail_service_providerDataView],
        containerType: ContainerType.nhContain,
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7', 'y8', 'y9', 'y10'],
                titleName: ['LOGO', '名称', '地址', '工作人员总数', '评级情况', '机构类型', '机构性质', '法定代表人', '地址', '联系电话']
            }
        )
    }
);

/**
 * 服务商--服务总次数--数值
 */
const sep_detail_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_total_quantity-num',
        title: '服务总次数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 0,
        dataViews: [sep_detail_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 6,
                name_fields: ["服务总次数"],
                value_field: "y"
            }
        )
    }
);
/**
 * 服务商--服务商服务满意度对比--配置--饼图
 */
let sep_statistics_nature_satisfaction_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 服务商--服务商服务评分对比--配置--饼图
 */
let sep_statistics_nature_score_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务商--服务商服务人数统计--配置--饼图
 */
let sep_statistics_nature_service_personal_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务商--服务商服务次数统计--配置--饼图
 */
let sep_statistics_nature_service_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务商--服务商服务收入统计--配置--饼图
 */
let sep_statistics_nature_income_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务商--服务商数量走势--配置--折线图
 */
let sep_statistics_time_service_provider_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '服务商数量走势',
    },
};
/**
 * 服务商--幸福院位置--星空图
 */
const sep_map_location_service_providerModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_map_location_service_provider',
        title: '幸福院位置',
        titleable: false,
        dataViews: [sep_map_location_service_providerDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z",
                scatterObject: [{
                    name: "星空图",
                    symbol: "image://data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAABICAYAAAC5mNZRAAAFwElEQVRoQ92bW4wURRSGvzPLTW5yi8EYNAiGSHhQeZFgVMREQBIlRpAHUROEGGXRnV5f0JAAhoTpWQTUsBBFEwVEBBS5qNxMjAED+oAkKl4Qo6IEUG4r7u4cU90L7Mz07FbP7Fya87h76tT/9amuqjlVLVxJphqTgnjqtTsNDKWRAQi9EaqABmL8Q4qj1PAbIqmC+rBp7OodwFpgTzig17Qv5xmLMA5lDDAYJXcM4RzKfoTtwBoc+cVGn7XPUu1KI3NRnkepQvg4XUy9duYsI4jL15eCLtXe/McjwKPAKK9hPiY0AxuI4VIjX+YTIq2Nq+NRlgA3tfr7dh+oTq8jxQzgSZRrvQyAeZqzgGkoPQsW0DqA8CnCQuKyO1RcVSHJvShzgLsC2m4XkuqQYiHQqZXDKaBvqM7ycRZ+ADYj7KGK74hxhGq5kBbKDKsL3AKMB2+kDMvZlbBNSOgKLzOVYIKinEY4CzQCPVEGhJC2VXB1KsrqEI0q11XYIryqAznPH5WrMoQyD8iYq4dQhodoWpmuwkc+UFKXkeKZylQZQpWw+eK0PYlmNoRoWpmurYD60cxxIFaZSi1VCR9e3ikk9ABwm2XTSnX7oDVQAnAqVamlrlZASZ1Aii2WDSvTTdh0OUOvay9OcjJjC1SZwnOr2pi+207oF96OOrqWBbQAvJ1sNE3YkJmhe4Cd0aTxVL+fDrRKu3GCv1G6RhQqA8jf1+1q+XkdPSZhfXY9wNUXUOZHjwYQ3ssGWqyjaeLziAKtywYyhZIznELpETkoIQDIUCR0G3iFkmiZ8G5wTS2htcCiaNF479DaYCBXR3oFwqhZTiDVGElOoPSJFJOwJncZ19WNKA9GCghWtwU0C2XplQOU0BHAwUgBCe/kzpBfRz6Gck0koISfgYltH6e4uhZlSkUDCaYWvoQuvES1nG4PaAZKfUUCCX8C9XSlnlny+0WNbQPV6VCaOVwxQMLfwCZgHb3YwUwxBf00a/8Ez9WjKIMKhDoD/AWcQ7xTBXP4pd7aDt1R7zizN3D1pRNB/wTiBPBNyyngDkaylzHS1JYWG6A3UR4LBeSL3oiwnir28awctWpvFvQ36MGNNLQnPFe89oGSOo0Ub1kJghTCcq5iPk/LMcs2HepmAzTIO9Fuz4RfESYTl73tuRbz/+0Dmd5dPYwytA0hx+jCncyWsk8gtkD1qHeoHGzCwziyvphP3ja2HVBSp5DyLjZkm1mh4wxBxMxaZTc7oISa7Y9ZyIKAtuDIxLKTtAiwA/Lfo4MoZsOabkJEgZK6hBTVAZn4kVppa8IoafLsM1SnD9DsbTsyM2Quy/TjOTHbkrKbPdBi7UOTtxXJPras4j5q5JOy07Tspex1uLofZWRAlubhyFz7QMXztM+QPzEsQjElrkz7jFq5u3gy7SOHA6rTcTRjipCZ79EF+tOHJ+Rf+66L4xkOKKE9EK9M3DlAzlhqZVdxZNpHDQdk4ibUFPJHB2RpAY68aN91cTzzAZoHZAsX9uHI7cWRaR81HyDz8mffRDRXMKsYUO71KDyQf8PQLKLdAobdQzhS1jtD4YH86XsnijlgzpztluPIU/YDpOM98wNK6hxSmCsAmUBHcGRwx8u0j5gf0GIdRRPmkka2xRhGXL63l9CxnvkB7dZOHPDWo6Drz7OplbIV+fMD8tcjc9FpQsCw24Yj2X/v2ETkjJY/kKtxFDcAyHz70J8aaSgRQ1o3+QMl9VZSfBUoWpiAI9l7vhIQ5g/kH1seR+mXpTPGK8TFfGZQcssfyH+PzCI6KWDY/YQjQ0pOE/oHXqbChJqr0ctyDLubceTbUkMVliFXh6McyrEe3U9ctkYLyB925jODgRnC3+YGHmeymGOTklphGTJSXV2NMvWSamEVcaaX5FO1gEfVEUDTUVa2xF6Jw8xyloULB3pZr6eRw8RYQQ3V5YQxD7VwIBPFfC05U86X9GXJ0dn/KHCm3U7j8KgAAAAASUVORK5CYII=",
                    symbolSize: 100
                }]
            },
            "sep_map_location_service_provider-modules",
            option
        )
    }
);
/**
 * 服务商--服务商介绍--图片轮播
 */
const sep_detail_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_picture_list-carousel',
        title: '服务商介绍',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [sep_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务商--服务商介绍--图片轮播-弹窗
 */
const sep_detail_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_picture_list-carousel-dialog',
        title: '服务商介绍',
        titleable: false,
        isDialog: true,
        parentContainerID: sep_map_location_service_providerModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [sep_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务商--活动介绍--图片轮播-弹窗
 */
const sep_detail_activity_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_activity_picture_list-carousel-dialog',
        title: '活动介绍',
        titleable: false,
        isDialog: true,
        parentContainerID: sep_map_location_service_providerModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [sep_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务商--服务商服务满意度对比--数据源--饼图
 */
const sep_statistics_nature_satisfaction_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_nature_satisfaction_quantity_pie',
        title: '服务商服务满意度对比',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        dataViews: [sep_statistics_nature_satisfaction_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'sep_statistics_nature_satisfaction_quantity_pie_displayer',
            sep_statistics_nature_satisfaction_quantity_pieOption
        )
    }
);
sep_statistics_nature_satisfaction_quantity_displayModuleControl;
/**
 * 服务商--活动介绍--图片轮播
 */
const sep_detail_activity_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_activity_picture_list-carousel',
        title: '活动介绍',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [sep_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务商--服务商服务评分对比--饼图
 */
const sep_statistics_nature_score_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_nature_score_quantity_pie',
        title: '服务商服务评分对比',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        dataViews: [sep_statistics_nature_score_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'sep_statistics_nature_score_quantity_pie_displayer',
            sep_statistics_nature_score_quantity_pieOption
        )
    }
);
/**
 * 服务商--服务商服务人数统计--饼图
 */
const sep_statistics_nature_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_nature_service_personal_quantity_pie',
        title: '服务商服务人数统计',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [sep_statistics_nature_service_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'sep_statistics_nature_service_personal_quantity_pie_displayer',
            sep_statistics_nature_service_personal_quantity_pieOption
        )
    }
);
/**
 * 服务商--服务商服务次数统计--饼图
 */
const sep_statistics_nature_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_nature_service_quantity_pie',
        title: '服务商服务次数统计',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [sep_statistics_nature_service_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'sep_statistics_nature_service_quantity_pie_displayer',
            sep_statistics_nature_service_quantity_pieOption
        )
    }
);

/**
 * 服务商--宣传视频--服务商介绍
 */
const sep_detail_promotional_video_surveillance_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_promotional_video_surveillance_pie',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [sep_detail_promotional_video_surveillance_quantity_DataView],
        containerType: ContainerType.nhContain,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://localhost:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 600,
                height: 600
            }
        )
    }
);

/**
 * 服务商--视频监控--服务商现场
 */
const sep_detail_video_surveillance_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_detail_video_surveillance_pie',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        dataViews: [sep_detail_video_surveillance_quantity_DataView],
        containerType: ContainerType.nhContain,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoDisplayerControl,
            {
                videoConfig: {
                    url: "",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                }
            }
        )
    }
);

/**
 * 服务商--服务商服务收入统计--饼图
 */
const sep_statistics_nature_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_nature_income_quantity_pie',
        title: '服务商服务收入统计',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [sep_statistics_nature_income_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'sep_statistics_nature_income_quantity_pie_displayer',
            sep_statistics_nature_income_quantity_pieOption
        )
    }
);
/**
 * 服务商--服务商数量走势--折线图
 */
const sep_statistics_time_service_provider_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_time_service_provider_quantity_multi_line',
        title: '服务商数量走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        dataViews: [sep_statistics_time_service_provider_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_time_service_provider_quantity_multi_line_displayer',
            sep_statistics_time_service_provider_quantity_multilineOption
        )
    }
);
// const legendOption = [
//     {
//         title: "服务商",
//         key: "服务商"
//     }
// ];

/** 
 * 图例控制器
 */
// const legendControl = createObject(
//     LegendMenuControl,
//     {
//         echar_instance: sep_map_location_service_providerModuleControl.displayer,
//         option: legendOption
//     }
// );

/**
 * 服务商--镇街服务商数量分布--数据源--柱状图
 */
const sep_statistics_street_service_provider_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-source-sep_statistics_street_service_provider_quantity`,
    `sep_statistics_street_service_provider_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--镇街服务商数量分布--数据视图--柱状图
 */
const sep_statistics_street_service_provider_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_street_service_provider_quantity`,
    dataSourceID: sep_statistics_street_service_provider_quantity_dataSource.id
});

/**
 * 服务商--镇街服务商数量分布--配置--柱状图
 */
let sep_statistics_street_service_provider_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}张",
            showMinLabel: false,
        },
        name: '服务商数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--镇街服务商数量分布--柱状图
 */
const sep_statistics_street_service_provider_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_street_service_provider_quantity_displayer',
        title: '镇街服务商数量分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_street_service_provider_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_street_service_provider_quantity-single-bar-displayer',
            sep_statistics_street_service_provider_quantity_barOption
        )
    }
);
/**
 * 服务商--服务商满意度排名（TOP10）--数据源--柱状图
 */
const sep_statistics_service_provider_satisfaction_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-sep_statistics_service_provider_satisfaction_quantity`,
    `sep_statistics_service_provider_satisfaction_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商满意度排名（TOP10）--数据视图--柱状图
 */
const sep_statistics_service_provider_satisfaction_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_service_provider_satisfaction_quantity`,
    dataSourceID: sep_statistics_service_provider_satisfaction_quantity_dataSource.id
});

/**
 * 服务商--服务商满意度排名（TOP10）--配置--柱状图
 */
let sep_statistics_service_provider_satisfaction_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}张",
            showMinLabel: false,
        },
        name: '服务商满意度'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--服务商满意度--柱状图
 */
const sep_statistics_service_provider_satisfaction_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_service_provider_satisfaction_quantity_displayer',
        title: '服务商满意度排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_service_provider_satisfaction_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_service_provider_satisfaction_quantity-bar-displayer',
            sep_statistics_service_provider_satisfaction_quantity_barOption
        )
    }
);

/**
 * 服务商--服务商满意度排名（TOP10）--数据源--柱状图
 */
const sep_statistics_service_provider_score_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-sep_statistics_service_provider_score_top10_quantity`,
    `sep_statistics_service_provider_score_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商满意度排名（TOP10）--数据视图--柱状图
 */
const sep_statistics_service_provider_score_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_service_provider_score_top10_quantity`,
    dataSourceID: sep_statistics_service_provider_score_top10_quantity_dataSource.id
});

/**
 * 服务商--服务商满意度排名（TOP10）--配置--柱状图
 */
let sep_statistics_service_provider_score_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}张",
            showMinLabel: false,
        },
        name: '服务商评分(TOP10)'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--服务商满意度排名（TOP10）--柱状图
 */
const sep_statistics_service_provider_score_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_service_provider_score_top10_quantity_displayer',
        title: '服务商满意度排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_service_provider_score_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_service_provider_score_top10_quantity-bar-displayer',
            sep_statistics_service_provider_score_top10_quantity_barOption
        )
    }
);

/**
 * 服务商--服务商服务人数排名（TOP10）--数据源--柱状图
 */
const sep_statistics_service_provider_service_personal_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-sep_statistics_service_provider_service_personal_top10_quantity`,
    `sep_statistics_service_provider_service_personal_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商服务人数排名（TOP10）--数据视图--柱状图
 */
const sep_statistics_service_provider_service_personal_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_service_provider_service_personal_top10_quantity`,
    dataSourceID: sep_statistics_service_provider_service_personal_top10_quantity_dataSource.id
});

/**
 * 服务商--服务商服务人数排名（TOP10）--配置--柱状图
 */
let sep_statistics_service_provider_service_personal_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}张",
            showMinLabel: false,
        },
        name: '服务人员人数(TOP10)'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--服务商服务人数排名（TOP10）--柱状图
 */
const sep_statistics_service_provider_service_personal_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_service_provider_service_personal_top10_quantity_displayer',
        title: '服务商服务人数排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_service_provider_service_personal_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_service_provider_service_personal_top10_quantity-bar-displayer',
            sep_statistics_service_provider_service_personal_top10_quantity_barOption
        )
    }
);

/**
 * 服务商--服务商服务次数排名（TOP10)--数据源--柱状图
 */
const sep_statistics_service_provider_service_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-sep_statistics_service_provider_service_top10_quantity`,
    `sep_statistics_service_provider_service_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商服务次数排名（TOP10)--数据视图--柱状图
 */
const sep_statistics_service_provider_service_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_service_provider_service_top10_quantity`,
    dataSourceID: sep_statistics_service_provider_service_top10_quantity_dataSource.id
});

/**
 * 服务商--服务商服务次数排名（TOP10)--配置--柱状图
 */
let sep_statistics_service_provider_service_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数(TOP10)'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--服务商服务次数排名（TOP10)--柱状图
 */
const sep_statistics_service_provider_service_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_service_provider_service_top10_quantity_displayer',
        title: '服务商服务次数排名（TOP10)',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_service_provider_service_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_service_provider_service_top10_quantity-bar-displayer',
            sep_statistics_service_provider_service_top10_quantity_barOption
        )
    }
);

/**
 * 服务商--服务商服务收入排名（TOP10)--数据源--柱状图
 */
const sep_statistics_service_provider_income_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data-sep_statistics_service_provider_income_top10_quantity`,
    `sep_statistics_service_provider_income_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务商--服务商服务收入排名（TOP10)--数据视图--柱状图
 */
const sep_statistics_service_provider_income_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data-view-sep_statistics_service_provider_income_top10_quantity`,
    dataSourceID: sep_statistics_service_provider_income_top10_quantity_dataSource.id
});

/**
 * 服务商--服务商服务收入排名（TOP10)--配置--柱状图
 */
let sep_statistics_service_provider_income_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务收入(TOP10)'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务商--服务商服务收入排名（TOP10)--柱状图
 */
const sep_statistics_service_provider_income_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'sep_statistics_service_provider_income_top10_quantity_displayer',
        title: '服务商服务收入排名（TOP10)',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [sep_statistics_service_provider_income_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'sep_statistics_service_provider_income_top10_quantity-bar-displayer',
            sep_statistics_service_provider_income_top10_quantity_barOption
        )
    }
);
sep_statistics_service_provider_income_top10_quantity_displayModuleControl;

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportControl,
    {
        dataSources: [
        ],
        modules: [
        ],
        layout: layoutSelect,
        autoQuery: 1,
    }
);

const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(CommonLayoutMiddleControl);
const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    sep_statistics_total_quantityDataSource,
                    sep_statistics_street_service_provider_quantity_dataSource,
                    sep_statistics_service_provider_satisfaction_quantity_dataSource,
                    sep_statistics_service_provider_service_personal_top10_quantity_dataSource,
                    sep_statistics_service_provider_service_top10_quantity_dataSource,
                    sep_statistics_nature_satisfaction_quantity_DataSource,
                    sep_statistics_service_provider_score_top10_quantity_dataSource,
                    sep_statistics_nature_score_quantity_DataSource,
                    sep_statistics_nature_service_personal_quantity_DataSource,
                    sep_statistics_nature_service_quantity_DataSource,
                    sep_statistics_service_provider_income_top10_quantity_dataSource,
                    sep_statistics_nature_income_quantity_DataSource,
                    sep_statistics_time_service_provider_quantity_DataSource,

                ],
                modules: [
                    // 0
                    sep_statistics_total_quantityModuleControl,
                    // 1
                    sep_statistics_street_service_provider_quantity_displayModuleControl,
                    sep_statistics_time_service_provider_quantity_displayModuleControl,
                    // 2
                    sep_statistics_service_provider_satisfaction_quantity_displayModuleControl,
                    sep_statistics_nature_satisfaction_quantity_displayModuleControl,
                    sep_statistics_service_provider_score_top10_quantity_displayModuleControl,
                    sep_statistics_nature_score_quantity_displayModuleControl,
                    // 3
                    sep_statistics_nature_service_personal_quantity_displayModuleControl,
                    sep_statistics_service_provider_service_personal_top10_quantity_displayModuleControl,
                    // 4
                    sep_statistics_service_provider_service_top10_quantity_displayModuleControl,
                    sep_statistics_nature_service_quantity_displayModuleControl,
                    // 5
                    sep_statistics_service_provider_income_top10_quantity_displayModuleControl,
                    sep_statistics_nature_income_quantity_displayModuleControl,
                ],
                layout: layoutLeft,
                autoQuery: 1,
            }
        )
    });
const middleControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            sep_map_location_service_providerDataSource,
        ],
        modules: [
            sep_map_location_service_providerModuleControl,
        ],
        layout: layoutMiddle,
        autoQuery: 1,
    }
);
const middleReport = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportModuleControl,
            {
                containerType: ContainerType.blank,
                report: middleControl
            }
        )
    }
);
const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    sep_detail_total_quantityDataSource,
                    sep_detail_picture_listCarouselDataSource,
                    sep_detail_service_providerDataSource,
                    sep_detail_activity_picture_listCarouselDataSource,
                    sep_detail_promotional_video_surveillance_DataSource,
                    sep_detail_video_surveillance_DataSource,
                ],
                modules: [
                    // 0
                    sep_detail_total_quantityModuleControl,
                    // 1
                    sep_detail_service_providerModuleControl,
                    // 2
                    sep_detail_picture_list_DisplayerModule,
                    sep_detail_picture_list_DialogDisplayerModule,
                    // 3
                    sep_detail_activity_picture_list_DisplayerModule,
                    sep_detail_activity_picture_list_DialogDisplayerModule,
                    // 4
                    sep_detail_promotional_video_surveillance_displayModuleControl,
                    // 5
                    sep_detail_video_surveillance_displayModuleControl,
                ],
                layout: layoutRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5" },
                autoQuery: 1,
            }
        )
    }
);
const layoutAll = createObject(CommonLayoutControl);
const irServiceProviderControl = createObject(
    SmartReportControl,
    {
        id: 'irServiceProvider',
        modules: [selectControl, leftControl, middleReport, rightControl],
        dataSources: [],
        layout: layoutAll,
        // autoQuery: 1,
        isMain: true,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irServiceProviderControl };