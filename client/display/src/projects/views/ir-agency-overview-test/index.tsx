import { IDataService, createObject } from "pao-aop";
import { remote } from "src/projects/remote";
import { ReportDataView, DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, AjaxRemoteDataQueryer } from "src/business/report/smart";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { IDisplayControlService } from "src/business/models/control";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { SingleBubbleControl } from "src/business/report/displayers/nh-welfare/single-bubble";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";

/**
 * 综合业务统计--总体--卡片
 */
const new_comprehensive_statistics_overallDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_comprehensive_statistics_overall`,
    `new_activity_statistical_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 综合业务统计--服务情况--柱状图
 */
const new_comprehensive_statistics_service_situationDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_comprehensive_statistics_service_situation`,
    `new_institutions_classification`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 综合业务统计--服务占比--表格
 */
const new_comprehensive_statistics_service_shareDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_comprehensive_statistics_service_share`,
    `new_institutions_occupancy_ranking_bed`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--总体--泡泡
 */
const new_call_center_overallDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_call_center_overall`,
    `new_aswhole_call_center_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--卡片
 */
const new_call_center_cardDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_call_center_card`,
    `new_activity_list_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构总览--统计--卡片
 */
const new_agency_overview_statisticsDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_agency_overview_statistics`,
    `new_aswhole_call_center_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构总览--表格
 */
const new_institutional_overview_formDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_institutional_overview_form`,
    `new_institutions_occupancy_ranking_eld`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构展示--长者数量--饼图
 */
const new_number_of_institutional_eldersDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_number_of_institutional_elders`,
    `new_household_registration_elder_sum`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构展示--长者年龄占比--饼图
 */
const new_ag_ratio_of_institutional_eldersDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_ag_ratio_of_institutional_elders`,
    `new_sum_statistical_chart_welfare_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构展示--长者爱好--饼图
 */
const new_institutional_exhibition_elderly_loveDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_institutional_exhibition_elderly_love`,
    `new_org_detail_origin_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构展示--入住趋势图--折线图
 */
const new_elderly_occupancy_trend_chartDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_elderly_occupancy_trend_chart`,
    `new_elderly_occupancy_trend_chart`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 居家服务总览--总体--卡片
 */
const new_home_service_overview_overallDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_list_happiness_single`,
    `new_home_service_overview_overall`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 居家服务总览--表格
 */
const new_home_service_overview_formDataSource = new AjaxRemoteDataQueryer(
    `com_detail_service_products`,
    `new_home_service_overview_form`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区居家--总体--卡片
 */
const new_community_home_overallDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_task_card`,
    `new_community_home_overall`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区居家--柱状图
 */
const new_community_home_bar_chartDataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_ability_elder_quantity`,
    `new_community_home_bar_chart`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 区县长者总览--柱状图
 */
const new_district_and_county_elders_overview_histogramDataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_street_elder_quantity`,
    `new_district_and_county_elders_overview_histogram`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区居家服务中心总览--总体--卡片
 */
const new_service_center_overview_overallDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_service_center_overview_overall`,
    `new_activity_statistical_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区居家服务中心总览--表格
 */
const new_service_center_overview_formDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_service_center_overview_form`,
    `new_institutions_occupancy_ranking_bed`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 综合业务统计--总体--卡片
 */
const new_comprehensive_statistics_overallDataView = createObject(ReportDataView, {
    id: `data_view_new_comprehensive_statistics_overall`,
    dataSourceID: new_comprehensive_statistics_overallDataSource.id
});

/**
 * 综合业务统计--服务情况--柱状图
 */
const new_comprehensive_statistics_service_situationDataView = createObject(ReportDataView, {
    id: `data_view_new_comprehensive_statistics_service_situation`,
    dataSourceID: new_comprehensive_statistics_service_situationDataSource.id
});

/**
 * 综合业务统计--服务占比--表格
 */
const new_comprehensive_statistics_service_shareDataView = createObject(ReportDataView, {
    id: `data_view_new_comprehensive_statistics_service_share`,
    dataSourceID: new_comprehensive_statistics_service_shareDataSource.id
});

/**
 * 呼叫中心--总体--泡泡
 */
const new_call_center_overallDataView = createObject(ReportDataView, {
    id: `data_view_new_call_center_overall`,
    dataSourceID: new_call_center_overallDataSource.id
});

/**
 * 呼叫中心--卡片
 */
const new_call_center_cardDataView = createObject(ReportDataView, {
    id: `data_view_new_call_center_card`,
    dataSourceID: new_call_center_cardDataSource.id
});

/**
 * 机构总览--统计--卡片
 */
const new_agency_overview_statisticsDataView = createObject(ReportDataView, {
    id: `data_view_new_agency_overview_statistics`,
    dataSourceID: new_agency_overview_statisticsDataSource.id
});

/**
 * 机构总览--表格
 */
const new_institutional_overview_formDataView = createObject(ReportDataView, {
    id: `data_view_new_institutional_overview_form`,
    dataSourceID: new_institutional_overview_formDataSource.id
});

/**
 * 机构展示--长者数量--饼图
 */
const new_number_of_institutional_eldersDataView = createObject(ReportDataView, {
    id: `data_view_new_number_of_institutional_elders`,
    dataSourceID: new_number_of_institutional_eldersDataSource.id
});

/**
 * 机构展示--长者年龄占比--饼图
 */
const new_ag_ratio_of_institutional_eldersDataView = createObject(ReportDataView, {
    id: `data_view_new_ag_ratio_of_institutional_elders`,
    dataSourceID: new_ag_ratio_of_institutional_eldersDataSource.id
});

/**
 * 机构展示--长者爱好--饼图
 */
const new_institutional_exhibition_elderly_loveDataView = createObject(ReportDataView, {
    id: `data_view_new_institutional_exhibition_elderly_love`,
    dataSourceID: new_institutional_exhibition_elderly_loveDataSource.id
});

/**
 * 机构展示--入住趋势图--折线图
 */
const new_elderly_occupancy_trend_chartDataView = createObject(ReportDataView, {
    id: `data_view_new_elderly_occupancy_trend_chart`,
    dataSourceID: new_elderly_occupancy_trend_chartDataSource.id
});

/**
 * 居家服务总览--总体--卡片
 */
const new_home_service_overview_overallDataView = createObject(ReportDataView, {
    id: `data_view_new_home_service_overview_overall`,
    dataSourceID: new_home_service_overview_overallDataSource.id
});

/**
 * 居家服务总览--表格
 */
const new_home_service_overview_formDataView = createObject(ReportDataView, {
    id: `data_view_new_home_service_overview_form`,
    dataSourceID: new_home_service_overview_formDataSource.id
});

/**
 * 社区居家--总体--卡片
 */
const new_community_home_overallDataView = createObject(ReportDataView, {
    id: `data_view_new_community_home_overall`,
    dataSourceID: new_community_home_overallDataSource.id
});

/**
 * 社区居家--柱状图
 */
const new_community_home_bar_chartDataView = createObject(ReportDataView, {
    id: `data_view_new_community_home_bar_chart`,
    dataSourceID: new_community_home_bar_chartDataSource.id
});

/**
 * 区县长者总览--柱状图
 */
const new_district_and_county_elders_overview_histogramDataView = createObject(ReportDataView, {
    id: `data_view_new_district_and_county_elders_overview_histogram`,
    dataSourceID: new_district_and_county_elders_overview_histogramDataSource.id
});

/**
 * 社区居家服务中心总览--总体--卡片
 */
const new_service_center_overview_overallDataView = createObject(ReportDataView, {
    id: `data_view_new_service_center_overview_overall`,
    dataSourceID: new_service_center_overview_overallDataSource.id
});

/**
 * 社区居家服务中心总览--表格
 */
const new_service_center_overview_formDataView = createObject(ReportDataView, {
    id: `data_view_new_service_center_overview_form`,
    dataSourceID: new_service_center_overview_formDataSource.id
});

/**
 * 综合业务统计--服务情况--柱状图
 */
let new_comprehensive_statistics_service_situationOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        name: "家",
        // axisLabel: {
        //     showMinLabel: false,
        // }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构展示--长者数量--饼图
 */
let new_number_of_institutional_eldersOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 机构展示--长者年龄占比--饼图
 */
let new_ag_ratio_of_institutional_eldersOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 机构展示--长者爱好--饼图
 */
let new_institutional_exhibition_elderly_loveOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 机构展示--入住趋势图--折线图
 */
let new_elderly_occupancy_trend_chartOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 社区居家--柱状图
 */
let new_community_home_bar_chartOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        // axisLabel: {
        //     showMinLabel: false,
        // }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 区县长者总览--柱状图
 */
let new_district_and_county_elders_overview_histogramOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        // axisLabel: {
        //     showMinLabel: false,
        // }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 综合业务统计--总体--卡片
 */
const new_comprehensive_statistics_overallDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_comprehensive_statistics_overall-pie',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [new_comprehensive_statistics_overallDataView],
        title: '机构入住长者排名',
        groupType: GroupContainerType.carousel,
        displayer: createObject(
            MoreSquaresControl,
            {
                square_span: 24,
                name_fields: ['机构养老', '社区服务中心'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 综合业务统计--服务情况--柱状图
 */
const new_comprehensive_statistics_service_situationModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_comprehensive_statistics_service_situation_pie',
        titleable: true,
        title: '综合业务统计--服务情况',
        groupType: GroupContainerType.carousel,
        dataViews: [new_comprehensive_statistics_service_situationDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_comprehensive_statistics_service_situation_bar_displayer',
            new_comprehensive_statistics_service_situationOption
        )
    }
);

/**
 * 综合业务统计--服务占比--表格
 */
const new_comprehensive_statistics_service_shareDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_comprehensive_statistics_service_share-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '综合业务统计--服务占比',
        dataViews: [new_comprehensive_statistics_service_shareDataView],
        groupType: GroupContainerType.carousel,
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '占比',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 呼叫中心--总体--泡泡
 */
const new_call_center_overallDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_call_center_overall-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "呼叫中心--总体",
        groupType: GroupContainerType.carousel,
        dataViews: [new_call_center_overallDataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 呼叫中心--卡片
 */
const new_call_center_cardModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_call_center_card-num',
        title: '呼叫中心--卡片',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_call_center_cardDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['响应时间', '未接听', '突发事件'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 机构总览--统计--卡片
 */
const new_agency_overview_statisticsModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_agency_overview_statistics-num',
        title: '机构总览--统计',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_agency_overview_statisticsDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                square_span: 24,
                name_fields: ['服务长者'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 机构总览--表格
 */
const new_institutional_overview_formModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_institutional_overview_form-num',
        title: '机构总览',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_institutional_overview_formDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '长者数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 730
            }
        )
    }
);

/**
 * 机构展示--长者数量--饼图
 */
const new_number_of_institutional_eldersModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_number_of_institutional_elders-num',
        title: '机构展示--长者数量',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_number_of_institutional_eldersDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_number_of_institutional_eldersdisplayer',
            new_number_of_institutional_eldersOption
        )
    }
);

/**
 * 机构展示--长者年龄占比--饼图
 */
const new_ag_ratio_of_institutional_eldersModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_ag_ratio_of_institutional_elders-num',
        title: '机构展示--长者年龄占比',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_ag_ratio_of_institutional_eldersDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_ag_ratio_of_institutional_eldersdisplayer',
            new_ag_ratio_of_institutional_eldersOption
        )
    }
);

/**
 * 机构展示--长者爱好--饼图
 */
const new_institutional_exhibition_elderly_loveModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_institutional_exhibition_elderly_love-num',
        title: '机构展示--长者爱好',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_institutional_exhibition_elderly_loveDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_institutional_exhibition_elderly_lovedisplayer',
            new_institutional_exhibition_elderly_loveOption
        )
    }
);

/**
 * 机构展示--入住趋势图--折线图
 */
const new_elderly_occupancy_trend_chartModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_elderly_occupancy_trend_chart-num',
        title: '机构展示--入住趋势图',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_elderly_occupancy_trend_chartDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_elderly_occupancy_trend_chart_displayer',
            new_elderly_occupancy_trend_chartOption
        )
    }
);

/**
 * 居家服务总览--总体--卡片
 */
const new_home_service_overview_overallModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_home_service_overview_overall-num',
        title: '居家服务总览--总体',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_home_service_overview_overallDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['累计服务长者'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 居家服务总览--表格
 */
const new_home_service_overview_formModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_home_service_overview_form-num',
        title: '居家服务总览',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_home_service_overview_formDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '长者数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 社区居家--总体--卡片
 */
const new_community_home_overallModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_community_home_overall-num',
        title: '社区居家--总体',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_community_home_overallDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                square_span: 24,
                name_fields: ['好评率', '服务客户', '累计派工'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 社区居家--柱状图
 */
const new_community_home_bar_chartModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_community_home_bar_chart-num',
        title: '社区居家',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_community_home_bar_chartDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_community_home_bar_chart_displayer',
            new_community_home_bar_chartOption
        )
    }
);

/**
 * 区县长者总览--柱状图
 */
const new_district_and_county_elders_overview_histogramModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_district_and_county_elders_overview_histogram-num',
        title: '区县长者总览',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_district_and_county_elders_overview_histogramDataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_district_and_county_elders_overview_histogram_displayer',
            new_district_and_county_elders_overview_histogramOption
        )
    }
);

/**
 * 社区居家服务中心总览--总体--卡片
 */
const new_service_center_overview_overallModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_service_center_overview_overall-num',
        title: '社区居家服务中心总览--总体',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_service_center_overview_overallDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                square_span: 24,
                name_fields: ['服务长者'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 社区居家服务中心总览--表格
 */
const new_service_center_overview_formModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_service_center_overview_form-num',
        title: '社区居家服务中心总览--表格',
        titleable: true,
        groupType: GroupContainerType.carousel,
        dataViews: [new_service_center_overview_formDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '长者数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                autoQuery: 1,
                layout: layoutSelect,
            }
        )
    }
);

const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(CommonLayoutMiddleControl);
const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_comprehensive_statistics_overallDataSource,
                    new_comprehensive_statistics_service_situationDataSource,
                    new_comprehensive_statistics_service_shareDataSource,
                    new_number_of_institutional_eldersDataSource,
                    new_ag_ratio_of_institutional_eldersDataSource,
                    new_institutional_exhibition_elderly_loveDataSource,
                    new_elderly_occupancy_trend_chartDataSource,
                    new_community_home_overallDataSource,
                    new_community_home_bar_chartDataSource
                ],
                modules: [
                    new_comprehensive_statistics_overallDisplayerModule,
                    new_comprehensive_statistics_service_situationModuleControl,
                    new_comprehensive_statistics_service_shareDisplayerModule,
                    new_number_of_institutional_eldersModuleControl,
                    new_ag_ratio_of_institutional_eldersModuleControl,
                    new_institutional_exhibition_elderly_loveModuleControl,
                    new_elderly_occupancy_trend_chartModuleControl,
                    new_community_home_overallModuleControl,
                    new_community_home_bar_chartModuleControl
                ],
                layout: layoutLeft,
                autoQuery: 1
            }
        )
    });

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_call_center_overallDataSource,
                    new_call_center_cardDataSource,
                    new_home_service_overview_overallDataSource,
                    new_home_service_overview_formDataSource,
                    new_district_and_county_elders_overview_histogramDataSource
                ],
                modules: [
                    new_call_center_overallDisplayerModule,
                    new_call_center_cardModuleControl,
                    new_home_service_overview_overallModuleControl,
                    new_home_service_overview_formModuleControl,
                    new_district_and_county_elders_overview_histogramModuleControl
                ],
                layout: layoutMiddle,
                autoQuery: 1,
            }
        )
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_agency_overview_statisticsDataSource,
                    new_institutional_overview_formDataSource,
                    new_service_center_overview_overallDataSource,
                    new_service_center_overview_formDataSource
                ],
                modules: [
                    new_agency_overview_statisticsModuleControl,
                    new_institutional_overview_formModuleControl,
                    new_service_center_overview_overallModuleControl,
                    new_service_center_overview_formModuleControl
                ],
                layout: layoutRight,
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(CommonLayoutControl);
const irAgencyOverviewTestControl = createObject(
    SmartReportControl,
    {
        id: "irAgencyOverviewTest",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightControl],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        isMain: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irAgencyOverviewTestControl };