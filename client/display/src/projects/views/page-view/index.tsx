import { ReportPageControl, SmartReportControl } from 'src/business/report/smart';
import { createObject } from 'pao-aop';
import { AjaxJsonRpcFactory } from 'pao-aop-client';
import { IDisplayControlService } from 'src/business/models/control';
import { SelectIframeLayoutControl } from 'src/projects/components/layout/select-iframe-layout';
import { remote } from 'src/projects/remote';
// import { RemotePath } from 'src/projects/router';
import { irCommunityHomeControl } from '../ir-community-home';
import { irServiceProviderControl } from '../ir-service-provider';
import { irServicePersonalControl } from '../ir-service-personal';
import { irElderControl } from '../ir-elder';
import { irComprehensiveControl } from '../ir-comprehensive';
import { irWelfareCentreControl } from '../ir-welfare-centre';

const selectiframeLayout = createObject(SelectIframeLayoutControl);

const iframeWelfareCentreModule = createObject(ReportPageControl, { page: irWelfareCentreControl, id: 'irWelfareCentre' });
const iframeCommunityHomeModule = createObject(ReportPageControl, { page: irCommunityHomeControl, id: 'irCommunityHome' });
const iframeServiceProviderModule = createObject(ReportPageControl, { page: irServiceProviderControl, id: 'irServiceProvider' });
const iframeServicePersonalModule = createObject(ReportPageControl, { page: irServicePersonalControl, id: 'irServicePersonal' });
const iframeElderModule = createObject(ReportPageControl, { page: irElderControl, id: 'irElder' });
const iframeComprehensiveModule = createObject(ReportPageControl, { page: irComprehensiveControl, id: 'irComprehensive' });

export const pageReport = createObject(
    SmartReportControl,
    {
        modules: [iframeWelfareCentreModule, iframeCommunityHomeModule, iframeServiceProviderModule, iframeServicePersonalModule, iframeElderModule, iframeComprehensiveModule],
        layout: selectiframeLayout,
        iframeable: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);