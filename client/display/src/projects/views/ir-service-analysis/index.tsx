import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { SmartReportControl, SmartReportModuleControl, AjaxRemoteDataQueryer, ReportDataView, DataDisplayerModuleControl } from "src/business/report/smart";
import { } from "../echartCommonVar";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { SingleBubbleControl } from "src/business/report/displayers/nh-welfare/single-bubble";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { HomeLayoutLeftControl } from "src/projects/components/layout/new-common-layout/home-left";
import { iconMap, getCallCenterEle, noBeadhouse, noBlessed, noService, noServePropleServing, noServePropleFree, noElderAddr, noElderDevice } from "src/projects/util-tool";
import { LegendMenuControl } from "src/projects/components/lengend-menu";

/**
 * 居家养老总数统计--泡泡
 */
const new_sum_statistical_bubbleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_bubble`,
    `new_sum_statistical_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 居家养老总数统计--星级饼图--饼图
 */
const new_sum_statistical_chartDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_chart`,
    `new_sum_statistical_chart`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务商排名--评分--表格
 */
const new_service_ranking_scoreDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_service_ranking_score`,
    `new_service_ranking_score`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务商排名--服务次数--表格
 */
const new_service_ranking_timeDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_service_ranking_time`,
    `new_service_ranking_time`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 热门服务排名--表格
 */
const new_hot_service_rankingDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_hot_service_ranking`,
    `new_hot_service_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员年龄分布比例--饼图
 */
const new_com_detail_service_products_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_detail_service_products_list`,
    `new_com_detail_service_products_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 图片轮播--图片轮播
 */
const new_sep_detail_picture_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sep_detail_picture_list`,
    `new_sep_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员服务量排名--表格
 */
const new_servicer_service_num_rankingDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_servicer_service_num_ranking`,
    `new_servicer_service_num_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 介绍--自定义组件
 */
const new_sep_detail_service_providerDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sep_detail_service_provider`,
    `new_sep_detail_service_provider`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员评价排名--表格
 */
const new_servicer_appraise_rankingDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_servicer_appraise_ranking`,
    `new_servicer_appraise_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者健康指标超标人数--饼图
 */
const new_eld_health_exceeding_standard_numDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_eld_health_exceeding_standard_num`,
    `new_eld_health_exceeding_standard_num`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 地图--地图
 */
const new_service_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_service_map`,
    `new_service_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 居家养老总数统计--泡泡
 */
const new_sum_statistical_bubbleDataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_bubble`,
    dataSourceID: new_sum_statistical_bubbleDataSource.id
});

/**
 * 居家养老总数统计--星级饼图--饼图
 */
const new_sum_statistical_chartDataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_chart`,
    dataSourceID: new_sum_statistical_chartDataSource.id
});

/**
 * 服务商排名--评分--表格
 */
const new_service_ranking_scoreDataView = createObject(ReportDataView, {
    id: `data_view_new_service_ranking_score`,
    dataSourceID: new_service_ranking_scoreDataSource.id
});

/**
 * 服务商排名--服务次数--表格
 */
const new_service_ranking_timeDataView = createObject(ReportDataView, {
    id: `data_view_new_service_ranking_time`,
    dataSourceID: new_service_ranking_timeDataSource.id
});

/**
 * 热门服务排名--表格
 */
const new_hot_service_rankingDataView = createObject(ReportDataView, {
    id: `data_view_new_hot_service_ranking`,
    dataSourceID: new_hot_service_rankingDataSource.id
});

/**
 * 服务人员年龄分布比例--饼图
 */
const new_com_detail_service_products_listDataView = createObject(ReportDataView, {
    id: `data_view_new_com_detail_service_products_list`,
    dataSourceID: new_com_detail_service_products_listDataSource.id
});

/**
 * 图片轮播--图片轮播
 */
const new_sep_detail_picture_listDataView = createObject(ReportDataView, {
    id: `data_view_new_sep_detail_picture_list`,
    dataSourceID: new_sep_detail_picture_listDataSource.id
});

/**
 * 服务人员服务量排名--表格
 */
const new_servicer_service_num_rankingDataView = createObject(ReportDataView, {
    id: `data_view_new_servicer_service_num_ranking`,
    dataSourceID: new_servicer_service_num_rankingDataSource.id
});

/**
 * 介绍--自定义组件
 */
const new_sep_detail_service_providerDataView = createObject(ReportDataView, {
    id: `data_view_new_sep_detail_service_provider`,
    dataSourceID: new_sep_detail_service_providerDataSource.id
});

/**
 * 服务人员评价排名--表格
 */
const new_servicer_appraise_rankingDataView = createObject(ReportDataView, {
    id: `data_view_new_servicer_appraise_rankingDataSource`,
    dataSourceID: new_servicer_appraise_rankingDataSource.id
});

/**
 * 长者健康指标超标人数--饼图
 */
const new_eld_health_exceeding_standard_numDataView = createObject(ReportDataView, {
    id: `data_view_new_eld_health_exceeding_standard_numDataSource`,
    dataSourceID: new_eld_health_exceeding_standard_numDataSource.id
});

/**
 * 地图--地图
 */
const new_service_mapDataView = createObject(ReportDataView, {
    id: `data_view_new_service_mapDataSource`,
    dataSourceID: new_service_mapDataSource.id
});

/**
 * 居家养老总数统计--星级饼图--饼图
 */
let new_sum_statistical_chartOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 服务人员年龄分布比例--星级饼图--饼图
 */
let new_com_detail_service_products_listOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 长者健康指标超标人数--星级饼图--饼图
 */
let new_eld_health_exceeding_standard_numOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

// 地图--地图
let option: EChartsOptions = {
    legend: {
        selected: {
            "养老机构": false,
            "社区幸福院": false,
            "居家服务商": true,
            "服务人员_服务中": false,
            "服务人员_空闲": false,
            "长者_住址": false,
            "长者_设备": false
        },
        data: ["养老机构", "社区幸福院", "居家服务商", "服务人员_服务中", "服务人员_空闲", "长者_住址", "长者_设备"]
    },
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};

/**
 * 居家养老总数统计--泡泡
 */
const new_sum_statistical_bubbleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_bubble_pie',
        titleable: true,
        title: '居家养老总数统计',
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [new_sum_statistical_bubbleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 居家养老总数统计--星级饼图--饼图
 */
const org_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_chart_pie',
        titleable: false,
        // title: '居家养老总数统计',
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        dataViews: [new_sum_statistical_chartDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_sum_statistical_chartdisplayer',
            new_sum_statistical_chartOption
        )
    }
);

/**
 * 服务商排名--评分--表格
 */
const new_service_ranking_scoreDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_service_ranking_score-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '服务商排名',
        dataViews: [new_service_ranking_scoreDataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务商排名',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '评分排名',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 服务商排名--服务次数--表格
 */
const new_service_ranking_timeDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_service_ranking_time-table',
        titleable: true,
        title: '服务商排名',
        containerType: ContainerType.frameContain,
        dataViews: [new_service_ranking_timeDataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务商名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '服务次数',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 热门服务排名--表格
 */
const new_hot_service_rankingModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_hot_service_ranking_pie',
        titleable: true,
        title: '热门服务排名',
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [new_hot_service_rankingDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '购买次数',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 服务人员年龄分布比例--饼图
 */
const new_com_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_detail_service_products_list_pie',
        titleable: true,
        title: '服务人员年龄分布比例',
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 0,
        dataViews: [new_com_detail_service_products_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_com_detail_service_products_listdisplayer',
            new_com_detail_service_products_listOption
        )
    }
);

/**
 * 图片轮播--图片轮播
 */
const new_sep_detail_picture_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sep_detail_picture_list-carousel',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [new_sep_detail_picture_listDataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

/**
 * 服务人员服务量排名--表格
 */
const new_servicer_service_num_rankingDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_servicer_service_num_ranking-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '服务人员服务量排名',
        dataViews: [new_servicer_service_num_rankingDataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '人员姓名',
                        dataIndex: 'y1',
                        width: "12em"
                    },
                    {
                        title: '所属机构',
                        dataIndex: 'y2',
                        width: '20em'
                    },
                    {
                        title: '服务人次',
                        dataIndex: 'y3',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 介绍--介绍组件
 */
const new_sep_detail_service_providerDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_welfare_centre',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [new_sep_detail_service_providerDataView],
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['名称', '地址', '评级情况', '机构类型', '机构性质', '法定代表人', '联系电话'],
                max_height: 900
            }
        )
    }
);

/**
 * 服务人员评价排名--表格
 */
const new_servicer_appraise_rankingDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_servicer_appraise_ranking-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '服务人员评价排名',
        dataViews: [new_servicer_appraise_rankingDataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '人员姓名',
                        dataIndex: 'y1',
                        width: '12em'
                    },
                    {
                        title: '所属机构',
                        dataIndex: 'y2',
                        width: '20em'
                    },
                    {
                        title: '评分排名',
                        dataIndex: 'y3',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 长者健康指标超标人数--饼图
 */
const new_eld_health_exceeding_standard_numModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_eld_health_exceeding_standard_num_pie',
        titleable: true,
        title: '长者健康指标超标人数',
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        dataViews: [new_eld_health_exceeding_standard_numDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_eld_health_exceeding_standard_numdisplayer',
            new_eld_health_exceeding_standard_numOption
        )
    }
);

/**
 * 地图-地图
 */
const new_service_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_service_map_community',
        // title: '地图',
        titleable: false,
        dataViews: [new_service_mapDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'longitude',
                latitudeFieldName: 'latitude',
                callLatitudeFieldName: "y",
                callLongitudeFieldName: "x",
                nameFieldName: "name",
                ponitIconSizeFieldName: "num",
                getElementFn: getCallCenterEle,
                scatterObject: iconMap
            },
            "new_service_map_community_displayer",
            option
        )
    }
);

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                autoQuery: 1,
                layout: layoutSelect,
            }
        )
    }
);

/** 图例配置 */
const legendOption = [
    {
        title: "养老机构",
        key: ["养老机构"],
        icon: noBeadhouse,
        selected: false
    },
    {
        title: "社区幸福院",
        key: ["社区幸福院"],
        icon: noBlessed,
        selected: false
    },
    {
        title: "居家服务商",
        key: ["居家服务商"],
        icon: noService,
        selected: true
    },
    {
        title: "服务人员_服务中",
        key: ["服务人员_服务中"],
        icon: noServePropleServing,
        selected: false
    },
    {
        title: "服务人员_空闲",
        key: ["服务人员_空闲"],
        icon: noServePropleFree,
        selected: false
    },
    {
        title: "长者_住址",
        key: ["长者_住址"],
        icon: noElderAddr,
        selected: false
    },
    {
        title: "长者_设备",
        key: ["长者_设备"],
        icon: noElderDevice,
        selected: false
    }
];

/** 
 * 图例控制器
 */
const legendControl = createObject(
    LegendMenuControl,
    {
        display_control: new_service_mapModuleControl.displayer,
        option: legendOption
    }
);

const layoutLeft = createObject(HomeLayoutLeftControl);
const layoutMiddle = createObject(
    CommonLayoutMiddleControl,
    {
        legend_control: legendControl
    }
);
const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_sum_statistical_chartDataSource,
                    new_service_ranking_scoreDataSource,
                    new_service_ranking_timeDataSource,
                    new_hot_service_rankingDataSource,
                    new_sum_statistical_bubbleDataSource
                ],
                modules: [
                    // 0
                    new_sum_statistical_bubbleModuleControl,
                    // 1
                    org_detail_service_products_listModuleControl,
                    // 2
                    new_service_ranking_scoreDisplayerModule,
                    // 3
                    new_service_ranking_timeDisplayerModule,
                    // 4
                    new_hot_service_rankingModuleControl
                    // 5
                ],
                layout: layoutLeft,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "服务商" },
                autoQuery: 1
            }
        )
    });

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_service_mapDataSource
                ],
                modules: [
                    new_service_mapModuleControl
                ],
                layout: layoutMiddle,
                autoQuery: 5000,
            }
        )
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_com_detail_service_products_listDataSource,
                    new_sep_detail_picture_listDataSource,
                    new_servicer_service_num_rankingDataSource,
                    new_sep_detail_service_providerDataSource,
                    new_servicer_appraise_rankingDataSource,
                    new_eld_health_exceeding_standard_numDataSource
                ],
                modules: [
                    // 0
                    new_com_detail_service_products_listModuleControl,
                    // 1
                    new_sep_detail_picture_listModuleControl,
                    // 2
                    new_servicer_service_num_rankingDisplayerModule,
                    // 3
                    new_sep_detail_service_providerDisplayerModule,
                    // 4
                    new_servicer_appraise_rankingDisplayerModule,
                    // 5
                    new_eld_health_exceeding_standard_numModuleControl
                ],
                layout: layoutRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "服务商" },
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(CommonLayoutControl);
const irServiceAnalysisControl = createObject(
    SmartReportControl,
    {
        id: "irServiceAnalysis",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightControl],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        isMain: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irServiceAnalysisControl };