import { createObject, IDataService } from "pao-aop";
import { AjaxJsonRpcFactory } from "pao-aop-client";
import { AjaxRemoteDataQueryer, DataDisplayerModuleControl, ReportDataView, SmartReportControl } from "src/business/report/smart";
import { createLiquidfillDataDisplayer } from "src/business/report/template/liquidfill";
import { ContainerType } from "src/business/report/widget";
import { CommonLayoutMiddleControl } from "../components/layout/common-layout/middle";
import { remote } from "../remote";

const remoteService_Fac = new AjaxJsonRpcFactory(IDataService, remote.url, 'IBigScreenService');

const dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data-source-new_activity_statistical_welfare_single`,
        command: `new_activity_statistical_welfare_single`,
        remoteService_Fac
    });

const reportView = createObject(ReportDataView, {
    id: `data-source-new_activity_statistical_welfare_single`,
    dataSourceID: dataSource.id
});

const liquidfillModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_personal_list',
        titleable: true,
        title: '服务人员列表',
        dataViews: [reportView],
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'y',
                nameFieldName: 'x'
            },
            "liquidfill",
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value;
                    },
                    fontSize: 28
                }
            } as any)
    }
);

const report = createObject(
    SmartReportControl,
    {
        dataSources: [dataSource],
        modules: [liquidfillModule],
        layout: createObject(CommonLayoutMiddleControl),
        autoQuery: 1
    }
);

export { report };
