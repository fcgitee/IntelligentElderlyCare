import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject, newGuid } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, ReportDataView, AjaxRemoteDataQueryer } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { } from "../echartCommonVar";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { VideoControl } from "src/business/components/buss-components/video";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";

/**
 * 服务人员--服务人员总数--数据源--数值
 */
const spe_statistics_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_total_quantity`,
    `spe_statistics_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员简介--自定义控件
 */
const spe_detail_cardDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_card`,
    `spe_detail_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员--当前任务卡片--自定义控件
 */
const spe_detail_task_cardDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_task_card`,
    `spe_detail_task_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员--服务总次数--数据源--数值
 */
const spe_detail_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_total_quantity`,
    `spe_detail_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--幸福院位置--数据源--星空图
 */
const spe_map_locationDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_map_location`,
    `spe_map_location`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务人员地图--数据源--明细热力图
 */
const spe_detail_service_personal_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_service_personal_map`,
    `spe_detail_service_personal_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员位置--数据视图--明细星空图
 */
const spe_detail_task_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_task_map`,
    `spe_detail_task_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员介绍--数据源--图片轮播
 */
const spe_detail_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_picture_list`,
    `spe_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--活动介绍--数据源--图片轮播
 */
const spe_detail_activity_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_activity_picture_list`,
    `spe_detail_activity_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员总数--数据视图--数值
 */
const spe_statistics_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_total_quantity`,
    dataSourceID: spe_statistics_total_quantityDataSource.id
});

/**
 * 服务人员--服务人员简介--自定义控件
 */
const spe_detail_cardDataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_card`,
    dataSourceID: spe_detail_cardDataSource.id
});

/**
 * 服务人员--当前任务卡片--自定义控件
 */
const spe_detail_task_cardDataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_task_card`,
    dataSourceID: spe_detail_task_cardDataSource.id
});

/**
 * 服务人员--服务总次数--数据视图--数值
 */
const spe_detail_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_total_quantity`,
    dataSourceID: spe_detail_total_quantityDataSource.id
});
/**
 * 服务人员--服务人员学历分布--数据源--饼图
 */
const spe_statistics_education_service_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_education_service_personal_quantity`,
    `spe_statistics_education_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员性别比例--数据源--饼图
 */
const spe_statistics_sex_service_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_sex_service_personal_quantity`,
    `spe_statistics_sex_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员来源地分布--数据源--饼图
 */
const spe_statistics_source_service_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_source_service_personal_quantity`,
    `spe_statistics_source_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务人员评价学历分布--数据源--饼图
 */
const spe_statistics_education_evaluate_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_education_evaluate_quantity`,
    `spe_statistics_education_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务人员评价性别分布--数据源--饼图
 */
const spe_statistics_sex_evaluate_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_sex_evaluate_quantity`,
    `spe_statistics_sex_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务人员次数学历分布--数据源--饼图
 */
const spe_statistics_education_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_education_quantity`,
    `spe_statistics_education_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务人员服务评价性别比例--数据源--饼图
 */
const spe_statistics_sex_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_sex_quantity`,
    `spe_statistics_sex_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务人员服务收入学历分布--数据源--饼图
 */
const spe_statistics_education_income_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_education_income_quantity`,
    `spe_statistics_education_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--服务现场视频--摄像头视频
 */
const spe_detail_live_video_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_live_video`,
    `spe_detail_live_video`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员--服务人员服务收入性别比例--数据源--饼图
 */
const spe_statistics_sex_income_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_sex_income_quantity`,
    `spe_statistics_sex_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员数量走势--数据源--折线图
 */
const spe_statistics_time_service_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_statistics_time_service_personal_quantity`,
    `spe_statistics_time_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 服务人员--幸福院位置--数据视图--星空图
 */
const spe_map_locationDataView = createObject(ReportDataView, {
    id: `data_view_spe_map_location`,
    dataSourceID: spe_map_locationDataSource.id
});
/**
 * 服务人员--服务人员地图--数据源--明细热力图
 */
const spe_detail_service_personal_mapDataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_service_personal_map`,
    dataSourceID: spe_detail_service_personal_mapDataSource.id
});
/**
 * 服务人员--服务人员位置--数据视图--明细星空图
 */
const spe_detail_task_mapDataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_task_map`,
    dataSourceID: spe_detail_task_mapDataSource.id
});
/**
 * 服务人员--服务人员总数--数值
 */
const spe_statistics_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_total_quantity-num',
        title: '服务人员总数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [spe_statistics_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['服务人员总数', '福利院服务人员总数', '社区服务人员总数'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 服务人员--服务人员简介--自定义控件
 */
const spe_detail_cardModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_card-num',
        title: '服务人员简介',
        titleable: false,
        dataViews: [spe_detail_cardDataView],
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4'],
                titleName: ['照片', '评级', '资质信息', '所在机构']
            }
        )
    }
);

/**
 * 服务人员--当前任务卡片--自定义控件
 */
const spe_detail_task_cardModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_task_card-num',
        title: '当前任务卡片',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [spe_detail_task_cardDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['任务编号', '服务产品', '服务地点', '服务时间', '服务商', '服务对象', '服务人员']
            }
        )
    }
);

/**
 * 服务人员--服务总次数--数值
 */
const spe_detail_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_total_quantity-num',
        title: '服务总次数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 0,
        dataViews: [spe_detail_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['总服务次数', '总服务人次', '总服务收入'],
                value_field: 'y',
            }
        )
    }
);
/**
 * 服务人员--服务人员介绍--数据视图--图片轮播
 */
const spe_detail_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-spe_detail_picture_list-carousel`,
    dataSourceID: spe_detail_picture_listCarouselDataSource.id
});
/**
 * 服务人员--活动介绍--数据视图--图片轮播
 */
const spe_detail_activity_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-spe_detail_activity_picture_list-carousel`,
    dataSourceID: spe_detail_activity_picture_listCarouselDataSource.id
});
/**
 * 服务人员--服务人员学历分布--数据视图--饼图
 */
const spe_statistics_education_service_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_education_service_personal_quantity`,
    dataSourceID: spe_statistics_education_service_personal_quantity_DataSource.id
});
/**
 * 服务人员--服务人员性别比例--数据视图--饼图
 */
const spe_statistics_sex_service_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_sex_service_personal_quantity`,
    dataSourceID: spe_statistics_sex_service_personal_quantity_DataSource.id
});
/**
 * 服务人员--服务人员来源地分布--数据视图--饼图
 */
const spe_statistics_source_service_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_source_service_personal_quantity`,
    dataSourceID: spe_statistics_source_service_personal_quantity_DataSource.id
});
/**
 * 服务人员--服务人员评价学历分布--数据视图--饼图
 */
const spe_statistics_education_evaluate_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_education_evaluate_quantity`,
    dataSourceID: spe_statistics_education_evaluate_quantity_DataSource.id
});
/**
 * 服务人员--服务人员评价性别分布--数据视图--饼图
 */
const spe_statistics_sex_evaluate_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_sex_evaluate_quantity`,
    dataSourceID: spe_statistics_sex_evaluate_quantity_DataSource.id
});
/**
 * 服务人员--服务人员次数学历分布--数据视图--饼图
 */
const spe_statistics_education_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_education_quantity`,
    dataSourceID: spe_statistics_education_quantity_DataSource.id
});
/**
 * 服务人员--服务人员服务评价性别比例--数据视图--饼图
 */
const spe_statistics_sex_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_sex_quantity`,
    dataSourceID: spe_statistics_sex_quantity_DataSource.id
});
/**
 * 服务人员--服务人员服务收入学历分布--数据视图--饼图
 */
const spe_statistics_education_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_education_income_quantity`,
    dataSourceID: spe_statistics_education_income_quantity_DataSource.id
});

/**
 * 服务人员--服务现场视频--摄像头视频
 */
const spe_detail_live_video_DataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_live_video`,
    dataSourceID: spe_detail_live_video_DataSource.id
});

/**
 * 服务人员--服务人员服务收入性别比例--数据视图--饼图
 */
const spe_statistics_sex_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_sex_income_quantity`,
    dataSourceID: spe_statistics_sex_income_quantity_DataSource.id
});
/**
 * 服务人员--服务人员数量走势--数据视图--折线图
 */
const spe_statistics_time_service_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_time_service_personal_quantity`,
    dataSourceID: spe_statistics_time_service_personal_quantity_DataSource.id
});
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};
/**
 * 服务人员--服务人员学历分布--配置--饼图
 */
let spe_statistics_education_service_personal_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员性别比例--配置--饼图
 */
let spe_statistics_sex_service_personal_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员来源地分布--配置--饼图
 */
let spe_statistics_source_service_personal_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员评价学历分布--配置--饼图
 */
let spe_statistics_education_evaluate_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员评价性别分布--配置--饼图
 */
let spe_statistics_sex_evaluate_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员次数学历分布--饼图
 */
let spe_statistics_education_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员服务评价性别比例--配置--饼图
 */
let spe_statistics_sex_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员服务收入学历分布--配置--饼图
 */
let spe_statistics_education_income_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员服务收入性别比例--配置--饼图
 */
let spe_statistics_sex_income_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 服务人员--服务人员数量走势--配置--折线图
 */
let spe_statistics_time_service_personal_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '服务人员数量走势',
    },
};
/**
 * 服务人员--服务人员地图--数据源--明细热力图
 */
const spe_detail_service_personal_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_service_personal_map',
        title: '服务人员位置',
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        titleable: false,
        dataViews: [spe_detail_service_personal_mapDataView],
        containerType: ContainerType.frameContain,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
            },
            "spe_detail_service_personal_map_modules",
            option
        )
    }
);
/**
 * 服务人员--幸福院位置--数据源--星空图
 */
const spe_detail_task_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_task_map_control',
        title: '幸福院位置',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [spe_detail_task_mapDataView],
        containerType: ContainerType.frameContain,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "spe_detail_task_map_modules",
            option
        )
    }
);

/**
 * 服务人员--幸福院位置--数据源--星空图
 */
const spe_map_locationModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_map_location',
        title: '幸福院位置',
        titleable: false,
        dataViews: [spe_map_locationDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "spe_map_location_modules",
            option
        )
    }
);
/**
 * 服务人员--服务人员介绍--图片轮播
 */
const spe_detail_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_picture_list-carousel',
        title: '服务人员介绍',
        titleable: false,
        containerType: ContainerType.frameContain,
        dataViews: [spe_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务人员--服务人员介绍--图片轮播-弹窗
 */
const spe_detail_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_picture_list-carousel-dialog',
        title: '服务人员介绍',
        titleable: false,
        isDialog: true,
        parentContainerID: spe_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [spe_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务人员--服务人员地图--数据源--明细热力图-弹窗
 */
const spe_detail_service_personal_mapDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_service_personal_map-dialog',
        title: '服务人员地图',
        titleable: false,
        dataViews: [spe_detail_service_personal_mapDataView],
        isDialog: true,
        parentContainerID: spe_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "spe_detail_service_personal_map_modules-dialog",
            option
        )
    }
);
/**
 * 服务人员--服务人员位置--数据视图--明细星空图-弹窗
 */
const spe_detail_task_mapDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_task_map_control-dialog',
        title: '服务人员位置',
        titleable: false,
        dataViews: [spe_detail_task_mapDataView],
        isDialog: true,
        parentContainerID: spe_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "spe_detail_task_map_modules",
            option
        )
    }
);
/**
 * 服务人员--服务人员学历分布--饼图
 */
const spe_statistics_education_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_education_service_personal_quantity_pie',
        title: '服务人员学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [spe_statistics_education_service_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_education_service_personal_quantity_pie_displayer',
            spe_statistics_education_service_personal_quantity_pieOption
        )
    }
);
/**
 * 服务人员--活动介绍--图片轮播
 */
const spe_detail_activity_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_activity_picture_list-carousel',
        title: '活动介绍',
        titleable: false,
        containerType: ContainerType.frameContain,
        dataViews: [spe_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 服务人员--服务人员性别比例--饼图
 */
const spe_statistics_sex_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_sex_service_personal_quantity_pie',
        title: '服务人员性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [spe_statistics_sex_service_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_sex_service_personal_quantity_pie_displayer',
            spe_statistics_sex_service_personal_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员来源地分布--饼图
 */
const spe_statistics_source_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_source_service_personal_quantity_pie',
        title: '服务人员来源地分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [spe_statistics_source_service_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_source_service_personal_quantity_pie_displayer',
            spe_statistics_source_service_personal_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员评价学历分布--饼图
 */
const spe_statistics_education_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_education_evaluate_quantity_pie',
        title: '服务人员评价学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [spe_statistics_education_evaluate_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_education_evaluate_quantity_pie_displayer',
            spe_statistics_education_evaluate_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员评价性别分布--饼图
 */
const spe_statistics_sex_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_sex_evaluate_quantity_pie',
        title: '服务人员评价性别分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [spe_statistics_sex_evaluate_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_sex_evaluate_quantity_pie_displayer',
            spe_statistics_sex_evaluate_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员次数学历分布--饼图
 */
const spe_statistics_education_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_education_quantity_pie',
        title: '服务人员次数学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [spe_statistics_education_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_education_quantity_pie_displayer',
            spe_statistics_education_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员服务评价性别比例--饼图
 */
const spe_statistics_sex_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_sex_quantity_pie',
        title: '服务人员服务评价性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [spe_statistics_sex_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_sex_quantity_pie_displayer',
            spe_statistics_sex_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员服务收入学历分布--饼图
 */
const spe_statistics_education_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_education_income_quantity_pie',
        title: '服务人员服务收入学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [spe_statistics_education_income_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_education_income_quantity_pie_displayer',
            spe_statistics_education_income_quantity_pieOption
        )
    }
);

/** 直播模块id */
const liveId = newGuid();

/**
 * 服务人员--现场服务视频--摄像头视频
 */
const spe_detail_live_videoModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: liveId,
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [spe_detail_live_video_DataView],
        containerType: ContainerType.nhContain,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://localhost:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 600,
                height: 600
            }
        )
    }
);

/**
 * 服务人员--服务人员服务收入性别比例--饼图
 */
const spe_statistics_sex_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_sex_income_quantity_pie',
        title: '服务人员服务收入性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [spe_statistics_sex_income_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'spe_statistics_sex_income_quantity_pie_displayer',
            spe_statistics_sex_income_quantity_pieOption
        )
    }
);
/**
 * 服务人员--服务人员数量走势--折线图
 */
const spe_statistics_time_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_time_service_personal_quantity_multi_line',
        title: '服务人员数量走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [spe_statistics_time_service_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_time_service_personal_quantity_multi_line_displayer',
            spe_statistics_time_service_personal_quantity_multilineOption
        )
    }
);

/**
 * 服务人员--社区服务人员总数--数据源--柱状图
 */
const spe_statistics_community_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_community_quantity`,
    `spe_statistics_community_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--社区服务人员总数--数据视图--柱状图
 */
const spe_statistics_community_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_community_quantity`,
    dataSourceID: spe_statistics_community_quantity_dataSource.id
});

/**
 * 服务人员--社区服务人员总数--配置--柱状图
 */
let spe_statistics_community_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--社区服务人员总数--柱状图
 */
const spe_statistics_community_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_community_quantity_displayer',
        title: '社区服务人员总数',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 0,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_community_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_community_quantity_bar_displayer',
            spe_statistics_community_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员年龄段分布--数据源--柱状图
 */
const spe_statistics_age_service_personal_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_age_service_personal_quantity`,
    `spe_statistics_age_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员年龄段分布--数据视图--柱状图
 */
const spe_statistics_age_service_personal_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_age_service_personal_quantity`,
    dataSourceID: spe_statistics_age_service_personal_quantity_dataSource.id
});

/**
 * 服务人员--服务人员年龄段分布--配置--柱状图
 */
let spe_statistics_age_service_personal_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员年龄段分布--柱状图
 */
const spe_statistics_age_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_age_service_personal_quantity_displayer',
        title: '服务人员年龄段分布',
        titleable: true,
        group: 'left-carousel6',
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_age_service_personal_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_age_service_personal_quantity_bar_displayer',
            spe_statistics_age_service_personal_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员证件类型数量分布--数据源--柱状图
 */
const spe_statistics_type_service_personal_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_type_service_personal_quantity`,
    `spe_statistics_type_service_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员证件类型数量分布--数据视图--柱状图
 */
const spe_statistics_type_service_personal_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_type_service_personal_quantity`,
    dataSourceID: spe_statistics_type_service_personal_quantity_dataSource.id
});

/**
 * 服务人员--服务人员证件类型数量分布--配置--柱状图
 */
let spe_statistics_type_service_personal_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '证件类型'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员证件类型数量分布--柱状图
 */
const spe_statistics_type_service_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_type_service_personal_quantity_displayer',
        title: '服务人员证件类型数量分布',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_type_service_personal_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_type_service_personal_quantity_bar_displayer',
            spe_statistics_type_service_personal_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员满意度（TOP10）--数据源--柱状图
 */
const spe_statistics_service_personal_evaluate_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_service_personal_evaluate_quantity`,
    `spe_statistics_service_personal_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员满意度（TOP10）--数据视图--柱状图
 */
const spe_statistics_service_personal_evaluate_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_service_personal_evaluate_quantity`,
    dataSourceID: spe_statistics_service_personal_evaluate_quantity_dataSource.id
});

/**
 * 服务人员--服务人员满意度（TOP10）--配置--柱状图
 */
let spe_statistics_service_personal_evaluate_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员满意度（TOP10）--柱状图
 */
const spe_statistics_service_personal_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_service_personal_evaluate_quantity_displayer',
        title: '服务人员满意度（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_service_personal_evaluate_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_service_personal_evaluate_quantity_bar_displayer',
            spe_statistics_service_personal_evaluate_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员评价年龄段分布--数据源--柱状图
 */
const spe_statistics_age_evaluate_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_age_evaluate_quantity`,
    `spe_statistics_age_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员评价年龄段分布--数据视图--柱状图
 */
const spe_statistics_age_evaluate_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_age_evaluate_quantity`,
    dataSourceID: spe_statistics_age_evaluate_quantity_dataSource.id
});

/**
 * 服务人员--服务人员评价年龄段分布--配置--柱状图
 */
let spe_statistics_age_evaluate_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员评价年龄段分布--柱状图
 */
const spe_statistics_age_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_spe_statistics_age_evaluate_quantity',
        title: '服务人员评价年龄段分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_age_evaluate_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_age_evaluate_quantity_bar_displayer',
            spe_statistics_age_evaluate_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员服务次数排名（TOP10）--数据源--柱状图
 */
const spe_statistics_service_personal_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_service_personal_top10_quantity`,
    `spe_statistics_service_personal_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员服务次数排名（TOP10）--数据视图--柱状图
 */
const spe_statistics_service_personal_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_service_personal_top10_quantity`,
    dataSourceID: spe_statistics_service_personal_top10_quantity_dataSource.id
});

/**
 * 服务人员--服务人员服务次数排名（TOP10）--配置--柱状图
 */
let spe_statistics_service_personal_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员服务次数排名--柱状图
 */
const spe_statistics_service_personal_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_service_personal_top10_quantity_displayer',
        title: '服务人员服务次数排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_service_personal_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_service_personal_top10_quantity_bar_displayer',
            spe_statistics_service_personal_top10_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员勤奋榜（TOP10）--数据源--柱状图
 */
const spe_statistics_age_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_age_quantity`,
    `spe_statistics_age_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员勤奋榜（TOP10）--数据视图--柱状图
 */
const spe_statistics_age_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_age_quantity`,
    dataSourceID: spe_statistics_age_quantity_dataSource.id
});

/**
 * 服务人员--服务人员勤奋榜（TOP10）--配置--柱状图
 */
let spe_statistics_age_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员勤奋榜（TOP10）--柱状图
 */
const spe_statistics_age_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_age_quantity_displayer',
        title: '服务人员勤奋榜（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_age_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_age_quantity_bar_displayer',
            spe_statistics_age_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员收入（TOP10）--数据源--柱状图
 */
const spe_statistics_service_personal_income_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_service_personal_income_top10_quantity`,
    `spe_statistics_service_personal_income_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员收入（TOP10）--数据视图--柱状图
 */
const spe_statistics_service_personal_income_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_service_personal_income_top10_quantity`,
    dataSourceID: spe_statistics_service_personal_income_top10_quantity_dataSource.id
});

/**
 * 服务人员--服务人员收入（TOP10）--配置--柱状图
 */
let spe_statistics_service_personal_income_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务收入（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员收入（TOP10）--柱状图
 */
const spe_statistics_service_personal_income_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_service_personal_income_top10_quantity_displayer',
        title: '服务人员收入（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_service_personal_income_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_service_personal_income_top10_quantity_bar_displayer',
            spe_statistics_service_personal_income_top10_quantity_barOption
        )
    }
);

/**
 * 服务人员--服务人员收入（TOP10）年龄分布--数据源--柱状图
 */
const spe_statistics_age_income_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_spe_statistics_age_income_quantity`,
    `spe_statistics_age_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 服务人员--服务人员收入（TOP10）年龄分布--数据视图--柱状图
 */
const spe_statistics_age_income_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_spe_statistics_age_income_quantity`,
    dataSourceID: spe_statistics_age_income_quantity_dataSource.id
});

/**
 * 服务人员--服务人员收入（TOP10）年龄分布--配置--柱状图
 */
let spe_statistics_age_income_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务收入（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 服务人员--服务人员收入（TOP10）年龄分布--柱状图
 */
const spe_statistics_age_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_statistics_age_income_quantity_displayer',
        title: '服务人员收入（TOP10）年龄分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [spe_statistics_age_income_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'spe_statistics_age_income_quantity_bar_displayer',
            spe_statistics_age_income_quantity_barOption
        )
    }
);
/** 
 * 服务人员--服务记录--表格
 */
const spe_detail_service_record_top10_list_DataSource = new AjaxRemoteDataQueryer(
    `data_source_spe_detail_service_record_top10_list`,
    `spe_detail_service_record_top10_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 服务人员--服务记录--表格
 */
const spe_detail_service_record_top10_list_DataView = createObject(ReportDataView, {
    id: `data_view_spe_detail_service_record_top10_list`,
    dataSourceID: spe_detail_service_record_top10_list_DataSource.id
});
/**
 * 服务人员--服务记录--表格
 */
const spe_detail_service_record_top10_listDisplayerModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'spe_detail_service_record_top10_list-table',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        title: '服务记录',
        dataViews: [spe_detail_service_record_top10_list_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务时间',
                        dataIndex: 'y1',
                        align: 'center'
                    },
                    {
                        title: '服务产品',
                        dataIndex: 'y2',
                        align: 'center'
                    },
                    {
                        title: '服务评价',
                        dataIndex: 'y3',
                        align: 'center'
                    }
                ],
                maxHeigh: 200
            }
        )
    }
);
const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(
    CommonLayoutMiddleControl
);

const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    spe_statistics_total_quantityDataSource,
                    spe_statistics_community_quantity_dataSource,
                    spe_statistics_age_service_personal_quantity_dataSource,
                    spe_statistics_type_service_personal_quantity_dataSource,
                    spe_statistics_service_personal_evaluate_quantity_dataSource,
                    spe_statistics_age_evaluate_quantity_dataSource,
                    spe_statistics_education_evaluate_quantity_DataSource,
                    spe_statistics_sex_evaluate_quantity_DataSource,
                    spe_statistics_service_personal_top10_quantity_dataSource,
                    spe_statistics_education_quantity_DataSource,
                    spe_statistics_age_quantity_dataSource,
                    spe_statistics_sex_quantity_DataSource,
                    spe_statistics_service_personal_income_top10_quantity_dataSource,
                    spe_statistics_education_income_quantity_DataSource,
                    spe_statistics_age_income_quantity_dataSource,
                    spe_statistics_sex_income_quantity_DataSource,
                    spe_statistics_time_service_personal_quantity_DataSource,
                ],
                modules: [
                    // 0
                    spe_statistics_community_quantity_displayModuleControl,
                    // 1
                    spe_statistics_total_quantityModuleControl,
                    // 2
                    spe_statistics_type_service_personal_quantity_displayModuleControl,
                    // 3
                    spe_statistics_education_evaluate_quantity_displayModuleControl,
                    spe_statistics_service_personal_evaluate_quantity_displayModuleControl,
                    spe_statistics_sex_evaluate_quantity_displayModuleControl,
                    spe_statistics_age_evaluate_quantity_displayModuleControl,
                    // 4
                    spe_statistics_education_quantity_displayModuleControl,
                    spe_statistics_service_personal_top10_quantity_displayModuleControl,
                    spe_statistics_age_quantity_displayModuleControl,
                    spe_statistics_sex_quantity_displayModuleControl,
                    // 5
                    spe_statistics_service_personal_income_top10_quantity_displayModuleControl,
                    spe_statistics_education_income_quantity_displayModuleControl,
                    spe_statistics_age_income_quantity_displayModuleControl,
                    spe_statistics_sex_income_quantity_displayModuleControl,
                    spe_statistics_time_service_personal_quantity_displayModuleControl,
                    spe_statistics_age_service_personal_quantity_displayModuleControl,
                ],
                layout: layoutLeft,
                autoQuery: 1,
            })
    }
);
const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportControl,
    {
        dataSources: [
        ],
        modules: [
        ],
        layout: layoutSelect,
        autoQuery: 1,
    }
);
const middleControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            spe_map_locationDataSource
        ],
        modules: [
            spe_map_locationModuleControl,
        ],
        layout: layoutMiddle,
    }
);
const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    spe_detail_total_quantityDataSource,
                    spe_detail_picture_listCarouselDataSource,
                    spe_detail_activity_picture_listCarouselDataSource,
                    spe_detail_cardDataSource,
                    spe_detail_task_cardDataSource,
                    spe_detail_service_personal_mapDataSource,
                    spe_detail_task_mapDataSource,
                    spe_detail_live_video_DataSource,
                    spe_detail_service_record_top10_list_DataSource,
                    spe_statistics_education_service_personal_quantity_DataSource,
                    spe_statistics_sex_service_personal_quantity_DataSource,
                    spe_statistics_source_service_personal_quantity_DataSource,
                ],
                modules: [
                    // 0
                    spe_statistics_community_quantity_displayModuleControl,
                    spe_detail_total_quantityModuleControl,
                    // 1
                    spe_detail_picture_list_DisplayerModule,
                    // 2
                    spe_detail_activity_picture_list_DisplayerModule,
                    // 3
                    spe_detail_live_videoModuleControl,
                    spe_detail_service_record_top10_listDisplayerModuleControl,
                    spe_statistics_education_service_personal_quantity_displayModuleControl,
                    spe_statistics_sex_service_personal_quantity_displayModuleControl,
                    spe_statistics_source_service_personal_quantity_displayModuleControl,
                    // 4
                    spe_detail_cardModuleControl,
                    spe_detail_task_cardModuleControl,
                    spe_detail_service_personal_mapModuleControl,
                    spe_detail_task_mapModuleControl,
                    spe_detail_picture_list_DialogDisplayerModule,
                    spe_detail_service_personal_mapDialogModuleControl,
                    spe_detail_task_mapDialogModuleControl,
                ],
                layout: layoutRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5" },
                autoQuery: 1,
            }
        )
    });
const middleReport = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportModuleControl,
            {
                containerType: ContainerType.blank,
                report: middleControl
            }
        )
    }
);
/**
 * react布局控制器
 */
const layoutAll = createObject(CommonLayoutControl);
const irServicePersonalControl = createObject(
    SmartReportControl,
    {
        id: 'irServicePersonal',
        modules: [selectControl, leftControl, middleReport, rightControl],
        dataSources: [],
        layout: layoutAll,
        // autoQuery: 1,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);
export { irServicePersonalControl };