import { createObject, IDataService } from "pao-aop";
import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, ReportDataView, AjaxRemoteDataQueryer } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { } from "../echartCommonVar";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { ListDisplayControl } from "src/business/report/displayers/nh-welfare/list-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { VideoControl } from "src/business/components/buss-components/video";

/**
 * 社区养老--幸福院总数--数据源--数值
 */
const com_statistics_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_total_quantity`,
    `com_statistics_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老-明细-人员风采
 */
const com_detail_personal_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_personal_list`,
    `com_detail_personal_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老-明细-最新活动情况
 */
const com_detail_activityDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_activity`,
    `com_detail_activity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区养老-服务产品列表
 */
const com_detail_service_products_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_service_products_list`,
    `com_detail_service_products_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老-星空图
 * 社区养老--幸福院位置--数据源--星空图
 */
const com_map_location_communityDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_map_location_community`,
    `com_map_location_community`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--幸福院环境--数据源--图片轮播
 */
const com_detail_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_picture_list`,
    `com_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老--服务价格表--数据源--图片轮播
 */
const com_detail_service_price_pictureCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_service_price_picture`,
    `com_detail_service_price_picture`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老--幸福院活动--数据源--图片轮播
 */
const com_detail_activity_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_activity_picture_list`,
    `com_detail_activity_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老--幸福院现场--视频监控（摄像头列表，点击放大）
 */
const com_detail_video_surveillance_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_detail_video_surveillance`,
    `com_detail_video_surveillance`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--幸福院总数--数据视图--数值
 */
const com_statistics_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_total_quantity`,
    dataSourceID: com_statistics_total_quantityDataSource.id
});
/**
 * 社区养老--幸福院类型比例--数据源--饼图
 */
const com_statistics_nature_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_nature_quantity`,
    `com_statistics_nature_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--活动次数性别比例--数据源--饼图
 */
const com_statistics_sex_activity_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_sex_activity_quantity`,
    `com_statistics_sex_activity_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--长者学历活动次数比例--数据源--饼图
 */
const com_statistics_education_activity_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_education_activity_quantity`,
    `com_statistics_education_activity_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--长者学历上课次数比例--数据源--饼图
 */
const com_statistics_education_attend_class_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_education_attend_class_quantity`,
    `com_statistics_education_attend_class_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--长者签到人数时间分布--数据源--折线图
 */
const com_statistics_time_sign_in_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_time_sign_in_quantity`,
    `com_statistics_time_sign_in_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--活动数量时间分布--数据源--折线图
 */
const com_statistics_time_activity_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_time_activity_quantity`,
    `com_statistics_time_activity_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--长者饭堂用餐人数分布--数据源--折线图
 */
const com_statistics_time_meals_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_time_meals_quantity`,
    `com_statistics_time_meals_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--长者教育人数分布--数据源--折线图
 */
const com_statistics_time_education_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_time_education_quantity`,
    `com_statistics_time_education_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--活动平均人数分布--数据源--折线图
 */
const com_statistics_time_activity_personal_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_com_statistics_time_activity_personal_quantity`,
    `com_statistics_time_activity_personal_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 社区养老--幸福院位置--数据视图--星空图
 */
const com_map_location_communityDataView = createObject(ReportDataView, {
    id: `data-view_com_map_location_community`,
    dataSourceID: com_map_location_communityDataSource.id
});
/**
 * 社区养老-明细-人员风采
 */
const com_detail_personal_listDataView = createObject(ReportDataView, {
    id: `data-view_com_detail_personal_list`,
    dataSourceID: com_detail_personal_listDataSource.id
});
/**
 * 社区养老-明细-最新活动情况
 */
const com_detail_activityDataView = createObject(ReportDataView, {
    id: `data-view_com_detail_activity`,
    dataSourceID: com_detail_activityDataSource.id
});
/**
 * 社区养老-服务产品列表
 */
const com_detail_service_products_listDataView = createObject(ReportDataView, {
    id: `data-view_com_detail_service_products_list`,
    dataSourceID: com_detail_service_products_listDataSource.id
});

/**
 * 社区养老--图片列表轮播
 * 社区养老--幸福院环境--数据视图--图片轮播
 */
const com_detail_picture_listCarousel_DataView = createObject(ReportDataView, {
    id: `data-view-com_detail_picture_list-carousel`,
    dataSourceID: com_detail_picture_listCarouselDataSource.id
});
/**
 * 社区养老--服务价格表--数据视图--图片轮播
 */
const com_detail_service_price_pictureCarousel_DataView = createObject(ReportDataView, {
    id: `data-view-com_detail_service_price_picture-carousel`,
    dataSourceID: com_detail_service_price_pictureCarouselDataSource.id
});
/**
 * 社区养老--幸福院活动--数据视图--图片轮播
 */
const org_detail_activity_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_activity_picture_list-carousel`,
    dataSourceID: com_detail_activity_picture_listCarouselDataSource.id
});
/**
 * 社区养老--幸福院现场--视频监控（摄像头列表，点击放大）
 */
const com_detail_video_surveillance_DataView = createObject(ReportDataView, {
    id: `data_view_com_detail_video_surveillance`,
    dataSourceID: com_detail_video_surveillance_DataSource.id
});
/**
 * 社区养老--幸福院总数--数值
 */
const com_statistics_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_total_quantity-num',
        title: '幸福院总数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [com_statistics_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['幸福院总数', '福利院服务总次数', '幸福院服务人员总数'],
                value_field: 'y',
            }
        )
    }
);
/**
 * 社区养老--幸福院类型比例--数据视图--饼图
 */
const com_statistics_nature_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_nature_quantity`,
    dataSourceID: com_statistics_nature_quantity_DataSource.id
});
/**
 * 社区养老--活动次数性别比例--数据视图--饼图
 */
const com_statistics_sex_activity_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_sex_activity_quantity`,
    dataSourceID: com_statistics_sex_activity_quantity_DataSource.id
});
/**
 * 社区养老--长者学历活动次数比例--数据视图--饼图
 */
const com_statistics_education_activity_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_education_activity_quantity`,
    dataSourceID: com_statistics_education_activity_quantity_DataSource.id
});
/**
 * 社区养老--长者学历上课次数比例--数据视图--饼图
 */
const com_statistics_education_attend_class_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_education_attend_class_quantity`,
    dataSourceID: com_statistics_education_attend_class_quantity_DataSource.id
});
/**
 * 社区养老--长者签到人数时间分布--数据视图--折线图
 */
const com_statistics_time_sign_in_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_time_sign_in_quantity`,
    dataSourceID: com_statistics_time_sign_in_quantity_DataSource.id
});
/**
 * 社区养老--活动数量时间分布--数据视图--折线图
 */
const com_statistics_time_activity_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_time_activity_quantity`,
    dataSourceID: com_statistics_time_activity_quantity_DataSource.id
});
/**
 * 社区养老--长者饭堂用餐人数分布--数据视图--折线图
 */
const com_statistics_time_meals_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_time_meals_quantity`,
    dataSourceID: com_statistics_time_meals_quantity_DataSource.id
});
/**
 * 社区养老--长者教育人数分布--数据视图--折线图
 */
const com_statistics_time_education_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_time_education_quantity`,
    dataSourceID: com_statistics_time_education_quantity_DataSource.id
});
/**
 * 社区养老--活动平均人数分布--数据视图--折线图
 */
const com_statistics_time_activity_personal_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_time_activity_personal_quantity`,
    dataSourceID: com_statistics_time_activity_personal_quantity_DataSource.id
});
/** 
 * 社区养老--服务产品介绍--表格
 */
const com_detail_service_products_DataSource = new AjaxRemoteDataQueryer(
    `data-source-com_detail_service_products`,
    `com_detail_service_products`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 社区养老--服务产品介绍--表格
 */
const com_detail_service_products_DataView = createObject(ReportDataView, {
    id: `data-view-com_detail_service_products`,
    dataSourceID: com_detail_service_products_DataSource.id
});
/**
 * 社区养老--服务产品介绍--表格
 */
const com_detail_service_productsDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_service_products-table',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        title: '服务产品介绍',
        dataViews: [com_detail_service_products_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'center'
                    },
                    {
                        title: '图片',
                        dataIndex: 'y2',
                        align: 'center'
                    },
                    {
                        title: '简介',
                        dataIndex: 'y3',
                        align: 'center'
                    }
                ],
                maxHeigh: 200
            }
        )
    }
);
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};
/**
 * 社区养老--幸福院类型比例--配置--饼图
 */
let com_statistics_nature_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 社区养老--活动次数性别比例--配置--饼图
 */
let com_statistics_sex_activity_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 社区养老--长者学历活动次数比例--配置--饼图
 */
let com_statistics_education_activity_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 社区养老--长者学历上课次数比例--配置--饼图
 */
let com_statistics_education_attend_class_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
        textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 社区养老--长者签到人数时间分布--配置--折线图
 */
let com_statistics_time_sign_in_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者签到人数',
    },
};
/**
 * 社区养老--活动数量时间分布--配置--折线图
 */
let com_statistics_time_activity_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '活动数量',
    },
};
/**
 * 社区养老--长者饭堂用餐人数分布--配置--折线图
 */
let com_statistics_time_meals_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '饭堂用餐人数',
    },
};
/**
 * 社区养老--长者教育人数分布--配置--折线图
 */
let com_statistics_time_education_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者教育人数',
    },
};
/**
 * 社区养老--活动平均人数分布--配置--折线图
 */
let com_statistics_time_activity_personal_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '活动平均人数',
    },
};
/**
 * 社区养老-明细-人员风采
 */
const com_detail_personal_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_personal_list_Module',
        titleable: false,
        title: '人员风采',
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 2,
        dataViews: [com_detail_personal_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '人员风采',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);
/**
 * 社区养老-明细-最新活动
 */
const com_detail_activityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_activity_module',
        titleable: false,
        title: '最新活动',
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 1,
        dataViews: [com_detail_activityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '最新活动',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);

/**
 * 社区养老-服务产品列表
 */
const com_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_service_products_list',
        titleable: false,
        title: '服务产品',
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 6,
        dataViews: [com_detail_service_products_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务产品',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);

/**
 * 社区养老_-星空图
 * 社区养老--幸福院位置--星空图
 */
const com_map_location_communityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_map_location_community',
        title: '幸福院位置',
        titleable: false,
        dataViews: [com_map_location_communityDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "com_map_location_community_displayer",
            option
        )
    }
);
// const legendOption = [
//     {
//         title: "养老院",
//         key: "养老院"
//     }
// ];
/**
 * 社区养老--幸福院类型比例--配置--饼图
 */
const com_statistics_nature_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_nature_quantity_pie',
        title: '幸福院类型比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        dataViews: [com_statistics_nature_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'com_statistics_nature_quantity_pie_displayer',
            com_statistics_nature_quantity_pieOption
        )
    }
);
/**
 * 社区养老--活动次数性别比例--饼图
 */
const com_statistics_sex_activity_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_sex_activity_quantity_pie',
        title: '活动次数性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [com_statistics_sex_activity_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'com_statistics_sex_activity_quantity_pie_displayer',
            com_statistics_sex_activity_quantity_pieOption
        )
    }
);
/**
 * 社区养老--长者学历活动次数比例--饼图
 */
const com_statistics_education_activity_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_education_activity_quantity_pie',
        title: '长者学历活动次数比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [com_statistics_education_activity_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'com_statistics_education_activity_quantity_pie_displayer',
            com_statistics_education_activity_quantity_pieOption
        )
    }
);
/**
 * 社区养老--长者学历上课次数比例--饼图
 */
const com_statistics_education_attend_class_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_education_attend_class_quantity_pie',
        title: '长者学历上课次数比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [com_statistics_education_attend_class_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'com_statistics_education_attend_class_quantity_pie_displayer',
            com_statistics_education_attend_class_quantity_pieOption
        )
    }
);
/**
 * 社区养老--长者签到人数时间分布--配置--折线图
 */
const com_statistics_time_sign_in_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_time_sign_in_quantity_multi_line',
        title: '长者签到人数时间分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [com_statistics_time_sign_in_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_time_sign_in_quantity_multi_line_displayer',
            com_statistics_time_sign_in_quantity_multilineOption
        )
    }
);
/**
 * 社区养老--活动数量时间分布--折线图
 */
const com_statistics_time_activity_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_time_activity_quantity_multi_line',
        title: '活动数量时间分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [com_statistics_time_activity_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_time_activity_quantity_multi_line_displayer',
            com_statistics_time_activity_quantity_multilineOption
        )
    }
);
/**
 * 社区养老--长者饭堂用餐人数分布--折线图
 */
const com_statistics_time_meals_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_time_meals_quantity_multi_line',
        title: '长者饭堂用餐人数分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [com_statistics_time_meals_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_time_meals_quantity_multi_line_displayer',
            com_statistics_time_meals_quantity_multilineOption
        )
    }
);
/**
 * 社区养老--长者教育人数分布--折线图
 */
const com_statistics_time_education_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_time_education_quantity_multi_line',
        title: '长者教育人数分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        dataViews: [com_statistics_time_education_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_time_education_quantity_multi_line_displayer',
            com_statistics_time_education_quantity_multilineOption
        )
    }
);
/**
 * 社区养老--活动平均人数分布--折线图
 */
const com_statistics_time_activity_personal_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_time_activity_personal_quantity_multi_line',
        title: '活动平均人数分布',
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 5,
        titleable: false,
        dataViews: [com_statistics_time_activity_personal_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_time_activity_personal_quantity_multi_line_displayer',
            com_statistics_time_activity_personal_quantity_multilineOption
        )
    }
);
// /** 
//  * 图例控制器
//  */
// const legendControl = createObject(
//     LegendMenuControl,
//     {
//         echar_instance: com_map_location_communityModuleControl.displayer,
//         option: legendOption
//     }
// );
/**
 * 社区养老--服务价格表--图片轮播
 */
const com_detail_service_price_picture_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_service_price_picture-carousel',
        title: '服务价格表',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [com_detail_service_price_pictureCarousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老--幸福院环境--图片轮播
 */
const com_detail_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_picture_list-carousel',
        title: '幸福院环境',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [com_detail_picture_listCarousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老--幸福院活动--数据视图--图片轮播
 */
const com_detail_activity_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_activity_picture_list-carousel',
        title: '幸福院活动',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老--幸福院环境--图片轮播-弹窗
 */
const com_detail_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_picture_list-carousel-dialog',
        title: '幸福院环境',
        titleable: false,
        isDialog: true,
        parentContainerID: com_map_location_communityModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [com_detail_picture_listCarousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老--幸福院活动--数据视图--图片轮播-弹窗
 */
const com_detail_activity_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_activity_picture_list-carousel-dialog',
        title: '幸福院活动',
        titleable: false,
        isDialog: true,
        parentContainerID: com_map_location_communityModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [org_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老--服务价格表--图片轮播-弹窗
 */
const com_detail_service_price_picture_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_service_price_picture-carousel-dialog',
        title: '服务价格表',
        titleable: false,
        isDialog: true,
        parentContainerID: com_map_location_communityModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [com_detail_service_price_pictureCarousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 社区养老-明细-人员风采-弹窗
 */
const com_detail_personal_listDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_personal_list-dialog',
        titleable: false,
        isDialog: true,
        parentContainerID: com_map_location_communityModuleControl.id!,
        dataViews: [com_detail_personal_listDataView],
        containerType: ContainerType.blank,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '人员风采',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']
            }
        )
    }
);
/**
 * 社区养老--镇街床幸福院数分布--数据源--柱状图
 */
const com_statistics_street_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_street_quantity`,
    `com_statistics_street_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--镇街床幸福院数分布--数据视图--柱状图
 */
const com_statistics_street_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_street_quantity`,
    dataSourceID: com_statistics_street_quantity_dataSource.id
});

/**
 * 社区养老--镇街床幸福院数分布--配置--柱状图
 */
let com_statistics_street_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '幸福院数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--镇街床幸福院数分布--柱状图
 */
const com_statistics_street_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_street_quantity_displayer',
        title: '镇街床幸福院数分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_street_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_street_quantity_bar_displayer',
            com_statistics_street_quantity_barOption
        )
    }
);

/**
 * 社区养老--幸福院星级数量分布--数据源--柱状图
 */
const com_statistics_star_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_star_quantity`,
    `com_statistics_star_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--幸福院星级数量分布--数据视图--柱状图
 */
const com_statistics_star_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_star_quantity`,
    dataSourceID: com_statistics_star_quantity_dataSource.id
});

/**
 * 社区养老--幸福院星级数量分布--配置--柱状图
 */
let com_statistics_star_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '星级'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '幸福院数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--幸福院星级数量分布--柱状图
 */
const com_statistics_star_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_star_quantity_displayer',
        title: '幸福院星级数量分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_star_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_star_quantity_bar_displayer',
            com_statistics_star_quantity_barOption
        )
    }
);

/**
 * 社区养老--服务商运营幸福院数量排名（TOP10）--数据源--柱状图
 */
const com_statistics_service_provider_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_service_provider_quantity`,
    `com_statistics_service_provider_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--服务商运营幸福院数量排名（TOP10）--数据视图--柱状图
 */
const com_statistics_service_provider_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_service_provider_quantity`,
    dataSourceID: com_statistics_service_provider_quantity_dataSource.id
});

/**
 * 社区养老--服务商运营幸福院数量排名（TOP10）--配置--柱状图
 */
let com_statistics_service_provider_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '经营服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '幸福院数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--服务商运营幸福院数量排名（TOP10）--柱状图
 */
const com_statistics_service_provider_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_service_provider_quantity_displayer',
        title: '服务商运营幸福院数量排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_service_provider_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_service_provider_quantity_bar_displayer',
            com_statistics_service_provider_quantity_barOption
        )
    }
);

/**
 * 社区养老--幸福院服务评分分布--数据源--柱状图
 */
const com_statistics_community_score_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_community_score`,
    `com_statistics_community_score`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--幸福院服务评分分布--数据视图--柱状图
 */
const com_statistics_community_score_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_community_score`,
    dataSourceID: com_statistics_community_score_dataSource.id
});

/**
 * 社区养老--幸福院服务评分分布--配置--柱状图
 */
let com_statistics_community_score_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '幸福院'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评分'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--幸福院服务评分分布--柱状图
 */
const com_statistics_community_score_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_community_score_displayer',
        title: '幸福院服务评分分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_community_score_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_community_score_bar_displayer',
            com_statistics_community_score_barOption
        )
    }
);

/**
 * 社区养老--幸福院满意度排名（TOP10）--数据源--柱状图
 */
const com_statistics_org_score_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_org_score`,
    `com_statistics_org_score`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--幸福院满意度排名（TOP10）--数据视图--柱状图
 */
const com_statistics_org_score_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_org_score`,
    dataSourceID: com_statistics_org_score_dataSource.id
});

/**
 * 社区养老--幸福院满意度排名（TOP10）--配置--柱状图
 */
let com_statistics_org_score_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '运营机构'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评分'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--幸福院满意度排名（TOP10）--柱状图
 */
const com_statistics_org_score_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_org_score_displayer',
        title: '幸福院满意度排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_org_score_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_org_score_bar_displayer',
            com_statistics_org_score_barOption
        )
    }
);

/**
 * 社区养老--服务人员满意度（TOP10）--数据源--柱状图
 */
const com_statistics_service_personal_score_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_service_personal_score`,
    `com_statistics_service_personal_score`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--服务人员满意度（TOP10）--数据视图--柱状图
 */
const com_statistics_service_personal_score_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_service_personal_score`,
    dataSourceID: com_statistics_service_personal_score_dataSource.id
});

/**
 * 社区养老--服务人员满意度（TOP10）--配置--柱状图
 */
let com_statistics_service_personal_score_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评分'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--服务人员满意度（TOP10）--柱状图
 */
const com_statistics_service_personal_score_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_service_personal_score_displayer',
        title: '服务人员满意度（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_service_personal_score_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_service_personal_score_bar_displayer',
            com_statistics_service_personal_score_barOption
        )
    }
);

/**
 * 社区养老--活动次数排名（TOP10）--数据源--柱状图
 */
const com_statistics_type_activity_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_type_activity_quantity`,
    `com_statistics_type_activity_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--活动次数排名（TOP10）--数据视图--柱状图
 */
const com_statistics_type_activity_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_type_activity_quantity`,
    dataSourceID: com_statistics_type_activity_quantity_dataSource.id
});

/**
 * 社区养老--活动次数排名（TOP10）--配置--柱状图
 */
let com_statistics_type_activity_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '活动类型'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '活动次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--活动次数排名（TOP10）--柱状图
 */
const com_statistics_type_activity_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_type_activity_quantity_displayer',
        title: '活动次数排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_type_activity_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_type_activity_quantity_bar_displayer',
            com_statistics_type_activity_quantity_barOption
        )
    }
);

/**
 * 社区养老--长者活动喜好--数据源--柱状图
 */
const com_statistics_age_activity_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_age_activity_quantity`,
    `com_statistics_age_activity_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--长者活动喜好--数据视图--柱状图
 */
const com_statistics_age_activity_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_age_activity_quantity`,
    dataSourceID: com_statistics_age_activity_quantity_dataSource.id
});

/**
 * 社区养老--长者活动喜好--配置--柱状图
 */
let com_statistics_age_activity_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '活动次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--长者活动喜好--柱状图
 */
const com_statistics_age_activity_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_age_activity_quantity_displayer',
        title: '长者活动喜好',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_age_activity_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_age_activity_quantity_bar_displayer',
            com_statistics_age_activity_quantity_barOption
        )
    }
);

/**
 * 社区养老--课程热度--数据源--柱状图
 */
const com_statistics_curriculum_attend_class_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_curriculum_attend_class_quantity`,
    `com_statistics_curriculum_attend_class_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--课程热度--数据视图--柱状图
 */
const com_statistics_curriculum_attend_class_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_curriculum_attend_class_quantity`,
    dataSourceID: com_statistics_curriculum_attend_class_quantity_dataSource.id
});

/**
 * 社区养老--课程热度--配置--柱状图
 */
let com_statistics_curriculum_attend_class_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '课程'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '上课次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--课程热度--柱状图
 */
const com_statistics_curriculum_attend_class_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_curriculum_attend_class_quantity_displayer',
        title: '课程热度',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_curriculum_attend_class_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_curriculum_attend_class_quantity_bar_displayer',
            com_statistics_curriculum_attend_class_quantity_barOption
        )
    }
);

/**
 * 社区养老--课程分类热度--数据源--柱状图
 */
const com_statistics_curriculum_type_attend_class_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_curriculum_type_attend_class_quantity`,
    `com_statistics_curriculum_type_attend_class_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 社区养老--课程分类热度--数据视图--柱状图
 */
const com_statistics_curriculum_type_attend_class_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_curriculum_type_attend_class_quantity`,
    dataSourceID: com_statistics_curriculum_type_attend_class_quantity_dataSource.id
});

/**
 * 社区养老--课程分类热度--配置--柱状图
 */
let com_statistics_curriculum_type_attend_class_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '课程分类'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '上课次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--课程分类热度--柱状图
 */
const com_statistics_curriculum_type_attend_class_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_curriculum_type_attend_class_quantity_displayer',
        title: '课程分类热度',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_curriculum_type_attend_class_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_curriculum_type_attend_class_quantity_bar_displayer',
            com_statistics_curriculum_type_attend_class_quantity_barOption
        )
    }
);

/**
 * 社区养老--幸福院长者饭堂用餐人次分布--数据源--柱状图
 */
const com_statistics_community_meals_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_community_meals_quantity`,
    `com_statistics_community_meals_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--幸福院长者饭堂用餐人次分布--数据视图--柱状图
 */
const com_statistics_community_meals_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_community_meals_quantity`,
    dataSourceID: com_statistics_community_meals_quantity_dataSource.id
});

/**
 * 社区养老--幸福院长者饭堂用餐人次分布--配置--柱状图
 */
let com_statistics_community_meals_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '幸福院'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者饭堂用餐人次'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--幸福院长者饭堂用餐人次分布--柱状图
 */
const com_statistics_community_meals_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_community_meals_quantity_displayer',
        title: '幸福院长者饭堂用餐人次分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_community_meals_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_community_meals_quantity_bar_displayer',
            com_statistics_community_meals_quantity_barOption
        )
    }
);

/**
 * 社区养老--长者饭堂用餐人次数排名（TOP10）--数据源--柱状图
 */
const com_statistics_service_provider_meals_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_service_provider_meals_quantity`,
    `com_statistics_service_provider_meals_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--长者饭堂用餐人次数排名（TOP10）--数据视图--柱状图
 */
const com_statistics_service_provider_meals_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_service_provider_meals_quantity`,
    dataSourceID: com_statistics_service_provider_meals_quantity_dataSource.id
});

/**
 * 社区养老--长者饭堂用餐人次数排名（TOP10）--柱状图
 */
let com_statistics_service_provider_meals_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者饭堂用餐人次'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--长者饭堂用餐人次数排名（TOP10）--柱状图
 */
const com_statistics_service_provider_meals_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_service_provider_meals_quantity_displayer',
        title: '长者饭堂用餐人次数排名（TOP10）',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_service_provider_meals_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_service_provider_meals_quantity_bar_displayer',
            com_statistics_service_provider_meals_quantity_barOption
        )
    }
);

/**
 * 社区养老--幸福院幸福小站使用次数分布--数据源--柱状图
 */
const com_statistics_community_happiness_station_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_com_statistics_community_happiness_station_quantity`,
    `com_statistics_community_happiness_station_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 社区养老--幸福院幸福小站使用次数分布--数据视图--柱状图
 */
const com_statistics_community_happiness_station_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_com_statistics_community_happiness_station_quantity`,
    dataSourceID: com_statistics_community_happiness_station_quantity_dataSource.id
});

/**
 * 社区养老--幸福院幸福小站使用次数分布--配置--柱状图
 */
let com_statistics_community_happiness_station_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '幸福院'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '幸福小站使用次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 社区养老--幸福院幸福小站使用次数分布--柱状图
 */
const com_statistics_community_happiness_station_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_statistics_community_happiness_station_quantity_displayer',
        title: '幸福院幸福小站使用次数分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [com_statistics_community_happiness_station_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'com_statistics_community_happiness_station_quantity_bar_displayer',
            com_statistics_community_happiness_station_quantity_barOption
        )
    }
);

/**
 * 机构养老--福利院现场--视频监控（摄像头列表，点击放大）
 */
const com_detail_video_surveillanceModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'com_detail_video_surveillance_displayer',
        titleable: false,
        dataViews: [com_detail_video_surveillance_DataView],
        containerType: ContainerType.nhContain,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,'
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        groupIndex: 2,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://183.235.223.56:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 600,
                height: 600
            }
        )
    }
);
const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(CommonLayoutMiddleControl);
const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportControl,
    {
        dataSources: [
        ],
        modules: [
        ],
        layout: layoutSelect
    }
);

const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    com_statistics_type_activity_quantity_dataSource,
                    com_statistics_total_quantityDataSource,
                    com_detail_video_surveillance_DataSource,
                    com_statistics_street_quantity_dataSource,
                    com_statistics_nature_quantity_DataSource,
                    com_statistics_star_quantity_dataSource,
                    com_statistics_service_provider_quantity_dataSource,
                    com_statistics_community_score_dataSource,
                    com_statistics_org_score_dataSource,
                    com_statistics_service_personal_score_dataSource,
                    com_statistics_sex_activity_quantity_DataSource,
                    com_statistics_age_activity_quantity_dataSource,
                    com_statistics_education_activity_quantity_DataSource,
                    com_statistics_education_attend_class_quantity_DataSource,
                    com_statistics_curriculum_attend_class_quantity_dataSource,
                    com_statistics_community_meals_quantity_dataSource,
                    com_statistics_service_provider_meals_quantity_dataSource,
                    com_statistics_community_happiness_station_quantity_dataSource,
                    com_statistics_time_sign_in_quantity_DataSource,
                    com_statistics_time_activity_quantity_DataSource,
                    com_statistics_time_meals_quantity_DataSource,
                    com_statistics_time_education_quantity_DataSource,
                    com_statistics_time_activity_quantity_DataSource,
                    com_statistics_time_activity_personal_quantity_DataSource
                ],
                modules: [
                    // 0
                    com_statistics_education_activity_quantity_displayModuleControl,
                    com_statistics_total_quantityModuleControl,
                    // 1
                    com_statistics_star_quantity_displayModuleControl,
                    com_statistics_street_quantity_displayModuleControl,
                    com_statistics_nature_quantity_displayModuleControl,
                    com_statistics_service_provider_quantity_displayModuleControl,
                    // 2
                    com_statistics_service_personal_score_displayModuleControl,
                    com_statistics_community_score_displayModuleControl,
                    com_statistics_org_score_displayModuleControl,
                    // 3
                    com_statistics_sex_activity_quantity_displayModuleControl,
                    com_statistics_type_activity_quantity_displayModuleControl,
                    com_statistics_age_activity_quantity_displayModuleControl,
                    // 4
                    com_statistics_curriculum_attend_class_quantity_displayModuleControl,
                    com_statistics_education_attend_class_quantity_displayModuleControl,
                    com_statistics_community_meals_quantity_displayModuleControl,
                    com_statistics_service_provider_meals_quantity_displayModuleControl,
                    com_statistics_community_happiness_station_quantity_displayModuleControl,
                    // 5
                    com_statistics_time_activity_quantity_displayModuleControl,
                    com_statistics_time_sign_in_quantity_displayModuleControl,
                    com_statistics_time_meals_quantity_displayModuleControl,
                    com_statistics_time_education_quantity_displayModuleControl,
                    com_statistics_time_activity_personal_quantity_displayModuleControl,
                ],
                layout: layoutLeft,
                autoQuery: 1,
            }
        )
    }
);
const leftControlTab2 = createObject(
    SmartReportControl,
    {
        title: "",
        dataSources: [
            com_statistics_curriculum_attend_class_quantity_dataSource,
            com_statistics_curriculum_type_attend_class_quantity_dataSource,
        ],
        modules: [
            com_statistics_age_activity_quantity_displayModuleControl,
            com_statistics_curriculum_attend_class_quantity_displayModuleControl,
            com_statistics_curriculum_type_attend_class_quantity_displayModuleControl,
            com_statistics_community_meals_quantity_displayModuleControl,
            com_statistics_service_provider_meals_quantity_displayModuleControl,
        ],
        layout: layoutLeft,
    }
);
const middleControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            com_map_location_communityDataSource
        ],
        modules: [
            com_map_location_communityModuleControl
        ],
        layout: layoutMiddle,
        autoQuery: 1,
    }
);
const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    com_detail_picture_listCarouselDataSource,
                    com_detail_activity_picture_listCarouselDataSource,
                    com_detail_personal_listDataSource,
                    com_detail_activityDataSource,
                    com_detail_service_products_listDataSource,
                    com_detail_service_products_DataSource,
                ],
                modules: [
                    // 0
                    com_detail_activityModuleControl,
                    // 1
                    com_detail_video_surveillanceModuleControl,
                    com_detail_picture_list_DisplayerModule,
                    com_detail_picture_list_DialogDisplayerModule,
                    // 2
                    com_detail_service_price_picture_DisplayerModule,
                    com_detail_service_price_picture_DialogDisplayerModule,
                    com_detail_personal_listModuleControl,
                    com_detail_personal_listDialogModuleControl,
                    // 3
                    com_detail_activity_picture_list_DisplayerModule,
                    com_detail_activity_picture_list_DialogDisplayerModule,
                    // 4
                    com_detail_service_productsDisplayerModule,
                    // 5
                    com_detail_service_products_listModuleControl,
                ],
                layout: layoutRight,
                defaultParams: { id: 'db0b6c0a-d08d-11e9-a146-144f8aec0be5' },
                autoQuery: 1,
            }
        )
    }
);
const middleReport = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: middleControl
    }
);
leftControlTab2;
/**
 * react布局控制器
 */
const layoutAll = createObject(CommonLayoutControl);
const irCommunityHomeControl = createObject(
    SmartReportControl,
    {
        id: 'irCommunityHome',
        modules: [selectControl, leftControl, middleReport, rightControl],
        dataSources: [],
        layout: layoutAll,
        // autoQuery: 1,
        areAllQuery: true,
        isAsyncQuery: false,
        // remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irCommunityHomeControl };
