import { createObject, IDataService, newGuid } from "pao-aop";
import { AjaxJsonRpcFactory, AjaxRemoteDataQueryer, EChartsOptions } from "pao-aop-client";
import { VideoControl } from "src/business/components/buss-components/video";
import { CardIntroduceDisplayControl } from "src/business/report/displayers/nh-welfare/card-introduce-display";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { DescribeDisplayControl } from "src/business/report/displayers/nh-welfare/describe-display";
import { MonitorDisplayControl } from "src/business/report/displayers/nh-welfare/monitor-display";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { RowTableDisplayControl } from "src/business/report/displayers/nh-welfare/row-table-display";
import { DataDisplayerModuleControl, ReportDataView, SmartReportControl, SmartReportModuleControl } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { createLineBarMixDataDisplayer, createMultiRectangularCoordinateDataDisplayer, createStackMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { ContainerType } from "src/business/report/widget";
import { TimeLineControl } from "src/projects/components/time-line";
import { OrganizationDetailLayoutRightControl } from "../components/layout/organization-layout/detail-right";
import { OrganizationLayoutLeftControl } from "../components/layout/organization-layout/left";
import { OrganizationLayoutMiddleControl } from "../components/layout/organization-layout/middle";
import { OrganizationLayoutRightControl } from "../components/layout/organization-layout/right";
import { } from "./echartCommonVar";
import { TestTabsLayoutControl } from "./test-tabs-layout";
const url = `http://localhost:3100/remoteCall`;
/**
 * 养老机构整体情况--新增床位资助--折柱混合
 */
const yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_DataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhengtiqingkuang-xinzengchuangweizizhu`,
    `yanglaojigouzhengtiqingkuang-xinzengchuangweizizhu`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);
/**
 * 养老机构详情工作人员情况--学历--饼图
 */
const yanglaojigouzhangzheqingkuang_xueli_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigougongzuorenyuanqingkuang-xueli`,
    `duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-xueli`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情--床位总览--表格
 */
const chuangweizonglan_DataSource = new AjaxRemoteDataQueryer(
    `data-source-chuangweizonglan`,
    `bed_overview`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情--机构介绍--描述列表
 */
const yanglaojigouxiangqing_jigoujieshao_DataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouxiangqing-jigoujieshao`,
    `yanglaojigouxiangqing-jigoujieshao-describe`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情工作人员情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanqingkuang_chizheng_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigougongzuorenyuanqingkuang-chizheng`,
    `duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--户籍人数--饼图
 */
const yanglaojigouzhengtizhangzheqingkuang_huji_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhangzheqingkuang-huji`,
    `yanglaojigouzhangzheqingkuang-huji`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--子女数--饼图
 */
const yanglaojigouzhangzheqingkuang_zinvshu_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhangzheqingkuang_zinvshu`,
    `yanglaojigouzhangzheqingkuang-zinvshu`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--能力评估--饼图
 */
const yanglaojigouzhangzheqingkuang_nenglipinggu_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhangzheqingkuang_nenglipinggu`,
    `yanglaojigouzhangzheqingkuang-nenglipinggu`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--老人分类--饼图
 */
const yanglaojigouzhangzheqingkuang_laorenfenlei_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhangzheqingkuang_laorenfenlei`,
    `yanglaojigouzhangzheqingkuang-laorenfenlei`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况-时间轴
 */
const yanglaojigouzhangzheqingkuang_time_lineDataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhangzheqingkuang-time-line`,
    `yanglaojigouzhangzheqingkuang-time-line`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情--监控列表
 */
const yanglaojigouxiangqing_monitor_DataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouxiangqing-monitor`,
    `yanglaojigouxiangqing-monitor`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--年度建设投入资金--饼图
 */
const yanglaojigouzhengtiqingkuang_jianshetouru_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-yanglaojigouzhengtiqingkuang_jianshetouru`,
    `yanglaojigouzhengtiqingkuang-jianshetouru`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--入住、预约--方块
 */
const yanglaojigouzhangzheqingkuangRuzhuYuyueDataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhangzheqingkuang-ruzhu-yuyue`,
    `yanglaojigouzhangzheqingkuang-ruzhu-yuyue-square`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--机构统计--方块
 */
const yanglaojigouzhengtiqingkuangJigoutongjiDataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhengtiqingkuang-jigoutongji`,
    `yanglaojigouzhengtiqingkuang-jigoutongji-square`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 宜养生态系统整体情况-热力图、散点图合并
 */
const allMapDataSource = new AjaxRemoteDataQueryer(
    `data-source-yiyang-all-map`,
    `get_senior_citizens`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--男女人数--柱状堆叠图
 */
const yanglaojigouzhengtizhangzheqingkuang_nannv_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhangzheqingkuang-nannv`,
    `duidieBarTest-yanglaojigouzhangzheqingkuang-nannv`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构工作人员整体情况--男女人数--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigougongzuorenyuanzhengtiqingkuang-nannv`,
    `duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-nannv`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构工作人员整体情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng`,
    `duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构工作人员整体情况--学历--饼图
 */
const yanglaojigouzhengtizhangzheqingkuang_xueli_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigougongzuorenyuanzhengtiqingkuang-xueli`,
    `duidieBarTest-yanglaojigougongzuorenyuanzhengtiqingkuang-xueli`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--年度建设投入资金--柱状图
 */
const yanglaojigouzhengtiqingkuang_niandutouruzijin_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhengtiqingkuang-niandutouruzijin`,
    `yanglaojigouzhengtiqingkuang-niandutouruzijin`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体长者情况--健康状况--柱状图
 */
const yanglaojigouzhengtizhangzheqingkuang_jiankang_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhengtizhangzheqingkuang-jiankang`,
    `yanglaojigouzhengtizhangzheqingkuang-jiankang`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--机构等级--柱状图
 */
const yanglaojigouzhengtiqingkuang_jigoudengji_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhengtiqingkuang-jigoudengji`,
    `yanglaojigouzhengtiqingkuang-jigoudengji`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构整体情况--机构分类--柱状堆叠图
 */
const yanglaojigouzhengtiqingkuang_jigoufenlei_dataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouzhengtiqingkuang-jigoufenlei`,
    `duidieBarTest-jigoufenlei`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情--轮播
 */
const yanglaojigouxiangqingCarouselDataSource = new AjaxRemoteDataQueryer(
    `data-source-yanglaojigouxiangqing-carousel`,
    `yanglaojigouxiangqing-carousel`,
    new AjaxJsonRpcFactory(new IDataService, url, `IBigScreenService`)
);

/**
 * 养老机构详情--机构介绍--描述列表
 */
const yanglaojigouxiangqing_jigoujieshao_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-jigoujieshao`,
    dataSourceID: yanglaojigouxiangqing_jigoujieshao_DataSource.id
});

/**
 * 养老机构整体情况--新增床位资助--折柱混合
 */
const yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-xinzengchuangweizizhu`,
    dataSourceID: yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_DataSource.id
});

/**
 * 养老机构工作人员整体情况--男女人数--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigougongzuorenyuanzhengtiqingkuang-nannv`,
    dataSourceID: yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataSource.id
});

/**
 * 养老机构工作人员整体情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng`,
    dataSourceID: yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_dataSource.id
});

/**
 * 养老机构详情--机构统计--方块
 */
const yanglaojigouqingkuang_jigoutongji_square_DataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-jigoutongjie-square`,
    dataSourceID: yanglaojigouzhengtiqingkuangJigoutongjiDataSource.id
});

/**
 * 养老机构详情--床位总览--表格
 */
const chuanwei_DataView = createObject(ReportDataView, {
    id: `data-view-chuangweizonglan`,
    dataSourceID: chuangweizonglan_DataSource.id
});

/**
 * 养老机构整体长者情况--男女人数--柱状堆叠图
 */
const yanglaojigouzhengtizhangzheqingkuang_nannv_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhangzheqingkuang-nannv`,
    dataSourceID: yanglaojigouzhengtizhangzheqingkuang_nannv_dataSource.id
});

/**
 * 养老机构工作人员整体情况--学历--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangXueliPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhengtizhangzheqingkuang-xueli`,
    dataSourceID: yanglaojigouzhengtizhangzheqingkuang_xueli_dataSource.id
});

/**
 * 养老机构整体长者情况--户籍人数--饼图
 */
const yanglaojigouzhangzheqingkuangHujiPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhangzheqingkuang-huji`,
    dataSourceID: yanglaojigouzhengtizhangzheqingkuang_huji_PieDataSource.id
});

/**
 * 养老机构整体情况--年度建设投入资金--饼图
 */
const yanglaojigouzhengtiqingkuangjianshetouruPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhengtiqingkuang-jianshetouru`,
    dataSourceID: yanglaojigouzhengtiqingkuang_jianshetouru_PieDataSource.id
});

/**
 * 养老机构整体长者情况--子女数--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangZinvshuPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhangzheqingkuang-zinvshu`,
    dataSourceID: yanglaojigouzhangzheqingkuang_zinvshu_PieDataSource.id
});

/**
 * 养老机构整体长者情况--能力评估--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangNenglipingguPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhangzheqingkuang-nenglipinggu`,
    dataSourceID: yanglaojigouzhangzheqingkuang_nenglipinggu_PieDataSource.id
});

/**
 * 养老机构整体长者情况--老人分类--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhangzheqingkuang-laorenfenlei`,
    dataSourceID: yanglaojigouzhangzheqingkuang_laorenfenlei_PieDataSource.id
});

/**
 * 养老机构整体--时间轴
 */
const yanglaojigouzhengtizhangzheTimeLineDataView = createObject(ReportDataView, {
    id: `yanglaojigouzhangzheqingkuang-time-line`,
    dataSourceID: yanglaojigouzhangzheqingkuang_time_lineDataSource.id
});

/**
 * 养老机构整体--时间轴
 */
const yiyang_jigouzonglan_tableDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouzhangzheqingkuang-time-line',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtizhangzheTimeLineDataView],
        displayer: createObject(
            TimeLineControl,
        )
    }
);

/**
 * 养老机构整体情况--年度建设投入资金--柱状图
 */
const yanglaojigouzhengtiqingkuang_niandutouruzijin_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-niandutouruzijin`,
    dataSourceID: yanglaojigouzhengtiqingkuang_niandutouruzijin_dataSource.id
});

/**
 * 养老机构整体情况--机构等级--柱状图
 */
const yanglaojigouzhengtiqingkuang_jigoudengji_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-jigoudengji`,
    dataSourceID: yanglaojigouzhengtiqingkuang_jigoudengji_dataSource.id
});

/**
 * 养老机构详情--监控列表
 */
const yanglaojigouxiangqing_monitor_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouxiangqing-monitor`,
    dataSourceID: yanglaojigouxiangqing_monitor_DataSource.id
});

/**
 * 养老机构整体长者情况--健康状况--柱状图
 */
const yanglaojigouzhengtizhangzheqingkuang_jiankang_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtizhangzheqingkuang-jiankang`,
    dataSourceID: yanglaojigouzhengtizhangzheqingkuang_jiankang_dataSource.id
});

/**
 * 养老机构详情工作人员情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanqingkuang_chizheng_dataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigougongzuorenyuanqingkuang-chizheng`,
    dataSourceID: yanglaojigougongzuorenyuanqingkuang_chizheng_dataSource.id
});

/**
 * 养老机构整体情况--机构分类--柱状堆叠图
 */
const yanglaojigouzhengtiqingkuang_jigoufenlei_DataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-jigoufenlei-DataView`,
    dataSourceID: yanglaojigouzhengtiqingkuang_jigoufenlei_dataSource.id
});

/**
 * 宜养生态系统整体情况-热力图、散点图合并
 */
const allMapDataView = createObject(ReportDataView, {
    id: `data-view-yiyang-all-map`,
    dataSourceID: allMapDataSource.id
});

/**
 * 养老机构整体长者情况--入住、预约--方块
 */
const yanglaojigouzhangzheqingkuang_ruzhu_yuyue_square_DataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhangzheqingkuang-ruzhu-yuyue-square`,
    dataSourceID: yanglaojigouzhangzheqingkuangRuzhuYuyueDataSource.id
});

/**
 * 养老机构详情工作人员情况--学历--饼图
 */
const yanglaojigouzhangzheqingkuangXueliPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-yanglaojigouzhangzheqingkuang-xueli`,
    dataSourceID: yanglaojigouzhangzheqingkuang_xueli_dataSource.id
});

/**
 * 养老机构详情--轮播
 */
const yanglaojigouxiangqing_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouxiangqing-carousel`,
    dataSourceID: yanglaojigouxiangqingCarouselDataSource.id
});

/**
 * 养老机构整体情况--机构统计--方块
 */
const yanglaojigouzhengtiqingkuang_jigoutongji_square_DataView = createObject(ReportDataView, {
    id: `data-view-yanglaojigouzhengtiqingkuang-jigoutongjie-square`,
    dataSourceID: yanglaojigouzhengtiqingkuangJigoutongjiDataSource.id
});

/**
 * 养老机构整体情况--机构分类--柱状堆叠图
 */
let yanglaojigouzhengtiqingkuang_jigoufenlei_barOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}家"
        }
    },
};

/**
 * 养老机构工作人员整体情况--男女人数--柱状堆叠图
 */
let yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_barOption: EChartsOptions = {
    title: {
        text: "工作人员整体情况"
    },
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}人"
        }
    },
};

/**
 * 养老机构工作人员整体情况--持证比例--柱状堆叠图
 */
let yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_barOption: EChartsOptions = {
    title: {
        text: "工作人员持证比例"
    },
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}人"
        }
    },
};

/**
 * 养老机构整体长者情况--男女人数--柱状堆叠图
 */
let yanglaojigouzhengtizhangzheqingkuang_nannv_barOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
        axisLabel: {
            rotate: 25
        }
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}万人",
            showMinLabel: false,
        },
    },
};

/**
 * 养老机构整体情况--新增床位资助--折柱混合
 */
let yanglaojigouzhengtiqingkuangXinzengchuangweizizhuChartOption: EChartsOptions = {
    title: {
        text: "新增床位资助"
    },
    legend: {
        show: true,
        data: ['机构数', '投资']
    },
    yAxis: [
        {
            type: 'value',
            // name: '家',
            axisLabel: {
                formatter: "{value}万元",
                showMinLabel: false,
            }
        },
        {
            type: 'value',
            axisLabel: {
                formatter: "{value}家",
            }
        }
    ],
    series: [
        {
            name: "机构数",
            type: 'bar',
        },
        {
            name: "投资",
            type: 'line',
            yAxisIndex: 1
        }
    ]
};

/**
 * 养老机构工作人员整体情况--学历--饼图
 */
let yanglaojigouzhengtizhangzheqingkuangXueliPieChartOption: EChartsOptions = {
    title: {
        text: "人"
    },
    legend: {
        show: true
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构整体情况--年度建设投入资金--饼图
 */
let yanglaojigouzhengtiqingkuangJianshetouruPieChartOption: EChartsOptions = {
    title: {
        text: "年度建设投入资金"
    },
    legend: {
        show: false
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构整体情况--年度建设投入资金--饼图
 */
let yanglaojigouzhangzheqingkuangHujiPieChartOption: EChartsOptions = {
    title: {
        text: "户籍人数"
    },
    legend: {
        show: false
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构详情工作人员情况--学历--饼图
 */
let yanglaojigouzhangzheqingkuangXueliPieChartOption: EChartsOptions = {
    // title: {
    //     text: "长者类型（养老机构）"
    // },
    legend: {
        show: false
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构整体长者情况--子女数--饼图
 */
let yanglaojigouzhengtizhangzheqingkuangZinvshuPieChartOption: EChartsOptions = {
    title: {
        text: "子女数",
        padding: [40, 0, 0, 0]
    } as any,
    legend: {
        show: false
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构整体长者情况--能力评估--饼图
 */
let yanglaojigouzhengtizhangzheqingkuangNenglipingguPieChartOption: EChartsOptions = {
    // title: {
    //     text: "长者类型（养老机构）"
    // },
    legend: {
        show: true
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构详情工作人员情况--持证比例--柱状堆叠图
 */
let yanglaojigougongzuorenyuanqingkuang_chizheng_barOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}人"
        }
    },
};

/**
 * 养老机构整体长者情况--老人分类--饼图
 */
let yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieChartOption: EChartsOptions = {
    // title: {
    //     text: "长者类型（养老机构）"
    // },
    legend: {
        show: false
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老机构整体情况--年度建设投入资金--柱状图
 */
let yanglaojigouzhengtiqingkuang_niandutouruzijin_barOption: EChartsOptions = {
    title: {
        text: "年度建设投入资金"
    },
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}万元",
            showMinLabel: false,
        }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 养老机构整体情况--机构登记--柱状图
 */
let yanglaojigouzhengtiqingkuang_jigoudengji_barOption: EChartsOptions = {
    // title: {
    //     text: "长者数量统计"
    // },
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}家",
        }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 养老机构整体长者情况--健康状况--柱状图
 */
let yanglaojigouzhengtizhangzheqingkuang_jiankang_barOption: EChartsOptions = {
    // title: {
    //     text: "长者数量统计"
    // },
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        }
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: "{value}千人",
            showMinLabel: false,
        }
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 养老机构工作人员整体情况--男女人数--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'zhangzheshu-nenglitongji-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataView],
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'age',
                yAxisFieldNames: ['男', '女'],
                stackType: "bar"
            },
            'yanglaojigougongzuorenyuanzhengtiqingkuang-nannv-bar-displayer',
            yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_barOption
        )
    }
);

/**
 * 养老机构工作人员整体情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigougongzuorenyuanzhengtiqingkuang-chizheng-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_dataView],
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'classify',
                yAxisFieldNames: ['持专业证', '其他'],
                stackType: "bar"
            },
            'zhangzheshu-nenglitongji-bar-displayer',
            yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_barOption
        )
    }
);

/**
 * 养老机构详情工作人员情况--持证比例--柱状堆叠图
 */
const yanglaojigougongzuorenyuanqingkuang_chizheng_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigougongzuorenyuanqingkuang-chizheng-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigougongzuorenyuanqingkuang_chizheng_dataView],
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'classify',
                yAxisFieldNames: ['持专业证', '其他'],
                stackType: "bar"
            },
            'zhangzheshu-nenglitongji-bar-displayer',
            yanglaojigougongzuorenyuanqingkuang_chizheng_barOption
        )
    }
);

/**
 * 养老机构整体情况--机构分类--柱状堆叠图
 */
const yanglaojigouzhengtiqingkuang_jigoufenlei_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'zhangzheshu-nenglitongji-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtiqingkuang_jigoufenlei_DataView],
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'classify',
                yAxisFieldNames: ['工商登记', '民非登记', '事业单位'],
                stackType: "bar"
            },
            'zhangzheshu-nenglitongji-bar-displayer',
            yanglaojigouzhengtiqingkuang_jigoufenlei_barOption
        )
    }
);

/**
 * 养老机构整体长者情况--男女人数--柱状堆叠图
 */

const yanglaojigouzhengtizhangzheqingkuang_nannv_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'zhangzheshu-yiyang-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtizhangzheqingkuang_nannv_dataView],
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'age',
                yAxisFieldNames: ['男', '女'],
                stackType: "bar"
            },
            'zhangzheshu-yiyang-bar-displayer',
            yanglaojigouzhengtizhangzheqingkuang_nannv_barOption
        )
    }
);

/**
 * 养老机构详情工作人员情况--学历--饼图
 */
const yanglaojigouzhangzhexiangqingXueliPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhangzheqingkuangXueliPieDataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhangzhexiangqingXueli-single-pie-displayer',
            yanglaojigouzhangzheqingkuangXueliPieChartOption
        ),
    }
);

/**
 * 养老机构整体情况--学历--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangXueliPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhengtizhangzheqingkuangXueliPieDataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhengtizhangzheqingkuangXueli-single-pie-displayer',
            yanglaojigouzhengtizhangzheqingkuangXueliPieChartOption
        ),
    }
);

/**
 * 养老机构整体情况--年度建设投入资金--饼图
 */
const yanglaojigouzhengtiqingkuangJianshetouruPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtiqingkuangjianshetouruPieDataView],
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhengtiqingkuangJianshetouru-single-pie-displayer',
            yanglaojigouzhengtiqingkuangJianshetouruPieChartOption
        ),
    }
);

/**
 * 养老机构整体情况--新增床位资助--折柱混合
 */
const yanglaojigouzhengtiqingkuangXinzengchuangweizizhuDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_dataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createLineBarMixDataDisplayer(
            {
                yAxisFieldNames: ['机构数', '投资'],
                xAxisFieldName: 'type',
            },
            'yanglaojigouzhengtiqingkuangXinzengchuangweizizhu-displayer',
            yanglaojigouzhengtiqingkuangXinzengchuangweizizhuChartOption
        ),
    }
);

/**
 * 养老机构整体长者情况--户籍人数--饼图
 */
const yanglaojigouzhangzheqingkuangHujiPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhangzheqingkuangHujiPieDataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhangzheqingkuangHuji-single-pie-displayer',
            yanglaojigouzhangzheqingkuangHujiPieChartOption
        ),
    }
);

/**
 * 养老机构详情--服务介绍
 */
const service_introduce_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigoujigoutongjiqingkuang-jigoutongji-square',
        titleable: false,
        dataViews: [yanglaojigouqingkuang_jigoutongji_square_DataView],
        containerType: ContainerType.nhContain,
        displayer: createObject(
            CardIntroduceDisplayControl
        )
    }
);

/**
 * 养老机构整体长者情况--子女数--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangZinvshuPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhengtizhangzheqingkuangZinvshuPieDataView],
        titleable: false,
        containerType: ContainerType.nhContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhangzheqingkuangZinvshu-single-pie-displayer',
            yanglaojigouzhengtizhangzheqingkuangZinvshuPieChartOption
        ),
    }
);

/**
 * 养老机构整体长者情况--能力评估--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangNenglipingguPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhengtizhangzheqingkuangNenglipingguPieDataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhangzheqingkuangNenglipinggu-single-pie-displayer',
            yanglaojigouzhengtizhangzheqingkuangNenglipingguPieChartOption
        ),
    }
);

/**
 * 养老机构整体长者情况--老人分类--饼图
 */
const yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieDataView],
        containerType: ContainerType.nhContain,
        titleable: false,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'value',
                pieNameFieldName: 'name',
            },
            'yanglaojigouzhangzheqingkuangLaorenfenlei-single-pie-displayer',
            yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieChartOption
        ),
    }
);

/**
 * 宜养生态系统整体情况-热力图、散点图合并
 */
const allMapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'all-map',
        titleable: false,
        containerType: ContainerType.withdialog,
        dataViews: [allMapDataView],
        drillDialogModules: ["organizationDetailRightReportId"],
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'lon',
                latitudeFieldName: 'lat',
            }
        )
    }
);

/**
 * 养老机构整体长者情况--响应--方块
 */
const yanglaojigouzhangzheqingkuang_ruzhu_yuyue_square_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouzhangzheqingkuang-ruzhu-yuyue-square',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhangzheqingkuang_ruzhu_yuyue_square_DataView],
        displayer: createObject(
            MoreSquaresControl
        )
    }
);

/**
 * 养老机构整体情况--机构统计--方块
 */
const yanglaojigoujigoutongjiqingkuang_jigoutongji_square_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigoujigoutongjiqingkuang-jigoutongji-square',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtiqingkuang_jigoutongji_square_DataView],
        displayer: createObject(
            MoreSquaresControl
        )
    }
);

/**
 * 养老机构整体情况--年度建设投入资金--柱状图
 */
const yanglaojigouzhengtiqingkuang_niandutouruzijin_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouzhengtiqingkuang_niandutouruzijin-single-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtiqingkuang_niandutouruzijin_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'name',
                yAxisFieldNames: ['value']
            },
            'yanglaojigouzhengtiqingkuang_niandutouruzijin-single-bar-displayer',
            yanglaojigouzhengtiqingkuang_niandutouruzijin_barOption
        )
    }
);

/**
 * 养老机构详情--轮播
 */
const carousel_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouxiangqing-carousel',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouxiangqing_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

const id = newGuid();

/**
 * 养老机构整体情况--机构等级--柱状图
 */
const yanglaojigouzhengtiqingkuang_jigoudengji_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouzhengtiqingkuang_jigoudengji-single-bar',
        titleable: false,
        drillDialogModules: [id],
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtiqingkuang_jigoudengji_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'name',
                yAxisFieldNames: ['value']
            },
            'yanglaojigouzhengtiqingkuang_jigoudengji-single-bar-displayer',
            yanglaojigouzhengtiqingkuang_jigoudengji_barOption
        )
    }
);

/**
 * 养老机构详情--床位总览--表格
 */
const chuangweizonglan_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'welfare-table',
        titleable: true,
        title: "床位总览",
        dataViews: [chuanwei_DataView],
        containerType: ContainerType.nhContain,
        displayer: createObject(
            RowTableDisplayControl,
            {
                data_map: [
                    {
                        type: 1,
                        desc: "在院",
                        color: "inherit"
                    },
                    {
                        type: 2,
                        desc: "新入住",
                        color: "#F5A623"
                    },
                    {
                        type: 3,
                        desc: "请假",
                        color: "#7ED321"
                    },
                    {
                        type: 4,
                        desc: "退住",
                        color: "#001DF9"
                    },
                    {
                        type: 5,
                        desc: "转院",
                        color: "#B100FF"
                    },
                    {
                        type: 6,
                        desc: "死亡",
                        color: "#9B9B9B"
                    }
                ]
            }
        )
    }
);

/**
 * 养老机构详情--机构介绍--描述列表
 */
const describe_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigoujigoutongjiqingkuang-jigoutongji-square',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouxiangqing_jigoujieshao_dataView],
        displayer: createObject(
            DescribeDisplayControl
        )
    }
);

/** 直播模块id */
const liveId = newGuid();

/**
 * 养老机构详情--监控列表
 */
const monitor_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigoujigoutongjiqingkuang-jiankongliebiao',
        titleable: false,
        containerType: ContainerType.nhContain,
        drillDialogModules: [liveId],
        drillDataSources: [""],
        dataViews: [yanglaojigouxiangqing_monitor_dataView],
        displayer: createObject(
            MonitorDisplayControl
        )
    }
);

/**
 * 养老机构详情-监控--直播
 */
const jujiayanglaoxiangqing_liveModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: liveId,
        titleable: false,
        dataViews: [yanglaojigouxiangqing_monitor_dataView],
        containerType: ContainerType.nhContain,
        isDialog: true,
        parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://192.168.95.127:21000/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 3900,
                height: 2800
            }
        )
    }
);

/**
 * 养老机构整体长者情况--健康状况--柱状图
 */
const yanglaojigouzhengtizhangzheqingkuang_jiankang_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'yanglaojigouzhengtizhangzheqingkuang_jiankang-single-bar',
        titleable: false,
        containerType: ContainerType.nhContain,
        dataViews: [yanglaojigouzhengtizhangzheqingkuang_jiankang_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'name',
                yAxisFieldNames: ['value']
            },
            'yanglaojigouzhengtizhangzheqingkuang_jiankang-single-bar-displayer',
            yanglaojigouzhengtizhangzheqingkuang_jiankang_barOption
        )
    }
);

const layoutLeft = createObject(OrganizationLayoutLeftControl);
const layoutMiddle = createObject(OrganizationLayoutMiddleControl);
const layoutRight = createObject(OrganizationLayoutRightControl);
const layoutDetailRight = createObject(OrganizationDetailLayoutRightControl);

const organizationLeftControl = createObject(
    SmartReportControl,
    {
        title: '养老机构整体长者情况',
        dataSources: [
            yanglaojigouzhangzheqingkuangRuzhuYuyueDataSource,
            yanglaojigouzhengtizhangzheqingkuang_nannv_dataSource,
            yanglaojigouzhengtizhangzheqingkuang_huji_PieDataSource,
            yanglaojigouzhangzheqingkuang_zinvshu_PieDataSource,
            yanglaojigouzhengtizhangzheqingkuang_jiankang_dataSource,
            yanglaojigouzhangzheqingkuang_nenglipinggu_PieDataSource,
            yanglaojigouzhangzheqingkuang_laorenfenlei_PieDataSource,
        ],
        modules: [
            // 1
            yanglaojigouzhangzheqingkuang_ruzhu_yuyue_square_DisplayerModule,
            yanglaojigouzhengtizhangzheqingkuang_nannv_displayModuleControl,
            yanglaojigouzhangzheqingkuangHujiPieDisplayerModule,
            yanglaojigouzhengtizhangzheqingkuangZinvshuPieDisplayerModule,
            yanglaojigouzhengtizhangzheqingkuang_jiankang_displayModuleControl,
            // 6
            yanglaojigouzhengtizhangzheqingkuangNenglipingguPieDisplayerModule,
            yanglaojigouzhengtizhangzheqingkuangLaorenfenleiPieDisplayerModule,
        ],
        layout: layoutLeft,
    }
);

const organizationMiddleControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            yanglaojigouzhangzheqingkuang_time_lineDataSource,
            yanglaojigouzhangzheqingkuang_laorenfenlei_PieDataSource,
            allMapDataSource,
        ],
        modules: [
            // 1
            yiyang_jigouzonglan_tableDisplayerModule,
            allMapModuleControl,
        ],
        layout: layoutMiddle,
    }
);

const organizationRightControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            yanglaojigouzhengtiqingkuangJigoutongjiDataSource,
            yanglaojigouzhengtiqingkuang_jigoufenlei_dataSource,
            yanglaojigouzhengtiqingkuang_jigoudengji_dataSource,
            yanglaojigouzhengtiqingkuang_jianshetouru_PieDataSource,
            yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_DataSource,
            yanglaojigouzhengtiqingkuang_niandutouruzijin_dataSource,
            yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataSource,
            yanglaojigouzhengtizhangzheqingkuang_xueli_dataSource,
            yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_dataSource
        ],
        modules: [
            // 1
            yanglaojigoujigoutongjiqingkuang_jigoutongji_square_DisplayerModule,
            yanglaojigouzhengtiqingkuang_jigoufenlei_displayModuleControl,
            yanglaojigouzhengtiqingkuang_jigoudengji_displayModuleControl,
            yanglaojigouzhengtiqingkuangJianshetouruPieDisplayerModule,
            yanglaojigouzhengtiqingkuangXinzengchuangweizizhuDisplayerModule,
            // 6
            yanglaojigouzhengtiqingkuang_niandutouruzijin_displayModuleControl,
            yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_displayModuleControl,
            yanglaojigouzhengtizhangzheqingkuangXueliPieDisplayerModule,
            yanglaojigougongzuorenyuanzhengtiqingkuang_chizheng_displayModuleControl
        ],
        layout: layoutRight,
    }
);

const organizationDetailRightControl = createObject(
    SmartReportControl,
    {
        dataSources: [
            // 1
            yanglaojigouzhengtiqingkuangJigoutongjiDataSource,
            yanglaojigouzhengtiqingkuang_jigoufenlei_dataSource,
            yanglaojigouzhengtiqingkuang_jigoudengji_dataSource,
            yanglaojigouzhengtiqingkuang_jianshetouru_PieDataSource,
            yanglaojigouzhengtiqingkuang_xinzengchuangweizizhu_DataSource,
            // 6
            yanglaojigouzhengtiqingkuang_niandutouruzijin_dataSource,
            yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_dataSource,
            yanglaojigouzhangzheqingkuang_xueli_dataSource,
            yanglaojigougongzuorenyuanqingkuang_chizheng_dataSource,
            chuangweizonglan_DataSource,
            // 11
            yanglaojigouxiangqing_monitor_DataSource,
            yanglaojigouxiangqingCarouselDataSource,
            yanglaojigouxiangqing_jigoujieshao_DataSource
        ],
        modules: [
            // 1
            describe_DisplayerModule,
            carousel_DisplayerModule,
            monitor_DisplayerModule,
            jujiayanglaoxiangqing_liveModuleControl,
            service_introduce_DisplayerModule,
            chuangweizonglan_DisplayerModule,
            yanglaojigougongzuorenyuanzhengtiqingkuang_nannv_displayModuleControl,
            // 6
            yanglaojigouzhangzhexiangqingXueliPieDisplayerModule,
            yanglaojigougongzuorenyuanqingkuang_chizheng_displayModuleControl
        ],
        layout: layoutDetailRight
    }
);

const organizationLeftReport = createObject(
    SmartReportModuleControl,
    {
        id: '1',
        title: 'tabs1',
        group: "statisics",
        groupIndex: 0,
        containerType: ContainerType.blank,
        report: organizationLeftControl
    }
);

const organizationMiddleReport = createObject(
    SmartReportModuleControl,
    {
        id: '2',
        title: 'tabs2',
        group: "statisics",
        groupIndex: 0,
        containerType: ContainerType.blank,
        report: organizationMiddleControl
    }
);

// export const parentId = newGuid();
const organizationRightReport = createObject(
    SmartReportModuleControl,
    {
        id: "parentId",
        title: "parentId",
        containerType: ContainerType.withdialog,
        titleable: false,
        group: "statisics",
        groupIndex: 0,
        report: organizationRightControl
    }
);

const organizationDetailRightReport = createObject(
    SmartReportModuleControl,
    {
        id: "organizationDetailRightReportId",
        title: 'tabs4',
        containerType: ContainerType.blank,
        isDialog: true,
        parentContainerID: "parentId",
        group: "statisics",
        groupIndex: 0,
        report: organizationDetailRightControl,
    }
);
/**
 * react布局控制器
 */
const layoutAll = createObject(TestTabsLayoutControl);
const TestTabsControl = createObject(
    SmartReportControl,
    {
        modules: [
            organizationDetailRightReport,
            organizationRightReport,
            organizationMiddleReport,
            organizationLeftReport
        ],
        dataSources: [],
        layout: layoutAll,
        autoQuery: 1,
        areAllQuery: true,
        isAsyncQuery: false
    }
);

export { TestTabsControl };
