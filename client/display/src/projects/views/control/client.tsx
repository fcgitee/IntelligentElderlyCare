import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";

/**
 * 组件：大屏控制客户端视图状态
 */
export interface DisplayControlClientState extends BaseReactElementState {
}

/**
 * 组件：大屏控制客户端视图
 * 用于接收大屏控制信息的视图控件
 */
export class DisplayControlClient extends BaseReactElement<DisplayControlClientControl, DisplayControlClientState> {
    render() {
        return (<div>...</div>);
    }
}

/**
 * 控件：大屏控制客户端视图控制器
 * 用于接收大屏控制信息的视图控件
 */
@addon('DisplayControlClient', '大屏控制客户端视图', '用于接收大屏控制信息的视图控件')
@reactControl(DisplayControlClient)
export class DisplayControlClientControl extends BaseReactElementControl {

}