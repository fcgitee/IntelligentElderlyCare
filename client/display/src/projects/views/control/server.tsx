import { addon } from "pao-aop";
import { BaseReactElement, BaseReactElementControl, BaseReactElementState, reactControl } from "pao-aop-client";
import React from "react";

/**
 * 组件：大屏控制服务端状态
 */
export interface DisplayControlServerState extends BaseReactElementState {
}

/**
 * 组件：大屏控制服务端
 * 用于控制大屏显示的服务端
 */
export class DisplayControlServer extends BaseReactElement<DisplayControlServerControl, DisplayControlServerState> {
    render() {
        return (<div>...</div>);
    }
}

/**
 * 控件：大屏控制服务端控制器
 * 用于控制大屏显示的服务端
 */
@addon('DisplayControlServer', '大屏控制服务端', '用于控制大屏显示的服务端')
@reactControl(DisplayControlServer)
export class DisplayControlServerControl extends BaseReactElementControl {
    /** 客户端IP */
    client_host?: string;
}