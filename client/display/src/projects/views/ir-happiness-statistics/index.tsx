import { createObject, IDataService } from "pao-aop";
import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDisplayControlService } from "src/business/models/control";
import { AjaxRemoteDataQueryer, DataDisplayerModuleControl, ReportDataView, SmartReportControl, SmartReportModuleControl } from "src/business/report/smart";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { remote } from "src/projects/remote";
import { } from "../echartCommonVar";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { ListDisplayControl } from "src/business/report/displayers/nh-welfare/list-display";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { SingleBubbleControl } from "src/business/report/displayers/nh-welfare/single-bubble";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { VideoControl } from "src/business/components/buss-components/video";
import { HappinessLayoutDetailLeftControl } from "src/projects/components/layout/new-common-layout/happiness-left-detail";
import { NewCommonLayoutRightControl } from "src/projects/components/layout/new-common-layout/right";
import { NewCommonLayoutControl } from "src/projects/components/layout/new-common-layout";
import { HappinessLayoutLeftControl } from "src/projects/components/layout/new-common-layout/happiness-left";
import { iconMap, getCallCenterEle, noBeadhouse, noBlessed, noService, noServePropleServing, noServePropleFree, noElderAddr, noElderDevice } from "src/projects/util-tool";
import { LegendMenuControl } from "src/projects/components/lengend-menu";
// 幸福院统计分析总体
/**
 * 幸福院总数统计--饼图
 */
const new_com_statistics_star_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_new_com_statistics_star_quantity`,
    `new_com_statistics_star_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院总数统计--泡泡
 */
const new_sum_statistical_bubble_dataSource = new AjaxRemoteDataQueryer(
    `data_new_sum_statistical_bubble`,
    `new_sum_statistical_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院总数统计--泡泡
 */
const new_sum_statistical_bubble_dataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_bubble`,
    dataSourceID: new_sum_statistical_bubble_dataSource.id
});

/**
 * 幸福院总数统计--饼图
 */
const new_com_statistics_star_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_new_com_statistics_star_quantity`,
    dataSourceID: new_com_statistics_star_quantity_dataSource.id
});

/**
 * 幸福院总数统计--饼图
 */
let new_com_statistics_star_quantity_barOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 幸福院总数统计--泡泡
 */
const new_sum_statistical_bubbleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_bubble_pie',
        titleable: true,
        title: '幸福院总数统计',
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel",
        groupIndex: 0,
        dataViews: [new_sum_statistical_bubble_dataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 幸福院总数统计--饼图
 */
const new_com_statistics_star_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_statistics_star_quantity_displayer',
        title: '幸福院星级数量分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [new_com_statistics_star_quantity_dataView],
        displayer: createPieChartDataDisplayer(
            {
                pieNameFieldName: 'x',
                pieValueFieldName: 'y'
            },
            'new_com_statistics_star_quantity_bar_displayer',
            new_com_statistics_star_quantity_barOption
        )
    }
);

/** 
 * 幸福院排名--活动排名--表格
 */
const new_activity_ranking_DataSource = new AjaxRemoteDataQueryer(
    `data-source-new_activity_ranking`,
    `new_activity_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 幸福院排名--活动排名--表格
 */
const new_activity_ranking_DataView = createObject(ReportDataView, {
    id: `data-view-new_activity_ranking`,
    dataSourceID: new_activity_ranking_DataSource.id
});
/**
 * 幸福院排名--活动排名--表格
 */
const new_activity_rankingDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_ranking-table',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        title: '活动排名',
        dataViews: [new_activity_ranking_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/** 
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_ranking_DataSource = new AjaxRemoteDataQueryer(
    `data-source-new_eld_ranking`,
    `new_eld_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_ranking_DataView = createObject(ReportDataView, {
    id: `data-view-new_eld_ranking`,
    dataSourceID: new_eld_ranking_DataSource.id
});
/**
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_rankingDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_eld_ranking-table',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel5",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        title: '长者数量排名',
        dataViews: [new_eld_ranking_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/** 
 * 幸福小站人数--表格
 */
const new_happiness_station_people_num_DataSource = new AjaxRemoteDataQueryer(
    `data-source-new_happiness_station_people_num`,
    `new_happiness_station_people_num`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 幸福小站人数--表格
 */
const new_happiness_station_people_num_DataView = createObject(ReportDataView, {
    id: `data-view-new_happiness_station_people_num`,
    dataSourceID: new_happiness_station_people_num_DataSource.id
});
/**
 * 幸福小站人数--表格
 */
const new_happiness_station_people_numDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_happiness_station_people_num-table',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        title: '幸福小站人数',
        dataViews: [new_happiness_station_people_num_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '月份',
                        dataIndex: 'y1',
                        width: "20em",
                    },
                    {
                        title: '幸福小站名称',
                        dataIndex: 'y2',
                        width: "16em",
                    },
                    {
                        title: '参加人数',
                        dataIndex: 'y3',
                        align: "right"
                        // width: "16em",
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 活动列表--方块
 */
const new_activity_statisticalDataSource = new AjaxRemoteDataQueryer(
    `data-source-new_activity_statistical`,
    `new_activity_statistical`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--方块
 */
const new_activity_statisticalDataView = createObject(ReportDataView, {
    id: `data-view-new_activity_statistical`,
    dataSourceID: new_activity_statisticalDataSource.id
});

/**
 * 活动列表--方块
 */
const new_activity_statistical_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_statistical_module',
        titleable: true,
        title: "活动统计",
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel",
        groupIndex: 0,
        containerType: ContainerType.frameContain,
        dataViews: [new_activity_statisticalDataView],
        displayer: createObject(
            MoreSquaresControl,
            {
                // layout: "horizontal",
                square_span: 12,
                name_fields: ['活动次数', '参与人数'],
                value_field: 'y',
            }
        )
    }
);

/** 
 * 活动列表--列表
 */
const new_activity_list_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_list`,
    `new_activity_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 活动列表--列表
 */
const new_activity_list_DataView = createObject(ReportDataView, {
    id: `data_view_new_activity_list`,
    dataSourceID: new_activity_list_DataSource.id
});
/**
 * 活动列表--列表
 */
const new_activity_listDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_list-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '活动列表',
        drillDialogModules: ["new_video_monitoring_displayer"],
        dataViews: [new_activity_list_DataView],
        groupIndex: 0,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel",
        displayer: createObject(
            ListDisplayControl,
            {
                title: '活动列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2'],
                height_content: 900

            }
        )
    }
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_service_products_list`,
    `new_org_detail_service_products_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listDataView = createObject(ReportDataView, {
    id: `data_view_new_org_detail_service_products_list`,
    dataSourceID: new_org_detail_service_products_listDataSource.id
});

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_service_products_list',
        titleable: true,
        title: '服务清单',
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel3",
        groupIndex: 2,
        dataViews: [new_org_detail_service_products_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务清单',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2'],
                height_content: 900

            }
        )
    }
);

/**
 * 长者教育课程列表--表格
 */
const new_com_statistics_time_education_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_statistics_time_education_quantity`,
    `new_com_statistics_time_education_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者教育课程列表--表格
 */
const new_com_statistics_time_education_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_new_com_statistics_time_education_quantity`,
    dataSourceID: new_com_statistics_time_education_quantity_DataSource.id
});

/**
 * 长者教育课程列表--表格
 */
const new_com_detail_service_productsDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_detail_service_products-table',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        title: '服务产品介绍',
        dataViews: [new_com_statistics_time_education_quantity_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '幸福院名称',
                        dataIndex: 'y1',
                        width: "16em",
                    },
                    {
                        title: '课程名称',
                        dataIndex: 'y2',
                        width: "16em",
                    },
                    {
                        title: '开课时间',
                        dataIndex: 'y3',
                        width: "12em",
                    },
                    {
                        title: '报名人数',
                        dataIndex: 'y4',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 参与活动与教育人数趋势--折线图
 */
const new_activity_and_education_people_num_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_and_education_people_num`,
    `new_activity_and_education_people_num`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 参与活动与教育人数趋势--折线图
 */
const new_activity_and_education_people_num_DataView = createObject(ReportDataView, {
    id: `data_view_new_activity_and_education_people_num`,
    dataSourceID: new_activity_and_education_people_num_DataSource.id
});

/**
 * 参与活动与教育人数趋势--折线图
 */
let new_activity_and_education_people_num_multilineOption: EChartsOptions = {
    legend: {
        show: true,
        top: "1%"
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者数/万人',
    },
    series: [
        { name: "活动", type: 'line' }, { name: "教育", type: 'line' }
    ],
};

/**
 * 参与活动与教育人数趋势--折线图
 */
const new_activity_and_education_people_num_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_and_education_people_num-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "参与活动与教育人数趋势",
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel2",
        groupIndex: 1,
        dataViews: [new_activity_and_education_people_num_DataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2']
            },
            'new_activity_and_education_people_num-multi-line-displayer',
            new_activity_and_education_people_num_multilineOption)
    }
);

/**
 * 长者饭堂人数趋势--折线图
 */
const new_com_statistics_time_meals_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_statistics_time_meals_quantity`,
    `new_com_statistics_time_meals_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者饭堂人数趋势--折线图
 */
const new_com_statistics_time_meals_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_new_com_statistics_time_meals_quantity`,
    dataSourceID: new_com_statistics_time_meals_quantity_DataSource.id
});

/**
 * 长者饭堂人数趋势--折线图
 */
let new_com_statistics_time_meals_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '人',
    },
    series: [{
        type: "line"
    }]
};

/**
 * 长者饭堂人数趋势--折线图
 */
const new_com_statistics_time_meals_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_statistics_time_meals_quantity_multi_line',
        title: '长者饭堂用餐人数分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel4",
        groupIndex: 3,
        dataViews: [new_com_statistics_time_meals_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_com_statistics_time_meals_quantity_multi_line_displayer',
            new_com_statistics_time_meals_quantity_multilineOption
        )
    }
);

/**
 * 长者饭堂服务人次排名--表格
 */
const new_statistics_meals_service_person_time_ranking_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_statistics_meals_service_person_time_ranking`,
    `new_statistics_meals_service_person_time_ranking`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者饭堂服务人次排名--表格
 */
const new_statistics_meals_service_person_time_ranking_DataView = createObject(ReportDataView, {
    id: `data_view_new_statistics_meals_service_person_time_ranking`,
    dataSourceID: new_statistics_meals_service_person_time_ranking_DataSource.id
});

/**
 * 长者饭堂服务人次排名--表格
 */
const new_statistics_meals_service_person_time_rankingDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_statistics_meals_service_person_time_ranking-table',
        titleable: true,
        title: "长者饭堂服务人次排名",
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [new_statistics_meals_service_person_time_ranking_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '月份',
                        dataIndex: 'y1',
                        width: "16em",
                    },
                    {
                        title: '长者饭堂所在幸福院',
                        dataIndex: 'y2',
                        width: "16em",
                    },
                    {
                        title: '参加人数',
                        dataIndex: 'y3',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 机构养老--福利院位置--数据源--星空图
 */
const new_happiness_courtyard_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_happiness_courtyard_map`,
    `new_happiness_courtyard_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构养老--福利院位置--数据视图--星空图
 */
const new_happiness_courtyard_mapDataView = createObject(ReportDataView, {
    id: `data_view_new_happiness_courtyard_map`,
    dataSourceID: new_happiness_courtyard_mapDataSource.id
});

// /** 呼叫中心弹窗样式 */
// function getEle(data: any) {
//     let div = document.createElement("div");
//     div.style.backgroundColor = "rgba(135,135,135,0.5)";
//     div.style.color = "#fff";
//     div.style.padding = "40px";
//     div.style.fontSize = "80px";
//     div.style.width = "1500px";
//     div.style.boxShadow = "0px 4px 16px 0px rgba(0,131,197,0.5)";
//     div.style.borderRadius = "16px 16px 16px 0px";
//     div.style.border = "4px solid rgba(255,255,255,1)";

//     let content = document.createElement('div');
//     let nameAndAddressDiv = document.createElement('div');
//     let sexAndAgeDiv = document.createElement('div');
//     let phoneDiv = document.createElement('div');
//     let pthoneTypeDiv = document.createElement('div');
//     pthoneTypeDiv.innerText = data.phone_type;

//     nameAndAddressDiv.style.display = "flex";
//     sexAndAgeDiv.style.display = "flex";

//     content.style.marginBottom = '40px';

//     let nameDiv = document.createElement("div");

//     nameDiv.innerText = data.name;
//     nameDiv.style.marginRight = "40px";

//     let adress = document.createElement("div");
//     adress.innerText = data.address;
//     nameAndAddressDiv.appendChild(nameDiv);
//     nameAndAddressDiv.appendChild(adress);
//     let sex = document.createElement('div');
//     sex.innerText = data.sex;
//     sex.style.marginRight = "40px";

//     let age = document.createElement('div');
//     age.innerText = `${data.age}岁`;

//     sexAndAgeDiv.appendChild(sex);
//     sexAndAgeDiv.appendChild(age);

//     let phone = document.createElement('div');
//     phone.innerText = data.phone;
//     phoneDiv.appendChild(phone);

//     content.appendChild(nameAndAddressDiv);
//     content.appendChild(sexAndAgeDiv);
//     content.appendChild(phoneDiv);
//     content.appendChild(pthoneTypeDiv);

//     div.appendChild(content);
//     return div;
// }

let option: EChartsOptions = {
    legend: {
        selected: {
            "养老机构": false,
            "社区幸福院": true,
            "居家服务商": false,
            "服务人员_服务中": false,
            "服务人员_空闲": false,
            "长者_住址": false,
            "长者_设备": false
        },
        data: ["养老机构", "社区幸福院", "居家服务商", "服务人员_服务中", "服务人员_空闲", "长者_住址", "长者_设备"]
    },
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};

/**
 * 构养老--福利院位置--星空图
 */
const new_happiness_courtyard_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_happiness_courtyard_map',
        titleable: false,
        dataViews: [new_happiness_courtyard_mapDataView],
        drillDialogModules: ["leftDetailControl", "rightDetailControl"],
        containerType: ContainerType.withdialog,
        placement: 'bottom',
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'longitude',
                latitudeFieldName: 'latitude',
                callLatitudeFieldName: "y",
                callLongitudeFieldName: "x",
                nameFieldName: "name",
                ponitIconSizeFieldName: "num",
                getElementFn: getCallCenterEle,
                scatterObject: iconMap
            },
            "new_happiness_courtyard_map-modules",
            option
        )
    }
);

/**
 * 视频播放--视频
 */
const new_video_monitoringDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_video_monitoring`,
    `new_video_monitoring`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 视频播放--视频
 */
const new_video_monitoringDataView = createObject(ReportDataView, {
    id: `data_view_new_video_monitoring`,
    dataSourceID: new_video_monitoringDataSource.id
});

/**
 * 视频播放--视频
 */
const org_detail_video_surveillanceDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_video_monitoring_displayer',
        titleable: true,
        title: '视频播放',
        // groupType: GroupContainerType.carousel,
        // group: "happiness-right-carousel6",
        // groupIndex: 5,
        isDialog: true,
        parentContainerID: new_happiness_courtyard_mapModuleControl.id,
        dataViews: [new_video_monitoringDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "/build/upload/video/test.mp4",
                    type: "auto",
                    autoplay: true,
                    hotkey: false
                },
                width: 3720,
                height: 2800
            }
        )
    }
);

/** 单家 */
/**
 * 幸福院总数统计--泡泡（大）
 */
const new_sum_statistical_bubble_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_sum_statistical_bubble_happiness_single`,
    `new_sum_statistical_bubble_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院总数统计--星级饼图--饼图
 */
const new_com_statistics_star_quantity_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_statistics_star_quantity_happiness_single`,
    `new_com_statistics_star_quantity_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院排名--活动排名--表格
 */
const new_activity_ranking_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_ranking_happiness_single`,
    `new_activity_ranking_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_ranking_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_eld_ranking_happiness_single`,
    `new_eld_ranking_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院课程列表--表格
 */
const new_com_statistics_time_education_quantity_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_statistics_time_education_quantity_happiness_single`,
    `new_com_statistics_time_education_quantity_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statistical_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_statistical_happiness_single`,
    `new_activity_statistical_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 活动列表--卡片
 */
const new_activity_list_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_activity_list_happiness_single`,
    `new_activity_list_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 图片轮播--图片轮播
 */
const new_detail_picture_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_detail_picture_list`,
    `new_detail_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务人员列表--列表
 */
const new_servicer_listDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_servicer_list`,
    `new_servicer_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 介绍--自定义组件
 */
const new_detail_happinessDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_detail_happiness`,
    `new_detail_happiness`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_list_happiness_singleDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_org_detail_service_products_list_happiness_single`,
    `new_org_detail_service_products_list_happiness_single`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 监控--图片轮播
 */
const new_com_detail_video_surveillanceDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_com_detail_video_surveillance`,
    `new_com_detail_video_surveillance`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 幸福院总数统计--泡泡（小）
 */
const new_sum_statistical_bubble_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_sum_statistical_bubble_happiness_single`,
    dataSourceID: new_sum_statistical_bubble_happiness_singleDataSource.id
});

/**
 * 幸福院总数统计--星级饼图--饼图
 */
const new_com_statistics_star_quantity_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_com_statistics_star_quantity_happiness_single`,
    dataSourceID: new_com_statistics_star_quantity_happiness_singleDataSource.id
});

/**
 * 幸福院排名--活动排名--表格
 */
const new_activity_ranking_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_ranking_happiness_single`,
    dataSourceID: new_activity_ranking_happiness_singleDataSource.id
});

/**
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_ranking_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_eld_ranking_happiness_single`,
    dataSourceID: new_eld_ranking_happiness_singleDataSource.id
});

/**
 * 幸福院课程列表--表格
 */
const new_com_statistics_time_education_quantity_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_com_statistics_time_education_quantity_happiness_single`,
    dataSourceID: new_com_statistics_time_education_quantity_happiness_singleDataSource.id
});

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statistical_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_statistical_happiness_single`,
    dataSourceID: new_activity_statistical_happiness_singleDataSource.id
});

/**
 * 活动列表--卡片
 */
const new_activity_list_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_activity_list_happiness_single`,
    dataSourceID: new_activity_list_happiness_singleDataSource.id
});

/**
 * 服务人员列表--列表
 */
const new_servicer_listDataView = createObject(ReportDataView, {
    id: `data_view_new_servicer_list`,
    dataSourceID: new_servicer_listDataSource.id
});

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_list_happiness_singleDataView = createObject(ReportDataView, {
    id: `data_view_new_org_detail_service_products_list_happiness_single`,
    dataSourceID: new_org_detail_service_products_list_happiness_singleDataSource.id
});

/**
 * 图片轮播--图片轮播
 */
const new_detail_picture_listDataView = createObject(ReportDataView, {
    id: `data_view_new_detail_picture_list`,
    dataSourceID: new_detail_picture_listDataSource.id
});

/**
 * 介绍--自定义组件
 */
const new_detail_happinessDataView = createObject(ReportDataView, {
    id: `data_view_new_detail_happiness`,
    dataSourceID: new_detail_happinessDataSource.id
});

/**
 * 监控--图片轮播
 */
const new_com_detail_video_surveillanceDataView = createObject(ReportDataView, {
    id: `data_view_new_com_detail_video_surveillance`,
    dataSourceID: new_com_detail_video_surveillanceDataSource.id
});

/**
 * 幸福院总数统计--星级饼图--饼图
 */
let new_com_statistics_star_quantity_happiness_singleOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 养老院总数统计--泡泡（小）
 */
const new_sum_statistical_bubble_happiness_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_sum_statistical_bubble_happiness_single_pie',
        titleable: true,
        title: '养老院总数统计',
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel",
        groupIndex: 0,
        dataViews: [new_sum_statistical_bubble_happiness_singleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 幸福院总数统计--星级饼图--饼图
 */
const new_com_statistics_star_quantity_happiness_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_statistics_star_quantity_happiness_single_pie',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel2",
        title: '幸福院总数统计',
        groupIndex: 1,
        dataViews: [new_com_statistics_star_quantity_happiness_singleDataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_com_statistics_star_quantity_happiness_singledisplayer',
            new_com_statistics_star_quantity_happiness_singleOption
        )
    }
);

/**
 * 幸福院排名--活动排名--表格
 */
const new_activity_ranking_happiness_singleDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_ranking_happiness_single-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '幸福院排名--活动排名',
        dataViews: [new_activity_ranking_happiness_singleDataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel3",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '幸福院名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '活动次数',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 幸福院排名--长者数量排名--表格
 */
const new_eld_ranking_happiness_singleDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_eld_ranking_happiness_single-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '幸福院排名--长者数量排名',
        dataViews: [new_eld_ranking_happiness_singleDataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel4",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '幸福院名称',
                        dataIndex: 'y1',
                        align: 'left'
                    },
                    {
                        title: '长者数量',
                        dataIndex: 'y2',
                        align: 'right'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 幸福院课程列表--表格
 */
const new_com_statistics_time_education_quantity_happiness_singleDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_statistics_time_education_quantity_happiness_single-table',
        titleable: true,
        containerType: ContainerType.frameContain,
        title: '幸福院课程列表',
        dataViews: [new_com_statistics_time_education_quantity_happiness_singleDataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "happiness-left-carousel5",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '幸福院名称',
                        dataIndex: 'y1',
                        align: 'center'
                    },
                    {
                        title: '课程名称',
                        dataIndex: 'y2',
                        align: 'center'
                    },
                    {
                        title: '开课时间',
                        dataIndex: 'y3',
                        align: 'center'
                    },
                    {
                        title: '报名人数',
                        dataIndex: 'y4',
                        align: 'center'
                    }
                ],
                maxHeigh: 900
            }
        )
    }
);

/**
 * 活动列表--统计数据--卡片
 */
const new_activity_statistical_happiness_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_statistical_happiness_single-num',
        title: '活动列表--统计数据',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel",
        groupIndex: 0,
        dataViews: [new_activity_statistical_happiness_singleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                // layout: "horizontal",
                square_span: 12,
                name_fields: ['活动次数', '参与人数'],
                value_field: 'y',
            }
        )
    }
);

/**
 * 活动列表--卡片
 */
const new_activity_list_happiness_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_activity_list_happiness_single_module',
        titleable: true,
        title: '活动列表',
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel",
        groupIndex: 0,
        dataViews: [new_activity_list_happiness_singleDataView],
        drillDialogModules: ["new_video_monitoring_displayer"],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '活动列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);

/**
 * 图片轮播--图片轮播
 */
const new_detail_picture_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_detail_picture_list-carousel',
        titleable: true,
        title: '图片轮播',
        containerType: ContainerType.frameContain,
        dataViews: [new_detail_picture_listDataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel2",
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

/**
 * 服务人员列表--列表
 */
const new_servicer_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_servicer_list_module',
        titleable: true,
        title: '服务人员列表',
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel3",
        groupIndex: 2,
        dataViews: [new_servicer_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务人员列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']
            }
        )
    }
);

/**
 * 介绍--介绍组件
 */
const new_detail_happinessDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_detail_happiness_centre',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel4",
        title: '幸福院介绍',
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [new_detail_happinessDataView],
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['名称', '地址', '评级情况', '机构类型', '机构性质', '法定代表人', '联系电话'],
                max_height: 900
            }
        )
    }
);

/**
 * 服务产品列表--列表
 */
const new_org_detail_service_products_list_happiness_singleModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_org_detail_service_products_list_happiness_single_module',
        titleable: true,
        title: '服务产品列表',
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel5",
        groupIndex: 4,
        dataViews: [new_org_detail_service_products_list_happiness_singleDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务产品列表',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']
            }
        )
    }
);

/**
 * 监控--视频
 */
const new_com_detail_video_surveillanceModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_com_detail_video_surveillance_displayer',
        titleable: true,
        title: '监控',
        groupType: GroupContainerType.carousel,
        group: "happiness-right-carousel6",
        groupIndex: 5,
        dataViews: [new_com_detail_video_surveillanceDataView],
        containerType: ContainerType.frameContain,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://183.235.223.56:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    hotkey: false
                },
                width: 1830,
                height: 928,
                margin: 'auto'
            }
        )
    }
);

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                autoQuery: 1,
                layout: layoutSelect,
            }
        )
    }
);

/** 图例配置 */
const legendOption = [
    {
        title: "养老机构",
        key: ["养老机构"],
        icon: noBeadhouse,
        selected: false
    },
    {
        title: "社区幸福院",
        key: ["社区幸福院"],
        icon: noBlessed,
        selected: true
    },
    {
        title: "居家服务商",
        key: ["居家服务商"],
        icon: noService,
        selected: false
    },
    {
        title: "服务人员_服务中",
        key: ["服务人员_服务中"],
        icon: noServePropleServing,
        selected: false
    },
    {
        title: "服务人员_空闲",
        key: ["服务人员_空闲"],
        icon: noServePropleFree,
        selected: false
    },
    {
        title: "长者_住址",
        key: ["长者_住址"],
        icon: noElderAddr,
        selected: false
    },
    {
        title: "长者_设备",
        key: ["长者_设备"],
        icon: noElderDevice,
        selected: false
    }
];

/** 
 * 图例控制器
 */
const legendControl = createObject(
    LegendMenuControl,
    {
        display_control: new_happiness_courtyard_mapModuleControl.displayer,
        option: legendOption
    }
);

const layoutLeft = createObject(HappinessLayoutLeftControl);
const layoutMiddle = createObject(
    CommonLayoutMiddleControl,
    {
        legend_control: legendControl
    }
);

const layoutRight = createObject(NewCommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        id: "happiness-leftControl",
        containerType: ContainerType.withdialog,
        placement: 'left',
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_sum_statistical_bubble_dataSource,
                    new_com_statistics_star_quantity_dataSource,
                    new_activity_ranking_DataSource,
                    new_eld_ranking_DataSource,
                    new_happiness_station_people_num_DataSource
                ],
                modules: [
                    // 0
                    new_sum_statistical_bubbleModuleControl,
                    // 2
                    new_com_statistics_star_quantity_displayModuleControl,
                    // 3
                    new_activity_rankingDisplayerModule,
                    // 4
                    new_eld_rankingDisplayerModule,
                    // 5
                    new_happiness_station_people_numDisplayerModule
                ],
                layout: layoutLeft,
                defaultParams: { type: '幸福院' },
                autoQuery: 1
            }
        )
    });

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_happiness_courtyard_mapDataSource,
                    new_video_monitoringDataSource
                ],
                modules: [
                    new_happiness_courtyard_mapModuleControl,
                    org_detail_video_surveillanceDialogModuleControl
                ],
                layout: layoutMiddle,
                autoQuery: 5000,
            }
        )
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        id: "happiness-rightControl",
        containerType: ContainerType.withdialog,
        titleable: false,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_activity_statisticalDataSource,
                    new_activity_list_DataSource,
                    new_activity_and_education_people_num_DataSource,
                    new_org_detail_service_products_listDataSource,
                    new_com_statistics_time_education_quantity_DataSource,
                    new_com_statistics_time_meals_quantity_DataSource,
                    new_statistics_meals_service_person_time_ranking_DataSource
                ],
                modules: [
                    // 1
                    new_activity_statistical_DisplayerModule,
                    new_activity_listDisplayerModule,
                    // 2
                    new_activity_and_education_people_num_multilineDisplayerModule,
                    // 3
                    new_org_detail_service_products_listModuleControl,
                    // 4
                    new_com_statistics_time_meals_quantity_displayModuleControl,
                    // 5
                    new_com_detail_service_productsDisplayerModule,
                    // 6
                    new_statistics_meals_service_person_time_rankingDisplayerModule
                ],
                layout: layoutRight,
                defaultParams: { type: '幸福院' },
                autoQuery: 1
            }
        )
    }
);

const layoutDetailLeft = createObject(HappinessLayoutDetailLeftControl);
const layoutDetailRight = createObject(NewCommonLayoutRightControl);

const leftDetailControl = createObject(
    SmartReportModuleControl,
    {
        id: "leftDetailControl",
        isDialog: true,
        parentContainerID: "happiness-leftControl",
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_sum_statistical_bubble_happiness_singleDataSource,
                    new_com_statistics_star_quantity_happiness_singleDataSource,
                    new_activity_ranking_happiness_singleDataSource,
                    new_eld_ranking_happiness_singleDataSource,
                    new_com_statistics_time_education_quantity_happiness_singleDataSource
                ],
                modules: [
                    // 0
                    new_sum_statistical_bubble_happiness_singleModuleControl,
                    // 1
                    new_com_statistics_star_quantity_happiness_singleModuleControl,
                    // 2
                    new_activity_ranking_happiness_singleDisplayerModule,
                    // 3
                    new_eld_ranking_happiness_singleDisplayerModule,
                    // 4
                    new_com_statistics_time_education_quantity_happiness_singleDisplayerModule,
                    // 5
                ],
                layout: layoutDetailLeft,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "幸福院" },
                autoQuery: 1
            }
        )
    }
);

const rightDetailControl = createObject(
    SmartReportModuleControl,
    {
        id: "rightDetailControl",
        containerType: ContainerType.blank,
        isDialog: true,
        parentContainerID: "happiness-rightControl",
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_activity_statistical_happiness_singleDataSource,
                    new_activity_list_happiness_singleDataSource,
                    new_detail_picture_listDataSource,
                    new_servicer_listDataSource,
                    new_detail_happinessDataSource,
                    new_org_detail_service_products_list_happiness_singleDataSource,
                    new_com_detail_video_surveillanceDataSource,
                ],
                modules: [
                    // 0
                    new_activity_statistical_happiness_singleModuleControl,
                    new_activity_list_happiness_singleModuleControl,
                    // 1
                    new_detail_picture_listModuleControl,
                    // 2
                    new_servicer_listModuleControl,
                    // 3
                    new_detail_happinessDisplayerModule,
                    // 4
                    new_org_detail_service_products_list_happiness_singleModuleControl,
                    // 5
                    new_com_detail_video_surveillanceModuleControl
                ],
                layout: layoutDetailRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5", type: "幸福院" },
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(NewCommonLayoutControl);
const irHappinessStatisticsControl = createObject(
    SmartReportControl,
    {
        id: "irWelfareCentre",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightControl, leftDetailControl, rightDetailControl],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        isMain: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irHappinessStatisticsControl };
