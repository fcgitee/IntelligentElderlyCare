import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, ReportDataView, AjaxRemoteDataQueryer } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { VideoControl } from "src/business/components/buss-components/video";

/** 综合-服务评价（TOP10） */
const cop_statistics_item_evaluate_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_item_evaluate_top10_quantity`,
    `cop_statistics_item_evaluate_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务现场视频-摄像头视频 */
const cop_detail_live_video_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_detail_live_video`,
    `cop_detail_live_video`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价（TOP10） */
const cop_statistics_item_evaluate_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_item_evaluate_top10_quantity`,
    dataSourceID: cop_statistics_item_evaluate_top10_quantity_dataSource.id
});

/** 综合-服务现场视频-摄像头视频 */
const cop_detail_live_video_dataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_live_video`,
    dataSourceID: cop_detail_live_video_dataSource.id
});

/** 综合-服务评价（TOP10） */
let cop_statistics_item_evaluate_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务项目'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价（TOP10） */
const cop_statistics_item_evaluate_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_item_evaluate_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_item_evaluate_top10_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_item_evaluate_top10_quantity_bar_displayer',
            cop_statistics_item_evaluate_top10_quantity_barOption
        )
    }
);

/**
 * 综合-服务现场视频--摄像头视频
 */
const cop_detail_live_videoModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_live_video_pie',
        titleable: false,
        dataViews: [cop_detail_live_video_dataView],
        containerType: ContainerType.frameContain,
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://localhost:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 1800,
                height: 1200
            }
        )
    }
);

/** 综合-服务评价（TOP10） */
const cop_statistics_org_evaluate_top11_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_org_evaluate_top11_quantity`,
    `cop_statistics_org_evaluate_top11_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-任务服务商卡片--自定义控件 */
const cop_detail_service_provider_card_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_detail_service_provider_card`,
    `cop_detail_service_provider_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-任务服务人员卡片--自定义控件 */
const cop_detail_service_personal_card_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_detail_service_personal_card`,
    `cop_detail_service_personal_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-任务对象卡片--自定义控件 */
const cop_detail_task_card_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_detail_task_card`,
    `cop_detail_task_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-当前服务卡片--自定义控件 */
const cop_detail_service_card_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_detail_service_card`,
    `cop_detail_service_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价（TOP10） */
const cop_statistics_org_evaluate_top11_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_org_evaluate_top11_quantity`,
    dataSourceID: cop_statistics_org_evaluate_top11_quantity_dataSource.id
});

/** 综合-任务服务商卡片--自定义控件 */
const cop_detail_service_provider_card_dataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_service_provider_card`,
    dataSourceID: cop_detail_service_provider_card_dataSource.id
});

/** 综合-任务服务人员卡片--自定义控件 */
const cop_detail_service_personal_card_dataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_service_personal_card`,
    dataSourceID: cop_detail_service_personal_card_dataSource.id
});

/** 综合-任务对象卡片--自定义控件 */
const cop_detail_task_card_dataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_task_card`,
    dataSourceID: cop_detail_task_card_dataSource.id
});

/** 综合-当前服务卡片--自定义控件 */
const cop_detail_service_card_dataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_service_card`,
    dataSourceID: cop_detail_service_card_dataSource.id
});

/** 综合-服务评价（TOP10） */
let cop_statistics_org_evaluate_top11_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价（TOP10） */
const cop_statistics_org_evaluate_top11_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_org_evaluate_top11_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_org_evaluate_top11_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_org_evaluate_top11_quantity_bar_displayer',
            cop_statistics_org_evaluate_top11_quantity_barOption
        )
    }
);

/** 综合-任务服务商卡片--自定义控件 */
const cop_detail_service_provider_card_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_service_provider_card_displayer',
        titleable: true,
        title: '任务服务商卡片',
        containerType: ContainerType.frameContain,
        dataViews: [cop_detail_service_provider_card_dataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4'],
                titleName: ['图片', '名称', '联系方式', '级别']
            }
        )
    }
);

/** 综合-任务服务人员卡片--自定义控件 */
const cop_detail_service_personal_card_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_service_personal_card_displayer',
        titleable: true,
        title: '任务服务人员卡片',
        containerType: ContainerType.frameContain,
        dataViews: [cop_detail_service_personal_card_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5'],
                titleName: ['照片', '姓名', '联系方式', '级别', '当前经纬度']
            }
        )
    }
);

/** 综合-任务对象卡片--自定义控件 */
const cop_detail_task_card_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_task_card_displayer',
        titleable: true,
        title: '任务对象卡片',
        containerType: ContainerType.frameContain,
        dataViews: [cop_detail_task_card_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7', 'y8'],
                titleName: ['照片', '姓名', '联系方式', '行为能力', '当前经纬度', '主家属姓名', '主家属关系', '主家属联系方式']
            }
        )
    }
);

/** 综合-当前服务卡片--自定义控件 */
const cop_detail_service_card_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_service_card_displayer',
        titleable: true,
        title: '当前服务卡片',
        containerType: ContainerType.frameContain,
        dataViews: [cop_detail_service_card_dataView],
        groupIndex: 0,
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['任务编号', '服务产品', '服务地点', '服务时间', '服务商', '服务对象', '服务人员']
            }
        )
    }
);

/** 综合-服务评价（TOP12） */
const cop_statistics_service_personal_evaluate_top12_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_service_personal_evaluate_top12_quantity`,
    `cop_statistics_service_personal_evaluate_top12_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价（TOP12） */
const cop_statistics_service_personal_evaluate_top12_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_service_personal_evaluate_top12_quantity`,
    dataSourceID: cop_statistics_service_personal_evaluate_top12_quantity_dataSource.id
});

/** 综合-服务评价（TOP12） */
let cop_statistics_service_personal_evaluate_top12_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价（TOP12） */
const cop_statistics_service_personal_evaluate_top12_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_service_personal_evaluate_top12_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_service_personal_evaluate_top12_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_service_personal_evaluate_top12_quantity_bar_displayer',
            cop_statistics_service_personal_evaluate_top12_quantity_barOption
        )
    }
);

/** 综合-服务评价（TOP10） */
const cop_statistics_elder_evaluate_top13_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_elder_evaluate_top13_quantity`,
    `cop_statistics_elder_evaluate_top13_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价（TOP10） */
const cop_statistics_elder_evaluate_top13_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_evaluate_top13_quantity`,
    dataSourceID: cop_statistics_elder_evaluate_top13_quantity_dataSource.id
});

/** 综合-服务评价（TOP10） */
let cop_statistics_elder_evaluate_top13_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价（TOP10） */
const cop_statistics_elder_evaluate_top13_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_evaluate_top13_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_elder_evaluate_top13_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_elder_evaluate_top13_quantity_bar_displayer',
            cop_statistics_elder_evaluate_top13_quantity_barOption
        )
    }
);

/** 综合-服务评价 */
const cop_statistics_age_evaluate_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_age_evaluate_quantity`,
    `cop_statistics_age_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价 */
const cop_statistics_age_evaluate_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_age_evaluate_quantity`,
    dataSourceID: cop_statistics_age_evaluate_quantity_dataSource.id
});

/** 综合-服务评价 */
let cop_statistics_age_evaluate_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价 */
const cop_statistics_age_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_age_evaluate_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_age_evaluate_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_age_evaluate_quantity_bar_displayer',
            cop_statistics_age_evaluate_quantity_barOption
        )
    }
);

/** 综合-服务评价 */
const cop_statistics_street_evaluate_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_street_evaluate_quantity`,
    `cop_statistics_street_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务评价 */
const cop_statistics_street_evaluate_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_street_evaluate_quantity`,
    dataSourceID: cop_statistics_street_evaluate_quantity_dataSource.id
});

/** 综合-服务评价 */
let cop_statistics_street_evaluate_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务评价'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务评价 */
const cop_statistics_street_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_street_evaluate_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_street_evaluate_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_street_evaluate_quantity_bar_displayer',
            cop_statistics_street_evaluate_quantity_barOption
        )
    }
);

/** 综合-服务次数（TOP10） */
const cop_statistics_item_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_item_top10_quantity`,
    `cop_statistics_item_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数（TOP10） */
const cop_statistics_item_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_item_top10_quantity`,
    dataSourceID: cop_statistics_item_top10_quantity_dataSource.id
});

/** 综合-服务次数（TOP10） */
let cop_statistics_item_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务项目'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数（TOP10） */
const cop_statistics_item_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_item_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_item_top10_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_item_top10_quantity_bar_displayer',
            cop_statistics_item_top10_quantity_barOption
        )
    }
);

/** 综合-服务次数（TOP10） */
const cop_statistics_org_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_org_top10_quantity`,
    `cop_statistics_org_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数（TOP10） */
const cop_statistics_org_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_org_top10_quantity`,
    dataSourceID: cop_statistics_org_top10_quantity_dataSource.id
});

/** 综合-服务次数（TOP10） */
let cop_statistics_org_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数（TOP10） */
const cop_statistics_org_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_org_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_org_top10_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_org_top10_quantity_bar_displayer',
            cop_statistics_org_top10_quantity_barOption
        )
    }
);

/** 综合-服务次数（TOP10） */
const cop_statistics_service_personal_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_service_personal_top10_quantity`,
    `cop_statistics_service_personal_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数（TOP10） */
const cop_statistics_service_personal_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_service_personal_top10_quantity`,
    dataSourceID: cop_statistics_service_personal_top10_quantity_dataSource.id
});

/** 综合-服务次数（TOP10） */
let cop_statistics_service_personal_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数（TOP10） */
const cop_statistics_service_personal_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_service_personal_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_service_personal_top10_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_service_personal_top10_quantity_bar_displayer',
            cop_statistics_service_personal_top10_quantity_barOption
        )
    }
);

/** 综合-服务次数（TOP10） */
const cop_statistics_elder_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_elder_top10_quantity`,
    `cop_statistics_elder_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数（TOP10） */
const cop_statistics_elder_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_top10_quantity`,
    dataSourceID: cop_statistics_elder_top10_quantity_dataSource.id
});

/** 综合-服务次数（TOP10） */
let cop_statistics_elder_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数（TOP10） */
const cop_statistics_elder_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_elder_top10_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_elder_top10_quantity_bar_displayer',
            cop_statistics_elder_top10_quantity_barOption
        )
    }
);

/** 综合-服务次数 */
const cop_statistics_elder_age_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_elder_age_quantity`,
    `cop_statistics_elder_age_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数 */
const cop_statistics_elder_age_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_age_quantity`,
    dataSourceID: cop_statistics_elder_age_quantity_dataSource.id
});

/** 综合-服务次数 */
let cop_statistics_elder_age_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数 */
const cop_statistics_elder_age_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_age_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_elder_age_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_elder_age_quantity_bar_displayer',
            cop_statistics_elder_age_quantity_barOption
        )
    }
);

/** 综合-服务次数 */
const cop_statistics_street_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_street_quantity`,
    `cop_statistics_street_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务次数 */
const cop_statistics_street_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_street_quantity`,
    dataSourceID: cop_statistics_street_quantity_dataSource.id
});

/** 综合-服务次数 */
let cop_statistics_street_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务次数 */
const cop_statistics_street_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_street_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_street_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_street_quantity_bar_displayer',
            cop_statistics_street_quantity_barOption
        )
    }
);

/** 综合-服务费用（TOP10） */
const cop_statistics_item_fee_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_item_fee_top10_quantity`,
    `cop_statistics_item_fee_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务费用（TOP10） */
const cop_statistics_item_fee_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_item_fee_top10_quantity`,
    dataSourceID: cop_statistics_item_fee_top10_quantity_dataSource.id
});

/** 综合-服务费用（TOP10） */
let cop_statistics_item_fee_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务项目'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务费用（TOP10） */
const cop_statistics_item_fee_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_item_fee_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_item_fee_top10_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_item_fee_top10_quantity_bar_displayer',
            cop_statistics_item_fee_top10_quantity_barOption
        )
    }
);

/** 综合-服务费用（TOP10） */
const cop_statistics_org_fee_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_org_fee_top10_quantity`,
    `cop_statistics_org_fee_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务费用（TOP10） */
const cop_statistics_org_fee_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_org_fee_top10_quantity`,
    dataSourceID: cop_statistics_org_fee_top10_quantity_dataSource.id
});

/** 综合-服务费用（TOP10） */
let cop_statistics_org_fee_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务商'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务费用（TOP10） */
const cop_statistics_org_fee_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_org_fee_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_org_fee_top10_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_org_fee_top10_quantity_bar_displayer',
            cop_statistics_org_fee_top10_quantity_barOption
        )
    }
);

/** 综合-服务收入（TOP10） */
const cop_statistics_service_personal_fee_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_service_personal_fee_top10_quantity`,
    `cop_statistics_service_personal_fee_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务收入（TOP10） */
const cop_statistics_service_personal_fee_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_service_personal_fee_top10_quantity`,
    dataSourceID: cop_statistics_service_personal_fee_top10_quantity_dataSource.id
});

/** 综合-服务收入（TOP10） */
let cop_statistics_service_personal_fee_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '服务人员'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务收入'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务收入（TOP10） */
const cop_statistics_service_personal_fee_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_service_personal_fee_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_service_personal_fee_top10_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_service_personal_fee_top10_quantity_bar_displayer',
            cop_statistics_service_personal_fee_top10_quantity_barOption
        )
    }
);

/** 综合-服务费用（TOP10） */
const cop_statistics_elder_fee_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_elder_fee_top10_quantity`,
    `cop_statistics_elder_fee_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务费用（TOP10） */
const cop_statistics_elder_fee_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_fee_top10_quantity`,
    dataSourceID: cop_statistics_elder_fee_top10_quantity_dataSource.id
});

/** 综合-服务费用（TOP10） */
let cop_statistics_elder_fee_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务费用（TOP10） */
const cop_statistics_elder_fee_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_fee_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_elder_fee_top10_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_elder_fee_top10_quantity_bar_displayer',
            cop_statistics_elder_fee_top10_quantity_barOption
        )
    }
);

/** 综合-服务费用 */
const cop_statistics_elder_age_fee_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_elder_age_fee_quantity`,
    `cop_statistics_elder_age_fee_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务费用 */
const cop_statistics_elder_age_fee_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_age_fee_quantity`,
    dataSourceID: cop_statistics_elder_age_fee_quantity_dataSource.id
});

/** 综合-服务费用 */
let cop_statistics_elder_age_fee_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务费用 */
const cop_statistics_elder_age_fee_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_age_fee_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_elder_age_fee_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_elder_age_fee_quantity_bar_displayer',
            cop_statistics_elder_age_fee_quantity_barOption
        )
    }
);

/** 综合-服务费用 */
const cop_statistics_street_fee_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_street_fee_quantity`,
    `cop_statistics_street_fee_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-服务费用 */
const cop_statistics_street_fee_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_street_fee_quantity`,
    dataSourceID: cop_statistics_street_fee_quantity_dataSource.id
});

/** 综合-服务费用 */
let cop_statistics_street_fee_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-服务费用 */
const cop_statistics_street_fee_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_street_fee_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_street_fee_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_street_fee_quantity_bar_displayer',
            cop_statistics_street_fee_quantity_barOption
        )
    }
);

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_donor_income_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_donor_income_top10_quantity`,
    `cop_statistics_donor_income_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_donor_income_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_donor_income_top10_quantity`,
    dataSourceID: cop_statistics_donor_income_top10_quantity_dataSource.id
});

/** 综合-慈善收入金额（TOP10） */
let cop_statistics_donor_income_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '捐赠人'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '慈善收入金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_donor_income_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_donor_income_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_donor_income_top10_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_donor_income_top10_quantity_bar_displayer',
            cop_statistics_donor_income_top10_quantity_barOption
        )
    }
);

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_item_income_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_item_income_top10_quantity`,
    `cop_statistics_item_income_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_item_income_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_item_income_top10_quantity`,
    dataSourceID: cop_statistics_item_income_top10_quantity_dataSource.id
});

/** 综合-慈善收入金额（TOP10） */
let cop_statistics_item_income_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '慈善项目'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '慈善收入金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-慈善收入金额（TOP10） */
const cop_statistics_item_income_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_item_income_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_item_income_top10_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_item_income_top10_quantity_bar_displayer',
            cop_statistics_item_income_top10_quantity_barOption
        )
    }
);

/** 综合-慈善使用金额 */
const cop_statistics_org_use_amount_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_org_use_amount_quantity`,
    `cop_statistics_org_use_amount_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-慈善使用金额 */
const cop_statistics_org_use_amount_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_org_use_amount_quantity`,
    dataSourceID: cop_statistics_org_use_amount_quantity_dataSource.id
});

/** 综合-慈善使用金额 */
let cop_statistics_org_use_amount_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '养老院'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '慈善使用金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-慈善使用金额 */
const cop_statistics_org_use_amount_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_org_use_amount_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_org_use_amount_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_org_use_amount_quantity_bar_displayer',
            cop_statistics_org_use_amount_quantity_barOption
        )
    }
);

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_donor_use_amount_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_donor_use_amount_top10_quantity`,
    `cop_statistics_donor_use_amount_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_donor_use_amount_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_donor_use_amount_top10_quantity`,
    dataSourceID: cop_statistics_donor_use_amount_top10_quantity_dataSource.id
});

/** 综合-慈善使用金额（TOP10） */
let cop_statistics_donor_use_amount_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '捐赠人'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '慈善使用金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_donor_use_amount_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_donor_use_amount_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.nhContain,
        dataViews: [cop_statistics_donor_use_amount_top10_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_donor_use_amount_top10_quantity_bar_displayer',
            cop_statistics_donor_use_amount_top10_quantity_barOption
        )
    }
);

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_item_use_amount_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_cop_statistics_item_use_amount_top10_quantity`,
    `cop_statistics_item_use_amount_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_item_use_amount_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_item_use_amount_top10_quantity`,
    dataSourceID: cop_statistics_item_use_amount_top10_quantity_dataSource.id
});

/** 综合-慈善使用金额（TOP10） */
let cop_statistics_item_use_amount_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '慈善项目'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '慈善使用金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/** 综合-慈善使用金额（TOP10） */
const cop_statistics_item_use_amount_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_item_use_amount_top10_quantity_displayer',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [cop_statistics_item_use_amount_top10_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'cop_statistics_item_use_amount_top10_quantity_bar_displayer',
            cop_statistics_item_use_amount_top10_quantity_barOption
        )
    }
);

/**
 * 综合--服务商总数--数据源--数值
 */
const cop_statistics_service_provider_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_service_provider_total_quantity`,
    `cop_statistics_service_provider_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--服务事件位置--数据源--星空图
 */
const cop_map_locationDataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_map_location`,
    `cop_map_location`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--当前任务地图--数据视图--明细星空图
 */
const cop_detail_task_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_detail_task_map`,
    `cop_detail_task_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--服务商总数--数据视图--数值
 */
const cop_statistics_service_provider_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_service_provider_total_quantity`,
    dataSourceID: cop_statistics_service_provider_total_quantityDataSource.id
});
/**
 * 综合--长者类型服务评价比例--数据源--饼图
 */
const cop_statistics_elder_type_evaluate_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_elder_type_evaluate_quantity`,
    `cop_statistics_elder_type_evaluate_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--长者类型服务次数比例--数据源--饼图
 */
const cop_statistics_elder_type_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_elder_type_quantity`,
    `cop_statistics_elder_type_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--长者类型服务费用比例--数据源--饼图
 */
const cop_statistics_elder_type_fee_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_elder_type_fee_quantity`,
    `cop_statistics_elder_type_fee_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--慈善资金收入类型对比--数据源--饼图
 */
const cop_statistics_donation_income_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_donation_income_quantity`,
    `cop_statistics_donation_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--慈善资金支出类型对比--数据源--饼图
 */
const cop_statistics_type_use_amount_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_type_use_amount_quantity`,
    `cop_statistics_type_use_amount_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--服务次数走势--数据源--饼图
 */
const cop_statistics_time_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_time_quantity`,
    `cop_statistics_time_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--服务收益走势--数据源--饼图
 */
const cop_statistics_time_profit_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_time_profit_quantity`,
    `cop_statistics_time_profit_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--慈善收入走势--数据源--饼图
 */
const cop_statistics_time_income_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_time_income_quantity`,
    `cop_statistics_time_income_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--慈善发放走势--数据源--饼图
 */
const cop_statistics_time_grant_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_time_grant_quantity`,
    `cop_statistics_time_grant_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--补贴发放走势--数据源--饼图
 */
const cop_statistics_time_subsidy_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_cop_statistics_time_subsidy_quantity`,
    `cop_statistics_time_subsidy_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 综合--服务事件位置--数据视图--星空图
 */
const cop_map_locationDataView = createObject(ReportDataView, {
    id: `data_view_cop_map_location`,
    dataSourceID: cop_map_locationDataSource.id
});
/**
 * 综合--当前任务地图--数据视图--明细星空图
 */
const cop_detail_task_mapDataView = createObject(ReportDataView, {
    id: `data_view_cop_detail_task_map`,
    dataSourceID: cop_detail_task_mapDataSource.id
});
/**
 * 综合--服务商总数--数值
 */
const cop_statistics_service_provider_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_service_provider_total_quantity-num',
        title: '服务商总数',
        titleable: false,
        dataViews: [cop_statistics_service_provider_total_quantityDataView],
        groupIndex: 0,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['服务商总数', '服务人员总数', '长者总数', '服务评价总数', '服务总次数', '服务费用总数'],
                value_field: 'y',
            }
        )
    }
);
/**
 * 综合--长者类型服务评价比例--数据视图--饼图
 */
const cop_statistics_elder_type_evaluate_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_type_evaluate_quantity`,
    dataSourceID: cop_statistics_elder_type_evaluate_quantity_DataSource.id
});
/**
 * 综合--长者类型服务次数比例--数据视图--饼图
 */
const cop_statistics_elder_type_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_type_quantity`,
    dataSourceID: cop_statistics_elder_type_quantity_DataSource.id
});
/**
 * 综合--长者类型服务费用比例--数据视图--饼图
 */
const cop_statistics_elder_type_fee_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_elder_type_fee_quantity`,
    dataSourceID: cop_statistics_elder_type_fee_quantity_DataSource.id
});
/**
 * 综合--慈善资金收入类型对比--数据视图--饼图
 */
const cop_statistics_donation_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_donation_income_quantity`,
    dataSourceID: cop_statistics_donation_income_quantity_DataSource.id
});
/**
 * 综合--慈善资金支出类型对比--数据视图--饼图
 */
const cop_statistics_type_use_amount_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_type_use_amount_quantity`,
    dataSourceID: cop_statistics_type_use_amount_quantity_DataSource.id
});
/**
 * 综合--服务次数走势--数据视图--饼图
 */
const cop_statistics_time_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_time_quantity`,
    dataSourceID: cop_statistics_time_quantity_DataSource.id
});
/**
 * 综合--服务收益走势--数据视图--饼图
 */
const cop_statistics_time_profit_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_time_profit_quantity`,
    dataSourceID: cop_statistics_time_profit_quantity_DataSource.id
});
/**
 * 综合--慈善收入走势--数据视图--饼图
 */
const cop_statistics_time_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_time_income_quantity`,
    dataSourceID: cop_statistics_time_income_quantity_DataSource.id
});
/**
 * 综合--慈善发放走势--数据视图--饼图
 */
const cop_statistics_time_grant_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_time_grant_quantity`,
    dataSourceID: cop_statistics_time_grant_quantity_DataSource.id
});
/**
 * 综合--补贴发放走势--数据视图--饼图
 */
const cop_statistics_time_subsidy_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_cop_statistics_time_subsidy_quantity`,
    dataSourceID: cop_statistics_time_subsidy_quantity_DataSource.id
});
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};
/**
 * 综合--长者类型服务评价比例--配置--饼图
 */
let cop_statistics_elder_type_evaluate_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--长者类型服务次数比例--配置--饼图
 */
let cop_statistics_elder_type_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--长者类型服务费用比例--配置--饼图
 */
let cop_statistics_elder_type_fee_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--慈善资金收入类型对比--配置--饼图
 */
let cop_statistics_donation_income_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--慈善资金支出类型对比--配置--饼图
 */
let cop_statistics_type_use_amount_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--服务次数走势--配置--饼图
 */
let cop_statistics_time_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--服务收益走势--配置--饼图
 */
let cop_statistics_time_profit_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--慈善收入走势--配置--饼图
 */
let cop_statistics_time_income_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--慈善发放走势--配置--饼图
 */
let cop_statistics_time_grant_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--补贴发放走势--配置--饼图
 */
let cop_statistics_time_subsidy_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 综合--服务事件位置--星空图
 */
const cop_map_locationModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_map_location',
        title: '服务事件位置',
        titleable: false,
        dataViews: [cop_map_locationDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "cop_map_location_modules",
            option
        )
    }
);
/**
 * 综合--当前任务地图--数据视图--明细星空图
 */
const cop_detail_task_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_task_map_control',
        title: '服务人员位置',
        titleable: false,
        dataViews: [cop_detail_task_mapDataView],
        containerType: ContainerType.frameContain,
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "cop_detail_task_map_modules",
            option
        )
    }
);

/**
 * 综合--当前任务地图--数据视图--明细星空图-弹窗
 */
const cop_detail_task_mapDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_detail_task_map_control-dialog',
        title: '服务人员位置',
        titleable: false,
        dataViews: [cop_detail_task_mapDataView],
        isDialog: true,
        parentContainerID: cop_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "cop_detail_task_map_modules-dialog",
            option
        )
    }
);
/**
 * 综合--长者类型服务评价比例--饼图
 */
const cop_statistics_elder_type_evaluate_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_type_evaluate_quantity_pie',
        title: '长者类型服务评价比例',
        titleable: false,
        dataViews: [cop_statistics_elder_type_evaluate_quantity_DataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_elder_type_evaluate_quantity_pie_displayer',
            cop_statistics_elder_type_evaluate_quantity_pieOption
        )
    }
);
/**
 * 综合--长者类型服务次数比例--饼图
 */
const cop_statistics_elder_type_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_type_quantity_pie',
        title: '长者类型服务次数比例',
        titleable: false,
        dataViews: [cop_statistics_elder_type_quantity_DataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_elder_type_quantity_pie_displayer',
            cop_statistics_elder_type_quantity_pieOption
        )
    }
);
/**
 * 综合--长者类型服务费用比例--饼图
 */
const cop_statistics_elder_type_fee_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_elder_type_fee_quantity_pie',
        title: '长者类型服务费用比例',
        titleable: false,
        dataViews: [cop_statistics_elder_type_fee_quantity_DataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_elder_type_fee_quantity_pie_displayer',
            cop_statistics_elder_type_fee_quantity_pieOption
        )
    }
);
/**
 * 综合--慈善资金收入类型对比--饼图
 */
const cop_statistics_donation_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_donation_income_quantity_pie',
        title: '慈善资金收入类型对比',
        titleable: false,
        dataViews: [cop_statistics_donation_income_quantity_DataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_donation_income_quantity_pie_displayer',
            cop_statistics_donation_income_quantity_pieOption
        )
    }
);
/**
 * 综合--慈善资金支出类型对比--饼图
 */
const cop_statistics_type_use_amount_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_type_use_amount_quantity_pie',
        title: '慈善资金支出类型对比',
        titleable: false,
        dataViews: [cop_statistics_type_use_amount_quantity_DataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_type_use_amount_quantity_pie_displayer',
            cop_statistics_type_use_amount_quantity_pieOption
        )
    }
);
/**
 * 综合--服务次数走势--饼图
 */
const cop_statistics_time_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_time_quantity_pie',
        title: '服务次数走势',
        titleable: false,
        dataViews: [cop_statistics_time_quantity_DataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_time_quantity_pie_displayer',
            cop_statistics_time_quantity_pieOption
        )
    }
);
/**
 * 综合--服务收益走势--饼图
 */
const cop_statistics_time_profit_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_time_profit_quantity_pie',
        title: '服务收益走势',
        titleable: false,
        dataViews: [cop_statistics_time_profit_quantity_DataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_time_profit_quantity_pie_displayer',
            cop_statistics_time_profit_quantity_pieOption
        )
    }
);
/**
 * 综合--慈善收入走势--饼图
 */
const cop_statistics_time_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_time_income_quantity_pie',
        title: '慈善收入走势',
        titleable: false,
        dataViews: [cop_statistics_time_income_quantity_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_time_income_quantity_pie_displayer',
            cop_statistics_time_income_quantity_pieOption
        )
    }
);
/**
 * 综合--慈善发放走势--饼图
 */
const cop_statistics_time_grant_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_time_grant_quantity_pie',
        title: '慈善发放走势',
        titleable: false,
        dataViews: [cop_statistics_time_grant_quantity_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_time_grant_quantity_pie_displayer',
            cop_statistics_time_grant_quantity_pieOption
        )
    }
);
/**
 * 综合--补贴发放走势--饼图
 */
const cop_statistics_time_subsidy_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'cop_statistics_time_subsidy_quantity_pie',
        title: '补贴发放走势',
        titleable: false,
        dataViews: [cop_statistics_time_subsidy_quantity_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'cop_statistics_time_subsidy_quantity_pie_displayer',
            cop_statistics_time_subsidy_quantity_pieOption
        )
    }
);

const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(
    CommonLayoutMiddleControl
);

const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    cop_statistics_time_income_quantity_DataSource,
                    cop_statistics_time_grant_quantity_DataSource,
                    cop_statistics_time_subsidy_quantity_DataSource,
                    cop_statistics_elder_evaluate_top13_quantity_dataSource,
                    cop_statistics_item_use_amount_top10_quantity_dataSource,
                    cop_statistics_elder_type_evaluate_quantity_DataSource,
                    cop_statistics_elder_type_quantity_DataSource,
                    cop_statistics_elder_type_fee_quantity_DataSource,
                    cop_statistics_donation_income_quantity_DataSource,
                    cop_statistics_type_use_amount_quantity_DataSource,
                    cop_statistics_elder_age_fee_quantity_dataSource,
                    cop_statistics_street_fee_quantity_dataSource,
                    cop_statistics_donor_income_top10_quantity_dataSource,
                    cop_statistics_item_income_top10_quantity_dataSource,
                    cop_statistics_org_use_amount_quantity_dataSource,
                    cop_statistics_donor_use_amount_top10_quantity_dataSource,
                    cop_statistics_elder_age_quantity_dataSource,
                    cop_statistics_street_quantity_dataSource,
                    cop_statistics_item_fee_top10_quantity_dataSource,
                    cop_statistics_org_fee_top10_quantity_dataSource,
                    cop_statistics_service_personal_fee_top10_quantity_dataSource,
                    cop_statistics_elder_fee_top10_quantity_dataSource,
                    cop_statistics_age_evaluate_quantity_dataSource,
                    cop_statistics_street_evaluate_quantity_dataSource,
                    cop_statistics_item_top10_quantity_dataSource,
                    cop_statistics_org_top10_quantity_dataSource,
                    cop_statistics_service_personal_top10_quantity_dataSource,
                    cop_statistics_elder_top10_quantity_dataSource,
                    cop_statistics_service_provider_total_quantityDataSource,
                    cop_statistics_item_evaluate_top10_quantity_dataSource,
                    cop_statistics_org_evaluate_top11_quantity_dataSource,
                    cop_statistics_service_personal_evaluate_top12_quantity_dataSource,
                    cop_statistics_time_quantity_DataSource,
                    cop_statistics_time_profit_quantity_DataSource,
                ],
                modules: [
                    // 0
                    cop_statistics_service_provider_total_quantityModuleControl,
                    // 1
                    cop_statistics_time_profit_quantity_displayModuleControl,
                    // 2
                    cop_statistics_elder_evaluate_top13_quantity_displayModuleControl,
                    cop_statistics_elder_type_evaluate_quantity_displayModuleControl,
                    cop_statistics_age_evaluate_quantity_displayModuleControl,
                    cop_statistics_street_evaluate_quantity_displayModuleControl,
                    cop_statistics_item_top10_quantity_displayModuleControl,
                    cop_statistics_org_top10_quantity_displayModuleControl,
                    cop_statistics_service_personal_top10_quantity_displayModuleControl,
                    cop_statistics_elder_top10_quantity_displayModuleControl,
                    cop_statistics_item_evaluate_top10_quantity_displayModuleControl,
                    cop_statistics_org_evaluate_top11_quantity_displayModuleControl,
                    cop_statistics_service_personal_evaluate_top12_quantity_displayModuleControl,
                    // 3
                    cop_statistics_elder_type_quantity_displayModuleControl,
                    cop_statistics_elder_type_fee_quantity_displayModuleControl,
                    cop_statistics_elder_age_fee_quantity_displayModuleControl,
                    cop_statistics_street_fee_quantity_displayModuleControl,
                    cop_statistics_elder_age_quantity_displayModuleControl,
                    cop_statistics_street_quantity_displayModuleControl,
                    cop_statistics_item_fee_top10_quantity_displayModuleControl,
                    cop_statistics_org_fee_top10_quantity_displayModuleControl,
                    cop_statistics_service_personal_fee_top10_quantity_displayModuleControl,
                    cop_statistics_elder_fee_top10_quantity_displayModuleControl,
                    // 4
                    cop_statistics_time_quantity_displayModuleControl,
                    cop_statistics_item_use_amount_top10_quantity_displayModuleControl,
                    cop_statistics_donation_income_quantity_displayModuleControl,
                    cop_statistics_type_use_amount_quantity_displayModuleControl,
                    cop_statistics_donor_income_top10_quantity_displayModuleControl,
                    cop_statistics_item_income_top10_quantity_displayModuleControl,
                    cop_statistics_org_use_amount_quantity_displayModuleControl,
                    cop_statistics_donor_use_amount_top10_quantity_displayModuleControl,
                    // 5
                    cop_statistics_time_subsidy_quantity_displayModuleControl,
                    cop_statistics_time_income_quantity_displayModuleControl,
                    cop_statistics_time_grant_quantity_displayModuleControl,
                ],
                layout: layoutLeft,
                // autoQuery: 1
            }
        )
    }
);

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    // cop_map_locationDataSource
                ],
                modules: [
                    // cop_map_locationModuleControl,
                ],
                layout: layoutMiddle,
                // autoQuery: 1
            }
        )
    }
);
const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    cop_detail_service_card_dataSource,
                    cop_detail_service_provider_card_dataSource,
                    cop_detail_service_personal_card_dataSource,
                    cop_detail_task_card_dataSource,
                    cop_detail_task_mapDataSource,
                    cop_detail_live_video_dataSource,
                ],
                modules: [
                    // 0
                    cop_detail_service_card_displayModuleControl,
                    // 1
                    cop_detail_service_provider_card_displayModuleControl,
                    // 2
                    cop_detail_service_personal_card_displayModuleControl,
                    // 3
                    cop_detail_task_card_displayModuleControl,
                    cop_detail_task_mapModuleControl,
                    cop_detail_task_mapDialogModuleControl,
                    cop_detail_live_videoModuleControl
                ],
                // autoQuery: 1,
                layout: layoutRight,
                defaultParams: { id: '4dd41f76-da08-11e9-860d-144f8aec0be8' },
            }
        )
    }
);
const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportControl,
    {
        dataSources: [

        ],
        modules: [

        ],
        layout: layoutSelect,
        autoQuery: 1,
    }
);

const middleReport = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: middleControl
    }
);

/**
 * react布局控制器
 */
const layoutAll = createObject(CommonLayoutControl);
const irComprehensiveControl = createObject(
    SmartReportControl,
    {
        id: 'irComprehensive',
        modules: [selectControl, leftControl, middleReport, rightControl],
        dataSources: [],
        layout: layoutAll,
        // autoQuery: 1,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irComprehensiveControl };