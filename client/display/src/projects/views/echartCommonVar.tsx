import echarts from "echarts";

export const X_ASIX_FONT_COLOR = "white";
export const X_ASIX_FONT_SIZE = 24;

export const TITLE_COLOR = "white";
export const TITLE_FONT_SIZE = 36;

export const Y_ASIX_FONT_COLOR = "white";
export const Y_ASIX_FONT_SIZE = 24;
export const Y_NAME_FONT_SIZE = 24;
/** yAxis-nameTextStyle */
export const Y_NAME_FONT_COLOR = "white";

export const LEGEND_COLOR = "white";
export const LEGEND_FONT_SIZE = 24;

export const ECHART_FONT_WEIGHT = 500;

export class EchartOptionUtil {
    /**
     * 获取标题option
     */
    // static getTitleOption(title: string) {
    //     return {
    //         // TODO: padding
    //         text: title,
    //         left: 'left',
    //         textStyle: {
    //             color: TITLE_COLOR,
    //             fontSize: TITLE_FONT_SIZE,
    //             // TODO: 字重500
    //             fontWeight: "normal" as "normal",
    //         },
    //         padding: 60
    //     };
    // }

    /**
     * 获取标题legend的axisLabel样式
     */
    // static getLegendOption() {
    //     return {
    //         textStyle: {
    //             color: LEGEND_COLOR,
    //             fontSize: LEGEND_FONT_SIZE
    //         },
    //         top: 100
    //     };
    // }

    /**
     * 获取标题xAxis的axisLabel样式
     */
    // static getXAxisLabel() {
    //     return {
    //         color: LEGEND_COLOR,
    //         fontSize: LEGEND_FONT_SIZE
    //     };
    // }

    /**
     * 获取标题yAxis的nameTextStyle样式
     */
    // static getYAxisNameTextStyle() {
    //     return {
    //         fontSize: Y_NAME_FONT_SIZE,
    //         color: Y_NAME_FONT_COLOR
    //     };
    // }

    /**
     * 获取标题yAxis的Label样式
     */
    // static getYAxisLabel() {
    //     return {
    //         fontSize: Y_NAME_FONT_SIZE,
    //         color: Y_NAME_FONT_COLOR
    //     };
    // }

    /**
     * 获取一般图标的grid样式
     */
    // static getGrid() {
    //     return {
    //         top: 170,
    //         left: 120,
    //         right: 120
    //     };
    // }

    /**
     * 获取pie类型的grid样式
     */
    // static getPieGrid() {
    //     return {
    //         top: 170,
    //         left: 120,
    //         right: 120
    //     };
    // }

    /**
     * 获取柱状图渐变色样式，全部为渐变色
     * @param No 第几根柱子，1：紫色；2：蓝色；3：don't know
     */
    static getBarColor(No: 1 | 2 | 3) {
        switch (No) {
            case 1:
                return new (echarts.graphic as any).LinearGradient(0, 0, 0, 1, [
                    { offset: 0, color: '#FF00E9' },
                    { offset: 1, color: '#FF00CE' }
                ]);
            case 2:
                return new (echarts.graphic as any).LinearGradient(0, 0, 0, 1, [
                    { offset: 0, color: '#00BDFD' },
                    { offset: 1, color: '#016CFF' }
                ]);
            case 3:
                return new (echarts.graphic as any).LinearGradient(0, 0, 0, 1, [
                    { offset: 0, color: '#00BDFD' },
                    { offset: 1, color: '#016CFF' }
                ]);
            default:
                break;
        }
    }

    /**
     * 获取折线图渐变色样式
     * @param No 第几根折线，1：紫色；2：蓝色；3：don't know
     */
    // static getLineColor(No: 1 | 2 | 3) {
    //     switch (No) {
    //         case 1:
    //             return "#00ACF9";
    //         case 2:
    //             return new (echarts.graphic as any).LinearGradient(0, 0, 0, 1, [
    //                 { offset: 0, color: '#00BDFD' },
    //                 { offset: 1, color: '#016CFF' }
    //             ]);
    //         case 3:
    //             return new (echarts.graphic as any).LinearGradient(0, 0, 0, 1, [
    //                 { offset: 0, color: '#00BDFD' },
    //                 { offset: 1, color: '#016CFF' }
    //             ]);
    //         default:
    //             break;
    //     }
    // }

    /**
     * 获取bar上的文字配置
     */
    // static getBarLable() {
    //     return {
    //         normal: {
    //             show: true,
    //             position: 'top',
    //             color: "white",
    //             fontSize: 24
    //         }
    //     };
    // }

    /**
     * 获取pie半径几中心点配置
     */
    // static getPieSeriesOption() {
    //     return {
    //         radius: ['32.5%', '65%'],
    //         center: ['50%', '60%'],
    //         label: {
    //             fontSize: 20,
    //             color: "white",
    //             formatter: '{b}: {d}%'
    //         }
    //     };
    // }

    /**
     * 获取pie颜色
     */
    // static getPieColor() {
    //     return ["#54CA76" /** 浅绿色 */, "#9664E2" /** 紫色 */, "#FBD436" /** 黄色 */, "#37CBCB" /** 蓝偏绿 */, "#F2627B" /** 浅红 */, "#40A3FC" /** 浅蓝色 */];
    // }
}