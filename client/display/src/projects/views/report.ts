import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, AjaxRemoteDataQueryer, ReportDataView } from "src/business/report/smart";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { ListDisplayControl } from "src/business/report/displayers/nh-welfare/list-display";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { CarouselWithNumDisplayDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-num-display";
import { RoomStatusDisplayControl } from "src/projects/components/display/room-status-display";
// import { VideoControl } from "src/business/components/buss-components/video";

const remoteService_Fac = new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`);

/**
 * 养老院总数--明细--人员风采
 */
const org_detail_personal_listDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_personal_list`,
        command: `org_detail_personal_list`,
        remoteService_Fac
    }
);
/**
 * 养老院总数--明细--服务产品
 */
const org_detail_service_products_listDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_service_products_list`,
        command: `org_detail_service_products_list`,
        remoteService_Fac
    }
);
/**
 * 养老院总数--明细--服务产品介绍轮播
 */
const org_detail_service_productsDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_service_products`,
        command: `org_detail_service_products`,
        remoteService_Fac
    }
);

/**
 * 养老院总数--养老院入住总人数--养老院服务人员总人数
 * 机构养老--养老机构总数--数据源--数值
 */
const org_statistics_total_quantityDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_total_quantity`,
        command: `org_statistics_total_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院位置--数据源--星空图
 */
const org_map_location_welfare_centreDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_map_location_welfare_centre`,
        command: `org_map_location_welfare_centre`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--服务人员总数--数据源--数值
 */
const org_detail_total_quantityDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_total_quantity`,
        command: `org_detail_total_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院环境--数据源--图片轮播
 */
const org_detail_picture_listCarouselDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_picture_list`,
        command: `org_detail_picture_list`,
        remoteService_Fac
    }
);
/**
 * 机构养老--服务价格表--数据源--图片轮播
 */
const org_detail_service_price_pictureCarouselDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_service_price_picture`,
        command: `org_detail_service_price_picture`,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院活动--数据源--图片轮播
 */
const org_detail_activity_picture_listCarouselDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_activity_picture_list`,
        command: `org_detail_activity_picture_list`,
        remoteService_Fac
    }
);

/**
 * 机构养老--养老院简介
 */
const org_detail_welfare_centreCarouselDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_welfare_centre`,
        command: `org_detail_welfare_centre`,
        remoteService_Fac
    }
);

/**
 * 机构养老--养老院简介
 */
const org_detail_welfare_centreRoomStatusDataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_welfare_centre_room_status`,
        command: `org_detail_welfare_centre_room_status`,
        remoteService_Fac
    }
);

/**
 * 机构养老--养老机构总数--数据视图--数值
 */
const org_statistics_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_total_quantity`,
    dataSourceID: org_statistics_total_quantityDataSource.id
});

/**
 * 养老院总数--明细--人员风采
 */
const org_detail_personal_listDataView = createObject(ReportDataView, {
    id: `data_view_org_detail_personal_list`,
    dataSourceID: org_detail_personal_listDataSource.id
});

/**
 * 养老院总数--明细--服务清单
 */
const org_detail_service_products_listDataView = createObject(ReportDataView, {
    id: `data_view_org_detail_service_products_list`,
    dataSourceID: org_detail_service_products_listDataSource.id
});

/**
 * 养老院总数--明细--服务清单轮播
 */
const org_detail_service_productsDataView = createObject(ReportDataView, {
    id: `data_view_org_detail_service_products_list`,
    dataSourceID: org_detail_service_productsDataSource.id
});

/**
 * 机构养老--养老院位置--数据视图--星空图
 */
const org_map_location_welfare_centreDataView = createObject(ReportDataView, {
    id: `data_view_org_map_location_welfare_centre`,
    dataSourceID: org_map_location_welfare_centreDataSource.id
});
/**
 * 机构养老--服务人员总数--数据视图--数值
 */
const org_detail_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_org_detail_total_quantity`,
    dataSourceID: org_detail_total_quantityDataSource.id
});
/**
 * 机构养老--养老院环境--数据视图--图片轮播
 */
const org_detail_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_picture_list-carousel`,
    dataSourceID: org_detail_picture_listCarouselDataSource.id
});
/**
 * 机构养老--服务价格表--数据视图--图片轮播
 */
const org_detail_service_price_picture_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_service_price_picture-carousel`,
    dataSourceID: org_detail_service_price_pictureCarouselDataSource.id
});
/**
 * 机构养老--养老院活动--数据视图--图片轮播
 */
const org_detail_activity_picture_list_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_activity_picture_list-carousel`,
    dataSourceID: org_detail_activity_picture_listCarouselDataSource.id
});

/**
 * 机构养老--养老院简介
 */
const org_detail_welfare_centre_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_welfare_centre`,
    dataSourceID: org_detail_welfare_centreCarouselDataSource.id
});

/**
 * 机构养老--房态图
 */
const org_detail_welfare_centre_room_status_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail-welfare-centre-room-status`,
    dataSourceID: org_detail_welfare_centreRoomStatusDataSource.id
});

/**
 * 机构养老--养老机构总数--数值
 */
const org_statistics_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_total_quantity-num',
        title: '养老机构总数',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        groupIndex: 0,
        dataViews: [org_statistics_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['养老院总数', '养老院入住总人数', '养老院服务人员总数'],
                value_field: 'y',
            }
        )
    }
);
/**
 * 养老院总数--明细--人员风采
 */
const org_detail_personal_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_personal_list',
        titleable: false,
        title: '人员风采',
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [org_detail_personal_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '人员风采',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2', 'y3'],
                height_content: 900
            }
        )
    }
);
/**
 * 养老院总数--明细--服务清单
 */
const org_detail_service_products_listModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_service_products_list',
        titleable: false,
        title: '服务清单',
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 2,
        dataViews: [org_detail_service_products_listDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务清单',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2'],
                height_content: 900

            }
        )
    }
);

/**
 * 养老院总数--明细--服务清单轮播
 */
const org_detail_service_productsModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_service_products',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [org_detail_service_productsDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            ListDisplayControl,
            {
                title: '服务清单',
                img_key_name: 'x',
                other_text_key_name: ['y1', 'y2']

            }
        )
    }
);
org_detail_service_productsModuleControl;
// /**
//  * 服务人员总数--总床位数--入住长者数量--总服务次数--总服务人数--总服务收入
//  * 机构养老--服务人员总数--数值
//  */
// const org_detail_total_quantityModuleControl = createObject(
//     DataDisplayerModuleControl,
//     {
//         id: 'org_detail_total_quantity-num',
//         title: '服务人员总数',
//         titleable: false,
//         groupType: GroupContainerType.carousel,
//         group: "right-carousel",
//         groupIndex: 0,
//         dataViews: [org_detail_total_quantityDataView],
//         containerType: ContainerType.frameContain,
//         displayer: createObject(
//             MoreSquaresControl,
//             {
//                 layout: "horizontal",
//                 square_span: 24,
//                 name_fields: ['服务人员总数', '总床位数', '入住长者数量'],
//                 value_field: 'y',
//             }
//         )
//     }
// );

/**
 * 机构养老--养老院简介
 */
const org_detail_welfare_centre_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_welfare_centre',
        title: '养老院简介',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_welfare_centre_carousel_DataView],
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['名称', '地址', '评级情况', '机构类型', '机构性质', '法定代表人', '联系电话'],
                max_height: 900
            }
        )
    }
);

/**
 * 机构养老--房态图
 */
const org_detail_welfare_centre_room_status_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_welfare_centre_room_status',
        title: '房态图',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_welfare_centre_room_status_DataView],
        displayer: createObject(
            RoomStatusDisplayControl
        )
    }
);

/**
 * 机构养老--养老院类型比例--数据源--饼图
 */
const org_statistics_nature_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_nature_quantity`,
        command: `org_statistics_nature_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--床位入住状况统计--数据源--饼图
 */
const org_statistics_live_bed_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_live_bed_quantity`,
        command: `org_statistics_live_bed_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--工作人员学历分布--数据源--饼图
 */
const org_statistics_education_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_education_personnel_quantity`,
        command: `org_statistics_education_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--工作人员资质数量分布--数据源--饼图
 */
const org_statistics_qualifications_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_qualifications_personnel_quantity`,
        command: `org_statistics_qualifications_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者性别比例--数据源--饼图
 */
const org_statistics_sex_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_sex_personnel_quantity`,
        command: `org_statistics_sex_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者子女数量比例--数据源--饼图
 */
const org_statistics_children_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_children_personnel_quantity`,
        command: `org_statistics_children_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者类型比例--数据源--饼图
 */
const org_statistics_identity_affiliation_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_identity_affiliation_personnel_quantity`,
        command: `org_statistics_identity_affiliation_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者预约数量性别比例--数据源--饼图
 */
const org_statistics_sex_subscribe_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_sex_subscribe_quantity`,
        command: `org_statistics_sex_subscribe_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者预约数量走势--数据源--折线图
 */
const org_statistics_time_subscribe_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_time_subscribe_quantity`,
        command: `org_statistics_time_subscribe_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院收入走势--数据源--折线图
 */
const org_statistics_time_income_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_time_income_quantity`,
        command: `org_statistics_time_income_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院支出走势--数据源--折线图
 */
const org_statistics_time_expenditure_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_statistics_time_expenditure_quantity`,
        command: `org_statistics_time_expenditure_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者性别比例--数据源--饼图
 */
const org_detail_sex_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_sex_quantity`,
        command: `org_detail_sex_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者学历分布--数据源--饼图
 */
const org_detail_education_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_education_quantity`,
        command: `org_detail_education_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--长者来源地分布--数据源--饼图
 */
const org_detail_origin_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_origin_quantity`,
        command: `org_detail_origin_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--工作人员性别比例--数据源--饼图
 */
const org_detail_sex_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_sex_personnel_quantity`,
        command: `org_detail_sex_personnel_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--工作人员学历分布--数据源--饼图
 */
const org_detail_education_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_education_personnel_quantity`,
        command: `org_detail_education_personnel_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--工作人员资质数量分布--数据源--饼图
 */
const org_detail_qualifications_personnel_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_qualifications_personnel_quantity`,
        command: `org_detail_qualifications_personnel_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--床位入住状况统计--数据源--饼图
 */
const org_detail_live_bed_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_live_bed_quantity`,
        command: `org_detail_live_bed_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--入住人数走势--数据源--折线图
 */
const org_detail_time_live_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_time_live_quantity`,
        command: `org_detail_time_live_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--预约入住人数走势--数据源--折线图
 */
const org_detail_time_line_up_quantity_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_time_line_up_quantity`,
        command: `org_detail_time_line_up_quantity`,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院现场--视频监控（摄像头列表，点击放大）
 */
const org_detail_video_surveillance_DataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_source_org_detail_video_surveillance`,
        command: `org_detail_video_surveillance`,
        remoteService_Fac
    }
);
/**
 * 机构养老--养老院类型比例--数据视图--饼图
 */
const org_statistics_nature_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_nature_quantity`,
    dataSourceID: org_statistics_nature_quantity_DataSource.id
});
/**
 * 机构养老--床位入住状况统计--数据视图--饼图
 */
const org_statistics_live_bed_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_live_bed_quantity`,
    dataSourceID: org_statistics_live_bed_quantity_DataSource.id
});
/**
 * 机构养老--工作人员学历分布--数据视图--饼图
 */
const org_statistics_education_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_education_personnel_quantity`,
    dataSourceID: org_statistics_education_personnel_quantity_DataSource.id
});
/**
 * 机构养老--工作人员资质数量分布--数据视图--饼图
 */
const org_statistics_qualifications_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_qualifications_personnel_quantity`,
    dataSourceID: org_statistics_qualifications_personnel_quantity_DataSource.id
});
/**
 * 机构养老--长者性别比例--数据视图--饼图
 */
const org_statistics_sex_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_sex_personnel_quantity`,
    dataSourceID: org_statistics_sex_personnel_quantity_DataSource.id
});
/**
 * 机构养老--长者子女数量比例--数据视图--饼图
 */
const org_statistics_children_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_children_personnel_quantity`,
    dataSourceID: org_statistics_children_personnel_quantity_DataSource.id
});
/**
 * 机构养老--长者类型比例--数据视图--饼图图
 */
const org_statistics_identity_affiliation_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_identity_affiliation_personnel_quantity`,
    dataSourceID: org_statistics_identity_affiliation_personnel_quantity_DataSource.id
});
/**
 * 机构养老--长者预约数量性别比例--数据视图--饼图
 */
const org_statistics_sex_subscribe_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_sex_subscribe_quantity`,
    dataSourceID: org_statistics_sex_subscribe_quantity_DataSource.id
});
/**
 * 机构养老--长者预约数量走势--数据视图--折线图
 */
const org_statistics_time_subscribe_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_line-org_statistics_time_subscribe_quantity`,
    dataSourceID: org_statistics_time_subscribe_quantity_DataSource.id
});
/**
 * 机构养老--养老院收入走势--数据视图--折线图
 */
const org_statistics_time_income_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_time_income_quantity`,
    dataSourceID: org_statistics_time_income_quantity_DataSource.id
});
/**
 * 机构养老--养老院支出走势--数据视图--折线图
 */
const org_statistics_time_expenditure_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_statistics_time_expenditure_quantity`,
    dataSourceID: org_statistics_time_expenditure_quantity_DataSource.id
});
/**
 * 机构养老--长者性别比例--数据视图--饼图
 */
const org_detail_sex_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_sex_quantity`,
    dataSourceID: org_detail_sex_quantity_DataSource.id
});
/**
 * 机构养老--长者学历分布--数据视图--饼图
 */
const org_detail_education_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_education_quantity`,
    dataSourceID: org_detail_education_quantity_DataSource.id
});
/**
 * 机构养老--长者来源地分布--数据视图--饼图
 */
const org_detail_origin_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_origin_quantity`,
    dataSourceID: org_detail_origin_quantity_DataSource.id
});
/**
 * 机构养老--工作人员性别比例--数据视图--饼图
 */
const org_detail_sex_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_sex_personnel_quantity`,
    dataSourceID: org_detail_sex_personnel_quantity_DataSource.id
});
/**
 * 机构养老--工作人员学历分布--数据视图--饼图
 */
const org_detail_education_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_education_personnel_quantity`,
    dataSourceID: org_detail_education_personnel_quantity_DataSource.id
});
/**
 * 机构养老--工作人员资质数量分布--数据视图--饼图
 */
const org_detail_qualifications_personnel_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_qualifications_personnel_quantity`,
    dataSourceID: org_detail_qualifications_personnel_quantity_DataSource.id
});
/**
 * 机构养老--床位入住状况统计--数据视图--饼图
 */
const org_detail_live_bed_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_live_bed_quantity`,
    dataSourceID: org_detail_live_bed_quantity_DataSource.id
});
/**
 * 机构养老--入住人数走势--数据视图--折线图
 */
const org_detail_time_live_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_time_live_quantity`,
    dataSourceID: org_detail_time_live_quantity_DataSource.id
});
/**
 * 机构养老--预约入住人数走势--数据视图--折线图
 */
const org_detail_time_line_up_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_org_detail_time_line_up_quantity`,
    dataSourceID: org_detail_time_line_up_quantity_DataSource.id
});
/**
 * 机构养老--养老院现场--视频监控（摄像头列表，点击放大）
 */
// const org_detail_video_surveillance_DataView = createObject(ReportDataView, {
//     id: `data_view_org_detail_video_surveillance`,
//     dataSourceID: org_detail_video_surveillance_DataSource.id
// });
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        // {a}\n{b}
        formatter: '{a}\n{b}'
    },
};
/**
 * 机构养老--养老院类型比例--配置--饼图
 */
let org_statistics_nature_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--床位入住状况统计--配置--饼图
 */
let org_statistics_live_bed_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};

/**
 * 机构养老--工作人员学历分布--配置--饼图
 */
let org_statistics_education_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--工作人员资质数量分布--配置--饼图
 */
let org_statistics_qualifications_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者性别比例--配置--饼图
 */
let org_statistics_sex_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者子女数量比例--配置--饼图
 */
let org_statistics_children_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者类型比例--配置--饼图
 */
let org_statistics_identity_affiliation_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者预约数量性别比例--配置--饼图
 */
let org_statistics_sex_subscribe_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者预约数量走势--配置--折线图
 */
let org_statistics_time_subscribe_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        name: '时间',
    },
    yAxis: {
        type: 'value',
        name: '预约数量统计',
    },
    series: [
        { name: '长者预约数量', type: 'line' }
    ],
};
/**
 * 机构养老--养老院收入走势--配置--折线图
 */
let org_statistics_time_income_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '养老院收入走势',
    },
};
/**
 * 机构养老--养老院支出走势--配置--折线图
 */
let org_statistics_time_expenditure_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '养老院支出走势',
    },
};
/**
 * 机构养老--长者性别比例--配置--饼图
 */
let org_detail_sex_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者学历分布--数据源--配置
 */
let org_detail_education_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--长者来源地分布--配置--饼图
 */
let org_detail_origin_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--工作人员性别比例--配置--饼图
 */
let org_detail_sex_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--工作人员学历分布--配置--饼图
 */
let org_detail_education_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--工作人员资质数量分布--配置--饼图
 */
let org_detail_qualifications_personnel_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--床位入住状况统计--配置--饼图
 */
let org_detail_live_bed_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 机构养老--入住人数走势--配置--折线图
 */
let org_detail_time_live_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '入住人数走势',
    },
};
/**
 * 机构养老--预约入住人数走势--配置--折线图
 */
let org_detail_time_line_up_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '预约入住人数走势',
    },
};

/** 呼叫中心弹窗样式 */
function getEle(data: any) {
    let div = document.createElement("div");
    div.style.backgroundColor = "rgba(135,135,135,0.5)";
    div.style.color = "#fff";
    div.style.padding = "40px";
    div.style.fontSize = "80px";
    div.style.width = "1500px";
    div.style.boxShadow = "0px 4px 16px 0px rgba(0,131,197,0.5)";
    div.style.borderRadius = "16px 16px 16px 0px";
    div.style.border = "4px solid rgba(255,255,255,1)";

    let content = document.createElement('div');
    let nameAndAddressDiv = document.createElement('div');
    let sexAndAgeDiv = document.createElement('div');
    let phoneDiv = document.createElement('div');
    let pthoneTypeDiv = document.createElement('div');
    pthoneTypeDiv.innerText = data.phone_type;

    nameAndAddressDiv.style.display = "flex";
    sexAndAgeDiv.style.display = "flex";

    content.style.marginBottom = '40px';

    let nameDiv = document.createElement("div");

    nameDiv.innerText = data.name;
    nameDiv.style.marginRight = "40px";

    let adress = document.createElement("div");
    adress.innerText = data.address;
    nameAndAddressDiv.appendChild(nameDiv);
    nameAndAddressDiv.appendChild(adress);
    let sex = document.createElement('div');
    sex.innerText = data.sex;
    sex.style.marginRight = "40px";

    let age = document.createElement('div');
    age.innerText = `${data.age}岁`;

    sexAndAgeDiv.appendChild(sex);
    sexAndAgeDiv.appendChild(age);

    let phone = document.createElement('div');
    phone.innerText = data.phone;
    phoneDiv.appendChild(phone);

    content.appendChild(nameAndAddressDiv);
    content.appendChild(sexAndAgeDiv);
    content.appendChild(phoneDiv);
    content.appendChild(pthoneTypeDiv);

    div.appendChild(content);
    return div;
}

/**
 * 构养老--养老院位置--星空图
 */
const org_map_location_welfare_centreModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_map_location_welfare_centre',
        title: '养老院位置',
        titleable: false,
        dataViews: [org_map_location_welfare_centreDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                getElementFn: getEle
            },
            "org_map_location_welfare_centre-modules",
            option
        )
    }
);
/**
 * 机构养老--养老院环境--图片轮播
 */
const org_detail_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_picture_list-carousel',
        title: '养老院环境',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 机构养老--服务价格表--图片轮播
 */
const org_detail_service_price_picture_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_service_price_picture-carousel',
        title: '服务价格表',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_service_price_picture_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 机构养老--养老院环境--图片轮播-弹窗
 */
const org_detail_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_picture_list-carousel-Dialog',
        title: '养老院环境',
        titleable: false,
        isDialog: true,
        parentContainerID: org_map_location_welfare_centreModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [org_detail_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 机构养老--养老院活动--图片轮播
 */
const org_detail_activity_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_activity_picture_list-carousel',
        title: '养老院活动',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel",
        groupIndex: 0,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_activity_picture_list_carousel_DataView, org_detail_total_quantityDataView],
        displayer: createObject(
            CarouselWithNumDisplayDisplayControl,
            {
                title: "养老院活动",
                num_display_data_key_field_name: ['福利院总数', '福利院入住总人数', '福利院服务人员总数'],
                carousel_img_data_view_id: org_detail_activity_picture_list_carousel_DataView.id,
                num_display_data_view_id: org_detail_total_quantityDataView.id,
                num_display_data_value_field_name: "y",
                max_height: 800
            }
        )
    }
);
/**
 * 机构养老--养老院活动--图片轮播-弹窗
 */
const org_detail_activity_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_activity_picture_list-carousel-Dialog',
        title: '养老院活动',
        titleable: false,
        containerType: ContainerType.frameContain,
        isDialog: true,
        parentContainerID: org_map_location_welfare_centreModuleControl.id!,
        dataViews: [org_detail_activity_picture_list_carousel_DataView],
        displayer: createObject(
            CarouselWithNumDisplayDisplayControl,
            {
                title: "养老院活动",
                num_display_data_key_field_name: ['福利院总数', '福利院入住总人数', '福利院服务人员总数'],
                carousel_img_data_view_id: org_detail_activity_picture_list_carousel_DataView.id,
                num_display_data_view_id: org_detail_total_quantityDataView.id,
                num_display_data_value_field_name: "y",
                max_height: 800
            }
        )
    }
);
/**
 * 机构养老--服务价格表--图片轮播-弹窗
 */
const org_detail_service_price_picture_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_service_price_picture-carousel-dialog',
        title: '服务价格表',
        titleable: false,
        containerType: ContainerType.frameContain,
        isDialog: true,
        parentContainerID: org_map_location_welfare_centreModuleControl.id!,
        dataViews: [org_detail_service_price_picture_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 机构养老--养老院类型比例--饼图
 */
const org_statistics_nature_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_nature_quantity_pie',
        title: '养老院类型比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        dataViews: [org_statistics_nature_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_nature_quantity_pie_displayer',
            org_statistics_nature_quantity_pieOption
        )
    }
);
/**
 * 机构养老--床位入住状况统计--饼图
 */
const org_statistics_live_bed_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_live_bed_quantity_pie',
        title: "床位入住状况统计",
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        dataViews: [org_statistics_live_bed_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_live_bed_quantity_pie_displayer',
            org_statistics_live_bed_quantity_pieOption
        )
    }
);
/**
 * 机构养老--工作人员学历分布--饼图
 */
const org_statistics_education_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_education_personnel_quantity_pie',
        title: '工作人员学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [org_statistics_education_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_education_personnel_quantity_pie_displayer',
            org_statistics_education_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--工作人员资质数量分布--饼图
 */
const org_statistics_qualifications_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_qualifications_personnel_quantity_pie',
        title: '工作人员资质数量分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [org_statistics_qualifications_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_qualifications_personnel_quantity_pie_displayer',
            org_statistics_qualifications_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者性别比例--饼图
 */
const org_statistics_sex_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_sex_personnel_quantity_pie',
        title: '长者性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        dataViews: [org_statistics_sex_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_sex_personnel_quantity_pie_displayer',
            org_statistics_sex_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者子女数量比例--饼图
 */
const org_statistics_children_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_children_personnel_quantity_pie',
        title: '长者子女数量比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        dataViews: [org_statistics_children_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_children_personnel_quantity_pie_displayer',
            org_statistics_children_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者类型比例--配置--饼图
 */
const org_statistics_identity_affiliation_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_identity_affiliation_personnel_quantity_pie',
        title: '长者类型比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        dataViews: [org_statistics_identity_affiliation_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_identity_affiliation_personnel_quantity_pie_displayer',
            org_statistics_identity_affiliation_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者预约数量性别比例--饼图
 */
const org_statistics_sex_subscribe_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_sex_subscribe_quantity_pie',
        title: '长者预约数量性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        dataViews: [org_statistics_sex_subscribe_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_statistics_sex_subscribe_quantity_pie_displayer',
            org_statistics_sex_subscribe_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者预约数量走势--折线图
 */
const org_statistics_time_subscribe_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_time_subscribe_quantity_multi_line',
        title: '长者预约数量走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 4,
        dataViews: [org_statistics_time_subscribe_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_time_subscribe_quantity_multi_line_displayer',
            org_statistics_time_subscribe_quantity_multilineOption
        )
    }
);
/**
 * 机构养老--养老院收入走势--折线图
 */
const org_statistics_time_income_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_time_income_quantity_multi_line',
        title: '养老院收入走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 4,
        dataViews: [org_statistics_time_income_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_time_income_quantity_multi_line_displayer',
            org_statistics_time_income_quantity_multilineOption
        )
    }
);
/**
 * 机构养老--养老院支出走势--折线图
 */
const org_statistics_time_expenditure_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_time_expenditure_quantity_multi_line',
        title: '养老院支出走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        groupIndex: 4,
        dataViews: [org_statistics_time_expenditure_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_time_expenditure_quantity_multi_line_displayer',
            org_statistics_time_expenditure_quantity_multilineOption
        )
    }
);
/**
 * 机构养老--长者性别比例--饼图
 */
const org_detail_sex_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_sex_quantity_pie',
        title: '长者性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [org_detail_sex_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_sex_quantity_pie_displayer',
            org_detail_sex_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者学历分布--饼图
 */
const org_detail_education_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_education_quantity_pie',
        title: '长者学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [org_detail_education_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_education_quantity_pie_displayer',
            org_detail_education_quantity_pieOption
        )
    }
);
/**
 * 机构养老--长者来源地分布--饼图
 */
const org_detail_origin_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_origin_quantity_pie',
        title: '长者来源地分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        dataViews: [org_detail_origin_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_origin_quantity_pie_displayer',
            org_detail_origin_quantity_pieOption
        )
    }
);
/**
 * 机构养老--工作人员性别比例--饼图
 */
const org_detail_sex_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_sex_personnel_quantity_pie',
        title: '工作人员性别比例',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [org_detail_sex_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_sex_personnel_quantity_pie_displayer',
            org_detail_sex_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--工作人员学历分布--饼图
 */
const org_detail_education_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_education_personnel_quantity_pie',
        title: '工作人员学历分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [org_detail_education_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_education_personnel_quantity_pie_displayer',
            org_detail_education_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--工作人员资质数量分布--饼图
 */
const org_detail_qualifications_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_qualifications_personnel_quantity_pie',
        title: '工作人员资质数量分布',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        dataViews: [org_detail_qualifications_personnel_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_qualifications_personnel_quantity_pie_displayer',
            org_detail_qualifications_personnel_quantity_pieOption
        )
    }
);
/**
 * 机构养老--床位入住状况统计--饼图
 */
const org_detail_live_bed_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_live_bed_quantity_pie',
        title: '床位入住状况统计',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        dataViews: [org_detail_live_bed_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'org_detail_live_bed_quantity_pie_displayer',
            org_detail_live_bed_quantity_pieOption
        )
    }
);
/**
 * 机构养老--入住人数走势--折线图
 */
const org_detail_time_live_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_time_live_quantity_multi_line',
        title: '入住人数走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        dataViews: [org_detail_time_live_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_time_live_quantity_multi_line_displayer',
            org_detail_time_live_quantity_multilineOption
        )
    }
);
org_detail_time_live_quantity_displayModuleControl;
/**
 * 机构养老--预约入住人数走势--折线图
 */
const org_detail_time_line_up_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_time_line_up_quantity_multi_line',
        title: '预约入住人数走势',
        titleable: false,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        dataViews: [org_detail_time_line_up_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_time_line_up_quantity_multi_line_displayer',
            org_detail_time_line_up_quantity_multilineOption
        )
    }
);
org_detail_time_line_up_quantity_displayModuleControl;
/** 
 * 图例控制器
 */
// const legendControl = createObject(
//     LegendMenuControl,
//     {
//         echar_instance: org_card_location_welfare_centreModuleControl.displayer,
//         option: legendOption
//     }
// );

/**
 * 机构养老--镇街养老院数量分布--数据源--柱状图
 */
const org_statistics_street_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_street_quantity`,
        command: `org_statistics_street_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--镇街养老院数量分布--数据视图--柱状图
 */
const org_statistics_street_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_street_quantity`,
    dataSourceID: org_statistics_street_quantity_dataSource.id
});

/**
 * 机构养老--镇街养老院数量分布--配置--柱状图
 */
let org_statistics_street_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '养老院数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--镇街养老院数量分布--柱状图
 */
const org_statistics_street_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_street_quantity_displayer',
        title: '镇街养老院数量分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_street_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_street_quantity_bar_displayer',
            org_statistics_street_quantity_barOption
        )
    }
);

/**
 * 机构养老--养老院床位统计--数据源--柱状图
 */
const org_statistics_welfare_centre_bed_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_welfare_centre_bed_quantity`,
        command: `org_statistics_welfare_centre_bed_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--养老院按镇街统计--数据视图--柱状图
 */
const org_statistics_welfare_centre_bed_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_welfare_centre_bed_quantity`,
    dataSourceID: org_statistics_welfare_centre_bed_quantity_dataSource.id
});

/**
 * 机构养老--养老院床位统计--配置--柱状图
 */
let org_statistics_welfare_centre_bed_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '养老院'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '床位数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--养老院床位统计--柱状图
 */
const org_statistics_welfare_centre_bed_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_welfare_centre_bed_quantity_displayer',
        title: '养老院床位统计',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_welfare_centre_bed_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_street_bed_quantity_bar_displayer',
            org_statistics_welfare_centre_bed_quantity_barOption
        )
    }
);

/**
 * 机构养老--镇街床位统计--数据源--柱状图
 */
const org_statistics_street_bed_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_street_bed_quantity`,
        command: `org_statistics_street_bed_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);
/**
 * 机构养老--镇街床位统计--数据视图--柱状图
 */
const org_statistics_street_bed_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_street_bed_quantity`,
    dataSourceID: org_statistics_street_bed_quantity_dataSource.id
});

/**
 * 机构养老--镇街床位统计--配置--柱状图
 */
let org_statistics_street_bed_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '按镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '床位数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--镇街床位统计--柱状图
 */
const org_statistics_street_bed_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_street_bed_quantity_displayer',
        title: '镇街床位统计',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        groupIndex: 1,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_street_bed_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_street_bed_quantity_bar_displayer',
            org_statistics_street_bed_quantity_barOption
        )
    }
);

/**
 * 机构养老--长者年龄段分布--数据源--柱状图
 */
const org_statistics_age_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_age_personnel_quantity`,
        command: `org_statistics_age_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--长者年龄段分布--数据视图--柱状图
 */
const org_statistics_age_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_ability_personnel_quantity`,
    dataSourceID: org_statistics_age_personnel_quantity_dataSource.id
});

/**
 * 机构养老--长者年龄段分布--配置--柱状图
 */
let org_statistics_age_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--长者年龄段分布--柱状图
 */
const org_statistics_age_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_age_personnel_quantity_displayer',
        title: '长者年龄段分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_age_personnel_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_age_personnel_quantity_bar_displayer',
            org_statistics_age_personnel_quantity_barOption
        )
    }
);

/**
 * 机构养老--长者行为能力评分分布--数据源--柱状图
 */
const org_statistics_ability_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_ability_personnel_quantity`,
        command: `org_statistics_ability_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--长者行为能力评分分布--数据视图--柱状图
 */
const org_statistics_ability_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_ability_personnel_quantity`,
    dataSourceID: org_statistics_ability_personnel_quantity_dataSource.id
});

/**
 * 机构养老--预约长者行为能力评分分布--配置--柱状图
 */
let org_statistics_ability_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '能力'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--长者行为能力评分分布--柱状图
 */
const org_statistics_ability_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_ability_personnel_quantity_displayer',
        title: '长者行为能力评分分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_ability_personnel_quantity_dataView],
        // org_statistics_welfare_centre_bed_quantity_dataView
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_ability_personnel_quantity_bar_displayer',
            org_statistics_ability_personnel_quantity_barOption
        )
    }
);
/**
 * 机构养老--长者健康指标分布--数据源--柱状图
 */
const org_statistics_health_indicators_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_health_indicators_personnel_quantity`,
        command: `org_statistics_health_indicators_personnel_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--长者健康指标分布--数据视图--柱状图
 */
const org_statistics_health_indicators_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_health_indicators_personnel_quantity`,
    dataSourceID: org_statistics_health_indicators_personnel_quantity_dataSource.id
});

/**
 * 机构养老--长者健康指标分布--配置--柱状图
 */
let org_statistics_health_indicators_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '能健康指标'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 机构养老--长者健康指标分布--柱状图
 */
const org_statistics_health_indicators_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_health_indicators_personnel_quantity_displayer',
        title: '长者健康指标分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_health_indicators_personnel_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_health_indicators_personnel_quantity_bar_displayer',
            org_statistics_health_indicators_personnel_quantity_barOption
        )
    }
);

/**
 * 机构养老--预约长者数量年龄段分布--数据源--柱状图
 */
const org_statistics_age_subscribe_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_age_subscribe_quantity`,
        command: `org_statistics_age_subscribe_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--预约长者数量年龄段分布--数据视图--柱状图
 */
const org_statistics_age_subscribe_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_age_subscribe_quantity`,
    dataSourceID: org_statistics_age_subscribe_quantity_dataSource.id
});

/**
 * 机构养老--预约长者数量年龄段分布--配置--柱状图
 */
let org_statistics_age_subscribe_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '预约数量统计'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 机构养老--预约长者数量年龄段分布--柱状图
 */
const org_statistics_age_subscribe_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_age_subscribe_quantity_displayer',
        title: '预约长者数量年龄段分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_age_subscribe_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_age_subscribe_quantity_bar_displayer',
            org_statistics_age_subscribe_quantity_barOption
        )
    }
);

/**
 * 机构养老--预约长者行为能力评分分布--数据源--柱状图
 */
const org_statistics_ability_subscribe_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_statistics_ability_subscribe_quantity`,
        command: `org_statistics_ability_subscribe_quantity`,
        isCache: true,
        cacheSpeed: 24 * 30,
        remoteService_Fac
    }
);

/**
 * 机构养老--预约长者行为能力评分分布--数据视图--柱状图
 */
const org_statistics_ability_subscribe_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_statistics_ability_subscribe_quantity`,
    dataSourceID: org_statistics_ability_subscribe_quantity_dataSource.id
});

/**
 * 机构养老--预约长者行为能力评分分布--配置--柱状图
 */
let org_statistics_ability_subscribe_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '能力'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '预约数量统计'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    }]
};

/**
 * 机构养老--预约长者行为能力评分分布--柱状图
 */
const org_statistics_ability_subscribe_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_statistics_ability_subscribe_quantity_displayer',
        title: '预约长者行为能力评分分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [org_statistics_ability_subscribe_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_statistics_ability_subscribe_quantity_bar_displayer',
            org_statistics_ability_subscribe_quantity_barOption
        )
    }
);

/**
 * 机构养老--长者年龄段分布--数据源--柱状图
 */
const org_detail_age_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_detail_age_quantity`,
        command: `org_detail_age_quantity`,
        remoteService_Fac
    }
);

/**
 * 机构养老--长者年龄段分布--数据视图--柱状图
 */
const org_detail_age_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_detail_age_quantity`,
    dataSourceID: org_detail_age_quantity_dataSource.id
});

/**
 * 机构养老--长者年龄段分布--配置--柱状图
 */
let org_detail_age_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--长者年龄段分布--柱状图
 */
const org_detail_age_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_age_quantity_displayer',
        title: '长者年龄段分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_age_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_age_quantity_bar_displayer',
            org_detail_age_quantity_barOption
        )
    }
);

/**
 * 机构养老--长者行为能力评分分布--数据源--柱状图
 */
const org_detail_ability_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_detail_ability_quantity`,
        command: `org_detail_ability_quantity`,
        remoteService_Fac
    }
);

/**
 * 机构养老--长者行为能力评分分布--数据视图--柱状图
 */
const org_detail_ability_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_detail_ability_quantity`,
    dataSourceID: org_detail_ability_quantity_dataSource.id
});

/**
 * 机构养老--长者行为能力评分分布--配置--柱状图
 */
let org_detail_ability_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '行为能力'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--长者行为能力评分分布--柱状图
 */
const org_detail_ability_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_ability_quantity_displayer',
        title: '长者行为能力评分分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        groupIndex: 3,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_ability_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_ability_quantity_bar_displayer',
            org_detail_ability_quantity_barOption
        )
    }
);

/**
 * 机构养老--工作人员年龄段分布--数据源--柱状图
 */
const org_detail_age_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_detail_age_personnel_quantity`,
        command: `org_detail_age_personnel_quantity`,
        remoteService_Fac
    }
);

/**
 * 机构养老--工作人员年龄段分布--数据视图--柱状图
 */
const org_detail_age_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_detail_age_personnel_quantity`,
    dataSourceID: org_detail_age_personnel_quantity_dataSource.id
});

/**
 * 机构养老--工作人员年龄段分布--配置--柱状图
 */
let org_detail_age_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--工作人员年龄段分布--柱状图
 */
const org_detail_age_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_age_personnel_quantity_displayer',
        title: '工作人员年龄段分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        groupIndex: 4,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_age_personnel_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_age_personnel_quantity_bar_displayer',
            org_detail_age_personnel_quantity_barOption
        )
    }
);

/**
 * 机构养老--岗位人员数统计--数据源--柱状图
 */
const org_detail_post_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_detail_post_personnel_quantity`,
        command: `org_detail_post_personnel_quantity`,
        remoteService_Fac
    }
);

/**
 * 机构养老--岗位人员数统计--数据视图--柱状图
 */
const org_detail_post_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_detail_post_personnel_quantity`,
    dataSourceID: org_detail_post_personnel_quantity_dataSource.id
});

/**
 * 机构养老--岗位人员数统计--配置--柱状图
 */
let org_detail_post_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '岗位'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '工作人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--岗位人员数统计--柱状图
 */
const org_detail_post_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_post_personnel_quantity_displayer',
        title: '岗位人员数统计',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_post_personnel_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_post_personnel_quantity_bar_displayer',
            org_detail_post_personnel_quantity_barOption
        )
    }
);

/**
 * 机构养老--工作人员星级分布--数据源--柱状图
 */
const org_detail_star_personnel_quantity_dataSource = createObject(
    AjaxRemoteDataQueryer,
    {
        id: `data_org_detail_star_personnel_quantity`,
        command: `org_detail_star_personnel_quantity`,
        remoteService_Fac
    }
);

/**
 * 机构养老--工作人员星级分布--数据视图--柱状图
 */
const org_detail_star_personnel_quantity_dataView = createObject(ReportDataView, {
    id: `data_view-org_detail_star_personnel_quantity`,
    dataSourceID: org_detail_star_personnel_quantity_dataSource.id
});

/**
 * 机构养老--工作人员星级分布--配置--柱状图
 */
let org_detail_star_personnel_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '星级'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '工作人员数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 机构养老--工作人员星级分布--柱状图
 */
const org_detail_star_personnel_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'org_detail_star_personnel_quantity_displayer',
        title: '工作人员星级分布',
        titleable: true,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        groupIndex: 5,
        containerType: ContainerType.frameContain,
        dataViews: [org_detail_star_personnel_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'org_detail_star_personnel_quantity_bar_displayer',
            org_detail_star_personnel_quantity_barOption
        )
    }
);

/**
 * 机构养老--养老院现场--视频监控（摄像头列表，点击放大）
 */
// const org_detail_video_surveillanceModuleControl = createObject(
//     DataDisplayerModuleControl,
//     {
//         id: 'org_detail_video_surveillance_displayer',
//         titleable: false,
//         title: '养老院现场视频监控',
//         groupType: GroupContainerType.carousel,
//         group: "right-carousel2",
//         groupIndex: 1,
//         dataViews: [org_detail_video_surveillance_DataView],
//         containerType: ContainerType.frameContain,
//         // isDialog: true,
//         // parentContainerID: allMapModuleControl.id,
//         displayer: createObject(
//             VideoControl,
//             {
//                 config: {
//                     url: "http://183.235.223.56:3300/live?port=21001&app=rtmp&stream=1",
//                     type: "flv",
//                     autoplay: true,
//                     live: true,
//                     hotkey: false
//                 },
//                 width: 1800,
//                 height: 1200
//             }
//         )
//     }
// );

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                layout: layoutSelect,
            }
        )
    }
);

const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(CommonLayoutMiddleControl);
const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    org_statistics_total_quantityDataSource,
                    org_statistics_street_quantity_dataSource,
                    org_statistics_welfare_centre_bed_quantity_dataSource,
                    org_statistics_street_bed_quantity_dataSource,
                    org_statistics_nature_quantity_DataSource,
                    org_statistics_live_bed_quantity_DataSource,
                    org_statistics_education_personnel_quantity_DataSource,
                    org_statistics_age_subscribe_quantity_dataSource,
                    org_statistics_ability_subscribe_quantity_dataSource,
                    org_statistics_age_personnel_quantity_dataSource,
                    org_statistics_ability_personnel_quantity_dataSource,
                    org_statistics_qualifications_personnel_quantity_DataSource,
                    org_statistics_sex_personnel_quantity_DataSource,
                    org_statistics_children_personnel_quantity_DataSource,
                    org_statistics_health_indicators_personnel_quantity_dataSource,
                    org_statistics_identity_affiliation_personnel_quantity_DataSource,
                    org_statistics_sex_subscribe_quantity_DataSource,
                    org_statistics_time_subscribe_quantity_DataSource,
                    org_statistics_time_income_quantity_DataSource,
                    org_statistics_time_expenditure_quantity_DataSource,
                ],
                modules: [
                    // 0
                    org_statistics_total_quantityModuleControl,
                    // 1
                    org_statistics_street_quantity_displayModuleControl,
                    org_statistics_nature_quantity_displayModuleControl,
                    org_statistics_welfare_centre_bed_displayModuleControl,
                    org_statistics_street_bed_quantity_displayModuleControl,
                    org_statistics_live_bed_quantity_displayModuleControl,
                    // 2
                    org_statistics_ability_personnel_quantity_displayModuleControl,
                    org_statistics_children_personnel_quantity_displayModuleControl,
                    org_statistics_health_indicators_personnel_quantity_displayModuleControl,
                    org_statistics_identity_affiliation_personnel_quantity_displayModuleControl,
                    // 3
                    org_statistics_education_personnel_quantity_displayModuleControl,
                    org_statistics_qualifications_personnel_quantity_displayModuleControl,
                    org_statistics_sex_personnel_quantity_displayModuleControl,
                    org_statistics_age_personnel_quantity_displayModuleControl,
                    // 4
                    org_statistics_sex_subscribe_quantity_displayModuleControl,
                    org_statistics_age_subscribe_quantity_displayModuleControl,
                    org_statistics_ability_subscribe_quantity_displayModuleControl,
                    // 5
                    org_statistics_time_subscribe_quantity_displayModuleControl,
                    org_statistics_time_income_quantity_displayModuleControl,
                    org_statistics_time_expenditure_quantity_displayModuleControl,

                ],
                layout: layoutLeft,
                autoQuery: 1
            }
        )
    });

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    org_map_location_welfare_centreDataSource
                ],
                modules: [
                    org_map_location_welfare_centreModuleControl,
                ],
                layout: layoutMiddle,
                autoQuery: 1
            }
        )
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    org_detail_total_quantityDataSource,
                    org_detail_picture_listCarouselDataSource,
                    org_detail_welfare_centreCarouselDataSource,
                    org_detail_personal_listDataSource,
                    org_detail_sex_personnel_quantity_DataSource,
                    org_detail_age_personnel_quantity_dataSource,
                    org_detail_welfare_centreRoomStatusDataSource,
                    org_detail_activity_picture_listCarouselDataSource,
                    org_detail_video_surveillance_DataSource,
                    org_detail_service_price_pictureCarouselDataSource,
                    org_detail_service_products_listDataSource,
                    org_detail_service_productsDataSource,
                    org_detail_sex_quantity_DataSource,
                    org_detail_education_quantity_DataSource,
                    org_detail_origin_quantity_DataSource,
                    org_detail_age_quantity_dataSource,
                    org_detail_ability_quantity_dataSource,
                    org_detail_education_personnel_quantity_DataSource,
                    org_detail_qualifications_personnel_quantity_DataSource,
                    org_detail_live_bed_quantity_DataSource,
                    org_detail_time_live_quantity_DataSource,
                    org_detail_time_line_up_quantity_DataSource,
                    org_detail_post_personnel_quantity_dataSource,
                    org_detail_star_personnel_quantity_dataSource,
                    org_detail_personal_listDataSource,
                ],
                modules: [
                    // 0
                    // org_detail_total_quantityModuleControl,
                    org_detail_activity_picture_list_DisplayerModule,
                    org_detail_activity_picture_list_DialogDisplayerModule,
                    // 1
                    // org_detail_video_surveillanceModuleControl,
                    org_detail_picture_list_DisplayerModule,
                    org_detail_picture_list_DialogDisplayerModule,
                    // 2
                    org_detail_welfare_centre_DisplayerModule,
                    org_detail_service_price_picture_DisplayerModule,
                    org_detail_service_price_picture_DialogDisplayerModule,
                    org_detail_service_products_listModuleControl,
                    // 3
                    // org_detail_service_productsModuleControl,
                    org_detail_sex_quantity_displayModuleControl,
                    org_detail_education_quantity_displayModuleControl,
                    org_detail_origin_quantity_displayModuleControl,
                    org_detail_age_quantity_displayModuleControl,
                    org_detail_ability_quantity_displayModuleControl,
                    org_detail_personal_listModuleControl,
                    // 4
                    org_detail_welfare_centre_room_status_DisplayerModule,
                    org_detail_sex_personnel_quantity_displayModuleControl,
                    org_detail_age_personnel_quantity_displayModuleControl,
                    org_detail_education_personnel_quantity_displayModuleControl,
                    org_detail_qualifications_personnel_quantity_displayModuleControl,
                    // 5
                    org_detail_star_personnel_quantity_displayModuleControl,
                    org_detail_live_bed_quantity_displayModuleControl,
                    // org_detail_time_live_quantity_displayModuleControl,
                    // org_detail_time_line_up_quantity_displayModuleControl,
                    org_detail_post_personnel_quantity_displayModuleControl,
                ],
                layout: layoutRight,
                defaultParams: { id: "daad11fa-d08d-11e9-ba89-144f8aec0be5" },
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(CommonLayoutControl);
const irWelfareCentreControl = createObject(
    SmartReportControl,
    {
        id: "irWelfareCentre",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightControl],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irWelfareCentreControl as SmartReportReduxControl };