import { createObject, IDataService } from "pao-aop";
import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDisplayControlService } from "src/business/models/control";
import { AjaxRemoteDataQueryer, DataDisplayerModuleControl, ReportDataView, SmartReportControl, SmartReportModuleControl } from "src/business/report/smart";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { createMultiRectangularCoordinateDataDisplayer, createStackMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { ContainerType } from "src/business/report/widget";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { remote } from "src/projects/remote";
import { } from "../echartCommonVar";
// import echarts from "echarts";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";
import { SingleBubbleControl } from "src/business/report/displayers/nh-welfare/single-bubble";
import { createLiquidfillDataDisplayer } from "src/business/report/template/liquidfill";
import { LegendMenuControl } from "src/projects/components/lengend-menu";
import { getCallCenterEle, iconMap, noBeadhouse, noBlessed, noService, noServePropleServing, noServePropleFree, noElderAddr, noElderDevice } from "src/projects/util-tool";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { NewDivCommonLayoutLeftControl } from "src/projects/components/layout/new-div-common-layout/left";
import { NewDivCommonLayoutMiddleControl } from "src/projects/components/layout/new-div-common-layout/middle";
import { NewDivCommonLayouRightLeftControl } from "src/projects/components/layout/new-div-common-layout/right-left";
import { NewDivCommonLayouRightRightControl } from "src/projects/components/layout/new-div-common-layout/right-right";
import { NewDivCommonLayoutControl } from "src/projects/components/layout/new-div-common-layout";
// 养老大数据整体统计分析

// function createDataSoureAndDataView(commandId:string, serName:string) {
//     return {
//         dataVource:   new AjaxRemoteDataQueryer(
//                 `data_source_new_eld_statistics_time_elder_quantity`,
//                 `new_eld_statistics_time_elder_quantity`,
//                 new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
//             ),
//         dataView: createObject(ReportDataView, {
//             id: `data-view-org_detail-welfare-centre-room-status`,
//             dataSourceID: org_detail_welfare_centreRoomStatusDataSource.id
//         })
//     }
// }

/**
 * 各镇街长者数量--饼图
 */
const new_org_detail_origin_quantity_PieDataSource = new AjaxRemoteDataQueryer(
    `data-source-pie-new_org_detail_origin_quantity`,
    `new_org_detail_origin_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 各镇街长者数量--饼图
 */
const new_org_detail_origin_quantityPieDataView = createObject(ReportDataView, {
    id: `data-view-pie-new_org_detail_origin_quantity`,
    dataSourceID: new_org_detail_origin_quantity_PieDataSource.id
});

/**
 * 各镇街长者数量--饼图
 */
let new_org_detail_origin_quantityPieChartOption: EChartsOptions = {
    legend: {
        show: false
    },
    grid: {
        containLabel: true
    },
    series: [
        {
            type: 'pie',
            center: ["50%", "50%"],
            radius: ['22%', '44%'],
        }
    ]
};

/**
 * 各镇街长者数量--饼图
 */
const new_org_detail_origin_quantityPieDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'single-pie',
        dataViews: [new_org_detail_origin_quantityPieDataView],
        titleable: true,
        title: "各镇街长者数量",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel4",
        // groupIndex: 3,
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'new_org_detail_origin_quantity-single-pie-displayer',
            new_org_detail_origin_quantityPieChartOption
        ),
    }
);

/**
 * 全区长者数--水波图
 */
const new_aswhole_all_elder_sum_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_all_elder_sum`,
    `new_aswhole_all_elder_sum`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 全区长者数--水波图
 */
const new_aswhole_all_elder_sum_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_all_elder_sum`,
    dataSourceID: new_aswhole_all_elder_sum_quantity_DataSource.id
});

/**
 * 全区长者数--水波图
 */
// let gaugeOption: EChartsOptions = {
//     series: [
//         {
//             type: 'gauge',
//             title: {
//                 show: true,
//                 color: "white",
//                 // offsetCenter: [0, '-40%'],       // x, y，单位px
//             },
//             axisLine: {
//                 show: true,
//                 lineStyle: {
//                     width: 20,
//                     color:
//                         [
//                             [
//                                 0.98, new echarts.graphic.LinearGradient(
//                                     0, 0, 1, 0, [{
//                                         offset: 0,
//                                         color: '#34C7FC'
//                                     },
//                                     {
//                                         offset: 1,
//                                         color: '#E46EFA'
//                                     }
//                                 ]
//                                 )
//                             ],
//                             [
//                                 1, 'white'
//                             ]
//                         ]
//                 }
//             },
//             detail: {
//                 // show: true,
//                 // // offsetCenter: [0, '10%'],
//                 // color: '#ddd',
//                 // formatter: function (params: any) {
//                 //     return `满意率\n ${params}%`;
//                 // },
//             }
//         }
//     ]
//     // backgroundColor: 'black'
// };

/**
 * 全区长者数--水波图
 */
const new_aswhole_all_elder_sumDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_all_elder_sum-gauge',
        dataViews: [new_aswhole_all_elder_sum_DataView],
        titleable: true,
        title: "全区长者数",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel3",
        // groupIndex: 2,
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'value',
                nameFieldName: "name"
            },
            'new_aswhole_all_elder-gauge-displayer',
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value + '%';
                    },
                    fontSize: 28,
                    color: '#000'
                }
            } as any
        )
        // displayer: createGaugeDataDisplayer(
        //     {
        //         gaugeValueFieldName: 'value',
        //         gaugeKeyFieldName: "name"
        //     },
        //     'new_aswhole_all_elder-gauge-displayer',
        //     gaugeOption
        // ),
    }
);
new_aswhole_all_elder_sumDisplayerModule;

/**
 * 各镇街机构数量统计--柱状堆叠图
 */
const new_aswhole_sum_statistical_street_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_sum_statistical_street`,
    `new_aswhole_sum_statistical_street`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 各镇街机构数量统计--柱状堆叠图
 */
const new_aswhole_sum_statistical_street_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_sum_statistical_street`,
    dataSourceID: new_aswhole_sum_statistical_street_DataSource.id
});

/**
 * 各镇街机构数量统计--柱状堆叠图
 */
let new_aswhole_sum_statistical_street_barOption: EChartsOptions = {
    grid: {
        top: 100
    },
    legend: {
        show: true,
        top: 30
    },
    xAxis: {
        type: 'category',
        boundaryGap: true,
    },
    yAxis: {
        type: 'value',
        name: "家"
    },
    series: [
        {
            type: 'bar',
            barWidth: 70
        }
    ]
};

/**
 * 各镇街机构数量统计--柱状堆叠图
 */
const new_aswhole_sum_statistical_street_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'znew_aswhole_sum_statistical_street-bar',
        titleable: true,
        title: "各镇街机构数量统计",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel",
        // groupIndex: 0,
        dataViews: [new_aswhole_sum_statistical_street_DataView],
        containerType: ContainerType.frameContain,
        displayer: createStackMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['养老院', '幸福院', '服务商'],
                stackType: "bar"
            },
            'zhangzheshu-yiyang-bar-displayer',
            new_aswhole_sum_statistical_street_barOption
        )
    }
);

/**
 * 养老机构数量趋势图--折线图
 */
const new_aswhole_pension_trends_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_pension_trends`,
    `new_aswhole_pension_trends`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老机构数量趋势图--折线图
 */
const new_aswhole_pension_trends_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_pension_trends`,
    dataSourceID: new_aswhole_pension_trends_DataSource.id
});

/**
 * 养老机构数量趋势图--折线图
 */
let new_aswhole_pension_trends_multilineOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '家',
    },
    series: [
        { type: 'line' }
    ]
};

/**
 * 养老机构数量趋势图--折线图
 */
const new_aswhole_pension_trends_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_pension_trends-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "养老机构数量趋势",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel2",
        // groupIndex: 1,
        dataViews: [new_aswhole_pension_trends_DataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_aswhole_pension_trends-multi-line-displayer',
            new_aswhole_pension_trends_multilineOption)
    }
);

/**
 * 全区长者数--柱状图
 */
const new_aswhole_all_elder_sum_bar_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_all_elder_sum_bar`,
    `new_aswhole_all_elder_sum_bar`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 全区长者数--柱状图
 */
const new_aswhole_all_elder_sum_bar_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_all_elder_sum_bar`,
    dataSourceID: new_aswhole_all_elder_sum_bar_DataSource.id
});

/**
 * 全区长者数--柱状图
 */
let new_aswhole_all_elder_sum_bar_multilineOption: EChartsOptions = {
    xAxis: {
        type: 'category',
    },
    grid: {
        left: 140,
        right: 40
    },
    yAxis: {
        type: 'value',
        name: '人',
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 全区长者数--柱状图
 */
const new_aswhole_all_elder_sum_bar_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_all_elder_sum_bar-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "全区长者数",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel3",
        // groupIndex: 2,
        dataViews: [new_aswhole_all_elder_sum_bar_DataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_aswhole_all_elder_sum_bar-multi-line-displayer',
            new_aswhole_all_elder_sum_bar_multilineOption)
    }
);

/**
 * 各镇街服务人员数量统计--柱状图
 */
const new_aswhole_service_personnel_sum_dataSource = new AjaxRemoteDataQueryer(
    `data_new_aswhole_service_personnel_sum`,
    `new_aswhole_service_personnel_sum`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 各镇街服务人员数量统计--柱状图
 */
const new_aswhole_service_personnel_sum_dataView = createObject(ReportDataView, {
    id: `data_view-new_aswhole_service_personnel_sum`,
    dataSourceID: new_aswhole_service_personnel_sum_dataSource.id
});

/**
 * 各镇街服务人员数量统计--柱状图
 */
let new_aswhole_service_personnel_sum_barOption: EChartsOptions = {
    grid: {
        bottom: 150
    },
    xAxis: {
        type: 'category',
        axisLabel: {
            margin: 55
        }
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '家'
    },
    series: [{
        type: 'pictorialBar',
        symbolSize: [100, 45],
        symbolOffset: [0, -20],
        symbolPosition: 'end',
        z: 12,
        label: {
            normal: {
                show: true,
                fontSize: 32,
                color: 'white',
                position: "top"
            }
        },
        itemStyle: {
            normal: {
                color: '#14b1eb'
            }
        },
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    },
    {
        type: 'pictorialBar',
        symbolSize: [100, 45],
        symbolOffset: [0, 20],
        z: 12,
        itemStyle: {
            normal: {
                color: '#14b1eb'
            }
        },
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    },
    {
        type: 'pictorialBar',
        symbolSize: [150, 75],
        symbolOffset: [0, 37],
        z: 11,
        itemStyle: {
            normal: {
                color: 'transparent',
                borderColor: '#14b1eb',
                borderWidth: 5
            }
        },
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    },
    {
        type: 'pictorialBar',
        symbolSize: [200, 100],
        symbolOffset: [0, 50],
        z: 10,
        itemStyle: {
            normal: {
                color: 'transparent',
                borderColor: '#14b1eb',
                borderType: 'dashed',
                borderWidth: 5
            }
        },
        animationDelay: function (idx: number) {
            return idx * 10;
        }
    },
    {
        type: 'bar',
        label: {
            normal: {
                show: false
            }
        },
        itemStyle: {
            normal: {
                color: '#14b1eb',
                opacity: .7
            }
        },
        silent: true,
        barWidth: 100,
        barGap: '-100%', // Make series be overlap
    }
    ]
};

/**
 * 各镇街服务人员数量统计--柱状图
 */
const new_aswhole_service_personnel_sum_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_service_personnel_sum_displayer',
        title: '镇街服务人员数量分布',
        titleable: true,
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel3",
        // groupIndex: 2,
        containerType: ContainerType.frameContain,
        dataViews: [new_aswhole_service_personnel_sum_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y', 'y', 'y', 'y', 'y']
            },
            'new_aswhole_service_personnel_sum_bar_displayer',
            new_aswhole_service_personnel_sum_barOption
        )
    }
);

/**
 * 呼叫中心--24小时呼叫记录--折线图
 */
const new_aswhole_call_center_line_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_call_center_line`,
    `new_aswhole_call_center_line`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--24小时呼叫记录--折线图
 */
const new_aswhole_call_center_line_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_call_center_line`,
    dataSourceID: new_aswhole_call_center_line_DataSource.id
});

/**
 * 呼叫中心--24小时呼叫记录--折线图
 */
let new_aswhole_call_center_line_multilineOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '次',
    },
    series: [
        { type: 'line' }
    ],
};

/**
 * 呼叫中心--24小时呼叫记录--折线图
 */
const new_aswhole_call_center_line_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_call_center_line-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        // title: "呼叫中心",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel6",
        // groupIndex: 5,
        dataViews: [new_aswhole_call_center_line_DataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_aswhole_call_center_line-multi-line-displayer',
            new_aswhole_call_center_line_multilineOption)
    }
);

/**
 * 呼叫中心--泡泡
 */
const new_aswhole_call_center_bubble_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_call_center_bubble`,
    `new_aswhole_call_center_bubble`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--泡泡
 */
const new_aswhole_call_center_bubble_bubble_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_call_center_bubble`,
    dataSourceID: new_aswhole_call_center_bubble_DataSource.id
});

/**
 * 呼叫中心--泡泡
 */
const new_aswhole_call_center_bubble_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_call_center_bubble-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "呼叫中心",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel6",
        // groupIndex: 5,
        dataViews: [new_aswhole_call_center_bubble_bubble_DataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_wf_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_sum_statistical_wf`,
    `new_aswhole_sum_statistical_wf`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_wf_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_sum_statistical_wf`,
    dataSourceID: new_aswhole_sum_statistical_wf_quantity_DataSource.id
});

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_wf_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_sum_statistical_wf-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "总数统计",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel",
        // groupIndex: 0,
        dataViews: [new_aswhole_sum_statistical_wf_DataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_hp_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_sum_statistical_hp`,
    `new_aswhole_sum_statistical_hp`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_hp_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_sum_statistical_hp`,
    dataSourceID: new_aswhole_sum_statistical_hp_quantity_DataSource.id
});

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_hp_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_sum_statistical_hp-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        title: "",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel",
        // groupIndex: 0,
        dataViews: [new_aswhole_sum_statistical_hp_DataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_pv_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_sum_statistical_pv`,
    `new_aswhole_sum_statistical_pv`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_pv_DataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_sum_statistical_pv`,
    dataSourceID: new_aswhole_sum_statistical_pv_quantity_DataSource.id
});

/**
 * 总数统计--泡泡
 */
const new_aswhole_sum_statistical_pv_multilineDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_sum_statistical_pv-multi-line',
        containerType: ContainerType.frameContain,
        titleable: true,
        // title: "总数统计",
        // groupType: GroupContainerType.carousel,
        // group: "left-carousel",
        // groupIndex: 0,
        dataViews: [new_aswhole_sum_statistical_pv_DataView],
        displayer: createObject(
            SingleBubbleControl
        )
    }
);

/**
 * 呼叫中心--未接通率--表盘
 */
const new_aswhole_call_center_big_bubble_leftDataSource = new AjaxRemoteDataQueryer(
    `data-source-new_aswhole_call_center_big_bubble_left`,
    `new_aswhole_call_center_big_bubble_left`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--未接通率--表盘
 */
const new_aswhole_call_center_big_bubble_leftDataView = createObject(ReportDataView, {
    id: `data-view-new_aswhole_call_center_big_bubble_left-gague`,
    dataSourceID: new_aswhole_call_center_big_bubble_leftDataSource.id
});

/**
 * 呼叫中心--未接通率--表盘
 */
const new_aswhole_call_center_big_bubble_leftDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'ew_aswhole_call_center_big_bubble_left-gauge',
        dataViews: [new_aswhole_call_center_big_bubble_leftDataView],
        titleable: true,
        // title: "呼叫中心",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel6",
        // groupIndex: 5,
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'value',
                nameFieldName: "name"
            },
            'ew_aswhole_call_center_big_bubble_left-gauge-displayer',
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value + '%';
                    },
                    fontSize: 28,
                    color: '#000'
                }
            } as any
        ),
    }
);

/**
 * 呼叫中心--满意率--表盘
 */
const new_aswhole_call_center_big_bubble_rightDataSource = new AjaxRemoteDataQueryer(
    `data-source-new_aswhole_call_center_big_bubble_right`,
    `new_aswhole_call_center_big_bubble_right`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 呼叫中心--满意率--表盘
 */
const new_aswhole_call_center_big_bubble_rightDataView = createObject(ReportDataView, {
    id: `data-view-new_aswhole_call_center_big_bubble_right-gague`,
    dataSourceID: new_aswhole_call_center_big_bubble_rightDataSource.id
});

/**
 * 呼叫中心--满意率--表盘
 */
const new_aswhole_call_center_big_bubble_rightDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'ew_aswhole_call_center_big_bubble_left-gauge',
        dataViews: [new_aswhole_call_center_big_bubble_rightDataView],
        titleable: true,
        // title: "呼叫中心",
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel6",
        // groupIndex: 5,
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'value',
                nameFieldName: "name"
            },
            'ew_aswhole_call_center_big_bubble_left-gauge-displayer',
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value + '%';
                    },
                    fontSize: 28,
                    color: '#000'
                }
            } as any
        ),
    }
);

/**
 * 长者--长者数量趋势图--数据源--折线图
 */
const new_eld_statistics_time_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_eld_statistics_time_elder_quantity`,
    `new_eld_statistics_time_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者--长者数量趋势图--数据视图--折线图
 */
const new_eld_statistics_time_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_new_eld_statistics_time_elder_quantity`,
    dataSourceID: new_eld_statistics_time_elder_quantity_DataSource.id
});

/**
 * 长者--长者数量趋势图--配置--折线图
 */
let new_eld_statistics_time_elder_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    // grid: {
    //     left: 140
    // },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '万人',
    },
    series: [
        { type: 'line' }
    ],
};

/**
 * 长者--长者数量趋势图--折线图
 */
const new_eld_statistics_time_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_eld_statistics_time_elder_quantity_multi_line',
        title: '长者数量趋势图',
        titleable: true,
        dataViews: [new_eld_statistics_time_elder_quantity_DataView],
        // group: "right-carousel4",
        // groupIndex: 3,
        // groupType: GroupContainerType.carousel,
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'new_eld_statistics_time_elder_quantity_multi_line_displayer',
            new_eld_statistics_time_elder_quantity_multilineOption
        )
    }
);

/**
 * 长者--14类长者政策扶持统计--柱状图
 */
const new_eld_statistics_subsidy_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_eld_statistics_subsidy_elder_quantity`,
    `new_eld_statistics_subsidy_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者--14类长者政策扶持统计--柱状图
 */
const new_eld_statistics_subsidy_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_new_eld_statistics_subsidy_elder_quantity`,
    dataSourceID: new_eld_statistics_subsidy_elder_quantity_DataSource.id
});

/**
 * 长者--14类长者政策扶持统计--柱状图
 */
let new_eld_statistics_subsidy_elder_quantity_pieOption: EChartsOptions = {
    // title: {
    //     text: '24小时呼叫记录',
    // },
    xAxis: {
        type: 'category',
    },
    yAxis: {
        type: 'value',
        name: '人',
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
    }]
};

/**
 * 长者--14类长者政策扶持统计--柱状图
 */
const new_eld_statistics_subsidy_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_eld_statistics_subsidy_elder_quantity_pie',
        title: '14类长者政策扶持统计',
        titleable: true,
        dataViews: [new_eld_statistics_subsidy_elder_quantity_DataView],
        // group: "left-carousel5",
        // groupIndex: 4,
        // groupType: GroupContainerType.carousel,
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                yAxisFieldNames: ['y'],
                xAxisFieldName: 'x',
            },
            'new_eld_statistics_subsidy_elder_quantity_pie_displayer',
            new_eld_statistics_subsidy_elder_quantity_pieOption
        )
    }
);

/**
 * 慈善补贴情况--折线图
 */
const new_cop_statistics_donation_income_quantity_line_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_cop_statistics_donation_income_quantity_line`,
    `new_cop_statistics_donation_income_quantity_line`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 慈善补贴情况--折线图
 */
const new_cop_statistics_donation_income_quantity_line_DataView = createObject(ReportDataView, {
    id: `data_view_new_cop_statistics_donation_income_quantity_line`,
    dataSourceID: new_cop_statistics_donation_income_quantity_line_DataSource.id

});

/**
 * 慈善补贴情况--折线图
 */
let new_cop_statistics_donation_income_quantity_line_pieOption: EChartsOptions = {
    legend: {
        show: true,
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '万元',
    },
    series: [
        { type: 'line' },
        { type: 'line' },
    ],
};

/**
 * 慈善补贴情况--折线图
 */
const new_cop_statistics_donation_income_quantity_line_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_cop_statistics_donation_income_quantity_line_multi_line',
        // title: '慈善补贴情况',
        titleable: true,
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel5",
        // groupIndex: 4,
        dataViews: [new_cop_statistics_donation_income_quantity_line_DataView],
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2']
            },
            'new_cop_statistics_donation_income_quantity_line_multi_line_displayer',
            new_cop_statistics_donation_income_quantity_line_pieOption
        )
    }
);

/**
 * 慈善金额--水波图
 */
const new_donation_amount_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_donation_amount`,
    `new_donation_amount`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 慈善金额--水波图
 */
const new_donation_amount_DataView = createObject(ReportDataView, {
    id: `data_view_new_donation_amount`,
    dataSourceID: new_donation_amount_DataSource.id
});

/**
 * 慈善金额--水波图
 */
// let new_donation_amount_pieOption: EChartsOptions = {
//     series: [
//         {
//             type: 'gauge',
//             title: {
//                 show: true,
//                 color: "white",
//                 // offsetCenter: [0, '-40%'],       // x, y，单位px
//             },
//             axisLine: {
//                 show: true,
//                 lineStyle: {
//                     width: 20,
//                     color:
//                         [
//                             [
//                                 0.98, new echarts.graphic.LinearGradient(
//                                     0, 0, 1, 0, [{
//                                         offset: 0,
//                                         color: '#34C7FC'
//                                     },
//                                     {
//                                         offset: 1,
//                                         color: '#E46EFA'
//                                     }
//                                 ]
//                                 )
//                             ],
//                             [
//                                 1, 'white'
//                             ]
//                         ]
//                 }
//             },
//             detail: {
//                 // show: true,
//                 // // offsetCenter: [0, '10%'],
//                 // color: '#ddd',
//                 // formatter: function (params: any) {
//                 //     return `满意率\n ${params}%`;
//                 // },
//             }
//         }
//     ]
// };

/**
 * 慈善金额--水波图
 */
const new_donation_amount_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_donation_amount_multi_line',
        // title: '慈善补贴情况',
        titleable: true,
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel5",
        // groupIndex: 4,
        dataViews: [new_donation_amount_DataView],
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'value',
                nameFieldName: "name"
            },
            'new_aswhole_all_elder-gauge-displayer',
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value + '%';
                    },
                    fontSize: 28,
                    color: '#000'
                }
            } as any
        ),
    }
);

/**
 * 补贴金额--水波图
 */
const new_subsidy_amount_DataSource = new AjaxRemoteDataQueryer(
    `data_source_new_subsidy_amount`,
    `new_subsidy_amount`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 补贴金额--水波图
 */
const new_subsidy_amount_DataView = createObject(ReportDataView, {
    id: `data_view_new_subsidy_amount`,
    dataSourceID: new_subsidy_amount_DataSource.id
});

/**
 * 补贴金额--水波图
 */
// let new_subsidy_amount_pieOption: EChartsOptions = {
//     series: [
//         {
//             type: 'gauge',
//             title: {
//                 show: true,
//                 color: "white",
//                 // offsetCenter: [0, '-40%'],       // x, y，单位px
//             },
//             axisLine: {
//                 show: true,
//                 lineStyle: {
//                     width: 20,
//                     color:
//                         [
//                             [
//                                 0.98, new echarts.graphic.LinearGradient(
//                                     0, 0, 1, 0, [{
//                                         offset: 0,
//                                         color: '#34C7FC'
//                                     },
//                                     {
//                                         offset: 1,
//                                         color: '#E46EFA'
//                                     }
//                                 ]
//                                 )
//                             ],
//                             [
//                                 1, 'white'
//                             ]
//                         ]
//                 }
//             },
//             detail: {
//                 // show: true,
//                 // // offsetCenter: [0, '10%'],
//                 // color: '#ddd',
//                 // formatter: function (params: any) {
//                 //     return `满意率\n ${params}%`;
//                 // },
//             }
//         }
//     ]
// };

/**
 * 补贴金额--水波图
 */
const new_subsidy_amount_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_subsidy_amount_multi_line',
        // title: '慈善补贴情况',
        titleable: true,
        // groupType: GroupContainerType.carousel,
        // group: "right-carousel5",
        // groupIndex: 4,
        dataViews: [new_subsidy_amount_DataView],
        containerType: ContainerType.frameContain,
        displayer: createLiquidfillDataDisplayer(
            {
                valueFieldName: 'value',
                nameFieldName: "name"
            },
            'new_aswhole_all_elder-gauge-displayer',
            {
                label: {
                    formatter: function (param: any) {
                        return param.name + '\n\n' + param.value + '%';
                    },
                    fontSize: 28,
                    color: '#000'
                }
            } as any
        ),
    }
);

/**
 * 机构养老--福利院位置--数据源--星空图
 */
const new_aswhole_mapDataSource = new AjaxRemoteDataQueryer(
    `data_source_new_aswhole_map`,
    `new_aswhole_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 机构养老--福利院位置--数据视图--星空图
 */
const new_aswhole_mapDataView = createObject(ReportDataView, {
    id: `data_view_new_aswhole_map`,
    dataSourceID: new_aswhole_mapDataSource.id
});

let option: EChartsOptions = {
    legend: {
        selected: {
            "养老机构": true,
            "社区幸福院": false,
            "居家服务商": false,
            "服务人员_服务中": false,
            "服务人员_空闲": false,
            "长者_住址": false,
            "长者_设备": false
        },
        data: ["养老机构", "社区幸福院", "居家服务商", "服务人员_服务中", "服务人员_空闲", "长者_住址", "长者_设备"]
    },
    // legend: {
    //     data: ['6666', '社区幸福院']
    // },
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};

/**
 * 构养老--福利院位置--星空图
 */
const new_aswhole_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_map',
        // title: '福利院位置',
        titleable: false,
        dataViews: [new_aswhole_mapDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'longitude',
                latitudeFieldName: 'latitude',
                callLatitudeFieldName: "y",
                callLongitudeFieldName: "x",
                nameFieldName: "name",
                ponitIconSizeFieldName: "num",
                getElementFn: getCallCenterEle,
                scatterObject: iconMap
            },
            "new_aswhole_map-modules",
            option
        )
    }
);

/**
 * 养老院图片--弹窗--图片轮播--测试
 */
const org_detail_service_price_pictureCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_org_detail_service_price_picture`,
    `org_detail_service_price_picture`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 养老院图片--弹窗--图片轮播--测试
 */
const org_detail_service_price_picture_carousel_DataView = createObject(ReportDataView, {
    id: `data-view-org_detail_service_price_picture-carousel`,
    dataSourceID: org_detail_service_price_pictureCarouselDataSource.id
});

/*
* 养老院图片--弹窗--图片轮播--测试
*/
const new_aswhole_map_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'new_aswhole_map-carousel-dialog',
        title: '福利院环境',
        titleable: false,
        isDialog: true,
        parentContainerID: new_aswhole_mapModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [org_detail_service_price_picture_carousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                ],
                modules: [
                ],
                autoQuery: 1,
                layout: layoutSelect,
            }
        )
    }
);

/** 图例配置 */
const legendOption = [
    {
        title: "养老机构",
        key: ["养老机构"],
        icon: noBeadhouse,
        selected: true
    },
    {
        title: "社区幸福院",
        key: ["社区幸福院"],
        icon: noBlessed,
        selected: false
    },
    {
        title: "居家服务商",
        key: ["居家服务商"],
        icon: noService,
        selected: false
    },
    {
        title: "服务人员_服务中",
        key: ["服务人员_服务中"],
        icon: noServePropleServing,
        selected: false
    },
    {
        title: "服务人员_空闲",
        key: ["服务人员_空闲"],
        icon: noServePropleFree,
        selected: false
    },
    {
        title: "长者_住址",
        key: ["长者_住址"],
        icon: noElderAddr,
        selected: false
    },
    {
        title: "长者_设备",
        key: ["长者_设备"],
        icon: noElderDevice,
        selected: false
    }
];

/** 
 * 图例控制器
 */
const legendControl = createObject(
    LegendMenuControl,
    {
        display_control: new_aswhole_mapModuleControl.displayer,
        option: legendOption
    }
);

const layoutLeft = createObject(NewDivCommonLayoutLeftControl);
const layoutMiddle = createObject(
    NewDivCommonLayoutMiddleControl,
    {
        legend_control: legendControl
    }
);
const layoutRightLeft = createObject(NewDivCommonLayouRightLeftControl);
const layoutRightRight = createObject(NewDivCommonLayouRightRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_aswhole_sum_statistical_wf_quantity_DataSource,
                    new_aswhole_sum_statistical_hp_quantity_DataSource,
                    new_aswhole_sum_statistical_pv_quantity_DataSource,
                    new_aswhole_all_elder_sum_bar_DataSource,
                    // new_aswhole_all_elder_sum_quantity_DataSource,
                    new_eld_statistics_subsidy_elder_quantity_DataSource,
                    new_org_detail_origin_quantity_PieDataSource
                ],
                modules: [
                    // 1
                    new_aswhole_sum_statistical_wf_multilineDisplayerModule,
                    new_aswhole_sum_statistical_hp_multilineDisplayerModule,
                    new_aswhole_sum_statistical_pv_multilineDisplayerModule,
                    // 3
                    new_aswhole_all_elder_sum_bar_multilineDisplayerModule,
                    // 4
                    new_org_detail_origin_quantityPieDisplayerModule,
                    // 5
                    new_eld_statistics_subsidy_elder_quantity_displayModuleControl
                ],
                layout: layoutLeft,
                autoQuery: 1
            }
        )
    });

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    new_aswhole_mapDataSource
                ],
                modules: [
                    new_aswhole_mapModuleControl
                ],
                layout: layoutMiddle,
                autoQuery: 5000,
            }
        )
    }
);

const rightLeftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_aswhole_sum_statistical_street_DataSource,
                    new_aswhole_service_personnel_sum_dataSource,
                    new_cop_statistics_donation_income_quantity_line_DataSource,
                    new_subsidy_amount_DataSource,
                    new_donation_amount_DataSource,
                ],
                modules: [
                    // 1
                    new_aswhole_sum_statistical_street_displayModuleControl,
                    // 3
                    new_aswhole_map_DialogDisplayerModule,
                    new_aswhole_service_personnel_sum_displayModuleControl,
                    // 5
                    new_subsidy_amount_displayModuleControl,
                    new_donation_amount_displayModuleControl,
                    new_cop_statistics_donation_income_quantity_line_displayModuleControl,
                ],
                layout: layoutRightLeft,
                autoQuery: 1
            }
        )
    }
);

const rightRightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    new_aswhole_pension_trends_DataSource,
                    new_eld_statistics_time_elder_quantity_DataSource,
                    new_aswhole_call_center_line_DataSource,
                    new_aswhole_call_center_bubble_DataSource,
                    new_aswhole_call_center_big_bubble_leftDataSource,
                    new_aswhole_call_center_big_bubble_rightDataSource
                ],
                modules: [
                    new_aswhole_pension_trends_multilineDisplayerModule,
                    new_eld_statistics_time_elder_quantity_displayModuleControl,
                    // 5
                    // new_subsidy_amount_displayModuleControl,
                    // new_donation_amount_displayModuleControl,
                    // new_cop_statistics_donation_income_quantity_line_displayModuleControl,
                    // 6
                    new_aswhole_call_center_bubble_multilineDisplayerModule,
                    new_aswhole_call_center_big_bubble_leftDisplayerModule,
                    new_aswhole_call_center_big_bubble_rightDisplayerModule,
                    new_aswhole_call_center_line_multilineDisplayerModule,
                ],
                layout: layoutRightRight,
                autoQuery: 1
            }
        )
    }
);

const layoutAll = createObject(NewDivCommonLayoutControl);
const irWelfareBigDataStatisticsControl = createObject(
    SmartReportControl,
    {
        id: "irWelfareBigDataStatistics",
        // defaultParams: { id: 'db2f563a-d08d-11e9-90a3-144f8aec0be5' },
        modules: [selectControl, leftControl, middleControl, rightLeftControl, rightRightControl],
        dataSources: [],
        layout: layoutAll,
        areAllQuery: true,
        isAsyncQuery: false,
        isMain: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irWelfareBigDataStatisticsControl };
