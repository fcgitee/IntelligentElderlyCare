import { AjaxJsonRpcFactory, EChartsOptions } from "pao-aop-client";
import { IDataService, createObject } from "pao-aop";
import { ContainerType, GroupContainerType } from "src/business/report/widget";
import { DataDisplayerModuleControl, SmartReportControl, SmartReportModuleControl, ReportDataView, AjaxRemoteDataQueryer } from "src/business/report/smart";
import { MoreSquaresControl } from "src/business/report/displayers/nh-welfare/more-squares";
import { createMultiRectangularCoordinateDataDisplayer } from "src/business/report/template/rectangle-coordinate";
import { createPieChartDataDisplayer } from "src/business/report/template/pie-chart";
import { CurrencyTableControl } from "src/business/report/displayers/table";
import { CommonLayoutLeftControl } from "src/projects/components/layout/common-layout/left";
import { CommonLayoutRightControl } from "src/projects/components/layout/common-layout/right";
import { CommonLayoutMiddleControl } from "src/projects/components/layout/common-layout/middle";
import { SelectLayoutControl } from "src/projects/components/layout/common-layout/select";
import { CommonLayoutControl } from "src/projects/components/layout/common-layout";
import { CardDetailDisplayControl } from "src/business/report/displayers/nh-welfare/card-detail-display";
import { IDisplayControlService } from "src/business/models/control";
import { remote } from "src/projects/remote";
import { VideoControl } from "src/business/components/buss-components/video";
import { CarouselWithTwoStatisticDisplayControl } from "src/business/report/displayers/nh-welfare/carousel-display/carousel-with-two-statistic";
import { createAllTypeMapDataDisplayer } from "src/business/report/template/all-type-map";

/**
 * 长者--入住福利院长者总人数--居家和社区服务长者总人数--数值
 */
const eld_statistics_total_quantityDataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_total_quantity`,
    `eld_statistics_total_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者地理位置--星空图
 */
const eld_map_locationDataSource = new AjaxRemoteDataQueryer(
    `data-source-eld_map_location`,
    `eld_map_location`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--个人简介--自定义控件
 */
const eld_detail_elderDataSource = new AjaxRemoteDataQueryer(
    `data-source-eld_detail_elder`,
    `eld_detail_elder`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者--个人简介--自定义控件
 */
const eld_detail_task_cardDataSource = new AjaxRemoteDataQueryer(
    `data-source-eld_detail_task_card`,
    `eld_detail_task_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者--长者地理位置--明细热力图
 */
const eld_detail_elder_mapDataSource = new AjaxRemoteDataQueryer(
    `data-source-eld_detail_elder_map`,
    `eld_detail_elder_map`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 长者--参与活动情况--图片轮播
 */
const eld_detail_activity_picture_listCarouselDataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_detail_activity_picture_list_carousel`,
    `eld_detail_activity_picture_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 长者总人数--入住福利院长者总人数--居家和社区服务长者总人数--数值
 */
const eld_statistics_total_quantityDataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_total_quantity`,
    dataSourceID: eld_statistics_total_quantityDataSource.id
});
/**
 * 长者--参与活动情况--图片轮播
 */
const eld_detail_activity_picture_listCarousel_DataView = createObject(ReportDataView, {
    id: `data-view-eld_detail_activity_picture_list-carousel`,
    dataSourceID: eld_detail_activity_picture_listCarouselDataSource.id
});
/**
 * 长者--长者病情分布--数据源--饼图
 */
const eld_statistics_condition_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_condition_elder_quantity`,
    `eld_statistics_condition_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者数量补贴类型分布--数据源--饼图
 */
const eld_statistics_subsidy_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_subsidy_elder_quantity`,
    `eld_statistics_subsidy_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--养老方式长者数量分布--数据源--饼图
 */
const eld_statistics_pension_mode_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_pension_mode_elder_quantity`,
    `eld_statistics_pension_mode_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者性别比例--数据源--饼图
 */
const eld_statistics_sex_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_sex_elder_quantity`,
    `eld_statistics_sex_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者服务次数病情分布--数据源--饼图
 */
const eld_statistics_condition_service_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_condition_service_quantity`,
    `eld_statistics_condition_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者购买服务金额病情分布--数据源--饼图
 */
const eld_statistics_condition_buy_money_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_condition_buy_money_quantity`,
    `eld_statistics_condition_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者数量走势--数据源--折线图
 */
const eld_statistics_time_elder_quantity_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_time_elder_quantity`,
    `eld_statistics_time_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者年龄走势--数据源--折线图
 */
const eld_statistics_time_elder_age_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_time_elder_age`,
    `eld_statistics_time_elder_age`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者男女比例走势--数据源--折线图
 */
const eld_statistics_time_elder_sex_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_statistics_time_elder_sex`,
    `eld_statistics_time_elder_sex`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);
/**
 * 长者--长者地理位置--星空图
 */
const eld_map_locationDataView = createObject(ReportDataView, {
    id: `data-view-eld_map_location`,
    dataSourceID: eld_map_locationDataSource.id
});

/**
 * 长者--个人简介--自定义控件
 */
const eld_detail_elderDataView = createObject(ReportDataView, {
    id: `data-view-eld_detail_elder`,
    dataSourceID: eld_detail_elderDataSource.id
});

/**
 * 长者--当前任务卡片--自定义控件
 */
const eld_detail_task_cardDataView = createObject(ReportDataView, {
    id: `data-view-eld_detail_task_card`,
    dataSourceID: eld_detail_task_cardDataSource.id
});

/**
 * 长者--长者地理位置--明细热力图
 */
const eld_detail_elder_mapDataView = createObject(ReportDataView, {
    id: `data-view-eld_detail_elder_map`,
    dataSourceID: eld_detail_elder_mapDataSource.id
});
/**
 * 长者--参与活动情况--图片轮播
 */
const eld_detail_activity_picture_list_DisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_activity_picture_list-carousel',
        title: '参与活动情况',
        titleable: false,
        containerType: ContainerType.frameContain,
        dataViews: [eld_detail_activity_picture_listCarousel_DataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "right-carousel5",
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);

/**
 * 长者总人数--入住福利院长者总人数--居家和社区服务长者总人数--数值
 */
const eld_statistics_total_quantityModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_total_quantity-num',
        title: '长者总数',
        titleable: false,
        groupIndex: 0,
        groupType: GroupContainerType.carousel,
        group: "left-carousel",
        dataViews: [eld_statistics_total_quantityDataView],
        containerType: ContainerType.frameContain,
        displayer: createObject(
            MoreSquaresControl,
            {
                layout: "horizontal",
                square_span: 24,
                name_fields: ['长者总数', '入住福利院长者总数', '居家和社区服务长者总数'],
                value_field: 'y',
            }
        )
    }
);
/**
 * 长者--长者病情分布--数据视图--饼图
 */
const eld_statistics_condition_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_condition_elder_quantity`,
    dataSourceID: eld_statistics_condition_elder_quantity_DataSource.id
});
/**
 * 长者--长者数量补贴类型分布--数据视图--饼图
 */
const eld_statistics_subsidy_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_subsidy_elder_quantity`,
    dataSourceID: eld_statistics_subsidy_elder_quantity_DataSource.id
});
/**
 * 长者--养老方式长者数量分布--数据视图--饼图
 */
const eld_statistics_pension_mode_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_pension_mode_elder_quantity`,
    dataSourceID: eld_statistics_pension_mode_elder_quantity_DataSource.id
});
/**
 * 长者--长者性别比例--数据视图--饼图
 */
const eld_statistics_sex_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_sex_elder_quantity`,
    dataSourceID: eld_statistics_sex_elder_quantity_DataSource.id
});
/**
 * 长者--长者服务次数病情分布--数据视图--饼图
 */
const eld_statistics_condition_service_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_condition_service_quantity`,
    dataSourceID: eld_statistics_condition_service_quantity_DataSource.id
});
/**
 * 长者--长者购买服务金额病情分布--数据视图--饼图
 */
const eld_statistics_condition_buy_money_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_condition_buy_money_quantity`,
    dataSourceID: eld_statistics_condition_buy_money_quantity_DataSource.id
});
/**
 * 长者--长者数量走势--数据视图--折线图
 */
const eld_statistics_time_elder_quantity_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_time_elder_quantity`,
    dataSourceID: eld_statistics_time_elder_quantity_DataSource.id
});
/**
 * 长者--长者年龄走势--数据视图--折线图
 */
const eld_statistics_time_elder_age_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_time_elder_age`,
    dataSourceID: eld_statistics_time_elder_age_DataSource.id
});
/**
 * 长者--长者男女比例走势--数据视图--折线图
 */
const eld_statistics_time_elder_sex_DataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_time_elder_sex`,
    dataSourceID: eld_statistics_time_elder_sex_DataSource.id
});
let option: EChartsOptions = {
    // tooltip: {
    //     triggerOn: "none"
    // },
    tooltip: {
        show: true,
        trigger: "item",
        // triggerOn: "mousemove",
        formatter: '{b}'
    },
};
/**
 * 长者--长者病情分布--配置--饼图
 */
let eld_statistics_condition_elder_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--长者数量补贴类型分布--配置--饼图
 */
let eld_statistics_subsidy_elder_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--养老方式长者数量分布--配置--饼图
 */
let eld_statistics_pension_mode_elder_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--长者性别比例--配置--饼图
 */
let eld_statistics_sex_elder_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--长者服务次数病情分布--配置--饼图
 */
let eld_statistics_condition_service_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--长者购买服务金额病情分布--配置--饼图
 */
let eld_statistics_condition_buy_money_quantity_pieOption: EChartsOptions = {
    legend: {
        show: true, textStyle: { color: '#333' }
    },
    series: [
        {
            type: 'pie',
        }
    ]
};
/**
 * 长者--长者数量走势--配置--折线图
 */
let eld_statistics_time_elder_quantity_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者数量走势',
    },
};
/**
 * 长者--长者年龄走势--配置--折线图
 */
let eld_statistics_time_elder_age_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者年龄比例走势',
    },
};
/**
 * 长者--长者男女比例走势--配置--折线图
 */
let eld_statistics_time_elder_sex_multilineOption: EChartsOptions = {
    legend: {
        show: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
    },
    yAxis: {
        type: 'value',
        name: '长者男女比例走势',
    },
};
/**
 * 长者--长者地理位置--明细热力图
 */
const eld_detail_elder_mapModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_map_location',
        title: '长者地理位置',
        titleable: false,
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "right-carousel2",
        dataViews: [eld_detail_elder_mapDataView],
        containerType: ContainerType.nhContain,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "eld_map_location-modules",
            option
        )
    }
);
eld_detail_elder_mapModuleControl;
/**
 * 长者--长者地理位置--星空图
 */
const eld_map_locationModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_map_location',
        title: '长者地理位置',
        titleable: false,
        dataViews: [eld_map_locationDataView],
        containerType: ContainerType.withdialog,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "eld_map_location-modules",
            option
        )
    }
);
// console.log(eld_map_locationModuleControl);
/**
 * 长者--参与活动情况--图片轮播-弹窗
 */
const eld_detail_activity_picture_list_DialogDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_activity_picture_list-carousel-dialog',
        title: '参与活动情况',
        titleable: false,
        isDialog: true,
        // parentContainerID: eld_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        dataViews: [eld_detail_activity_picture_listCarousel_DataView],
        displayer: createObject(
            CarouselWithTwoStatisticDisplayControl
        )
    }
);
/**
 * 长者--长者位置--明细热力图-弹窗
 */
const eld_detail_elder_mapDialogModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_map_location-dialog',
        title: '长者位置',
        titleable: false,
        dataViews: [eld_detail_elder_mapDataView],
        isDialog: true,
        // parentContainerID: eld_map_locationModuleControl.id!,
        containerType: ContainerType.blank,
        displayer: createAllTypeMapDataDisplayer(
            {
                longitudeFieldName: 'x',
                latitudeFieldName: 'y',
                nameFieldName: "z"
            },
            "eld_map_location-modules",
            option
        )
    }
);
eld_detail_elder_mapDialogModuleControl;
/**
 * 长者--个人简介--自定义控件
 */
const eld_detail_elderModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_elder',
        title: '个人简介',
        titleable: false,
        dataViews: [eld_detail_elderDataView],
        containerType: ContainerType.nhContain,
        groupIndex: 0,
        group: "right-carousel",
        groupType: GroupContainerType.carousel,
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['头像', '住址', '联系方式', '所在机构', '所在床位', '能力评估', '护理级别']
            }
        )
    }
);

/**
 * 长者--当前任务卡片--自定义控件
 */
const eld_detail_task_cardModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_task_card_pie',
        title: '当前任务卡片',
        titleable: false,
        dataViews: [eld_detail_task_cardDataView],
        containerType: ContainerType.nhContain,
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "right-carousel4",
        displayer: createObject(
            CardDetailDisplayControl,
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y1', 'y2', 'y3', 'y4', 'y5', 'y6', 'y7'],
                titleName: ['任务编号', '服务产品', '服务地点', '服务时间', '服务商', '服务对象', '服务人员']
            }
        )
    }
);

/**
 * 长者--长者病情分布--饼图
 */
const eld_statistics_condition_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_condition_elder_quantity_pie',
        title: '长者病情分布',
        titleable: false,
        dataViews: [eld_statistics_condition_elder_quantity_DataView],
        groupType: GroupContainerType.carousel,
        groupIndex: 2,
        group: "right-carousel3",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_condition_elder_quantity_pie_displayer',
            eld_statistics_condition_elder_quantity_pieOption
        )
    }
);
/**
 * 长者--长者数量补贴类型分布--饼图
 */
const eld_statistics_subsidy_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_subsidy_elder_quantity_pie',
        title: '长者数量补贴类型分布',
        titleable: false,
        dataViews: [eld_statistics_subsidy_elder_quantity_DataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_subsidy_elder_quantity_pie_displayer',
            eld_statistics_subsidy_elder_quantity_pieOption
        )
    }
);
/**
 * 长者--养老方式长者数量分布--饼图
 */
const eld_statistics_pension_mode_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_pension_mode_elder_quantity_pie',
        title: '养老方式长者数量分布',
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        titleable: false,
        dataViews: [eld_statistics_pension_mode_elder_quantity_DataView],
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_pension_mode_elder_quantity_pie_displayer',
            eld_statistics_pension_mode_elder_quantity_pieOption
        )
    }
);
/**
 * 长者--长者性别比例--饼图
 */
const eld_statistics_sex_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_sex_elder_quantity_pie',
        title: '长者性别比例',
        titleable: false,
        dataViews: [eld_statistics_sex_elder_quantity_DataView],
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_sex_elder_quantity_pie_displayer',
            eld_statistics_sex_elder_quantity_pieOption
        )
    }
);
/**
 * 长者--长者服务次数病情分布--配置--饼图
 */
const eld_statistics_condition_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_condition_service_quantity_pie',
        title: '长者服务次数病情分布',
        titleable: false,
        dataViews: [eld_statistics_condition_service_quantity_DataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_condition_service_quantity_pie_displayer',
            eld_statistics_condition_service_quantity_pieOption
        )
    }
);
/**
 * 长者--长者购买服务金额病情分布--饼图
 */
const eld_statistics_condition_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_condition_buy_money_quantity_pie',
        title: '长者购买服务金额病情分布',
        titleable: false,
        dataViews: [eld_statistics_condition_buy_money_quantity_DataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        containerType: ContainerType.frameContain,
        displayer: createPieChartDataDisplayer(
            {
                pieValueFieldName: 'y',
                pieNameFieldName: 'x',
            },
            'eld_statistics_condition_buy_money_quantity_pie_displayer',
            eld_statistics_condition_buy_money_quantity_pieOption
        )
    }
);
/**
 * 长者--长者数量走势--折线图
 */
const eld_statistics_time_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_time_elder_quantity_multi_line',
        title: '长者数量走势',
        titleable: false,
        dataViews: [eld_statistics_time_elder_quantity_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_time_elder_quantity_multi_line_displayer',
            eld_statistics_time_elder_quantity_multilineOption
        )
    }
);
/**
 * 长者--长者年龄走势--折线图
 */
const eld_statistics_time_elder_age_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_time_elder_age_multi_line',
        title: '长者年龄走势',
        titleable: false,
        dataViews: [eld_statistics_time_elder_age_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_time_elder_age_multi_line_displayer',
            eld_statistics_time_elder_age_multilineOption
        )
    }
);
/**
 * 长者--长者男女比例走势--折线图
 */
const eld_statistics_time_elder_sex_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_time_elder_sex_multi_line',
        title: '长者男女比例走势',
        titleable: false,
        dataViews: [eld_statistics_time_elder_sex_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        containerType: ContainerType.frameContain,
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_time_elder_sex_multi_line_displayer',
            eld_statistics_time_elder_sex_multilineOption
        )
    }
);
// const legendOption = [
//     {
//         title: "长者星空图",
//         key: "长者星空图"
//     },
//     {
//         title: "长者热力图",
//         key: "长者热力图"
//     }
// ];

/**
 * 长者--镇街长者数量分布--柱状图
 */
const eld_statistics_street_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_street_elder_quantity`,
    `eld_statistics_street_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--镇街长者数量分布--柱状图
 */
const eld_statistics_street_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_street_elder_quantity`,
    dataSourceID: eld_statistics_street_elder_quantity_dataSource.id
});

/**
 * 长者--镇街长者数量分布--柱状图
 */
let eld_statistics_street_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '镇街'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--镇街长者数量分布--柱状图
 */
const eld_statistics_street_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_street_elder_quantity_displayer',
        title: '镇街长者数量分布',
        titleable: true,
        groupIndex: 1,
        groupType: GroupContainerType.carousel,
        group: "left-carousel2",
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_street_elder_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_street_elder_quantity_bar_displayer',
            eld_statistics_street_elder_quantity_barOption
        )
    }
);

/**
 * 长者--长者行为能力评分分布--柱状图
 */
const eld_statistics_ability_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_ability_elder_quantity`,
    `eld_statistics_ability_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--长者行为能力评分分布--柱状图
 */
const eld_statistics_ability_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_ability_elder_quantity`,
    dataSourceID: eld_statistics_ability_elder_quantity_dataSource.id
});

/**
 * 长者--长者行为能力评分分布--柱状图
 */
let eld_statistics_ability_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '行为能力'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--长者行为能力评分分布--柱状图
 */
const eld_statistics_ability_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_ability_elder_quantity_displayer',
        title: '长者行为能力评分分布',
        titleable: true,
        containerType: ContainerType.frameContain,
        groupIndex: 0,
        group: "right-carousel",
        groupType: GroupContainerType.carousel,
        dataViews: [eld_statistics_ability_elder_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_ability_elder_quantity_bar_displayer',
            eld_statistics_ability_elder_quantity_barOption
        )
    }
);

/**
 * 长者--长者年龄段分布--柱状图
 */
const eld_statistics_age_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_age_elder_quantity`,
    `eld_statistics_age_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--长者年龄段分布--柱状图
 */
const eld_statistics_age_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_age_elder_quantity`,
    dataSourceID: eld_statistics_age_elder_quantity_dataSource.id
});

/**
 * 长者--长者年龄段分布--柱状图
 */
let eld_statistics_age_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--长者年龄段分布--柱状图
 */
const eld_statistics_age_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_age_elder_quantity_displayer',
        title: '长者年龄段分布',
        titleable: true,
        containerType: ContainerType.frameContain,
        groupIndex: 1,
        group: "right-carousel2",
        groupType: GroupContainerType.carousel,
        dataViews: [eld_statistics_age_elder_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_age_elder_quantity_bar_displayer',
            eld_statistics_age_elder_quantity_barOption
        )
    }
);

/**
 * 长者--健康指标长者数据统计--柱状图
 */
const eld_statistics_triglyceride_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_triglyceride_elder_quantity`,
    `eld_statistics_triglyceride_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--健康指标长者数据统计--柱状图
 */
const eld_statistics_triglyceride_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_triglyceride_elder_quantity`,
    dataSourceID: eld_statistics_triglyceride_elder_quantity_dataSource.id
});

/**
 * 长者--健康指标长者数据统计--柱状图
 */
let eld_statistics_triglyceride_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '甘油三酯'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--健康指标长者数据统计--柱状图
 */
const eld_statistics_triglyceride_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_triglyceride_elder_quantity_displayer',
        title: '健康指标长者数据统计',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_triglyceride_elder_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_triglyceride_elder_quantity_bar_displayer',
            eld_statistics_triglyceride_elder_quantity_barOption
        )
    }
);

/**
 * 长者--体温偏高长者数量--柱状图
 */
const eld_statistics_temperature_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_temperature_elder_quantity`,
    `eld_statistics_temperature_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体温偏高长者数量--柱状图
 */
const eld_statistics_temperature_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_temperature_elder_quantity`,
    dataSourceID: eld_statistics_temperature_elder_quantity_dataSource.id
});

/**
 * 长者--体温偏高长者数量--柱状图
 */
let eld_statistics_temperature_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体温偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体温偏高长者数量--柱状图
 */
const eld_statistics_temperature_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_temperature_elder_quantity_displayer',
        title: '体温偏高长者数量',
        titleable: true,
        containerType: ContainerType.frameContain,
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        dataViews: [eld_statistics_temperature_elder_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_temperature_elder_quantity_bar_displayer',
            eld_statistics_temperature_elder_quantity_barOption
        )
    }
);

/**
 * 长者--脉搏偏高长者数量--柱状图
 */
const eld_statistics_pulse_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_pulse_elder_quantity`,
    `eld_statistics_pulse_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--脉搏偏高长者数量--柱状图
 */
const eld_statistics_pulse_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_pulse_elder_quantity`,
    dataSourceID: eld_statistics_pulse_elder_quantity_dataSource.id
});

/**
 * 长者--脉搏偏高长者数量--柱状图
 */
let eld_statistics_pulse_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '脉搏偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--脉搏偏高长者数量--柱状图
 */
const eld_statistics_pulse_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_pulse_elder_quantity_displayer',
        title: '脉搏偏高长者数量',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_pulse_elder_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_pulse_elder_quantity_bar_displayer',
            eld_statistics_pulse_elder_quantity_barOption
        )
    }
);

/**
 * 长者--体水分率偏高长者数量--柱状图
 */
const eld_statistics_water_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_water_elder_quantity`,
    `eld_statistics_water_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体水分率偏高长者数量--柱状图
 */
const eld_statistics_water_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_water_elder_quantity`,
    dataSourceID: eld_statistics_water_elder_quantity_dataSource.id
});

/**
 * 长者--体水分率偏高长者数量--柱状图
 */
let eld_statistics_water_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体水分率偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体水分率偏高长者数量--柱状图
 */
const eld_statistics_water_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_water_elder_quantity_displayer',
        title: '体水分率偏高长者数量',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_water_elder_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_water_elder_quantity_bar_displayer',
            eld_statistics_water_elder_quantity_barOption
        )
    }
);

/**
 * 长者--血压偏高长者数量--柱状图
 */
const eld_statistics_blood_pressure_elder_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_blood_pressure_elder_quantity`,
    `eld_statistics_blood_pressure_elder_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--血压偏高长者数量--柱状图
 */
const eld_statistics_blood_pressure_elder_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_blood_pressure_elder_quantity`,
    dataSourceID: eld_statistics_blood_pressure_elder_quantity_dataSource.id
});

/**
 * 长者--血压偏高长者数量--柱状图
 */
let eld_statistics_blood_pressure_elder_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '血压偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '长者数量'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--血压偏高长者数量--柱状图
 */
const eld_statistics_blood_pressure_elder_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_blood_pressure_elder_quantity_displayer',
        title: '血压偏高长者数量',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_blood_pressure_elder_quantity_dataView],
        groupIndex: 2,
        groupType: GroupContainerType.carousel,
        group: "left-carousel3",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_blood_pressure_elder_quantity_bar_displayer',
            eld_statistics_blood_pressure_elder_quantity_barOption
        )
    }
);

/**
 * 长者--服务次数长者（TOP10）--柱状图
 */
const eld_statistics_elder_service_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_elder_service_top10_quantity`,
    `eld_statistics_elder_service_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--服务次数长者（TOP10）--柱状图
 */
const eld_statistics_elder_service_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_elder_service_top10_quantity`,
    dataSourceID: eld_statistics_elder_service_top10_quantity_dataSource.id
});

/**
 * 长者--服务次数长者（TOP10）--柱状图
 */
let eld_statistics_elder_service_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--服务次数长者（TOP10）--柱状图
 */
const eld_statistics_elder_service_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_elder_service_top10_quantity_displayer',
        title: '服务次数长者（TOP10）',
        titleable: true,
        containerType: ContainerType.frameContain,
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        dataViews: [eld_statistics_elder_service_top10_quantity_dataView],
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_elder_service_top10_quantity_bar_displayer',
            eld_statistics_elder_service_top10_quantity_barOption
        )
    }
);

/**
 * 长者--低密度蛋白偏高长者服务次数--柱状图
 */
const eld_statistics_protein_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_protein_service_quantity`,
    `eld_statistics_protein_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--低密度蛋白偏高长者服务次数--柱状图
 */
const eld_statistics_protein_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_protein_service_quantity`,
    dataSourceID: eld_statistics_protein_service_quantity_dataSource.id
});

/**
 * 长者--低密度蛋白偏高长者服务次数--柱状图
 */
let eld_statistics_protein_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '低密度蛋白偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--低密度蛋白偏高长者服务次数--柱状图
 */
const eld_statistics_protein_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_protein_service_quantity_displayer',
        title: '低密度蛋白偏高长者服务次数',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_protein_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_protein_service_quantity_bar_displayer',
            eld_statistics_protein_service_quantity_barOption
        )
    }
);

/**
 * 长者--健康指标服务数量统计--柱状图
 */
const eld_statistics_triglyceride_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_triglyceride_service_quantity`,
    `eld_statistics_triglyceride_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--健康指标服务数量统计--柱状图
 */
const eld_statistics_triglyceride_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_triglyceride_service_quantity`,
    dataSourceID: eld_statistics_triglyceride_service_quantity_dataSource.id
});

/**
 * 长者--健康指标服务数量统计--柱状图
 */
let eld_statistics_triglyceride_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '甘油三酯'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--健康指标服务数量统计--柱状图
 */
const eld_statistics_triglyceride_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_triglyceride_service_quantity_displayer',
        title: '健康指标服务数量统计',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_triglyceride_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_triglyceride_service_quantity_bar_displayer',
            eld_statistics_triglyceride_service_quantity_barOption
        )
    }
);

/**
 * 长者--体温偏高长者服务次数--柱状图
 */
const eld_statistics_temperature_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_temperature_service_quantity`,
    `eld_statistics_temperature_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体温偏高长者服务次数--柱状图
 */
const eld_statistics_temperature_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_temperature_service_quantity`,
    dataSourceID: eld_statistics_temperature_service_quantity_dataSource.id
});

/**
 * 长者--体温偏高长者服务次数--柱状图
 */
let eld_statistics_temperature_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体温偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体温偏高长者服务次数--柱状图
 */
const eld_statistics_temperature_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_temperature_service_quantity_displayer',
        title: '体温偏高长者服务次数',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_temperature_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_temperature_service_quantity_bar_displayer',
            eld_statistics_temperature_service_quantity_barOption
        )
    }
);

/**
 * 长者--脉博偏高长者服务次数--柱状图
 */
const eld_statistics_pulse_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_pulse_service_quantity`,
    `eld_statistics_pulse_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--脉博偏高长者服务次数--柱状图
 */
const eld_statistics_pulse_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_pulse_service_quantity`,
    dataSourceID: eld_statistics_pulse_service_quantity_dataSource.id
});

/**
 * 长者--脉博偏高长者服务次数--柱状图
 */
let eld_statistics_pulse_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '脉搏偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--脉博偏高长者服务次数--柱状图
 */
const eld_statistics_pulse_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_pulse_service_quantity_displayer',
        title: '脉博偏高长者服务次数',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_pulse_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_pulse_service_quantity_bar_displayer',
            eld_statistics_pulse_service_quantity_barOption
        )
    }
);

/**
 * 长者--体水分率偏高长者服务次数--柱状图
 */
const eld_statistics_water_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_water_service_quantity`,
    `eld_statistics_water_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体水分率偏高长者服务次数--柱状图
 */
const eld_statistics_water_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_water_service_quantity`,
    dataSourceID: eld_statistics_water_service_quantity_dataSource.id
});

/**
 * 长者--体水分率偏高长者服务次数--柱状图
 */
let eld_statistics_water_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体水分率偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体水分率偏高长者服务次数--柱状图
 */
const eld_statistics_water_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_water_service_quantity_displayer',
        title: '体水分率偏高长者服务次数',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_water_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_water_service_quantity_bar_displayer',
            eld_statistics_water_service_quantity_barOption
        )
    }
);

/**
 * 长者--血压偏高服务次数--柱状图
 */
const eld_statistics_blood_pressure_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_blood_pressure_service_quantity`,
    `eld_statistics_blood_pressure_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--血压偏高服务次数--柱状图
 */
const eld_statistics_blood_pressure_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_blood_pressure_service_quantity`,
    dataSourceID: eld_statistics_blood_pressure_service_quantity_dataSource.id
});

/**
 * 长者--血压偏高服务次数--柱状图
 */
let eld_statistics_blood_pressure_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '血压偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--血压偏高服务次数--柱状图
 */
const eld_statistics_blood_pressure_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_blood_pressure_service_quantity_displayer',
        title: '血压偏高服务次数',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_blood_pressure_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_blood_pressure_service_quantity_bar_displayer',
            eld_statistics_blood_pressure_service_quantity_barOption
        )
    }
);

/**
 * 长者--长者服务需求--柱状图
 */
const eld_statistics_age_service_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_age_service_quantity`,
    `eld_statistics_age_service_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--长者服务需求--柱状图
 */
const eld_statistics_age_service_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_age_service_quantity`,
    dataSourceID: eld_statistics_age_service_quantity_dataSource.id
});

/**
 * 长者--长者服务需求--柱状图
 */
let eld_statistics_age_service_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务次数'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--长者服务需求--柱状图
 */
const eld_statistics_age_service_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_age_service_quantity_displayer',
        title: '长者服务需求',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_age_service_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_age_service_quantity_bar_displayer',
            eld_statistics_age_service_quantity_barOption
        )
    }
);

/**
 * 长者--购买服务费用长者（TOP10）--柱状图
 */
const eld_statistics_elder_buy_money_top10_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_elder_buy_money_top10_quantity`,
    `eld_statistics_elder_buy_money_top10_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--购买服务费用长者（TOP10）--柱状图
 */
const eld_statistics_elder_buy_money_top10_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_elder_buy_money_top10_quantity`,
    dataSourceID: eld_statistics_elder_buy_money_top10_quantity_dataSource.id
});

/**
 * 长者--购买服务费用长者（TOP10）--柱状图
 */
let eld_statistics_elder_buy_money_top10_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '长者'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '服务费用（TOP10）'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--购买服务费用长者（TOP10）--柱状图
 */
const eld_statistics_elder_buy_money_top10_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_elder_buy_money_top10_quantity_displayer',
        title: '购买服务费用长者（TOP10）',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_elder_buy_money_top10_quantity_dataView],
        groupIndex: 3,
        groupType: GroupContainerType.carousel,
        group: "left-carousel4",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_elder_buy_money_top10_quantity_bar_displayer',
            eld_statistics_elder_buy_money_top10_quantity_barOption
        )
    }
);

/**
 * 长者--低密度蛋白偏高长者购买服务金额--柱状图
 */
const eld_statistics_protein_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_protein_buy_money_quantity`,
    `eld_statistics_protein_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--低密度蛋白偏高长者购买服务金额--柱状图
 */
const eld_statistics_protein_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_protein_buy_money_quantity`,
    dataSourceID: eld_statistics_protein_buy_money_quantity_dataSource.id
});

/**
 * 长者--低密度蛋白偏高长者购买服务金额--柱状图
 */
let eld_statistics_protein_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '低密度蛋白偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--低密度蛋白偏高长者购买服务金额--柱状图
 */
const eld_statistics_protein_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_protein_buy_money_quantity_displayer',
        title: '低密度蛋白偏高长者购买服务金额',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_protein_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_protein_buy_money_quantity_bar_displayer',
            eld_statistics_protein_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--健康指标购买服务金统计--柱状图
 */
const eld_statistics_triglyceride_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_triglyceride_buy_money_quantity`,
    `eld_statistics_triglyceride_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--健康指标购买服务金统计--柱状图
 */
const eld_statistics_triglyceride_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_triglyceride_buy_money_quantity`,
    dataSourceID: eld_statistics_triglyceride_buy_money_quantity_dataSource.id
});

/**
 * 长者--健康指标购买服务金统计--柱状图
 */
let eld_statistics_triglyceride_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '甘油三酯'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--健康指标购买服务金统计--柱状图
 */
const eld_statistics_triglyceride_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_triglyceride_buy_money_quantity_displayer',
        title: '健康指标购买服务金统计',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_triglyceride_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_triglyceride_buy_money_quantity_bar_displayer',
            eld_statistics_triglyceride_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--体温偏高长者购买服务金额--柱状图
 */
const eld_statistics_temperature_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_temperature_buy_money_quantity`,
    `eld_statistics_temperature_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体温偏高长者购买服务金额--柱状图
 */
const eld_statistics_temperature_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_temperature_buy_money_quantity`,
    dataSourceID: eld_statistics_temperature_buy_money_quantity_dataSource.id
});

/**
 * 长者--体温偏高长者购买服务金额--柱状图
 */
let eld_statistics_temperature_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体温偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--健康指标购买服务金统计--柱状图
 */
const eld_statistics_temperature_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_temperature_buy_money_quantity_displayer',
        title: '体温偏高长者购买服务金额',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_temperature_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_temperature_buy_money_quantity_bar_displayer',
            eld_statistics_temperature_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--脉博偏高长者购买服务金额--柱状图
 */
const eld_statistics_pulse_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_pulse_buy_money_quantity`,
    `eld_statistics_pulse_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--脉博偏高长者购买服务金额--柱状图
 */
const eld_statistics_pulse_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_pulse_buy_money_quantity`,
    dataSourceID: eld_statistics_pulse_buy_money_quantity_dataSource.id
});

/**
 * 长者--脉博偏高长者购买服务金额--柱状图
 */
let eld_statistics_pulse_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '脉搏偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--脉博偏高长者购买服务金额--柱状图
 */
const eld_statistics_pulse_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_pulse_buy_money_quantity_displayer',
        title: '脉博偏高长者购买服务金额',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_pulse_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_pulse_buy_money_quantity_bar_displayer',
            eld_statistics_pulse_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--体水分率偏高长者购买服务金额--柱状图
 */
const eld_statistics_water_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_water_buy_money_quantity`,
    `eld_statistics_water_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--体水分率偏高长者购买服务金额--柱状图
 */
const eld_statistics_water_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_water_buy_money_quantity`,
    dataSourceID: eld_statistics_water_buy_money_quantity_dataSource.id
});

/**
 * 长者--体水分率偏高长者购买服务金额--柱状图
 */
let eld_statistics_water_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '体水分率偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体水分率偏高长者购买服务金额--柱状图
 */
const eld_statistics_water_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_water_buy_money_quantity_displayer',
        title: '体水分率偏高长者购买服务金额',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_water_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_water_buy_money_quantity_bar_displayer',
            eld_statistics_water_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--血压偏高购买服务金额--柱状图
 */
const eld_statistics_blood_pressure_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_blood_pressure_buy_money_quantity`,
    `eld_statistics_blood_pressure_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--血压偏高购买服务金额--柱状图
 */
const eld_statistics_blood_pressure_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_blood_pressure_buy_money_quantity`,
    dataSourceID: eld_statistics_blood_pressure_buy_money_quantity_dataSource.id
});

/**
 * 长者--血压偏高购买服务金额--柱状图
 */
let eld_statistics_blood_pressure_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '血压偏高'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--体水分率偏高长者购买服务金额--柱状图
 */
const eld_statistics_blood_pressure_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_blood_pressure_buy_money_quantity_displayer',
        title: '血压偏高购买服务金额',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_blood_pressure_buy_money_quantity_dataView],
        groupIndex: 4,
        groupType: GroupContainerType.carousel,
        group: "left-carousel5",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_blood_pressure_buy_money_quantity_bar_displayer',
            eld_statistics_blood_pressure_buy_money_quantity_barOption
        )
    }
);

/**
 * 长者--长者服务金额投入--柱状图
 */
const eld_statistics_age_buy_money_quantity_dataSource = new AjaxRemoteDataQueryer(
    `data_eld_statistics_age_buy_money_quantity`,
    `eld_statistics_age_buy_money_quantity`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`),
    true,
    24 * 30
);

/**
 * 长者--长者服务金额投入--柱状图
 */
const eld_statistics_age_buy_money_quantity_dataView = createObject(ReportDataView, {
    id: `data_view_eld_statistics_age_buy_money_quantity`,
    dataSourceID: eld_statistics_age_buy_money_quantity_dataSource.id
});

/**
 * 长者--长者服务金额投入--柱状图
 */
let eld_statistics_age_buy_money_quantity_barOption: EChartsOptions = {
    xAxis: {
        type: 'category',
        axisLabel: {
            rotate: 15
        },
        name: '年龄'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            showMinLabel: false,
        },
        name: '购买服务金额'
    },
    series: [{
        type: 'bar',
        animationDelay: function (idx: number) {
            return idx * 10;
        },
        itemStyle: {
            // color: .getBarColor(2)
        }
    }]
};

/**
 * 长者--长者服务金额投入--柱状图
 */
const eld_statistics_age_buy_money_quantity_displayModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_statistics_age_buy_money_quantity_displayer',
        title: '长者服务金额投入',
        titleable: true,
        containerType: ContainerType.frameContain,
        dataViews: [eld_statistics_age_buy_money_quantity_dataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "left-carousel6",
        displayer: createMultiRectangularCoordinateDataDisplayer(
            {
                xAxisFieldName: 'x',
                yAxisFieldNames: ['y']
            },
            'eld_statistics_age_buy_money_quantity_bar_displayer',
            eld_statistics_age_buy_money_quantity_barOption
        )
    }
);
/** 
 * 长者--长者家属--表格
 */
const eld_detail_family_list_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_detail_family_list`,
    `eld_detail_family_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);
/**
 * 长者--长者家属--表格
 */
const eld_detail_family_list_DataView = createObject(ReportDataView, {
    id: `data_view_eld_detail_family_list`,
    dataSourceID: eld_detail_family_list_DataSource.id
});
/**
 * 长者--长者家属--表格
 */
const eld_detail_family_listDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_family_list-table',
        titleable: false,
        containerType: ContainerType.frameContain,
        title: '长者家属',
        dataViews: [eld_detail_family_list_DataView],
        groupIndex: 5,
        groupType: GroupContainerType.carousel,
        group: "right-carousel6",
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '姓名',
                        dataIndex: 'y1',
                        align: 'center'
                    },
                    {
                        title: '关系',
                        dataIndex: 'y2',
                        align: 'center'
                    },
                    {
                        title: '联系方式',
                        dataIndex: 'y3',
                        align: 'center'
                    }
                ],
                maxHeigh: 200
            }
        )
    }
);
/** 
 * 长者--服务记录--表格
 */
const eld_detail_service_record_top10_list_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_detail_service_record_top10_list`,
    `eld_detail_service_record_top10_list`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/** 
 * 长者--服务现场视频--服务现场
 */
const eld_detail_service_live_video_card_DataSource = new AjaxRemoteDataQueryer(
    `data_source_eld_detail_service_live_video_card`,
    `eld_detail_service_live_video_card`,
    new AjaxJsonRpcFactory(new IDataService, remote.url, `IBigScreenService`)
);

/**
 * 长者--服务记录--表格
 */
const eld_detail_service_record_top10_list_DataView = createObject(ReportDataView, {
    id: `data_view_eld_detail_service_record_top10_list`,
    dataSourceID: eld_detail_service_record_top10_list_DataSource.id
});

/**
 * 长者--服务现场视频--服务现场
 */
const eld_detail_service_live_video_card_DataView = createObject(ReportDataView, {
    id: `data_view_eld_detail_service_live_video_card`,
    dataSourceID: eld_detail_service_live_video_card_DataSource.id
});

/**
 * 长者--服务记录--表格
 */
const eld_detail_service_record_top10_listDisplayerModule = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_service_record_top10_list-table',
        titleable: false,
        containerType: ContainerType.frameContain,
        title: '服务记录',
        groupIndex: 2,
        group: "right-carousel3",
        groupType: GroupContainerType.carousel,
        dataViews: [eld_detail_service_record_top10_list_DataView],
        displayer: createObject(
            CurrencyTableControl,
            {
                config: [
                    {
                        title: '服务时间',
                        dataIndex: 'y1',
                        align: 'center'
                    },
                    {
                        title: '服务产品',
                        dataIndex: 'y2',
                        align: 'center'
                    },
                    {
                        title: '服务评价',
                        dataIndex: 'y3',
                        align: 'center'
                    }
                ],
                maxHeigh: 200
            }
        )
    }
);

/**
 * 综合-服务现场视频--摄像头视频
 */
const eld_detail_service_live_video_cardModuleControl = createObject(
    DataDisplayerModuleControl,
    {
        id: 'eld_detail_service_live_video_pie',
        titleable: false,
        dataViews: [eld_detail_service_live_video_card_DataView],
        containerType: ContainerType.nhContain,
        groupIndex: 1,
        group: "right-carousel2",
        groupType: GroupContainerType.carousel,
        // isDialog: true,
        // parentContainerID: allMapModuleControl.id,
        displayer: createObject(
            VideoControl,
            {
                config: {
                    url: "http://localhost:3300/live?port=21001&app=rtmp&stream=1",
                    type: "flv",
                    autoplay: true,
                    live: true,
                    hotkey: false
                },
                width: 600,
                height: 600
            }
        )
    }
);

/** 
 * 图例控制器
 */
// const legendControl = createObject(
//     LegendMenuControl,
//     {
//         echar_instance: eld_map_locationModuleControl.displayer,
//         option: legendOption
//     }
// );

const layoutSelect = createObject(SelectLayoutControl);

const selectControl = createObject(
    SmartReportControl,
    {
        dataSources: [
        ],
        modules: [
        ],
        layout: layoutSelect,
        autoQuery: 1,
    }
);

const layoutLeft = createObject(CommonLayoutLeftControl);
const layoutMiddle = createObject(CommonLayoutMiddleControl);
const layoutRight = createObject(CommonLayoutRightControl);

const leftControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    eld_statistics_pulse_elder_quantity_dataSource,
                    eld_statistics_water_service_quantity_dataSource,
                    eld_statistics_blood_pressure_service_quantity_dataSource,
                    eld_statistics_elder_buy_money_top10_quantity_dataSource,
                    eld_statistics_sex_elder_quantity_DataSource,
                    eld_statistics_condition_service_quantity_DataSource,
                    eld_statistics_condition_buy_money_quantity_DataSource,
                    eld_statistics_time_elder_quantity_DataSource,
                    eld_statistics_time_elder_age_DataSource,
                    eld_statistics_time_elder_sex_DataSource,
                    eld_statistics_water_buy_money_quantity_dataSource,
                    eld_statistics_blood_pressure_buy_money_quantity_dataSource,
                    eld_statistics_age_buy_money_quantity_dataSource,
                    eld_statistics_condition_elder_quantity_DataSource,
                    eld_statistics_subsidy_elder_quantity_DataSource,
                    eld_statistics_pension_mode_elder_quantity_DataSource,
                    eld_statistics_pulse_service_quantity_dataSource,
                    eld_statistics_age_service_quantity_dataSource,
                    eld_statistics_protein_buy_money_quantity_dataSource,
                    eld_statistics_triglyceride_buy_money_quantity_dataSource,
                    eld_statistics_temperature_buy_money_quantity_dataSource,
                    eld_statistics_pulse_buy_money_quantity_dataSource,
                    eld_statistics_water_elder_quantity_dataSource,
                    eld_statistics_blood_pressure_elder_quantity_dataSource,
                    eld_statistics_elder_service_top10_quantity_dataSource,
                    eld_statistics_protein_service_quantity_dataSource,
                    eld_statistics_triglyceride_service_quantity_dataSource,
                    eld_statistics_temperature_service_quantity_dataSource,
                    eld_statistics_total_quantityDataSource,
                    eld_statistics_street_elder_quantity_dataSource,
                    eld_statistics_ability_elder_quantity_dataSource,
                    eld_statistics_age_elder_quantity_dataSource,
                    eld_statistics_triglyceride_elder_quantity_dataSource,
                    eld_statistics_temperature_elder_quantity_dataSource,
                ],
                modules: [
                    // 0
                    eld_statistics_total_quantityModuleControl,
                    // 1
                    eld_statistics_street_elder_quantity_displayModuleControl,
                    eld_statistics_ability_elder_quantity_displayModuleControl,
                    eld_statistics_age_elder_quantity_displayModuleControl,
                    eld_statistics_condition_elder_quantity_displayModuleControl,
                    eld_statistics_subsidy_elder_quantity_displayModuleControl,
                    eld_statistics_pension_mode_elder_quantity_displayModuleControl,
                    eld_statistics_sex_elder_quantity_displayModuleControl,
                    // 2
                    eld_statistics_triglyceride_elder_quantity_displayModuleControl,
                    eld_statistics_temperature_elder_quantity_displayModuleControl,
                    eld_statistics_pulse_elder_quantity_displayModuleControl,
                    eld_statistics_blood_pressure_elder_quantity_displayModuleControl,
                    eld_statistics_water_elder_quantity_displayModuleControl,
                    // 3
                    eld_statistics_elder_service_top10_quantity_displayModuleControl,
                    eld_statistics_protein_service_quantity_displayModuleControl,
                    eld_statistics_triglyceride_service_quantity_displayModuleControl,
                    eld_statistics_temperature_service_quantity_displayModuleControl,
                    eld_statistics_pulse_service_quantity_displayModuleControl,
                    eld_statistics_age_service_quantity_displayModuleControl,
                    eld_statistics_condition_service_quantity_displayModuleControl,
                    eld_statistics_water_service_quantity_displayModuleControl,
                    eld_statistics_blood_pressure_service_quantity_displayModuleControl,
                    eld_statistics_elder_buy_money_top10_quantity_displayModuleControl,
                    // 4
                    eld_statistics_protein_buy_money_quantity_displayModuleControl,
                    eld_statistics_triglyceride_buy_money_quantity_displayModuleControl,
                    eld_statistics_temperature_buy_money_quantity_displayModuleControl,
                    eld_statistics_pulse_buy_money_quantity_displayModuleControl,
                    eld_statistics_water_buy_money_quantity_displayModuleControl,
                    eld_statistics_blood_pressure_buy_money_quantity_displayModuleControl,
                    eld_statistics_condition_buy_money_quantity_displayModuleControl,
                    // 5
                    eld_statistics_age_buy_money_quantity_displayModuleControl,
                    eld_statistics_time_elder_quantity_displayModuleControl,
                    eld_statistics_time_elder_age_displayModuleControl,
                    eld_statistics_time_elder_sex_displayModuleControl,
                ],
                layout: layoutLeft,
                autoQuery: 1,
            }
        )
    }
);

const middleControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                dataSources: [
                    // eld_map_locationDataSource
                ],
                modules: [
                    // eld_map_locationModuleControl,
                ],
                layout: layoutMiddle,
                defaultParams: { mapType: 'scatter' },
                autoQuery: 1,
            }
        )
    }
);
const middleReport = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: middleControl,
    }
);

const rightControl = createObject(
    SmartReportModuleControl,
    {
        containerType: ContainerType.blank,
        report: createObject(
            SmartReportControl,
            {
                title: "",
                dataSources: [
                    eld_detail_service_live_video_card_DataSource,
                    eld_detail_activity_picture_listCarouselDataSource,
                    eld_detail_elderDataSource,
                    eld_detail_task_cardDataSource,
                    eld_detail_elder_mapDataSource,
                    eld_detail_family_list_DataSource,
                    eld_detail_service_record_top10_list_DataSource,
                ],
                modules: [
                    // 0
                    eld_statistics_ability_elder_quantity_displayModuleControl,
                    eld_detail_elderModuleControl,
                    // 1
                    eld_statistics_age_elder_quantity_displayModuleControl,
                    eld_detail_service_live_video_cardModuleControl,
                    // eld_detail_elder_mapModuleControl,
                    // eld_detail_elder_mapDialogModuleControl,
                    // 2
                    eld_statistics_condition_elder_quantity_displayModuleControl,
                    eld_detail_service_record_top10_listDisplayerModule,
                    // 3
                    eld_detail_task_cardModuleControl,
                    // 4
                    eld_detail_activity_picture_list_DisplayerModule,
                    eld_detail_activity_picture_list_DialogDisplayerModule,
                    // 5
                    eld_detail_family_listDisplayerModule,
                ],
                layout: layoutRight,
                defaultParams: { id: '0bdae31a-cfd9-11e9-a2ef-144f8aec0be5' },
                autoQuery: 1,
            }
        )
    }
);

const layoutAll = createObject(CommonLayoutControl);
const irElderControl = createObject(
    SmartReportControl,
    {
        id: 'irElder',
        modules: [selectControl, leftControl, middleReport, rightControl],
        dataSources: [],
        layout: layoutAll,
        // autoQuery: 1,
        areAllQuery: true,
        isAsyncQuery: false,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);

export { irElderControl };