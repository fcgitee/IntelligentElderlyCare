import { ReactViewState, ReactView, reactControl, ReactViewControl, AjaxJsonRpcFactory } from "pao-aop-client";

import React from "react";
import './index.less';
import { addon, getObject } from "pao-aop";
import { Row, Col, message, DatePicker, Table, Icon, Modal, Button, Card, Switch } from "antd";
import Search from "antd/lib/input/Search";
import { IDisplayControlService } from "src/business/models/control";
import { TabletControlService } from "src/projects/app/tablet-pc-control";
import CheckboxGroup from "antd/lib/checkbox/Group";
import { remote } from "src/projects/remote";
/**
 * 组件：平板控制组件状态
 */
export interface computerState extends ReactViewState {
    /** 当前选中的tags */
    activetags?: string;
    /** 当前选择的区域 */
    activeregion?: string;
    /** 当前选择的城镇 */
    activetown?: string;
    /** 区域数组 */
    regiondata?: any;
    /** 城镇数组 */
    towndata?: any;
    /** 页面主要数据 */
    pagedata?: any;
    /** 页面table的column */
    columns?: any;
    /** 视频监控数据 */
    monitordata?: any;
    /** 操作功能数组 */
    operatingdata?: any;
    /** 选中报表的id */
    report_id?: string;
    /** 轮播设置对话框是否展示 */
    modal?: boolean;
    // 统计开始
    /** 养老统计左上是否开启自动轮播 */
    laojilefttopdata?: any;
    /** 养老统计左上值 */
    laojilefttop?: string[];
    /** 养老统计右上是否开启自动轮播 */
    laojirighttopdata?: any;
    /** 养老统计右上值 */
    laojirighttop?: string[];
    /** 养老统计左中是否开启自动轮播 */
    laojileftcenterdata?: any;
    /** 养老统计左中值 */
    laojileftcenter?: string[];
    /** 养老统计右中是否开启自动轮播 */
    laojirightcenterdata?: any;
    /** 养老统计右中值 */
    laojirightcenter?: string[];
    /** 养老统计左下是否开启自动轮播 */
    laojileftbottomdata?: any;
    /** 养老统计左下值 */
    laojileftbottom?: string[];
    /** 养老统计右下是否开启自动轮播 */
    laojirightbottomdata?: any;
    /** 养老统计右下值 */
    laojirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 养老明细左上是否开启自动轮播 */
    laoxilefttopdata?: any;
    /** 养老明细左上的值 */
    laoxilefttop?: string[];
    /** 养老明细右上是否开启自动轮播 */
    laoxirighttopdata?: any;
    /** 养老明细右上的值 */
    laoxirighttop?: string[];
    /** 养老明细左中是否开启自动轮播 */
    laoxileftcenterdata?: any;
    /** 养老明细左中的值 */
    laoxileftcenter?: string[];
    /** 养老明细右中是否开启自动轮播 */
    laoxirightcenterdata?: any;
    /** 养老明细右中的值 */
    laoxirightcenter?: string[];
    /** 养老明细左下是否开启自动轮播 */
    laoxileftbottomdata?: any;
    /** 养老明细左下的值 */
    laoxileftbottom?: string[];
    /** 养老明细右下是否开启自动轮播 */
    laoxirightbottomdata?: any;
    /** 养老明细右下的值 */
    laoxirightbottom?: string[];
    // 明细结束
    // 统计开始
    /** 社区统计左上是否开启自动轮播 */
    shejilefttopdata?: any;
    /** 社区统计左上的值 */
    shejilefttop?: string[];
    /** 社区统计右上是否开启自动轮播 */
    shejirighttopdata?: any;
    /** 社区统计右上的值 */
    shejirighttop?: string[];
    /** 社区统计左中是否开启自动轮播 */
    shejileftcenterdata?: any;
    /** 社区统计左中的值 */
    shejileftcenter?: string[];
    /** 社区统计右中是否开启自动轮播 */
    shejirightcetnerdata?: any;
    /** 社区统计右中的值 */
    shejirightcetner?: string[];
    /** 社区统计左下是否开启自动轮播 */
    shejileftbottomdata?: any;
    /** 社区统计左下的值 */
    shejileftbottom?: string[];
    /** 社区统计右下是否开启自动轮播 */
    shejirightbottomdata?: any;
    /** 社区统计右下的值 */
    shejirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 社区明细左上是否开启自动轮播 */
    shexilefttopdata?: any;
    /** 社区明细左上的值 */
    shexilefttop?: string[];
    /** 社区明细右上是否开启自动轮播 */
    shexirighttopdata?: any;
    /** 社区明细右上的值 */
    shexirighttop?: string[];
    /** 社区明细左中是否开启自动轮播 */
    shexileftcenterdata?: any;
    /** 社区明细左中的值 */
    shexileftcenter?: string[];
    /** 社区明细右中是否开启自动轮播 */
    shexirightcetnerdata?: any;
    /** 社区明细右中的值 */
    shexirightcetner?: string[];
    /** 社区明细左下是否开启自动轮播 */
    shexileftbottomdata?: any;
    /** 社区明细左下的值 */
    shexileftbottom?: string[];
    /** 社区明细右下是否开启自动轮播 */
    shexirightbottomdata?: any;
    /** 社区明细右下的值 */
    shexirightbottom?: string[];
    // 明细结束
    // 统计开始
    /** 服务商统计左上是否开启自动轮播 */
    serverjilefttopdata?: any;
    /** 服务商统计左上的值 */
    serverjilefttop?: string[];
    /** 服务商统计右上是否开启自动轮播 */
    serverjirighttopdata?: any;
    /** 服务商统计右上的值 */
    serverjirighttop?: string[];
    /** 服务商统计左中是否开启自动轮播 */
    serverjileftcenterdata?: any;
    /** 服务商统计左中的值 */
    serverjileftcenter?: string[];
    /** 服务商统计右中是否开启自动轮播 */
    serverjirightcetnerdata?: any;
    /** 服务商统计右中的值 */
    serverjirightcetner?: string[];
    /** 服务商统计左下是否开启自动轮播 */
    serverjileftbottomdata?: any;
    /** 服务商统计左下的值 */
    serverjileftbottom?: string[];
    /** 服务商统计右下是否开启自动轮播 */
    serverjirightbottomdata?: any;
    /** 服务商统计右下的值 */
    serverjirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 服务商明细左上是否开启自动轮播 */
    serverxilefttopdata?: any;
    /** 服务商明细左上的值 */
    serverxilefttop?: string[];
    /** 服务商明细右上是否开启自动轮播 */
    serverxirighttopdata?: any;
    /** 服务商明细右上的值 */
    serverxirighttop?: string[];
    /** 服务商明细左中是否开启自动轮播 */
    serverxileftcenterdata?: any;
    /** 服务商明细左中的值 */
    serverxileftcenter?: string[];
    /** 服务商明细右中是否开启自动轮播 */
    serverxirightcetnerdata?: any;
    /** 服务商明细右中的值 */
    serverxirightcetner?: string[];
    /** 服务商明细左下是否开启自动轮播 */
    serverxileftbottomdata?: any;
    /** 服务商明细左下的值 */
    serverxileftbottom?: string[];
    /** 服务商明细右下是否开启自动轮播 */
    serverxirightbottomdata?: any;
    /** 服务商明细右下的值 */
    serverxirightbottom?: string[];
    // 明细结束
    // 统计开始
    /** 长者统计左上是否开启自动轮播 */
    elderjilefttopdata?: any;
    /** 长者统计左上的值 */
    elderjilefttop?: string[];
    /** 长者统计右上是否开启自动轮播 */
    elderjirighttopdata?: any;
    /** 长者统计右上的值 */
    elderjirighttop?: string[];
    /** 长者统计左中是否开启自动轮播 */
    elderjileftcenterdata?: any;
    /** 长者统计左中的值 */
    elderjileftcenter?: string[];
    /** 长者统计右中是否开启自动轮播 */
    elderjirightcetnerdata?: any;
    /** 长者统计右中的值 */
    elderjirightcetner?: string[];
    /** 长者统计左下是否开启自动轮播 */
    elderjileftbottomdata?: any;
    /** 长者统计左下的值 */
    elderjileftbottom?: string[];
    /** 长者统计右下是否开启自动轮播 */
    elderjirightbottomdata?: any;
    /** 长者统计右下的值 */
    elderjirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 长者明细左上是否开启自动轮播 */
    elderxilefttopdata?: any;
    /** 长者明细左上的值 */
    elderxilefttop?: string[];
    /** 长者明细右上是否开启自动轮播 */
    elderxirighttopdata?: any;
    /** 长者明细右上的值 */
    elderxirighttop?: string[];
    /** 长者明细左中是否开启自动轮播 */
    elderxileftcenterdata?: any;
    /** 长者明细左中的值 */
    elderxileftcenter?: string[];
    /** 长者明细右中是否开启自动轮播 */
    elderxirightcetnerdata?: any;
    /** 长者明细右中的值 */
    elderxirightcetner?: string[];
    /** 长者明细左下是否开启自动轮播 */
    elderxileftbottomdata?: any;
    /** 长者明细左下的值 */
    elderxileftbottom?: string[];
    /** 长者明细右下是否开启自动轮播 */
    elderxirightbottomdata?: any;
    /** 长者明细右下的值 */
    elderxirightbottom?: string[];
    // 明细结束
    // 统计开始
    /** 服务人员统计左上是否开启自动轮播 */
    servicejilefttopdata?: any;
    /** 服务人员统计左上的值 */
    servicejilefttop?: string[];
    /** 服务人员统计右上是否开启自动轮播 */
    servicejirighttopdata?: any;
    /** 服务人员统计右上的值 */
    servicejirighttop?: string[];
    /** 服务人员统计左中是否开启自动轮播 */
    servicejileftcenterdata?: any;
    /** 服务人员统计左中的值 */
    servicejileftcenter?: string[];
    /** 服务人员统计右中是否开启自动轮播 */
    servicejirightcetnerdata?: any;
    /** 服务人员统计右中的值 */
    servicejirightcetner?: string[];
    /** 服务人员统计左下是否开启自动轮播 */
    servicejileftbottomdata?: any;
    /** 服务人员统计左下的值 */
    servicejileftbottom?: string[];
    /** 服务人员统计右下是否开启自动轮播 */
    servicejirightbottomdata?: any;
    /** 服务人员统计右下的值 */
    servicejirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 服务人员明细左上是否开启自动轮播 */
    servicexilefttopdata?: any;
    /** 服务人员明细左上的值 */
    servicexilefttop?: string[];
    /** 服务人员明细右上是否开启自动轮播 */
    servicexirighttopdata?: any;
    /** 服务人员明细右上的值 */
    servicexirighttop?: string[];
    /** 服务人员明细左中是否开启自动轮播 */
    servicexileftcenterdata?: any;
    /** 服务人员明细左中的值 */
    servicexileftcenter?: string[];
    /** 服务人员明细右中是否开启自动轮播 */
    servicexirightcetnerdata?: any;
    /** 服务人员明细右中的值 */
    servicexirightcetner?: string[];
    /** 服务人员明细左下是否开启自动轮播 */
    servicexileftbottomdata?: any;
    /** 服务人员明细左下的值 */
    servicexileftbottom?: string[];
    /** 服务人员明细右下是否开启自动轮播 */
    servicexirightbottomdata?: any;
    /** 服务人员明细右下的值 */
    servicexirightbottom?: string[];
    // 明细结束
    // 统计开始
    /** 综合统计左上是否开启自动轮播 */
    sumjilefttopdata?: any;
    /** 综合统计左上的值 */
    sumjilefttop?: string[];
    /** 综合统计右上是否开启自动轮播 */
    sumjirighttopdata?: any;
    /** 综合统计右上的值 */
    sumjirighttop?: string[];
    /** 综合统计左中是否开启自动轮播 */
    sumjileftcenterdata?: any;
    /** 综合统计左中的值 */
    sumjileftcenter?: string[];
    /** 综合统计右中是否开启自动轮播 */
    sumjirightcetnerdata?: any;
    /** 综合统计右中的值 */
    sumjirightcetner?: string[];
    /** 综合统计左下是否开启自动轮播 */
    sumjileftbottomdata?: any;
    /** 综合统计左下的值 */
    sumjileftbottom?: string[];
    /** 综合统计右下是否开启自动轮播 */
    sumjirightbottomdata?: any;
    /** 综合统计右下的值 */
    sumjirightbottom?: string[];
    // 统计结束
    // 明细开始
    /** 综合明细左上是否开启自动轮播 */
    sumxilefttopdata?: any;
    /** 综合明细左上的值 */
    sumxilefttop?: string[];
    /** 综合明细右上是否开启自动轮播 */
    sumxirighttopdata?: any;
    /** 综合明细右上的值 */
    sumxirighttop?: string[];
    /** 综合明细左中是否开启自动轮播 */
    sumxileftcenterdata?: any;
    /** 综合明细左中的值 */
    sumxileftcenter?: string[];
    /** 综合明细右中是否开启自动轮播 */
    sumxirightcetnerdata?: any;
    /** 综合明细右中的值 */
    sumxirightcetner?: string[];
    /** 综合明细左下是否开启自动轮播 */
    sumxileftbottomdata?: any;
    /** 综合明细左下的值 */
    sumxileftbottom?: string[];
    /** 综合明细右下是否开启自动轮播 */
    sumxirightbottomdata?: any;
    /** 综合明细右下的值 */
    sumxirightbottom?: string[];
    // 明细结束
}
/**
 * 组件：平板控制组件
 */
export class computer extends ReactView<computercontrol, computerState> {
    /** tag数组 */
    private tags?: any = [
        {
            text: '养老机构',
        }, {
            text: '社区幸福院',
        }, {
            text: '居家服务商',
        }, {
            text: '长者统计',
        }, {
            text: '服务人员',
        }, {
            text: '综合统计',
        },
    ];
    /** 搜索事件处理 */
    search(value: any) {
        if (!value) {
            message.error('请输入需要搜索的内容');
            return;
        }
    }
    // 初始化服务
    iDisplayControlService?() {
        return getObject(new AjaxJsonRpcFactory(new IDisplayControlService, remote.url, `IDisplayControlService`));
    }
    // 初始话表格数据服务
    tabletControlService?() {
        return getObject(new AjaxJsonRpcFactory(new TabletControlService, remote.url, `TabletControlService`));
    }
    /** 按钮点击事件 */
    onBtnClick(type: any) {
        let params = {
            "report_id": "",
            "option": {
                "params": {},
                "drillDataSources": [],
                "drillDialogModules": [] as string[]
            }
        };
        if (type.text === '展示地图') {
            switch (this.state.activetags) {
                case '养老机构':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case '社区幸福院':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case '居家服务商':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case '长者统计':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case '服务人员':
                    // 异常地址
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case '综合统计':
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = [];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                default:
                    break;
            }
        } else {
            switch (type.methodname) {
                case 'welfare-photo':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = ['org_detail_picture_list-carousel-Dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'welfare-activity-phote':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = ['org_detail_activity_picture_list-carousel-Dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'welfare-service-fee':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = ['org_detail_service_price_picture-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'welfare-video':
                    params.report_id = 'irWelfareCentre';
                    params.option.drillDialogModules = ['org_detail_video_surveillance_displayer_dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'community-home-photo':
                    // 异常地址
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = ['com_detail_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'community-home-activity-photo':
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = ['com_detail_activity_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'community-home-personal-photo':
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = ['com_detail_personal_list-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'community-home-service-fee':
                    params.report_id = 'irCommunityHome';
                    params.option.drillDialogModules = ['com_detail_service_price_picture-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-provide-photo':
                    params.report_id = 'irServiceProvider';
                    params.option.drillDialogModules = ['sep_detail_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-provide-activity-photo':
                case 'service-provide-activity-photo':
                    params.report_id = 'irServiceProvider';
                    params.option.drillDialogModules = ['sep_detail_activity_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-provide-video':
                    break;
                case 'elder-activity-photo':
                    params.report_id = 'irElder';
                    params.option.drillDialogModules = ['eld_detail_activity_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'elder-video':

                    break;
                case 'elder-map':
                    params.report_id = 'irElder';
                    params.option.drillDialogModules = ['eld_map_location-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'elder-hot-map':
                    params.report_id = 'irElder';
                    params.option.params = { maptype: 'heatMap' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'elder-scatter-map':
                    params.report_id = 'irElder';
                    params.option.params = { maptype: 'scatter' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-personal-photo':
                    params.report_id = 'irServicePersonal';
                    params.option.drillDialogModules = ['spe_detail_picture_list-carousel-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;

                case 'service-personal-map':
                    params.report_id = 'irServicePersonal';
                    params.option.drillDialogModules = ['spe_detail_service_personal_map-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-personal-video':

                    break;
                case 'service-personal-service-map':
                    params.report_id = 'irServicePersonal';
                    params.option.drillDialogModules = ['spe_detail_task_map_control-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-personal-hot-map':
                    params.report_id = 'irServicePersonal';
                    params.option.params = { maptype: 'heatMap' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'service-personal-scatter-map':
                    params.report_id = 'irServicePersonal';
                    params.option.params = { maptype: 'scatter' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                // 综合
                case 'comprehensive-video':

                    break;
                case 'comprehensive-map':
                    params.report_id = 'irComprehensive';
                    params.option.drillDialogModules = ['cop_detail_task_map_control-dialog'];
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'comprehensivehot-map':
                    params.report_id = 'irComprehensive';
                    params.option.params = { maptype: 'heatMap' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                case 'comprehensive-scatter-map':
                    params.report_id = 'irComprehensive';
                    params.option.params = { maptype: 'scatter' };
                    this.iDisplayControlService!()!['control_report']!(params)!
                        .then((data: any[]) => {
                            message.success('切操作成功');
                        })
                        .catch((error: any) => {
                            // debugger;
                            // console.log(error);
                        });
                    break;
                default:
                    break;
            }
        }
    }
    /** 视频切换 */
    changeVideo = (value: any, id: string, ) => {
        let params = {
            "report_id": this.state.report_id,
            "option": {
                "params": {
                    "vaule": value
                },
                "drillDataSources": [
                    id
                ],
            }
        };
        this.iDisplayControlService!()!['control_report']!(params)!
            .then((data: any[]) => {
                message.success('切操作成功');
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });

    }
    /** 切换视频按钮 */
    videoClick = (value: any) => {
        switch (this.state.report_id) {
            case 'irServiceProvider':
                this.changeVideo(value, 'data_view_sep_detail_video_surveillance');
                break;
            case 'irWelfareCentre':
                this.changeVideo(value, 'data_view_sep_detail_video_surveillance');
                break;
            case 'irCommunityHome':
                this.changeVideo(value, 'data_view_sep_detail_video_surveillance');
                break;
            default:
                break;
        }
    }
    /** 表格详细列点击 */
    tableRowClick = (value: any) => {
        console.info(value);
        let params = {
            "report_id": this.state.report_id,
            "option": {
                "params": value,
            }
        };
        this.iDisplayControlService!()!['control_report'](params)
            .then((data: any) => {
                if (data) {
                    message.info('操作成功');
                }
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });
    }
    constructor(props: any) {
        super(props);
        this.state = {
            activetags: '养老机构',
            pagedata: [],
            modal: false,
            regiondata: ['南海区'],
            towndata: ['桂城街道', '九江镇', '大沥镇', '狮山镇', '里水镇', '西樵镇'],
            monitordata: ['西门', '东门', '消防通道1', '消防通道2', '娱乐室', '体育室'],
            operatingdata: [
                {
                    subtitle: '放大展示',
                    featuresdata: [
                        {
                            text: '福利院图片',
                            methodname: 'welfare-photo'
                        }, {
                            text: '活动图片',
                            methodname: 'welfare-activity-phote'
                        }, {
                            text: '服务价格表',
                            methodname: 'welfare-service-fee'
                        }, {
                            text: '房态图',
                            methodname: ''
                        }, {
                            text: '视频监控',
                            methodname: 'welfare-video'
                        }, {
                            text: '展示地图',
                            methodname: ''
                        }
                    ]
                }
            ],
            columns: [],
            report_id: 'irWelfareCentre',
            laojilefttopdata: [{ label: '养老机构总数', value: 'org_statistics_total_quantity-num' }],
            laojilefttop: [],
            laojirighttopdata: [{ label: '镇街福利院数量分布', value: 'org_statistics_street_quantity_displayer' }, { label: '福利院类型比例', value: 'org_statistics_nature_quantity_pie' }, { label: '福利院床位统计', value: 'org_statistics_welfare_centre_bed_quantity_displayer' }, { label: '镇街床位统计', value: 'org_statistics_street_bed_quantity_displayer' }, { label: '床位入住状况统计', value: 'org_statistics_live_bed_quantity_pie' }],
            laojirighttop: [],
            laojileftcenterdata: [{ label: '长者行为能力评分分布', value: 'org_statistics_ability_personnel_quantity_displayer' }, { label: '长者子女数量比例', value: 'org_statistics_children_personnel_quantity_pie' }, { label: '长者健康指标分布', value: 'org_statistics_health_indicators_personnel_quantity_displayer' }, { label: '长者类型比例', value: 'org_statistics_identity_affiliation_personnel_quantity_pie' }],
            laojileftcenter: [],
            laojirightcenterdata: [{ label: '工作人员学历分布', value: 'org_statistics_education_personnel_quantity_pie' }, { label: '工作人员资质数量分布', value: 'org_statistics_qualifications_personnel_quantity_pie' }, { label: '长者性别比例', value: 'org_statistics_sex_personnel_quantity_pie' }, { label: '长者年龄段分布', value: 'org_statistics_age_personnel_quantity_displayer' }],
            laojirightcenter: [],
            laojileftbottomdata: [{ label: '长者预约数量性别比例', value: 'org_statistics_sex_subscribe_quantity_pie' }, { label: '预约长者数量年龄段分布', value: 'org_statistics_age_subscribe_quantity_displayer' }, { label: '预约长者行为能力评分分布', value: 'org_statistics_ability_subscribe_quantity_displayer' }],
            laojileftbottom: [],
            laojirightbottomdata: [{ label: '长者预约数量走势', value: 'org_statistics_time_subscribe_quantity_multi_line' }, { label: '福利院收入走势', value: 'org_statistics_time_income_quantity_multi_line' }, { label: '福利院支出走势', value: 'org_statistics_time_expenditure_quantity_multi_line' }],
            laojirightbottom: [],
            laoxilefttopdata: [{ label: '福利院活动', value: 'org_detail_activity_picture_list-carousel' }],
            laoxilefttop: [],
            laoxirighttopdata: [{ label: '福利院环境', value: 'org_detail_picture_list-carousel' }, { label: '福利院现场视频监控', value: 'org_detail_video_surveillance_displayer' }],
            laoxirighttop: [],
            laoxileftcenterdata: [{ label: '福利院简介', value: 'org_detail_welfare_centre' }, { label: '服务价格表', value: 'org_detail_service_price_picture-carousel' }, { label: '服务清单', value: 'org_detail_service_products_list' }],
            laoxileftcenter: [],
            laoxirightcenterdata: [{ label: '服务清单', value: 'org_detail_service_products' }, { label: '长者性别比例', value: 'org_detail_sex_quantity_pie' }, { label: '长者学历分布', value: 'org_detail_education_quantity_pie' }, { label: '长者来源地分布', value: 'org_detail_origin_quantity_pie' }, { label: '长者年龄段分布', value: 'org_detail_age_quantity_displayer' }, { label: '长者行为能力评分分布', value: 'org_detail_ability_quantity_displayer' }, { label: '人员风采', value: 'org_detail_personal_list' }],
            laoxirightcenter: [],
            laoxileftbottomdata: [{ label: '房态图', value: 'org_detail_welfare_centre_room_status' }, { label: '工作人员性别比例', value: 'org_detail_sex_personnel_quantity_pie' }, { label: '工作人员年龄段分布', value: 'org_detail_age_personnel_quantity_displayer' }, { label: '工作人员学历分布', value: 'org_detail_education_personnel_quantity_pie' }, { label: '工作人员资质数量分布', value: 'org_detail_qualifications_personnel_quantity_pie' }],
            laoxileftbottom: [],
            laoxirightbottomdata: [{ label: '床位入住状况统计', value: 'org_detail_live_bed_quantity_pie' }, { label: '入住人数走势', value: 'org_detail_time_live_quantity_multi_line' }, { label: '预约入住人数走势', value: 'org_detail_time_line_up_quantity_multi_line' }, { label: '岗位人员数统计', value: 'org_detail_post_personnel_quantity_displayer' }, { label: '工作人员星级分布', value: 'org_detail_star_personnel_quantity_displayer' }],
            laoxirightbottom: [],
            shejilefttopdata: [{ label: '幸福院总数', value: 'com_statistics_total_quantity-num' }],
            shejilefttop: [],
            shejirighttopdata: [{ label: '镇街床幸福院数分布', value: 'com_statistics_street_quantity_displayer' }, { label: '幸福院类型比例', value: 'com_statistics_nature_quantity_pie' }, { label: '幸福院星级数量分布', value: 'com_statistics_star_quantity_displayer' }, { label: '服务商运营幸福院数量排名（TOP10）', value: 'com_statistics_service_provider_quantity_displayer' }],
            shejirighttop: [],
            shejileftcenterdata: [{ label: '幸福院服务评分分布', value: 'com_statistics_community_score_displayer' }, { label: '幸福院满意度排名（TOP10）', value: 'com_statistics_org_score_displayer' }, { label: '服务人员满意度（TOP10）', value: 'com_statistics_service_personal_score_displayer' }],
            shejileftcenter: [],
            shejirightcetnerdata: [{ label: '活动次数排名（TOP10）', value: 'com_statistics_type_activity_quantity_displayer' }, { label: '活动次数性别比例', value: 'com_statistics_sex_activity_quantity_pie' }, { label: '长者活动喜好', value: 'com_statistics_age_activity_quantity_displayer' }, { label: '长者学历活动次数比例', value: 'com_statistics_education_activity_quantity_pie' }],
            shejirightcetner: [],
            shejileftbottomdata: [{ label: '长者学历上课次数比例', value: 'com_statistics_education_attend_class_quantity_pie' }, { label: '课程热度', value: 'com_statistics_curriculum_attend_class_quantity_displayer' }, { label: '课程分类热度', value: 'com_statistics_curriculum_type_attend_class_quantity_displayer' }, { label: '幸福院长者饭堂用餐人次分布', value: 'com_statistics_community_meals_quantity_displayer' }, { label: '长者饭堂用餐人次数排名（TOP10）', value: 'com_statistics_service_provider_meals_quantity_displayer' }, { label: '幸福院幸福小站使用次数分布', value: 'com_statistics_community_happiness_station_quantity_displayer' }],
            shejileftbottom: [],
            shejirightbottomdata: [{ label: '长者签到人数时间分布', value: 'com_statistics_time_sign_in_quantity_multi_line' }, { label: '活动数量时间分布', value: 'com_statistics_time_activity_quantity_multi_line' }, { label: '长者饭堂用餐人数分布', value: 'com_statistics_time_meals_quantity_multi_line' }, { label: '长者教育人数分布', value: 'com_statistics_time_education_quantity_multi_line' }, { label: '活动平均人数分布', value: 'com_statistics_time_activity_personal_quantity_multi_line' }],
            shejirightbottom: [],
            shexilefttopdata: [{ label: '最新活动', value: 'com_detail_activity_module' }],
            shexilefttop: [],
            shexirighttopdata: [{ label: '幸福院环境', value: 'com_detail_picture_list-carousel' }, { label: '福利院现场', value: 'com_detail_video_surveillance_displayer' }],
            shexirighttop: [],
            shexileftcenterdata: [{ label: '人员风采', value: 'com_detail_personal_list_Module' }],
            shexileftcenter: [],
            shexirightcetnerdata: [{ label: '幸福院活动', value: 'com_detail_activity_picture_list-carousel' }, { label: '服务价格表', value: 'com_detail_service_price_picture-carousel' }],
            shexirightcetner: [],
            shexileftbottomdata: [{ label: '服务产品介绍', value: 'com_detail_service_products-table' }],
            shexileftbottom: [],
            shexirightbottomdata: [{ label: '服务产品', value: 'com_detail_service_products_list' }],
            shexirightbottom: [],
            serverjilefttopdata: [{ label: '服务商总数', value: 'sep_statistics_total_quantity-num' }],
            serverjilefttop: [],
            serverjirighttopdata: [{ label: '镇街服务商数量分布', value: 'sep_statistics_street_service_provider_quantity_displayer' }, { label: '服务商数量走势', value: 'sep_statistics_time_service_provider_quantity_multi_line' }],
            serverjirighttop: [],
            serverjileftcenterdata: [{ label: '服务商满意度排名（TOP10）', value: 'sep_statistics_service_provider_satisfaction_quantity_displayer' }, { label: '服务商服务满意度对比', value: 'sep_statistics_nature_satisfaction_quantity_pie' }, { label: '服务商满意度排名（TOP10）', value: 'sep_statistics_service_provider_score_top10_quantity_displayer' }, { label: '服务商服务评分对比', value: 'sep_statistics_nature_score_quantity_pie' }],
            serverjileftcenter: [],
            serverjirightcetnerdata: [{ label: '服务商服务人数统计', value: 'sep_statistics_nature_service_personal_quantity_pie' }, { label: '服务商服务人数排名（TOP10）', value: 'sep_statistics_service_provider_service_personal_top10_quantity_displayer' }],
            serverjirightcetner: [],
            serverjileftbottomdata: [{ label: '服务商服务次数排名（TOP10)', value: 'sep_statistics_service_provider_service_top10_quantity_displayer' }, { label: '服务商服务次数统计', value: 'sep_statistics_nature_service_quantity_pie' }],
            serverjileftbottom: [],
            serverjirightbottomdata: [{ label: '服务商服务收入排名（TOP10)', value: 'sep_statistics_service_provider_income_top10_quantity_displayer' }, { label: '服务商服务收入统计', value: 'sep_statistics_nature_income_quantity_pie' }],
            serverjirightbottom: [],
            serverxilefttopdata: [{ label: '服务总次数', value: 'sep_detail_total_quantity-num' }],
            serverxilefttop: [],
            serverxirighttopdata: [{ label: '服务商简介', value: 'sep_detail_service_provider-num' }],
            serverxirighttop: [],
            serverxileftcenterdata: [{ label: '服务商介绍', value: 'sep_detail_picture_list-carousel' }],
            serverxileftcenter: [],
            serverxirightcetnerdata: [{ label: '活动介绍', value: 'sep_detail_activity_picture_list-carousel' }],
            serverxirightcetner: [],
            serverxileftbottomdata: [{ label: '服务商介绍', value: 'sep_detail_promotional_video_surveillance_pie' }],
            serverxileftbottom: [],
            serverxirightbottomdata: [{ label: '服务商现场', value: 'sep_detail_video_surveillance_pie' }],
            serverxirightbottom: [],
            elderjilefttopdata: [{ label: '长者总数', value: 'eld_statistics_total_quantity-num' }],
            elderjilefttop: [],
            elderjirighttopdata: [{ label: '镇街长者数量分布', value: 'eld_statistics_street_elder_quantity_displayer' }, { label: '长者行为能力评分分布', value: 'eld_statistics_ability_elder_quantity_displayer' }, { label: '长者年龄段分布', value: 'eld_statistics_age_elder_quantity_displayer' }, { label: '长者病情分布', value: 'eld_statistics_condition_elder_quantity_pie' }, { label: '长者数量补贴类型分布', value: 'eld_statistics_subsidy_elder_quantity_pie' }, { label: '养老方式长者数量分布', value: 'eld_statistics_pension_mode_elder_quantity_pie' }, { label: '长者性别比例', value: 'eld_statistics_sex_elder_quantity_pie' }],
            elderjirighttop: [],
            elderjileftcenterdata: [{ label: '健康指标长者数据统计', value: 'eld_statistics_triglyceride_elder_quantity_displayer' }, { label: '体温偏高长者数量', value: 'eld_statistics_temperature_elder_quantity_displayer' }, { label: '脉搏偏高长者数量', value: 'eld_statistics_pulse_elder_quantity_displayer' }, { label: '血压偏高长者数量', value: 'eld_statistics_blood_pressure_elder_quantity_displayer' }, { label: '体水分率偏高长者数量', value: 'eld_statistics_water_elder_quantity_displayer' }],
            elderjileftcenter: [],
            elderjirightcetnerdata: [{ label: '服务次数长者（TOP10）', value: 'eld_statistics_elder_service_top10_quantity_displayer' }, { label: '低密度蛋白偏高长者服务次数', value: 'eld_statistics_protein_service_quantity_displayer' }, { label: '健康指标服务数量统计', value: 'eld_statistics_triglyceride_service_quantity_displayer' }, { label: '体温偏高长者服务次数', value: 'eld_statistics_temperature_service_quantity_displayer' }, { label: '脉博偏高长者服务次数', value: 'eld_statistics_pulse_service_quantity_displayer' }, { label: '长者服务需求', value: 'eld_statistics_age_service_quantity_displayer' }, { label: '长者服务次数病情分布', value: 'eld_statistics_condition_service_quantity_pie' }, { label: '体水分率偏高长者服务次数', value: 'eld_statistics_water_service_quantity_displayer' }, { label: '血压偏高服务次数', value: 'eld_statistics_blood_pressure_service_quantity_displayer' }, { label: '购买服务费用长者（TOP10）', value: 'eld_statistics_elder_buy_money_top10_quantity_displayer' }],
            elderjirightcetner: [],
            elderjileftbottomdata: [{ label: '低密度蛋白偏高长者购买服务金额', value: 'eld_statistics_protein_buy_money_quantity_displayer' }, { label: '健康指标购买服务金统计', value: 'eld_statistics_triglyceride_buy_money_quantity_displayer' }, { label: '体温偏高长者购买服务金额', value: 'eld_statistics_temperature_buy_money_quantity_displayer' }, { label: '脉博偏高长者购买服务金额', value: 'eld_statistics_pulse_buy_money_quantity_displayer' }, { label: '体水分率偏高长者购买服务金额', value: 'eld_statistics_water_buy_money_quantity_displayer' }, { label: '血压偏高购买服务金额', value: 'eld_statistics_blood_pressure_buy_money_quantity_displayer' }, { label: '长者购买服务金额病情分布', value: 'eld_statistics_condition_buy_money_quantity_pie' }],
            elderjileftbottom: [],
            elderjirightbottomdata: [{ label: '长者服务金额投入', value: 'eld_statistics_age_buy_money_quantity_displayer' }, { label: '长者数量走势', value: 'eld_statistics_time_elder_quantity_multi_line' }, { label: '长者年龄走势', value: 'eld_statistics_time_elder_age_multi_line' }, { label: '长者男女比例走势', value: 'eld_statistics_time_elder_sex_multi_line' }],
            elderjirightbottom: [],
            elderxilefttopdata: [{ label: '个人简介', value: 'eld_detail_elder' }],
            elderxilefttop: [],
            elderxirighttopdata: [{ label: '服务现场视频', value: 'eld_detail_service_live_video_pie' }],
            elderxirighttop: [],
            elderxileftcenterdata: [{ label: '服务记录', value: 'eld_detail_service_record_top10_list-table' }],
            elderxileftcenter: [],
            elderxirightcetnerdata: [{ label: '当前任务卡片', value: 'eld_detail_task_card' }],
            elderxirightcetner: [],
            elderxileftbottomdata: [{ label: '参与活动情况', value: 'eld_detail_activity_picture_list-carousel' }],
            elderxileftbottom: [],
            elderxirightbottomdata: [{ label: '长者家属', value: 'eld_detail_family_list-table' }],
            elderxirightbottom: [],
            servicejilefttopdata: [],
            servicejilefttop: []
        };
    }
    /** 报表切换 */
    switch_report = (id: string) => {
        this.iDisplayControlService!()!['switch_report'](id)
            .then((data: any) => {
                if (data) {
                    message.info('操作成功');
                }
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });
    }
    /** 处理点击tag事件 */
    ontags(item: any) {
        this.setState({
            activetags: item
        });
        this.selectdata(item);
        if (item === '养老机构') {
            this.switch_report('irWelfareCentre');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '放大展示',
                        featuresdata: [
                            {
                                text: '福利院图片',
                                methodname: 'welfare-photo'
                            }, {
                                text: '活动图片',
                                methodname: 'welfare-activity-phote'
                            }, {
                                text: '服务价格表',
                                methodname: 'welfare-service-fee'
                            }, {
                                text: '房态图',
                                methodname: ''
                            }, {
                                text: '视频监控',
                                methodname: ''
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                        ]
                    }
                ],
                report_id: 'irWelfareCentre',
                // 左边是统计、右边是明细
            });
        } else if (item === '社区幸福院') {
            this.switch_report('irCommunityHome');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '放大展示',
                        featuresdata: [
                            {
                                text: '社区幸福院图片',
                                methodname: 'community-home-photo'
                            }, {
                                text: '活动图片',
                                methodname: 'community-home-activity-photo'
                            }, {
                                text: '人员图片',
                                methodname: 'community-home-personal-photo'
                            }, {
                                text: '服务价格表',
                                methodname: 'community-home-service-fee'
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                        ]
                    }
                ],
                report_id: 'irCommunityHome',
            });
        } else if (item === '居家服务商') {
            this.switch_report('irServiceProvider');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '放大展示',
                        featuresdata: [
                            {
                                text: '服务商图片',
                                methodname: 'service-provide-photo'
                            }, {
                                text: '活动图片',
                                methodname: 'service-provide-activity-photo'
                            }, {
                                text: '宣传视频',
                                methodname: 'service-provide-video'
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                            //  {
                            //     text: '服务价格表',
                            //     methodname: 'service-provide-service-fee'
                            // }
                        ]
                    }
                ],
                report_id: 'irServiceProvider',
            });
        } else if (item === '长者统计') {
            this.switch_report('irElder');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '放大展示',
                        featuresdata: [
                            {
                                text: '活动照片',
                                methodname: 'elder-activity-photo'
                            }, {
                                text: '现场视频',
                                methodname: 'elder-video'
                            }, {
                                text: '地图',
                                methodname: 'elder-map'
                            }
                        ]
                    }, {
                        subtitle: '地图类型切换',
                        featuresdata: [
                            {
                                text: '热力图',
                                methodname: 'elder-hot-map'
                            }, {
                                text: '散点图',
                                methodname: 'elder-scatter-map'
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                        ]
                    }
                ],
                report_id: 'irElder',
            });
        } else if (item === '服务人员') {
            this.switch_report('irServicePersonal');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '地图类型切换',
                        featuresdata: [
                            {
                                text: '人员图片',
                                methodname: 'service-personal-photo'
                            }, {
                                text: '人员地图',
                                methodname: 'service-personal-map'
                            },
                            //  {
                            //     text: '服务照片',
                            //     methodname: 'service-personal-service-photo'
                            // }, 
                            {
                                text: '服务现场视频',
                                methodname: 'service-personal-video'
                            }, {
                                text: '服务地图',
                                methodname: 'service-personal-service-map'
                            }
                        ]
                    }, {
                        subtitle: '地图类型切换',
                        featuresdata: [
                            {
                                text: '热力图',
                                methodname: 'service-personal-hot-map'
                            }, {
                                text: '散点图',
                                methodname: 'service-personal-scatter-map'
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                        ]
                    }
                ],
                report_id: 'irServicePersonal',
            });
        } else if (item === '综合统计') {
            this.switch_report('irComprehensive');
            this.setState({
                operatingdata: [
                    {
                        subtitle: '放大展示',
                        featuresdata: [
                            {
                                text: '服务现场视频',
                                methodname: 'comprehensive-video'
                            }, {
                                text: '服务地图',
                                methodname: 'comprehensive-map'
                            }
                        ]
                    }, {
                        subtitle: '地图类型切换',
                        featuresdata: [
                            {
                                text: '热力图',
                                methodname: 'comprehensivehot-map'
                            }, {
                                text: '散点图',
                                methodname: 'comprehensive-scatter-map'
                            }, {
                                text: '展示地图',
                                methodname: ''
                            }
                        ]
                    }
                ],
                report_id: 'irComprehensive',
            });
        }
    }
    /** 查询数据 */
    selectdata(type: string) {
        switch (type) {
            case '养老机构':
                this.tabletControlService!()!['get_welfare_center_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '养老机构名称',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            case '社区幸福院':
                this.tabletControlService!()!['get_community_home_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '社区幸福院',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            case '居家服务商':
                this.tabletControlService!()!['get_service_provider_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '居家服务商',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            case '长者统计':
                this.tabletControlService!()!['get_elder_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '姓名',
                                        dataIndex: 'name',
                                        key: 'name',
                                    },
                                    {
                                        title: '身份证号码',
                                        dataIndex: 'id_card',
                                        key: 'id_card',
                                    },
                                    {
                                        title: '电话号码',
                                        dataIndex: 'phone',
                                        key: 'phone',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            case '服务人员':
                this.tabletControlService!()!['get_service_personal_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '服务人员',
                                        dataIndex: 'name',
                                        key: 'name',
                                    },
                                    {
                                        title: '所属机构',
                                        dataIndex: 'source',
                                        key: 'source',
                                    },
                                    {
                                        title: '电话号码',
                                        dataIndex: 'phone',
                                        key: 'phone',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            case '综合统计':
                this.tabletControlService!()!['get_comprehensive_list']!()!
                    .then((data: any[]) => {
                        console.info(data);
                        if (data.length > 0) {
                            this.setState({
                                columns: [
                                    {
                                        title: '综合统计',
                                        dataIndex: 'name',
                                        key: 'name',
                                    }
                                ],
                                pagedata: data
                            });
                        }
                    })
                    .catch((error: any) => {
                        // debugger;
                        // console.log(error);
                    });
                break;
            default:
                break;
        }
    }
    componentDidMount() {
        this.selectdata('养老机构');
    }
    handleBack() {
        // console.log('adasda');
    }
    updatestate(item: string, carousel: string, modules: string) {
        let params;
        this.setState({ [item]: !this.state[item] }, () => {
            params = {
                report_id: "report",
                option: {
                    params: {
                        id: "123aaaa"
                    },
                    drillDataSources: [
                        "data-source-pie-huli-jibie"
                    ],
                    drillDialogModules: [
                        "dialog-module"
                    ],
                    drillGroupModules: {
                        [carousel]: {
                            autoplay: this.state[item],
                            modules: modules.split(',')
                        }
                    }
                }
            };
            if (this.state.activetags === '养老机构') {
                params.report_id = 'irWelfareCentre';
            } else if (this.state.activetags === '社区幸福院') {
                params.report_id = 'irCommunityHome';
            } else if (this.state.activetags === '居家服务商') {
                params.report_id = 'irServiceProvider';
            } else if (this.state.activetags === '长者统计') {
                params.report_id = 'irElder';
            } else if (this.state.activetags === '服务人员') {
                params.report_id = 'irServicePersonal';
            } else if (this.state.activetags === '综合统计') {
                params.report_id = 'irComprehensive';
            }
            this.httpmethod(params);
        });
    }
    httpmethod(params: any) {
        // console.log('发送请求', params);
        this.iDisplayControlService!()!['control_report'](params)
            .then((data: any) => {
                if (data) {
                    message.info('操作成功');
                }
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });
    }
    watchimg(item: string, modules: string, id: string) {
        let params = {
            report_id: "report",
            option: {
                params: {
                    id: "123aaaa"
                },
                drillDataSources: [
                    "data-source-pie-huli-jibie"
                ],
                drillDialogModules: [
                    "dialog-module"
                ],
                drillGroupModules: {
                    [item]: {
                        autoplay: this.state[modules],
                        modules: [id]
                    }
                }
            }
        };
        if (this.state.activetags === '养老机构') {
            params.report_id = 'irWelfareCentre';
        } else if (this.state.activetags === '社区幸福院') {
            params.report_id = 'irCommunityHome';
        } else if (this.state.activetags === '居家服务商') {
            params.report_id = 'irServiceProvider';
        } else if (this.state.activetags === '长者统计') {
            params.report_id = 'irElder';
        } else if (this.state.activetags === '服务人员') {
            params.report_id = 'irServicePersonal';
        } else if (this.state.activetags === '综合统计') {
            params.report_id = 'irComprehensive';
        }
        this.iDisplayControlService!()!['control_report'](params)
            .then((data: any) => {
                if (data) {
                    message.info('操作成功');
                }
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });
    }
    newwatchimg(item: string, modules: string, id: any) {
        this.setState({ [modules]: id });
        let params = {
            report_id: "report",
            option: {
                params: {
                    id: "123aaaa"
                },
                drillDataSources: [
                    "data-source-pie-huli-jibie"
                ],
                drillDialogModules: [
                    "dialog-module"
                ],
                drillGroupModules: {
                    [item]: {
                        autoplay: id && id.length > 0 ? true : false,
                        modules: id
                    }
                }
            }
        };
        if (this.state.activetags === '养老机构') {
            params.report_id = 'irWelfareCentre';
        } else if (this.state.activetags === '社区幸福院') {
            params.report_id = 'irCommunityHome';
        } else if (this.state.activetags === '居家服务商') {
            params.report_id = 'irServiceProvider';
        } else if (this.state.activetags === '长者统计') {
            params.report_id = 'irElder';
        } else if (this.state.activetags === '服务人员') {
            params.report_id = 'irServicePersonal';
        } else if (this.state.activetags === '综合统计') {
            params.report_id = 'irComprehensive';
        }
        this.iDisplayControlService!()!['control_report'](params)
            .then((data: any) => {
                if (data) {
                    message.info('操作成功');
                }
            })
            .catch((error: any) => {
                // debugger;
                // console.log(error);
            });
    }
    render() {
        return (
            <div className="control-div">
                <div className="control-header">
                    <Row gutter={16}>
                        {
                            this.tags.map((item: any, index: number) => {
                                return (
                                    <Col span={4} key={index}>
                                        <strong className={this.state.activetags === item.text ? 'tagsactive control-header-tags' : "control-header-tags"} onClick={() => { this.ontags(item.text); }}>
                                            <span>{item.text}</span>
                                        </strong>
                                    </Col>
                                );
                            })
                        }
                    </Row>
                </div>
                <Row className="control-body">
                    <Col span={6} push={18} className="control-right">
                        <div className="control-right-ctn">
                            {
                                this.state.activetags === '养老机构' || this.state.activetags === '社区幸福院' || this.state.activetags === '居家服务商' ?
                                    <div className="surveillance">
                                        <div className="surveillance-title">
                                            <h5>视屏监控</h5>
                                        </div>
                                        <div className="surveillance-body">
                                            {
                                                this.state.monitordata && this.state.monitordata.length > 0 ?
                                                    this.state.monitordata.map((item: any, index: number) => {
                                                        return (
                                                            <div className="monitor-btn" onClick={this.videoClick.bind(this, item)} key={index}>
                                                                {item}
                                                            </div>
                                                        );
                                                    }) : ''
                                            }
                                        </div>
                                    </div> : ''
                            }
                            <div className={this.state.activetags === '养老机构' || this.state.activetags === '社区幸福院' || this.state.activetags === '居家服务商' ? 'operating' : 'alloperating'}>
                                <div className="surveillance-title">
                                    <h5>操作功能</h5>
                                </div>
                                <div className="surveillance-body">
                                    {
                                        this.state.operatingdata && this.state.operatingdata.length > 0 ?
                                            this.state.operatingdata.map((item: any, index: number) => {
                                                return (
                                                    <div key={index} className="features-module">
                                                        <span>{item.subtitle}</span>
                                                        <div className="features-btnlist">
                                                            {
                                                                item.featuresdata.map((data: any, idx: number) => {
                                                                    return (<div className="features-btn" key={idx} onClick={() => { this.onBtnClick(data); }}>{data.text}</div>);
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                );
                                            })
                                            : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col span={18} pull={6} className="control-left">
                        <div className="control-left-ctn">
                            <div className="left-ctn-header">
                                <div className="search_input">
                                    <Search placeholder="点击搜索" size="large" onSearch={this.search} />
                                </div>
                                <div className="control-time">
                                    {
                                        this.state.activetags === '综合统计' ? <DatePicker size="large" placeholder="选择时间" /> : ''
                                    }
                                </div>
                            </div>
                            <div className="option-div">
                                <div className="column-div">
                                    <span className="column-title">区:</span>
                                    <div className="column-list">
                                        {
                                            this.state.regiondata && this.state.regiondata.length > 0 ? this.state.regiondata!.map((item: any, index: number) => {
                                                return (
                                                    <div key={index}>
                                                        {item}
                                                    </div>
                                                );
                                            }) : ''
                                        }
                                    </div>
                                </div>
                                <div className="column-div">
                                    <span className="column-title">街镇:</span>
                                    <div className="column-list">
                                        {
                                            this.state.towndata && this.state.towndata.length > 0 ? this.state.towndata!.map((item: any, index: number) => {
                                                return (
                                                    <div key={index}>
                                                        {item}
                                                    </div>
                                                );
                                            }) : ''
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="control-div-data">
                                <Table
                                    rowKey='id'
                                    columns={this.state.columns}
                                    dataSource={this.state.pagedata}
                                    onRow={
                                        record => {
                                            return {
                                                onClick: this.tableRowClick.bind(this, record), // 点击行
                                            };
                                        }
                                    }
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
                <div className="settings">
                    <Icon type="setting" onClick={() => { this.setState({ modal: true }); }} style={{ fontSize: 44 }} />
                </div>
                <Modal
                    closable={false}
                    footer={null}
                    width={744}
                    title="轮播设置"
                    visible={this.state.modal}
                    onCancel={this.handleBack}
                >
                    {
                        this.state.activetags === '养老机构' ?
                            <div className="div">
                                <div className="modal-item">
                                    <h5 className="modal-title">养老机构统计轮播设置</h5>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Card title="左上" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojilefttopdata}
                                                            value={this.state.laojilefttop}
                                                            onChange={(e) => { this.newwatchimg('left-carousel', 'laojilefttop', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右上" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojirighttopdata}
                                                            value={this.state.laojirighttop}
                                                            onChange={(e) => { this.newwatchimg('left-carousel2', 'laojirighttop', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="左中" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojileftcenterdata}
                                                            value={this.state.laojileftcenter}
                                                            onChange={(e) => { this.newwatchimg('left-carousel3', 'laojileftcenter', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右中" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojirightcenterdata}
                                                            value={this.state.laojirightcenter}
                                                            onChange={(e) => { this.newwatchimg('left-carousel4', 'laojirightcenter', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="左下" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojileftbottomdata}
                                                            value={this.state.laojileftbottom}
                                                            onChange={(e) => { this.newwatchimg('left-carousel5', 'laojileftbottom', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右下" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laojirightbottomdata}
                                                            value={this.state.laojirightbottom}
                                                            onChange={(e) => { this.newwatchimg('left-carousel6', 'laojirightbottom', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                                <div className="modal-item">
                                    <h5 className="modal-title">养老机构明细轮播设置</h5>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Card title="左上" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxilefttopdata}
                                                            value={this.state.laoxilefttop}
                                                            onChange={(e) => { this.newwatchimg('right-carousel', 'laoxilefttop', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右上" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxirighttopdata}
                                                            value={this.state.laoxirighttop}
                                                            onChange={(e) => { this.newwatchimg('right-carousel2', 'laoxirighttop', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="左中" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxileftcenterdata}
                                                            value={this.state.laoxileftcenter}
                                                            onChange={(e) => { this.newwatchimg('right-carousel3', 'laoxileftcenter', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右中" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxirightcenterdata}
                                                            value={this.state.laoxirightcenter}
                                                            onChange={(e) => { this.newwatchimg('right-carousel4', 'laoxirightcenter', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="左下" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxileftbottomdata}
                                                            value={this.state.laoxileftbottom}
                                                            onChange={(e) => { this.newwatchimg('right-carousel5', 'laoxileftbottom', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col span={8}>
                                            <Card title="右下" bordered={false}>
                                                <Row>
                                                    <Col>
                                                        <CheckboxGroup
                                                            options={this.state.laoxirightbottomdata}
                                                            value={this.state.laoxirightbottom}
                                                            onChange={(e) => { this.newwatchimg('right-carousel6', 'laoxirightbottom', e); }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            : this.state.activetags === '社区幸福院' ?
                                <div className="div">
                                    <div className="modal-item">
                                        <h5 className="modal-title">社区幸福院统计轮播设置</h5>
                                        <Row gutter={24}>
                                            <Col span={8}>
                                                <Card title="左上" bordered={false}>
                                                    <Row>
                                                        <Col>
                                                            <CheckboxGroup
                                                                options={this.state.shejilefttopdata}
                                                                value={this.state.shejilefttop}
                                                                onChange={(e) => { this.newwatchimg('left-carousel', 'shejilefttop', e); }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右上" bordered={false}>
                                                    <Row>
                                                        <Col>
                                                            <CheckboxGroup
                                                                options={this.state.shejirighttopdata}
                                                                value={this.state.shejirighttop}
                                                                onChange={(e) => { this.newwatchimg('left-carousel2', 'shejirighttop', e); }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="左中" bordered={false}>
                                                    <Row>
                                                        <Col>
                                                            <CheckboxGroup
                                                                options={this.state.shejileftcenterdata}
                                                                value={this.state.shejileftcenter}
                                                                onChange={(e) => { this.newwatchimg('left-carousel3', 'shejileftcenter', e); }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右中" bordered={false}>
                                                    <Row>
                                                        <Col>
                                                            <CheckboxGroup
                                                                options={this.state.shejirightcetnerdata}
                                                                value={this.state.shejirightcetner}
                                                                onChange={(e) => { this.newwatchimg('left-carousel4', 'shejirightcetner', e); }}
                                                            />
                                                        </Col>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shejirightcetnerdata} onClick={() => { this.updatestate('shejirightcetnerdata', 'left-carousel4', "com_statistics_type_activity_quantity_displayer,com_statistics_sex_activity_quantity_pie,com_statistics_age_activity_quantity_displayer,com_statistics_education_activity_quantity_pie"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'shejirightcetnerdata', 'com_statistics_type_activity_quantity_displayer'); }}>活动次数排名（TOP10）</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'shejirightcetnerdata', 'com_statistics_sex_activity_quantity_pie'); }}>活动次数性别比例</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'shejirightcetnerdata', 'com_statistics_age_activity_quantity_displayer'); }}>长者活动喜好</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'shejirightcetnerdata', 'com_statistics_education_activity_quantity_pie'); }}>长者学历活动次数比例</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="左下" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shejileftbottomdata} onClick={() => { this.updatestate('shejileftbottomdata', 'left-carousel5', "com_statistics_education_attend_class_quantity_pie,com_statistics_curriculum_attend_class_quantity_displayer,com_statistics_curriculum_type_attend_class_quantity_displayer,com_statistics_community_meals_quantity_displayer,com_statistics_service_provider_meals_quantity_displayer,com_statistics_community_happiness_station_quantity_displayer"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_education_attend_class_quantity_pie'); }}>长者学历上课次数比例</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_curriculum_attend_class_quantity_displayer'); }}>课程热度</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_curriculum_type_attend_class_quantity_displayer'); }}>课程分类热度</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_community_meals_quantity_displayer'); }}>幸福院长者饭堂用餐人次分布</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_service_provider_meals_quantity_displayer'); }}>长者饭堂用餐人次数排名（TOP10）</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'shejileftbottomdata', 'com_statistics_community_happiness_station_quantity_displayer'); }}>幸福院幸福小站使用次数分布</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右下" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shejirightbottomdata} onClick={() => { this.updatestate('shejirightbottomdata', 'left-carousel6', "com_statistics_time_sign_in_quantity_multi_line,com_statistics_time_activity_quantity_multi_line,com_statistics_time_meals_quantity_multi_line,com_statistics_time_education_quantity_multi_line"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'shejirightbottomdata', 'com_statistics_time_sign_in_quantity_multi_line'); }}>长者签到人数时间分布</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'shejirightbottomdata', 'com_statistics_time_activity_quantity_multi_line'); }}>活动数量时间分布</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'shejirightbottomdata', 'com_statistics_time_meals_quantity_multi_line'); }}>长者饭堂用餐人数分布</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'shejirightbottomdata', 'com_statistics_time_education_quantity_multi_line'); }}>长者教育人数分布</Button>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="modal-item">
                                        <h5 className="modal-title">社区幸福院明细轮播设置</h5>
                                        <Row gutter={24}>
                                            <Col span={8}>
                                                <Card title="左上" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexilefttopdata} onClick={() => { this.updatestate('shexilefttopdata', 'right-carousel', "com_detail_activity_module"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexilefttopdata} onClick={() => { this.watchimg('right-carousel', 'shexilefttopdata', 'com_detail_activity_module'); }}>最新活动</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右上" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexirighttopdata} onClick={() => { this.updatestate('shexirighttopdata', 'right-carousel2', "com_detail_picture_list-carousel,com_detail_video_surveillance_displayer"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'shexirighttopdata', 'com_detail_picture_list-carousel'); }}>幸福院环境</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'shexirighttopdata', 'com_detail_video_surveillance_displayer'); }}>福利院现场</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="左中" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexileftcenterdata} onClick={() => { this.updatestate('shexileftcenterdata', 'right-carousel3', "com_detail_personal_list_Module"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexileftcenterdata} onClick={() => { this.watchimg('right-carousel3', 'shexileftcenterdata', 'com_detail_personal_list_Module'); }}>人员风采</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右中" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexirightcetnerdata} onClick={() => { this.updatestate('shexirightcetnerdata', 'right-carousel4', "com_detail_activity_picture_list-carousel,com_detail_service_price_picture-carousel"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'shexirightcetnerdata', 'com_detail_activity_picture_list-carousel'); }}>幸福院活动</Button>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'shexirightcetnerdata', 'com_detail_service_price_picture-carousel'); }}>服务价格表</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="左下" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexileftbottomdata} onClick={() => { this.updatestate('shexileftbottomdata', 'right-carousel5', "com_detail_service_products-table"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexileftbottomdata} onClick={() => { this.watchimg('right-carousel5', 'shexileftbottomdata', 'com_detail_service_products-table'); }}>服务产品介绍</Button>
                                                </Card>
                                            </Col>
                                            <Col span={8}>
                                                <Card title="右下" bordered={false}>
                                                    <Row>
                                                        <Col span={8}>自动轮播</Col>
                                                        <Col span={8} offset={8} className="switch-div">
                                                            <Switch size="small" checked={this.state.shexirightbottomdata} onClick={() => { this.updatestate('shexirightbottomdata', 'right-carousel6', "com_detail_service_products_list"); }} />
                                                        </Col>
                                                    </Row>
                                                    <Button className="modal-item-div" block={true} disabled={this.state.shexirightbottomdata} onClick={() => { this.watchimg('right-carousel6', 'shexirightbottomdata', 'com_detail_service_products_list'); }}>服务产品</Button>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                                : this.state.activetags === '居家服务商' ?
                                    <div className="div">
                                        <div className="modal-item">
                                            <h5 className="modal-title">居家服务商统计轮播设置</h5>
                                            <Row gutter={24}>
                                                <Col span={8}>
                                                    <Card title="左上" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverjilefttopdata} onClick={() => { this.updatestate('serverjilefttopdata', 'left-carousel', "sep_statistics_total_quantity-num"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'serverjilefttopdata', 'sep_statistics_total_quantity-num'); }}>服务商总数</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右上" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.servicejirighttopdata} onClick={() => { this.updatestate('servicejirighttopdata', 'left-carousel2', "sep_statistics_street_service_provider_quantity_displayer,sep_statistics_time_service_provider_quantity_multi_line"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'sep_statistics_street_service_provider_quantity_displayer'); }}>镇街服务商数量分布</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'sep_statistics_time_service_provider_quantity_multi_line'); }}>服务商数量走势</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="左中" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverjileftcenterdata} onClick={() => { this.updatestate('serverjileftcenterdata', 'left-carousel3', "sep_statistics_service_provider_satisfaction_quantity_displayer,sep_statistics_nature_satisfaction_quantity_pie,sep_statistics_service_provider_score_top10_quantity_displayer,sep_statistics_nature_score_quantity_pie"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'serverjileftcenterdata', 'sep_statistics_service_provider_satisfaction_quantity_displayer'); }}>服务商满意度排名（TOP10）</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'serverjileftcenterdata', 'sep_statistics_nature_satisfaction_quantity_pie'); }}>服务商服务满意度对比</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'serverjileftcenterdata', 'sep_statistics_service_provider_score_top10_quantity_displayer'); }}>服务商服务满意度对比</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'serverjileftcenterdata', 'sep_statistics_nature_score_quantity_pie'); }}>服务商服务评分对比</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右中" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.servicejirightcetnerdata} onClick={() => { this.updatestate('servicejirightcetnerdata', 'left-carousel4', "sep_statistics_nature_service_personal_quantity_pie,sep_statistics_service_provider_service_personal_top10_quantity_displayer"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.servicejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'servicejirightcetnerdata', 'sep_statistics_nature_service_personal_quantity_pie'); }}>服务商服务人数统计</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.servicejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'servicejirightcetnerdata', 'sep_statistics_service_provider_service_personal_top10_quantity_displayer'); }}>服务商服务人数排名（TOP10）</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="左下" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverjileftbottomdata} onClick={() => { this.updatestate('serverjileftbottomdata', 'left-carousel5', "sep_statistics_service_provider_service_top10_quantity_displayer,sep_statistics_nature_service_quantity_pie"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'serverjileftbottomdata', 'sep_statistics_service_provider_service_top10_quantity_displayer'); }}>服务商服务次数排名（TOP10)</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'serverjileftbottomdata', 'sep_statistics_nature_service_quantity_pie'); }}>服务商服务次数统计</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右下" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverjirightbottomdata} onClick={() => { this.updatestate('serverjirightbottomdata', 'left-carousel6', "sep_statistics_service_provider_income_top10_quantity_displayer,sep_statistics_nature_income_quantity_pie"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'serverjirightbottomdata', 'sep_statistics_service_provider_income_top10_quantity_displayer'); }}>服务商服务收入排名（TOP10)</Button>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'serverjirightbottomdata', 'sep_statistics_nature_income_quantity_pie'); }}>服务商服务收入统计</Button>
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div className="modal-item">
                                            <h5 className="modal-title">居家服务商明细轮播设置</h5>
                                            <Row gutter={24}>
                                                <Col span={8}>
                                                    <Card title="左上" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverxilefttopdata} onClick={() => { this.updatestate('serverxilefttopdata', 'right-carousel', "sep_detail_total_quantity-num"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverxilefttopdata} onClick={() => { this.watchimg('right-carousel', 'serverxilefttopdata', 'sep_detail_total_quantity-num'); }}>服务总次数</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右上" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverxirighttopdata} onClick={() => { this.updatestate('serverxirighttopdata', 'right-carousel2', "sep_detail_service_provider-num"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverxirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'serverxirighttopdata', 'sep_detail_service_provider-num'); }}>服务商简介</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="左中" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverxileftcenterdata} onClick={() => { this.updatestate('serverxileftcenterdata', 'right-carousel3', "sep_detail_picture_list-carousel"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverxileftcenterdata} onClick={() => { this.watchimg('right-carousel3', 'serverxileftcenterdata', 'sep_detail_picture_list-carousel'); }}>服务商介绍</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右中" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.servicexirightcetnerdata} onClick={() => { this.updatestate('servicexirightcetnerdata', 'right-carousel4', "sep_detail_activity_picture_list-carousel"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.servicexirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'servicexirightcetnerdata', 'sep_detail_activity_picture_list-carousel'); }}>活动介绍</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="左下" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverxileftbottomdata} onClick={() => { this.updatestate('serverxileftbottomdata', 'right-carousel5', "sep_detail_promotional_video_surveillance_pie"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverxileftbottomdata} onClick={() => { this.watchimg('right-carousel5', 'serverxileftbottomdata', 'sep_detail_promotional_video_surveillance_pie'); }}>服务商宣传视频</Button>
                                                    </Card>
                                                </Col>
                                                <Col span={8}>
                                                    <Card title="右下" bordered={false}>
                                                        <Row>
                                                            <Col span={8}>自动轮播</Col>
                                                            <Col span={8} offset={8} className="switch-div">
                                                                <Switch size="small" checked={this.state.serverxirightbottomdata} onClick={() => { this.updatestate('serverxirightbottomdata', 'right-carousel6', "sep_detail_video_surveillance_pie"); }} />
                                                            </Col>
                                                        </Row>
                                                        <Button className="modal-item-div" block={true} disabled={this.state.serverxirightbottomdata} onClick={() => { this.watchimg('right-carousel6', 'serverxirightbottomdata', 'sep_detail_video_surveillance_pie'); }}>服务商现场视频监控</Button>
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </div>
                                    </div>
                                    : this.state.activetags === '长者统计' ?
                                        <div className="div">
                                            <div className="modal-item">
                                                <h5 className="modal-title">长者统计轮播设置</h5>
                                                <Row gutter={24}>
                                                    <Col span={8}>
                                                        <Card title="左上" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjilefttopdata} onClick={() => { this.updatestate('elderjilefttopdata', 'left-carousel', "eld_statistics_total_quantity-num"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'elderjilefttopdata', 'eld_statistics_total_quantity-num'); }}>长者总数</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右上" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjirighttopdata} onClick={() => { this.updatestate('elderjirighttopdata', 'left-carousel2', "eld_statistics_street_elder_quantity_displayer,eld_statistics_ability_elder_quantity_displayer,eld_statistics_age_elder_quantity_displayer,eld_statistics_condition_elder_quantity_pie,eld_statistics_subsidy_elder_quantity_pie,eld_statistics_pension_mode_elder_quantity_pie,eld_statistics_sex_elder_quantity_pie"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_street_elder_quantity_displayer'); }}>镇街长者数量分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_ability_elder_quantity_displayer'); }}>长者行为能力评分分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_age_elder_quantity_displayer'); }}>长者年龄段分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_subsidy_elder_quantity_pie'); }}>长者数量补贴类型分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_pension_mode_elder_quantity_pie'); }}>养老方式长者数量分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'elderjirighttopdata', 'eld_statistics_sex_elder_quantity_pie'); }}>长者性别比例</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="左中" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjileftcenterdata} onClick={() => { this.updatestate('elderjileftcenterdata', 'left-carousel3', "eld_statistics_triglyceride_elder_quantity_displayer,eld_statistics_temperature_elder_quantity_displayer,eld_statistics_pulse_elder_quantity_displayer,eld_statistics_blood_pressure_elder_quantity_displayer,eld_statistics_water_elder_quantity_displayer"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'elderjileftcenterdata', 'eld_statistics_triglyceride_elder_quantity_displayer'); }}>健康指标长者数据统计</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'elderjileftcenterdata', 'eld_statistics_temperature_elder_quantity_displayer'); }}>体温偏高长者数量</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'elderjileftcenterdata', 'eld_statistics_pulse_elder_quantity_displayer'); }}>脉搏偏高长者数量</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'elderjileftcenterdata', 'eld_statistics_blood_pressure_elder_quantity_displayer'); }}>血压偏高长者数量</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'elderjileftcenterdata', 'eld_statistics_water_elder_quantity_displayer'); }}>体水分率偏高长者数量</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右中" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjirightcetnerdata} onClick={() => { this.updatestate('elderjirightcetnerdata', 'left-carousel4', "eld_statistics_elder_service_top10_quantity_displayer,eld_statistics_protein_service_quantity_displayer,eld_statistics_triglyceride_service_quantity_displayer,eld_statistics_temperature_service_quantity_displayer,eld_statistics_pulse_service_quantity_displayer,eld_statistics_age_service_quantity_displayer,eld_statistics_condition_service_quantity_pie,eld_statistics_water_service_quantity_displayer,eld_statistics_blood_pressure_service_quantity_displayer,eld_statistics_elder_buy_money_top10_quantity_displayer"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_elder_service_top10_quantity_displayer'); }}>服务次数长者（TOP10）</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_protein_service_quantity_displayer'); }}>低密度蛋白偏高长者服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_triglyceride_service_quantity_displayer'); }}>健康指标服务数量统计</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_temperature_service_quantity_displayer'); }}>体温偏高长者服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_pulse_service_quantity_displayer'); }}>脉博偏高长者服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_age_service_quantity_displayer'); }}>长者服务需求</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_condition_service_quantity_pie'); }}>长者服务次数病情分布</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_water_service_quantity_displayer'); }}>体水分率偏高长者服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_blood_pressure_service_quantity_displayer'); }}>血压偏高服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'elderjirightcetnerdata', 'eld_statistics_elder_buy_money_top10_quantity_displayer'); }}>购买服务费用长者（TOP10）</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="左下" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjileftbottomdata} onClick={() => { this.updatestate('elderjileftbottomdata', 'left-carousel5', "eld_statistics_protein_buy_money_quantity_displayer,eld_statistics_triglyceride_buy_money_quantity_displayer,eld_statistics_temperature_buy_money_quantity_displayer,eld_statistics_pulse_buy_money_quantity_displayer,eld_statistics_water_buy_money_quantity_displayer,eld_statistics_blood_pressure_buy_money_quantity_displayer,eld_statistics_condition_buy_money_quantity_pie"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_protein_buy_money_quantity_displayer'); }}>低密度蛋白偏高长者购买服务金额</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_triglyceride_buy_money_quantity_displayer'); }}>健康指标购买服务金统计</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_temperature_buy_money_quantity_displayer'); }}>体温偏高长者购买服务金额</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_pulse_buy_money_quantity_displayer'); }}>脉博偏高长者购买服务金额</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_water_buy_money_quantity_displayer'); }}>体水分率偏高长者购买服务金额</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_blood_pressure_buy_money_quantity_displayer'); }}>血压偏高购买服务金额</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'elderjileftbottomdata', 'eld_statistics_condition_buy_money_quantity_pie'); }}>长者购买服务金额病情分布</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右下" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderjirightbottomdata} onClick={() => { this.updatestate('elderjirightbottomdata', 'left-carousel6', "eld_statistics_age_buy_money_quantity_displayer,eld_statistics_time_elder_quantity_multi_line,eld_statistics_time_elder_age_multi_line,eld_statistics_time_elder_sex_multi_line"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'elderjirightbottomdata', 'eld_statistics_age_buy_money_quantity_displayer'); }}>体温偏高长者数量</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'elderjirightbottomdata', 'eld_statistics_time_elder_quantity_multi_line'); }}>体温偏高长者服务次数</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'elderjirightbottomdata', 'eld_statistics_time_elder_age_multi_line'); }}>长者年龄走势</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'elderjirightbottomdata', 'eld_statistics_time_elder_sex_multi_line'); }}>长者男女比例走势</Button>
                                                        </Card>
                                                    </Col>
                                                </Row>
                                            </div>
                                            <div className="modal-item">
                                                <h5 className="modal-title">长者明细轮播设置</h5>
                                                <Row gutter={24}>
                                                    <Col span={8}>
                                                        <Card title="左上" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxilefttopdata} onClick={() => { this.updatestate('elderxilefttopdata', 'right-carousel', "eld_detail_activity_picture_list-carousel,eld_detail_service_live_video_pie"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxilefttopdata} onClick={() => { this.watchimg('right-carousel', 'elderxilefttopdata', 'eld_detail_activity_picture_list-carousel'); }}>参与活动情况</Button>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxilefttopdata} onClick={() => { this.watchimg('right-carousel', 'elderxilefttopdata', 'eld_detail_service_live_video_pie'); }}>服务现场视频</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右上" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxirighttopdata} onClick={() => { this.updatestate('elderxirighttopdata', 'right-carousel2', "eld_detail_elder"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'elderxirighttopdata', 'eld_detail_elder'); }}>个人简介</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="左中" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxileftcenterdata} onClick={() => { this.updatestate('elderxileftcenterdata', 'right-carousel3', "eld_detail_task_card"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxileftcenterdata} onClick={() => { this.watchimg('right-carousel3', 'elderxileftcenterdata', 'eld_detail_task_card'); }}>当前任务卡片</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右中" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxirightcetnerdata} onClick={() => { this.updatestate('elderxirightcetnerdata', 'right-carousel4', "eld_map_location"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'elderxirightcetnerdata', 'eld_map_location'); }}>长者地理位置</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="左下" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxileftbottomdata} onClick={() => { this.updatestate('elderxileftbottomdata', 'right-carousel5', "eld_detail_family_list-table"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxileftbottomdata} onClick={() => { this.watchimg('right-carousel5', 'elderxileftbottomdata', 'eld_detail_family_list-table'); }}>长者家属</Button>
                                                        </Card>
                                                    </Col>
                                                    <Col span={8}>
                                                        <Card title="右下" bordered={false}>
                                                            <Row>
                                                                <Col span={8}>自动轮播</Col>
                                                                <Col span={8} offset={8} className="switch-div">
                                                                    <Switch size="small" checked={this.state.elderxirightbottomdata} onClick={() => { this.updatestate('elderxirightbottomdata', 'right-carousel6', "eld_detail_service_record_top10_list-table"); }} />
                                                                </Col>
                                                            </Row>
                                                            <Button className="modal-item-div" block={true} disabled={this.state.elderxirightbottomdata} onClick={() => { this.watchimg('right-carousel6', 'elderxirightbottomdata', 'eld_detail_service_record_top10_list-table'); }}>服务记录</Button>
                                                        </Card>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </div>
                                        : this.state.activetags === '服务人员' ?
                                            <div className="div">
                                                <div className="modal-item">
                                                    <h5 className="modal-title">服务人员统计轮播设置</h5>
                                                    <Row gutter={24}>
                                                        <Col span={8}>
                                                            <Card title="左上" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejilefttopdata} onClick={() => { this.updatestate('servicejilefttopdata', 'left-carousel', "spe_detail_total_quantity-num,spe_statistics_age_quantity_displayer,spe_statistics_education_evaluate_quantity_pie,spe_statistics_service_personal_top10_quantity_displayer"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejilefttopdata} onClick={() => { this.watchimg('left-carousel', 'servicejilefttopdata', 'spe_detail_total_quantity-num'); }}>服务总次数</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejilefttopdata} onClick={() => { this.watchimg('left-carousel', 'servicejilefttopdata', 'spe_statistics_age_quantity_displayer'); }}>服务人员勤奋榜（TOP10）</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejilefttopdata} onClick={() => { this.watchimg('left-carousel', 'servicejilefttopdata', 'spe_statistics_education_evaluate_quantity_pie'); }}>服务人员评价学历分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejilefttopdata} onClick={() => { this.watchimg('left-carousel', 'servicejilefttopdata', 'spe_statistics_service_personal_top10_quantity_displayer'); }}>服务人员服务次数排名（TOP10）</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右上" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejirighttopdata} onClick={() => { this.updatestate('servicejirighttopdata', 'left-carousel2', "spe_statistics_community_quantity_displayer,spe_statistics_service_personal_income_top10_quantity_displayer,spe_statistics_sex_evaluate_quantity_pie,spe_statistics_time_service_personal_quantity_multi_line"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'spe_statistics_community_quantity_displayer'); }}>社区服务人员总数</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'spe_statistics_service_personal_income_top10_quantity_displayer'); }}>服务人员收入（TOP10）</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'spe_statistics_sex_evaluate_quantity_pie'); }}>服务人员评价性别分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'servicejirighttopdata', 'spe_statistics_time_service_personal_quantity_multi_line'); }}>服务人员数量走势</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="左中" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejileftcenterdata} onClick={() => { this.updatestate('servicejileftcenterdata', 'left-carousel3', "spe_statistics_age_service_personal_quantity_displayer,spe_statistics_age_income_quantity_displayer,spe_statistics_education_quantity_pie,spe_detail_service_record_top10_list-table"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'servicejileftcenterdata', 'spe_statistics_age_service_personal_quantity_displayer'); }}>服务人员年龄段分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'servicejileftcenterdata', 'spe_statistics_age_income_quantity_displayer'); }}>服务人员收入（TOP10）年龄分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'servicejileftcenterdata', 'spe_statistics_education_quantity_pie'); }}>服务人员次数学历分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'servicejileftcenterdata', 'spe_detail_service_record_top10_list-table'); }}>服务记录</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右中" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejirightcetnerdata} onClick={() => { this.updatestate('servicejirightcetnerdata', 'left-carousel4', "spe_statistics_type_service_personal_quantity_displayer,spe_statistics_education_service_personal_quantity_pie,spe_statistics_sex_quantity_pie"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'servicejirightcetnerdata', 'spe_statistics_type_service_personal_quantity_displayer'); }}>服务人员证件类型数量分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'servicejirightcetnerdata', 'spe_statistics_education_service_personal_quantity_pie'); }}>服务人员学历分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'servicejirightcetnerdata', 'spe_statistics_sex_quantity_pie'); }}>服务人员服务评价性别比例</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="左下" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejileftbottomdata} onClick={() => { this.updatestate('servicejileftbottomdata', 'left-carousel5', "spe_statistics_service_personal_evaluate_quantity_displayer,spe_statistics_sex_service_personal_quantity_pie,spe_statistics_education_income_quantity_pie"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'servicejileftbottomdata', 'spe_statistics_service_personal_evaluate_quantity_displayer'); }}>服务人员满意度（TOP10）</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'servicejileftbottomdata', 'spe_statistics_sex_service_personal_quantity_pie'); }}>服务人员性别比例</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'servicejileftbottomdata', 'spe_statistics_education_income_quantity_pie'); }}>服务人员服务收入学历分布</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右下" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicejirightbottomdata} onClick={() => { this.updatestate('servicejirightbottomdata', 'left-carousel6', "spe_spe_statistics_age_evaluate_quantity,spe_statistics_source_service_personal_quantity_pie,spe_statistics_sex_income_quantity_pie"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'servicejirightbottomdata', 'spe_spe_statistics_age_evaluate_quantity'); }}>服务人员评价年龄段分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'servicejirightbottomdata', 'spe_statistics_source_service_personal_quantity_pie'); }}>服务人员来源地分布</Button>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicejirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'servicejirightbottomdata', 'spe_statistics_sex_income_quantity_pie'); }}>服务人员服务收入性别比例</Button>
                                                            </Card>
                                                        </Col>
                                                    </Row>
                                                </div>
                                                <div className="modal-item">
                                                    <h5 className="modal-title">服务人员明细轮播设置</h5>
                                                    <Row gutter={24}>
                                                        <Col span={8}>
                                                            <Card title="左上" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexilefttopdata} onClick={() => { this.updatestate('servicexilefttopdata', 'right-carousel', "spe_detail_total_quantity-num"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexilefttopdata} onClick={() => { this.watchimg('right-carousel', 'servicexilefttopdata', 'spe_detail_total_quantity-num'); }}>服务总次数</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右上" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexirighttopdata} onClick={() => { this.updatestate('servicexirighttopdata', 'right-carousel2', "spe_detail_picture_list-carousel"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'servicexirighttopdata', 'spe_detail_picture_list-carousel'); }}>服务人员介绍</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="左中" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexileftcenterdata} onClick={() => { this.updatestate('servicexileftcenterdata', 'right-carousel3', "spe_detail_activity_picture_list-carousel"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexileftcenterdata} onClick={() => { this.watchimg('right-carousel3', 'servicexileftcenterdata', 'spe_detail_activity_picture_list-carousel'); }}>活动介绍</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右中" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexirightcetnerdata} onClick={() => { this.updatestate('servicexirightcetnerdata', 'right-carousel4', "spe_detail_card-num"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'servicexirightcetnerdata', 'spe_detail_card-num'); }}>服务人员简介</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="左下" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexileftbottomdata} onClick={() => { this.updatestate('servicexileftbottomdata', 'right-carousel5', "spe_detail_task_card-num"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexileftbottomdata} onClick={() => { this.watchimg('right-carousel5', 'servicexileftbottomdata', 'spe_detail_task_card-num'); }}>当前任务卡片</Button>
                                                            </Card>
                                                        </Col>
                                                        <Col span={8}>
                                                            <Card title="右下" bordered={false}>
                                                                <Row>
                                                                    <Col span={8}>自动轮播</Col>
                                                                    <Col span={8} offset={8} className="switch-div">
                                                                        <Switch size="small" checked={this.state.servicexirightbottomdata} onClick={() => { this.updatestate('servicexirightbottomdata', 'right-carousel6', "spe_detail_service_personal_map"); }} />
                                                                    </Col>
                                                                </Row>
                                                                <Button className="modal-item-div" block={true} disabled={this.state.servicexirightbottomdata} onClick={() => { this.watchimg('right-carousel6', 'servicexirightbottomdata', 'spe_detail_service_personal_map'); }}>服务人员位置</Button>
                                                            </Card>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </div>
                                            : this.state.activetags === '综合统计' ?
                                                <div className="div">
                                                    <div className="modal-item">
                                                        <h5 className="modal-title">综合统计轮播设置</h5>
                                                        <Row gutter={24}>
                                                            <Col span={8}>
                                                                <Card title="左上" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjilefttopdata} onClick={() => { this.updatestate('sumjilefttopdata', 'left-carousel', "cop_statistics_service_provider_total_quantity-num,cop_statistics_age_evaluate_quantity_displayer,cop_statistics_elder_age_quantity_displayer,cop_statistics_elder_age_fee_quantity_displayer,cop_statistics_item_use_amount_top10_quantity_displayer,cop_statistics_time_quantity_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_service_provider_total_quantity-num'); }}>服务商总数</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_age_evaluate_quantity_displayer'); }}>服务评价</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_elder_age_quantity_displayer'); }}>服务次数</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_elder_age_fee_quantity_displayer'); }}>服务费用</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_item_use_amount_top10_quantity_displayer'); }}>慈善使用金额（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjilefttopdata} onClick={() => { this.watchimg('left-carousel', 'sumjilefttopdata', 'cop_statistics_time_quantity_pie'); }}>服务次数走势</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右上" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjirighttopdata} onClick={() => { this.updatestate('sumjirighttopdata', 'left-carousel2', "cop_statistics_item_evaluate_top10_quantity_displayer,cop_statistics_street_evaluate_quantity_displayer,cop_statistics_street_quantity_displayer,cop_statistics_street_fee_quantity_displayer,cop_statistics_elder_type_evaluate_quantity_pie,cop_statistics_time_profit_quantity_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_item_evaluate_top10_quantity_displayer'); }}>服务评价（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_street_evaluate_quantity_displayer'); }}>服务评价</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_street_quantity_displayer'); }}>服务次数</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_street_fee_quantity_displayer'); }}>服务费用</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_elder_type_evaluate_quantity_pie'); }}>长者类型服务评价比例</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirighttopdata} onClick={() => { this.watchimg('left-carousel2', 'sumjirighttopdata', 'cop_statistics_time_profit_quantity_pie'); }}>服务收益走势</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="左中" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjileftcenterdata} onClick={() => { this.updatestate('sumjileftcenterdata', 'left-carousel3', "cop_statistics_org_evaluate_top11_quantity_displayer,cop_statistics_item_top10_quantity_displayer,cop_statistics_item_fee_top10_quantity_displayer,cop_statistics_donor_income_top10_quantity_displayer,cop_statistics_elder_type_quantity_pie,cop_statistics_time_income_quantity_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_org_evaluate_top11_quantity_displayer'); }}>服务评价（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_item_top10_quantity_displayer'); }}>服务次数（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_item_fee_top10_quantity_displayer'); }}>服务费用（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_donor_income_top10_quantity_displayer'); }}>慈善收入金额（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_elder_type_quantity_pie'); }}>长者类型服务次数比例</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftcenterdata} onClick={() => { this.watchimg('left-carousel3', 'sumjileftcenterdata', 'cop_statistics_time_income_quantity_pie'); }}>慈善收入走势</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右中" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjirightcetnerdata} onClick={() => { this.updatestate('sumjirightcetnerdata', 'left-carousel4', "cop_statistics_item_evaluate_top10_quantity_displayer,cop_statistics_org_top10_quantity_displayer,cop_statistics_org_fee_top10_quantity_displayer,cop_statistics_item_income_top10_quantity_displayer,cop_statistics_elder_type_fee_quantity_pie,cop_statistics_time_grant_quantity_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'sumjirightcetnerdata', 'cop_statistics_item_evaluate_top10_quantity_displayer'); }}>服务评价（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'sumjirightcetnerdata', 'cop_statistics_org_top10_quantity_displayer'); }}>服务次数（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'sumjirightcetnerdata', 'cop_statistics_org_fee_top10_quantity_displayer'); }}>服务费用（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'sumjirightcetnerdata', 'cop_statistics_elder_type_fee_quantity_pie'); }}>长者类型服务费用比例</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightcetnerdata} onClick={() => { this.watchimg('left-carousel4', 'sumjirightcetnerdata', 'cop_statistics_time_grant_quantity_pie'); }}>慈善发放走势</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="左下" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjileftbottomdata} onClick={() => { this.updatestate('sumjileftbottomdata', 'left-carousel5', "cop_statistics_org_evaluate_top11_quantity_displayer,cop_statistics_service_personal_top10_quantity_displayer,cop_statistics_service_personal_fee_top10_quantity_displayer,cop_statistics_org_use_amount_quantity_displayer,cop_statistics_donation_income_quantity_pie,cop_statistics_time_subsidy_quantity_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_org_evaluate_top11_quantity_displayer'); }}>服务评价（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_service_personal_top10_quantity_displayer'); }}>服务次数（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_service_personal_fee_top10_quantity_displayer'); }}>服务收入（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_org_use_amount_quantity_displayer'); }}>慈善使用金额</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_donation_income_quantity_pie'); }}>慈善资金收入类型对比</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjileftbottomdata} onClick={() => { this.watchimg('left-carousel5', 'sumjileftbottomdata', 'cop_statistics_time_subsidy_quantity_pie'); }}>补贴发放走势</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右下" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumjirightbottomdata} onClick={() => { this.updatestate('sumjirightbottomdata', 'left-carousel6', "cop_statistics_service_personal_evaluate_top12_quantity_displayer,cop_statistics_elder_top10_quantity_displayer,cop_statistics_elder_fee_top10_quantity_displayer,cop_statistics_donor_use_amount_top10_quantity_displayer,cop_statistics_type_use_amount_quantity_pie,cop_statistics_elder_evaluate_top13_quantity_displayer"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_service_personal_evaluate_top12_quantity_displayer'); }}>服务评价（TOP12）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_elder_top10_quantity_displayer'); }}>服务次数（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_elder_fee_top10_quantity_displayer'); }}>服务费用（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_donor_use_amount_top10_quantity_displayer'); }}>慈善使用金额（TOP10）</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_type_use_amount_quantity_pie'); }}>慈善资金支出类型对比</Button>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumjirightbottomdata} onClick={() => { this.watchimg('left-carousel6', 'sumjirightbottomdata', 'cop_statistics_elder_evaluate_top13_quantity_displayer'); }}>服务评价（TOP10）</Button>
                                                                </Card>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                    <div className="modal-item">
                                                        <h5 className="modal-title">综合统计明细轮播设置</h5>
                                                        <Row gutter={24}>
                                                            <Col span={8}>
                                                                <Card title="左上" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxilefttopdata} onClick={() => { this.updatestate('sumxilefttopdata', 'right-carousel', "cop_detail_service_card_displayer"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxilefttopdata} onClick={() => { this.watchimg('right-carousel', 'sumxilefttopdata', 'cop_detail_service_card_displayer'); }}>当前服务卡片</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右上" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxirighttopdata} onClick={() => { this.updatestate('sumxirighttopdata', 'right-carousel2', "cop_detail_service_provider_card_displayer"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxirighttopdata} onClick={() => { this.watchimg('right-carousel2', 'sumxirighttopdata', 'cop_detail_service_provider_card_displayer'); }}>任务服务商卡片</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="左中" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxileftcenterdata} onClick={() => { this.updatestate('sumxileftcenterdata', 'right-carousel3', "cop_detail_service_personal_card_displayer"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxileftcenterdata} onClick={() => { this.watchimg('right-carousel3', 'sumxileftcenterdata', 'cop_detail_service_personal_card_displayer'); }}>任务服务人员卡片</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右中" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxirightcetnerdata} onClick={() => { this.updatestate('sumxirightcetnerdata', 'right-carousel4', "cop_detail_task_card_displayer"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxirightcetnerdata} onClick={() => { this.watchimg('right-carousel4', 'sumxirightcetnerdata', 'cop_detail_task_card_displayer'); }}>任务对象卡片</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="左下" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxileftbottomdata} onClick={() => { this.updatestate('sumxileftbottomdata', 'right-carousel5', "cop_detail_task_map_control"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxileftbottomdata} onClick={() => { this.watchimg('right-carousel5', 'sumxileftbottomdata', 'cop_detail_task_map_control'); }}>服务人员位置</Button>
                                                                </Card>
                                                            </Col>
                                                            <Col span={8}>
                                                                <Card title="右下" bordered={false}>
                                                                    <Row>
                                                                        <Col span={8}>自动轮播</Col>
                                                                        <Col span={8} offset={8} className="switch-div">
                                                                            <Switch size="small" checked={this.state.sumxirightbottomdata} onClick={() => { this.updatestate('sumxirightbottomdata', 'right-carousel6', "cop_detail_live_video_pie"); }} />
                                                                        </Col>
                                                                    </Row>
                                                                    <Button className="modal-item-div" block={true} disabled={this.state.sumxirightbottomdata} onClick={() => { this.watchimg('right-carousel6', 'sumxirightbottomdata', 'cop_detail_live_video_pie'); }}>服务现场视频</Button>
                                                                </Card>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </div>
                                                : null
                    }
                    <div className="modal-footer">
                        <Button type="primary" onClick={() => { this.setState({ modal: false }); }} size='large'>
                            返回
                        </Button>
                    </div>
                </Modal>
            </div >
        );
    }
}

/**
 * 组件：平板控制组件
 * 平板控制组件
 */
@addon('computercontrol', '平板控制', '平板控制组件')
@reactControl(computer, true)
export class computercontrol extends ReactViewControl {
    constructor() {
        super();
    }
}