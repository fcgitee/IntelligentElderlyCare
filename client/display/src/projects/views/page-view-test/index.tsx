import { ReportPageControl, SmartReportControl } from 'src/business/report/smart';
import { createObject } from 'pao-aop';
import { AjaxJsonRpcFactory } from 'pao-aop-client';
import { IDisplayControlService } from 'src/business/models/control';
import { SelectIframeLayoutControlTest } from 'src/projects/components/layout/select-iframe-layout-test';
import { remote } from 'src/projects/remote';
import { irWelfareBigDataStatisticsControl } from '../ir-welfare-big-data-statistics';
import { irWelfareSumControl } from '../ir-welfare-sum';
import { irHappinessStatisticsControl } from '../ir-happiness-statistics';
import { irServiceAnalysisControl } from '../ir-service-analysis';
// import { RemotePath } from 'src/projects/router';

const selectiframeLayout = createObject(SelectIframeLayoutControlTest);
const iframeWelfareCentreModule = createObject(ReportPageControl, { page: irWelfareBigDataStatisticsControl, id: 'irWelfareBigDataStatistics' });
const iframeCommunityHomeModule = createObject(ReportPageControl, { page: irWelfareSumControl, id: 'irWelfareSum' });
const iframeServicePersonalModule = createObject(ReportPageControl, { page: irHappinessStatisticsControl, id: 'irHappinessStatistics' });
const iframeComprehensiveModule = createObject(ReportPageControl, { page: irServiceAnalysisControl, id: 'irServiceAnalysis' });

export const pageReportTest = createObject(
    SmartReportControl,
    {
        modules: [iframeWelfareCentreModule, iframeCommunityHomeModule, iframeServicePersonalModule, iframeComprehensiveModule],
        layout: selectiframeLayout,
        iframeable: true,
        remoteControl: true,
        remoteControlService_Fac: new AjaxJsonRpcFactory(IDisplayControlService, remote.url, 'IDisplayControlService')
    }
);