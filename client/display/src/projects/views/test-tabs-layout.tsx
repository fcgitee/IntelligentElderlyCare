import React from "react";
import { addon } from "pao-aop";
import { reactControl, BaseReactElementControl } from "pao-aop-client";
import { Layout, Row, Col, Skeleton } from "antd";
import { BaseLayoutControl } from "src/business/report/layout";
const { Content } = Layout;

/**
 * 组件：测试tabs
 */
export class TestTabsLayout extends React.Component<TestTabsLayoutControl, { time: string }> {
    render() {
        const { childControls } = this.props;
        return (
            <Layout className="OrganizationLayout">
                <Content style={{ display: "flex" }}>
                    {
                        !childControls ?
                            null :
                            (
                                <Row type={"flex"} style={{ flex: "1 1" }}>
                                    <Col className={"left-col"}>
                                        {childControls[0] ? childControls[0].createElement!() : <Skeleton />}
                                    </Col>
                                </Row>
                            )
                    }
                </Content>
            </Layout>
        );
    }
}

/**
 * 控件：养老院整体统计分析
 */
@addon('TestTabsLayout', '机构养老布局', '用于数据分析图的布局控件')
@reactControl(TestTabsLayout)
export class TestTabsLayoutControl extends BaseLayoutControl {
    constructor(childControls?: BaseReactElementControl[]) {
        super(childControls);
    }
}