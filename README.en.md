# IntelligentElderlyCare

#### Description
智慧养老系统

#### Software Architecture
Software architecture description

#### Installation

> 首次下载代码需要注意

1. 需要在根目录下创建对应的打包文件夹在根目录下执行`npm run mkdir`
2. 需要下载 **git** 子模块，在根目录下执行`npm run submodule`
3. 如果是nodejs版本大于8.16，需要在根目录运行`npm run setup_node`，关闭所有终端重新打开
4. 在根目录下执行`npm run install-www`命令安装模块
   1. 调试环境，`npm run start-www`
   2. 打包项目，`npm run build-www` 
5. 首次运行python服务需要下载模块，在根目录下执行`npm run pip`
6. 服务在根目录下运行`python main_server.py`

> 添加前端项目需要注意

e.g. 添加app项目为例

1. 将 ***client/www*** 下除 ***node_modules***、 ***src/bussiness*** 外的所有文件复制至 ***client/app***
2. 修改 ***client/app/package.json*** 内容
   1. `"homepage": "/build/app",`
   2. `"scripts": `
        ```
        {
            "start": "node ./src/business/color && react-scripts-ts-antd start",
            "build": "node ./src/business/color && react-scripts-ts-antd build",
            "prebuild": "rmdir /s/q ..\\..\\build\\app && mkdir ..\\..\\build\\app",
            "postbuild": "xcopy /s .\\build ..\\..\\build\\app && xcopy .\\build\\index.html ..\\..\\templates\\app && del ..\\..\\build\\app\\index.html",
            "test": "react-scripts-ts-antd test --env=jsdom"
        }
        ```
3. 修改根目录下 ***package.json*** 内容
   1. `"scripts": `
        ```
        {
            "mkdir":"mkdir build\\www && mkdir templates\\www && mkdir build\\app && mkdir templates\\app"
        }
        ```


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)