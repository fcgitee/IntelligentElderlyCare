'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-06-20 17:42:09
@LastEditTime : 2020-01-13 12:04:32
@LastEditors  : Please set LastEditors
'''

from flask import session

from server.app.iec_app import IECApp
from server.app.time_app import TimeTaskserver
from server.pao_python.pao import log
from server.service.time_task.control_task import ControlTaskService

# db_addr = "183.235.223.56"
# db_name = 'IEC_SYS'

# control_task_service = ControlTaskService(db_addr, 27017, db_name,session)
# time_task_server = TimeTaskserver(db_addr, 27017, db_name)
# time_task_server.control_service = [control_task_service]


wechat_payment_data = {
    # 商户号id
    'mch_id': '1480439852',
    # 接受支付结果的接口名
    'interface': '/wechat_payment_result',
    # 支付成功页面，前端支付成功url的demo：http://localhost:3000/app/paymentSucceed/JTdCJTIycGFnZV90eXBlJTIyOiUyMmNoYXJnZSUyMiwlMjJwYWdlX2luZm8lMjI6MzMzJTdE
    'success_page': '/app/paymentSucceed',
    # 商户平台设置的密钥key
    'mch_key': 'tuu7nh6bef8amu1xw86ujoz154uy6b15',
    # 统一下单接口
    'unifiedorder_url': 'https://api.mch.weixin.qq.com/pay/unifiedorder',
    # 公众账号id(微信支付分配的公众账号id，企业号corpid即为此appid)
    "appid": 'wx16cf2d0a786c23f7'
}

alipay_payment_data = {
    # h5支付
    # 支付宝分配给开发者的应用ID
    "app_id": "2018050860103091",
    "pu_k": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1cjelLoSOWyPZhjF2Pai++NBhi0AKDPNlr2TlvsBsJ4Qs705zAh225MWemoCNol4NDTvJYstgH+LBoSVWtKaLWR6dmNE8i5MgRPekP75Ei6BUqFS0rR1DHEUyPvZyJhIwJ2gLureE6rV81FUwJei22hRM8uD/tEiHzQdFnHqG/itcx6lS2YkdD8Z0tLZRdqeaIXI+fG3NNRCPPoHgO/T4tYIYi3MH+pI2CSAv+NDTQCBxk2RuVez8oMSNrHlVFbj7XmNI0KKme3eZrAFCn5uWqAFGcttHkpsrJHL37ow2y14smXruZ9UhlmVegFyXaIgYw4JQEZggtVevGz7n7IFKQIDAQAB",
    # TODO: 注意：由于实例化SDK客户端时需要指定应用的私钥信息，请务必注意不要将私钥信息配置在源码中（比如配置为常量或储存在配置文件的某个字段中等），因为私钥的保密等级往往比源码高得多，将会增加私钥泄露的风险。推荐将私钥信息储存在专用的私钥文件中，将私钥文件通过安全的流程分发到服务器的安全储存区域上，仅供自己的应用运行时读取
    "pr_k": "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDVyN6UuhI5bI9mGMXY9qL740GGLQAoM82WvZOW+wGwnhCzvTnMCHbbkxZ6agI2iXg0NO8liy2Af4sGhJVa0potZHp2Y0TyLkyBE96Q/vkSLoFSoVLStHUMcRTI+9nImEjAnaAu6t4TqtXzUVTAl6LbaFEzy4P+0SIfNB0Wceob+K1zHqVLZiR0PxnS0tlF2p5ohcj58bc01EI8+geA79Pi1ghiLcwf6kjYJIC/40NNAIHGTZG5V7PygxI2seVUVuPteY0jQoqZ7d5msAUKfm5aoAUZy20eSmyskcvfujDbLXiyZeu5n1SGWZV6AXJdoiBjDglARmCC1V68bPufsgUpAgMBAAECggEAH4fq3EC9Y17Rn5tTwvZNf507EtEcTtGlJvB7Di+jRitqBziHh8JP1X7SN+fev7By3DBafU0TvvoQxTtU0/pv5vYE4eUuTBHsErWdhAE3NylIxakpkA4xnU4oZKRQsLdVM6OL9p+JdZkiySpbt7QlvvqZz2va4gByaevDxoWyo8E6pSdomLrfytZBjw7gxTHPKkrmsx6KMcrySS1/wd2hA6/Ue7j2swIn+v/XbiPMmOcy0v0iIEjIYr3J/R9ZJ7xoqr0JwDULM3brSywa2er76WuffDr77EvWrE/tEzRIWk3SvaPCcPIadP/A7XTBX7eQxz8u+Mr7uWXRAIonX4eOqQKBgQD3l+hdZXK4CUPlVYIqJApdGG0yryYa6Sh3RihxQW6tzuhBmt0V3+EMP/trQfNtj/bq/ANLFtaoQLQA/qvopSg/y72SXNzjszFrP6XmnQfngen3lVE3gYODVPYG5MeB1jqJdKfLXJQ6qBmhLJ7F+sYsWVlji9Bm0tk2qPnM5aO2DwKBgQDdCxhBAjKMhl1zzksge/OhBoUlyPzxaIJQ3e3LcijF2dosYqKyb+XGLXw78rOJVpm5sSGDWlYEWC4uOMhS1iBGnF2g6ALu33nmhFWL3H2hDjjqZx66RCp3xbcrteSomDLXNRhvzGxQtwwL5P3Z38ngrsIznCYwqMsYtJrAt9wJRwKBgGaw/NFv+ErYnO6Lrrcnx5yhOvlgp99VgVEnKjL/PpacULyVLMmb182q4+6ypfJ1WaZQ7FTCnjmezIn1FvwhNDZxnKkOFxfiBMdE2NxzmQmuWoozxkHNV+yq2ng1f4Ichp7oYYf5WAmUGz08MoZO4s8WJ/sfzEo5fZosL5+KR4KFAoGBAK812CfPghn5d6IP52T8W4VMEcZ9pmIEoSdQguk8uCz8HgAIv0YJhGyuKUerfbxjNL8INLRVqtEOxKxGviNLL5JI575fqdOGuK2MP/L492vuRhcnXifvWGpQAn1XVMR+v+3uXExiEYl5Tscu4DkPNooufClEoND5OuN+ZRxVoMiJAoGAVu9e4I2UgZ1Jz/ZVFKZM3LPy/Xpt3NZfmOGFWkQSDle2nhLJyh5ATmwGR21OWnuvi7fNG5ZzpVy/INp314WmS8QK8KxNkfoG7Lg8F53ryQ91PEsclMAcq+4rxnVN3C/M94Z6ggxYCSUbAF5N30iVCr4XalKwdVEOrWRTNmAC+90=",
    # 接受支付结果的接口名
    'interface': '/alipay_result',
}

emi_callcenter_data = {
    # 域名
    'host': 'https://api.emicloud.com',
    # 子账号
    'subAccount': {
        'sid': 'b9d531e7e168e5348e8eae0a28036df8',
        'token': 'ec272c0ec964e22dc2a5867484a7b1e6'
    },
    # 应用id
    'appid': '35164c6126c33db52be23f81eb870cc7',
    # api接口版本
    'SoftwareVersion': '20190310',
    # 主账号
    'mainAccount': {
        'sid': 'c6da676173b11ac453827b9e638fcf8b',
        'token': 'de37b631dde434cbfc5b113cb2035412'
    },
    'gid': '229'
}

sms_data = {
    # 短信签名
    'SIGN': '南海健康',
    # 短信模板
    'TEMPLATE_CODE': 'SMS_184206495',
    # KEYID
    'KEYID': '',
    # SECRET
    'SECRET': '',
}

# 物联网网关地址
device_gateway_host = 'http://localhost:3010/remoteCall'
web_origins = '*'
# 启动智慧养老系统
app = IECApp(
    __name__,
    address='0.0.0.0',
    port=3100,
    web_origins=web_origins,
    # db_addr="0.0.0.0",
    # db_addr="192.168.95.127",
    # 域名
    realm_name="zf.e-health100.com",
    # 微信支付相关信息
    wechat_payment_data=wechat_payment_data,
    # db_addr="183.235.223.56",
    # 支付宝支付相关信息
    alipay_payment_data=alipay_payment_data,
    # 易米云通相关信息
    emi_callcenter_data=emi_callcenter_data,
    device_gateway_host=device_gateway_host,
    # 短信接口相关信息
    sms_data=sms_data,
    # db_addr="192.168.95.43",
    # db_addr="183.240.150.25",
    # db_addr="106.55.243.246",
    db_addr="",
    db_port=27017,
    db_user='',
    db_pwd='',
    db_name='',
    inital_password='',
    web_path="build",
    upload_file="upload",
    debug=True)

if __name__ == '__main__':
    app.run()
    # time_task_server.start()
